<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Sadardiri UIR</title>
  <meta content="sadardiri uir bertujuan sebagai evaluasi internal terhadap pencapaian kinerja Akademik dan non
  Akademik fakultas di lingkungan UIR sehingga tercipta siklus perbaikan yang berkelanjutan" name="description">
  <meta content="sadardiri uir, sadardiri, evaluasi kinerja, sistem evaluasi diri" name="keywords">

  <!-- Favicons -->
  <link href="{{ url('/') }}/frontend/img/uir.png" rel="icon">
  <link href="{{ url('/') }}/frontend/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="{{ url('/') }}/frontend/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="{{ url('/') }}/frontend/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="{{ url('/') }}/frontend/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="{{ url('/') }}/frontend/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="{{ url('/') }}/frontend/vendor/owl.carousel/{{ url('/') }}/frontend/owl.carousel.min.css" rel="stylesheet">
  <link href="{{ url('/') }}/frontend/vendor/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="{{ url('/') }}/frontend/css/style.css" rel="stylesheet">

</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top">
    <div class="container d-flex">

      <div class="logo mr-auto">


        <h1 class="text-light"><a href="{{ url('/') }}"><img alt="" src="{{ asset('backend/assets/img/uir.png')}}" style="width:30px "><span> Sadardiri</span></a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
       {{-- <a href=""><img src="{{ url('/') }}/frontend/img/logo.png" alt="" class="img-fluid"></a> --}}
      </div>

      <div class="header-social-links">
        <a href="#" class="twitter"><i class="icofont-twitter"></i></a>
        <a href="#" class="facebook"><i class="icofont-facebook"></i></a>
        <a href="#" class="instagram"><i class="icofont-instagram"></i></a>
        {{-- <a href="#" class="linkedin"><i class="icofont-linkedin"></i></i></a> --}}
      </div>

    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <section id="hero">

    <div class="container">
      <div class="row">
        <div class="col-lg-6 pt-5 pt-lg-0 order-2 order-lg-1 d-flex flex-column justify-content-center" data-aos="fade-up">
          <div>
            <h1>Selamat Datang di Sistem Daring Evaluasi Diri (Sadardiri) UIR</h1>
            <h2>Sistem sadar diri bertujuan sebagai evaluasi internal terhadap pencapaian kinerja Akademik dan non
                Akademik fakultas di lingkungan UIR sehingga tercipta siklus perbaikan yang berkelanjutan</h2>
            <a href="{{ route('login')}}" class="btn-get-started scrollto">Masuk</a>
          </div>
        </div>
        <div class="col-lg-6 order-1 order-lg-2 hero-img" data-aos="fade-left">
          <img src="{{ url('/') }}/frontend/img/hero-img.png" class="img-fluid" alt="">
        </div>
      </div>
    </div>

  </section><!-- End Hero -->

  <main id="main">

    @yield('content')




  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-4 col-sm-12">
            <div class="footer-info">
              <h3>Sadardiri UIR</h3>
              <p>
                Jl. Kaharuddin Nasution 113
                28284, Pekanbaru Riau <br>
                <strong>Phone:</strong>+62 761 674 674<br>
                <strong>Fax:</strong> +62 761 674 834<br>
                <strong>Email:</strong> info@uir.ac.id<br>
              </p>
              <div class="social-links mt-3">
                <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
                <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
                <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
                {{-- <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a> --}}
              </div>
            </div>
          </div>

          <div class="col-lg-4 col-md-4 col-sm-12">
            <div class="footer-info">
              <h3>Tautan Penting</h3>
              <ul class="list-unstyled li-space-lg">
                <li class="media">
                    <i class="bx bxs-right-arrow-alt"></i><p> Website Universitas Islam Riau <a class="turquoise" href="http://uir.ac.id">   uir.ac.id</a></p>
                </li>
                <li class="media">
                    <i class="bx bxs-right-arrow-alt"></i><p> Sistem Informasi Audit Mutu Internal <a class="turquoise" href="http://sadardiri.uir.ac.id">   Siami UIR</a></p>
                </li>
            </ul>
            </div>
          </div>

          <div class="col-lg-4 col-md-4 col-sm-12 footer-newsletter">
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15958.786685554316!2d101.452555!3d0.447421!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xe6f83f2deb032e8f!2sIslamic%20University%20of%20Riau!5e0!3m2!1sen!2sid!4v1609132713972!5m2!1sen!2sid" height="200" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
          </div>

        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright"> {{ date('Y') }}
        &copy; Copyright <strong><span>Sadardiri</span></strong>. All Rights Reserved

        - <a href="https://www.instagram.com/syabdandalimunthe/">Universitas Islam Riau</a>
      </div>
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top"><i class="bx bxs-up-arrow-alt"></i></a>

  <!-- Vendor JS Files -->
  <script src="{{ url('/') }}/frontend/vendor/jquery/jquery.min.js"></script>
  <script src="{{ url('/') }}/frontend/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="{{ url('/') }}/frontend/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="{{ url('/') }}/frontend/vendor/php-email-form/validate.js"></script>
  <script src="{{ url('/') }}/frontend/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="{{ url('/') }}/frontend/vendor/venobox/venobox.min.js"></script>
  <script src="{{ url('/') }}/frontend/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="{{ url('/') }}/frontend/vendor/aos/aos.js"></script>

  <!-- Template Main JS File -->
  <script src="{{ url('/') }}/frontend/js/main.js"></script>

  @yield('script')

</body>

</html>
