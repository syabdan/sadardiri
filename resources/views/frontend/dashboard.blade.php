@extends('frontend/layout')

@section('content')
<section id="services" class="services section-bg">
    <div class="container">

      <div class="section-title" data-aos="fade-up">
        <h2>Evaluasi Kinerja unit</h2>

      </div>

      <div class="row">
        @foreach ($users as $user)
            <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0 pb-5" data-aos="zoom-in" data-aos-delay="100" >
                <div class="icon-box" style="width:300px;">
                    <div class="icon"><i class="bx bx-user"></i></div>
                    <h4 class="title"><a href="{{ url('frontend/lihat/' . $user->unit['id']) }}">{{ $user->unit['nama_unit'] }}</a></h4>
                    <div >
                        <center>
                        <h6> {{ $user->email }} </h6>
                        <a class="btn btn-primary btn-sm" href="{{ url('frontend/lihat/' . $user->unit['id']) }}">Lihat Statistik</a>
                        </center>

                    </div>
                </div>
            </div>
        @endforeach
      </div>

    </div>
  </section>
  @endsection
