$(document).ready(function(){
   $('.tambah').click(function(){
		adanLoadingFadeIn();
		$.loadmodal({
			url: "{{ url($url_admin.'/penilaian-indikator-kinerja/create') }}",
			id: 'responsive',
			dlgClass: 'fade',
			bgClass: 'primary',
			title: 'Tambah',
			width: 'whatever',
			modal: {
				keyboard: true,
				// any other options from the regular $().modal call (see Bootstrap docs)
				},
          ajax: {
				dataType: 'html',
				method: 'GET',
				success: function(data, status, xhr){
            adanLoadingFadeOut();
				},
			},
        });
  });

	$(document).on("click",".ubah",function() {
    adanLoadingFadeIn();
		var id = $(this).attr('data-id');
		$.loadmodal({
			url: "{{ url($url_admin.'/penilaian-indikator-kinerja') }}/"+ id +"/edit",
      id: 'responsive',
			dlgClass: 'fade',
			bgClass: 'warning',
			title: 'Ubah',
			width: 'whatever',
			modal: {
				keyboard: true,
				// any other options from the regular $().modal call (see Bootstrap docs)
				},
          ajax: {
				dataType: 'html',
				method: 'GET',
				success: function(data, status, xhr){
            adanLoadingFadeOut();
				},
			},
        });
	});

    $(document).on("click",".reupload",function() {
        adanLoadingFadeIn();
            var id = $(this).attr('data-id');
            $.loadmodal({
                url: "{{ url($url_admin.'/penilaian-indikator-kinerja/reupload') }}/"+ id,
                id: 'responsive',
                dlgClass: 'fade',
                bgClass: 'warning',
                title: 'Reupload',
                width: 'whatever',
                modal: {
                    keyboard: true,
                    // any other options from the regular $().modal call (see Bootstrap docs)
                    },
                ajax: {
                    dataType: 'html',
                    method: 'GET',
                    success: function(data, status, xhr){
                adanLoadingFadeOut();
                    },
                },
            });
        });

	$(document).on("click",".hapus",function() {
    adanLoadingFadeIn();
		var id = $(this).attr('data-id');
		$.loadmodal({
			url: "{{ url($url_admin.'/penilaian-indikator-kinerja/hapus') }}/"+ id,
      id: 'responsive',
			dlgClass: 'fade',
			bgClass: 'danger',
			title: 'Hapus',
			width: '300px',
			modal: {
				keyboard: true,
				// any other options from the regular $().modal call (see Bootstrap docs)
				},
          ajax: {
				dataType: 'html',
				method: 'GET',
				success: function(data, status, xhr){
            adanLoadingFadeOut();
				},
			},
        });
	});

	$(document).on("click",".nilai",function() {
		adanLoadingFadeIn();
		var id = $(this).attr('data-id');
		$.loadmodal({
			url: "{{ url($url_admin.'/penilaian-indikator-kinerja') }}/nilai/"+ id,
      	id: 'responsive',
			dlgClass: 'fade',
			bgClass: 'warning',
			title: 'Atur Nilai',
			width: 'whatever',
			modal: {
				keyboard: true,

				},
          ajax: {
				dataType: 'html',
				method: 'GET',
				success: function(data, status, xhr){
					adanLoadingFadeOut();
				},
			},
        });
	});

	$(document).on("click",".detail",function() {
		adanLoadingFadeIn();
		var id = $(this).attr('data-id');
		$.loadmodal({
			url: "{{ url($url_admin.'/penilaian-indikator-kinerja') }}/detail/"+ id,
      	id: 'responsive',
			dlgClass: 'fade',
			bgClass: 'warning',
			title: 'Lihat Detail',
			width: '900px',
			size: 'modal-lg',
			modal: {
				keyboard: true,

				},
          ajax: {
				dataType: 'html',
				method: 'GET',
				success: function(data, status, xhr){
					$('.kirim-modal').hide();
					adanLoadingFadeOut();
				},
			},
        });
	});

    $(document).on("click",".catatan",function() {
		adanLoadingFadeIn();
		var id = $(this).attr('data-id');
		$.loadmodal({
			url: "{{ url($url_admin.'/penilaian-indikator-kinerja') }}/catatan/"+ id,
      	id: 'responsive',
			dlgClass: 'fade',
			bgClass: 'warning',
			title: 'Lihat Catatan Auditor',
			width: '900px',
			size: 'modal-lg',
			modal: {
				keyboard: true,

				},
          ajax: {
				dataType: 'html',
				method: 'GET',
				success: function(data, status, xhr){
					$('.kirim-modal').hide();
					adanLoadingFadeOut();
				},
			},
        });
	});

});
