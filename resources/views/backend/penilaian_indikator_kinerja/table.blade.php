<thead>
                <tr>
                    <th>No</th>
                    <th>Indikator</th>
                    <th>Dokumen</th>
                    <th>Target</th>
                    <th>Capaian Tengah</th>
                    <th>Capaian Akhir</th>
                    <th>Nilai</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php $no = 1;?>
				@foreach($datas as $data)
					<tr>
                        @if($data->parent_id != NULL)
                            <td>{{ $no.'.' }}</td>
                            <td>{{ strip_tags($data->isi_indikator_kinerja) ?? '' }}</td>
                            <td>
                            @if($data->id_penilaian)
                                <a href="{{ url('penilaian-indikator-kinerja/ambilFile/'.$data->id_penilaian)}}" target="_blank" class="btn btn-tbl-edit btn-xs"><i class="fa fa-download"></i> Unduh</a>
                            @endif
                            </td>
                            <td>{{ $data->target ?? ''}}</td>
                            <td>{{ $data->capaianmid ?? '' }}</td>
                            <td>{{ $data->capaian ?? '' }}</td>
                            <td>{{ $data->nilai_id ?? '' }}</td>
                            <td>
                                <a class="edit ubah" data-toggle="tooltip" data-placement="top" title="Edit" data-id="{{$data->id}}" dekan-id ="{{ \Auth::user()->id}}" href="#edit-{{$data->id}}/{{ \Auth::user()->id}}">
                                <i class="fa fa-pencil text-warning"></i>
                                </a>
                                <a class="edit nilai " data-toggle="tooltip" data-placement="top" title="Nilai" data-id="{{$data->id}}" assesor-id ="{{ \Auth::user()->id}}" href="#nilai-{{$data->id}}/{{ \Auth::user()->id}}">
                                    <i class="fa fa-pencil text-primary"></i>
                                </a>
                            </td>
					    <?php $no++; ?>
                        @else
                        <?php $no = 1;?>
                            <td colspan='2' style="color:blue; font-weight:bold">{{ strip_tags($data->isi_indikator_kinerja) ?? '' }}</td>
                            <td colspan="6"></td>
                        @endif										
					</tr>
				@endforeach
            </tbody>