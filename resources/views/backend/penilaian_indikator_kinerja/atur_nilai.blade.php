@section('style')
<link href="{{ URL::to('backend') }}/assets/plugins/summernote/summernote.css" rel="stylesheet">
@endsection

{!! Form::open(array('id' => 'frmAdan-nilai', 'class' => 'form account-form', 'method' => 'POST')) !!}
{{ csrf_field() }}

	<div class="form-group row">
		<label class="col-lg-3 col-md-4 control-label">Nilai
		</label>
		<div class="col-lg-9 col-md-8">
            {!! Form::select('nilai_asessor_id', $nilais, $datas->nilai_asessor_id, array('id' => 'nilai_asessor_id', 'class' => 'form-control select2', 'placeholder' => 'Pilih', 'style' => 'width:100%')) !!}
			{{-- <select class="form-control  select2" style="width:100%" name="nilai_asessor_id">
				<option value="">Pilih Nilai</option>
				@foreach($nilais as $nilai)
					<option value="{{$nilai->id}}">Bobot {{$nilai->id}} ({{$nilai->awal}} - {{ $nilai->akhir }})</option>
				@endforeach
			</select> --}}
		</div>
    </div>

    <div class="row">
        <div class="col-md-12 col-sm-12">
        <label for="catatan"> Catatan Auditor</label>
            <textarea name="catatan" id="summernote" cols="30" rows="10" >{!! strip_tags($datas->catatan) ?? '' !!}</textarea>
        </div>
    </div>

	{!! Form::hidden('id', $datas->id, array('id' => 'id')) !!}
	{!! Form::hidden('created_by', $datas->created_by, array('created_by' => 'created_by')) !!}
	{!! Form::hidden('indikator_kinerja_id', $datas->indikator_kinerja_id, array('indikator_kinerja_id' => 'indikator_kinerja_id')) !!}
	{!! Form::hidden('url', URL::previous(), array('id' => 'url')) !!}

</div>
<div class="row">
		<div class="col-md-12">
			<span class="pesan"></span>
		</div>
</div>
{!! Form::close() !!}
<script src="{{ URL::asset('resources/vendor/jquery/jquery.enc.js') }}"></script>
<script src="{{ URL::asset('adan/penilaian_indikator_kinerja/ajax.js') }}"></script>
<script src="{{ URL::asset('adan/includes/ajax.js') }}"></script>

<script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js" ></script>
<script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js" ></script>

<!-- summernote -->
<script src="{{ URL::to('backend') }}/assets/plugins/summernote/summernote.min.js" ></script>
<script src="{{ URL::to('backend') }}/assets/js/pages/summernote/summernote-data.js" ></script>

<!--select2-->
<script src="{{ URL::to('backend') }}/assets/plugins/select2/js/select2.js" ></script>

<script>
	$('.select2').select2();
</script>
