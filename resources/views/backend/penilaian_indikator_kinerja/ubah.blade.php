@section('style')
<link href="{{ URL::to('backend') }}/assets/plugins/summernote/summernote.css" rel="stylesheet">
<style>
</style>
@endsection

{!! Form::open(array('id' => 'frmAdan-ubah', 'class' => 'form account-form', 'method' => 'PUT')) !!}
{{ csrf_field() }}

<div class="form-group row">
    <div class="col-md-12">
        <h4> {!! strip_tags($datas->isi_indikator_kinerja) !!}</h4>
    </div>
</div>

<div class="table-wrap">
    <div class="table-responsive tblHeightSet small-slimscroll-style">
        <table class="table table-striped table-bordered dt-responsive nowrap" id="table-1" style="width:100%">
            <thead>
                <tr>
                    <th style="width:3px">No</th>
                    <th >Prodi</th>
                    <th style="width:15%">Target</th>
                    <th style="width:15%">Capaian Tengah</th>
                    <th style="width:15%">Capaian Akhir</th>
                </tr>
            </thead>
            <tbody>
			@php($no=1)
			@foreach($indikators_unit as $unit)
			@php($id_unit = $unit->unit_id)
			<input type="hidden" name="id_unit[]" value="{{ $id_unit }}">
            <tr>
				<td>{{$no}}</td>
				<td><h5 style="color:blue">{{ $unit->nama_unit}} @if(\Auth::user()->unit_id == 10) ({{ $unit->jenjang_pendidikan }}) @endif</h5></td>
				<td>{!! Form::number('target_unit[]', $unit->target ?? NULL, array('id' => 'target_unit[]', 'class' => 'form-control', 'placeholder' => 'eg. 100' , $periodes['alias'] == 'c_target' ? '' : 'readonly')) !!}</td>
				<td>{!! Form::number('capaianmid_unit[]', $unit->capaianmid ?? NULL, array('id' => 'capaianmid_unit[]', 'class' => 'form-control', 'placeholder' => 'eg. 100', $periodes['alias'] == 'c_mid' ? '' : 'readonly')) !!}</td>
				<td>{!! Form::number('capaian_unit[]', $unit->capaian ?? NULL, array('id' => 'capaian_unit[]', 'class' => 'form-control', 'placeholder' => 'eg. 100', $periodes['alias'] == 'c_akhir' ? '' : 'readonly')) !!}</td>
			</tr>
			@php($no++)

            @endforeach
			<tr>
				<td colspan="2"><button class="btn btn-xs btn-primary submit-rata" id="button_rata">Hitung Rata-rata</button> | <button class="btn btn-xs btn-success" id="button_total">Hitung Total</button></td>
				<td>{!! Form::text('target', !empty($indikators->target) ? number_format((float)$indikators->target, 2) : '' , array('id' => 'target', 'class' => 'form-control', 'placeholder' => 'eg. 100' , 'readonly')) !!}</td>
				<td>{!! Form::text('capaianmid', !empty($indikators->capaianmid) ? number_format((float)$indikators->capaianmid, 2) : '' , array('id' => 'capaianmid', 'class' => 'form-control', 'placeholder' => 'eg. 100', 'readonly')) !!}</td>
				<td>{!! Form::text('capaian', !empty($indikators->capaian) ? number_format((float)$indikators->capaian, 2) : '' , array('id' => 'capaian', 'class' => 'form-control', 'placeholder' => 'eg. 100','readonly')) !!}</td>
			</tr>
            </tbody>
        </table>
    </div>
</div>

	@if($datas->status_unggah == 1 && $periodes['alias'] == 'c_akhir')
    <div class="form-group row">
        {!! Form::label('Link Bukti Dokumen', 'Link Bukti Dokumen', array('class' => 'col-md-2 control-label')) !!}
        <div class="col-md-10">
        {!! Form::text('url_file_indikator', $datas->dataIndikator['url_file_indikator'], array('id' => 'url_file_indikator', 'class' => 'form-control', 'size' => 16, 'placeholder' => 'eg. https://drive.google.com/file/d/1prmuzkpeRoD7UeqTvQSmWV__yAhjp96C/view?usp=sharing')) !!}
        </div>
    </div>
	{{-- <div class="form-group row">
		{!! Form::label('Dokumen', 'File Bukti (Maks 10 MB *.PDF)', array('class' => 'col-md-4 form-label')) !!}
		<div class="col-md-8">
		{!! Form::file('file_indikator_kinerja', array('id' => 'file_indikator_kinerja', 'class' => 'form-control')) !!}
		</div>
	</div> --}}
	@endif

	{!! Form::hidden('id', $datas->id, array('id' => 'id')) !!}
	{!! Form::hidden('alias', $periodes['alias'], array('id' => 'alias')) !!}
	{!! Form::hidden('url', URL::previous(), array('id' => 'url')) !!}
</div>
<div class="row">
		<div class="col-md-12">
			<span class="pesan"></span>
		</div>
</div>
{!! Form::close() !!}
<script src="{{ URL::asset('resources/vendor/jquery/jquery.enc.js') }}"></script>
<script src="{{ URL::asset('adan/penilaian_indikator_kinerja/ajax.js') }}"></script>
<script src="{{ URL::asset('adan/includes/ajax.js') }}"></script>

<script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js" ></script>
<script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js" ></script>

<!-- summernote -->
<script src="{{ URL::to('backend') }}/assets/plugins/summernote/summernote.min.js" ></script>
<script src="{{ URL::to('backend') }}/assets/js/pages/summernote/summernote-data.js" ></script>

<!--select2-->
<script src="{{ URL::to('backend') }}/assets/plugins/select2/js/select2.js" ></script>

<script>
	$('.select2').select2();


	$('#button_rata').on('click', function(event){
		event.preventDefault();
		var form = $('#frmAdan-ubah')[0];
		$.ajax({
		headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
		url:"{{ url($url_admin.'/penilaian-indikator-kinerja/hitung') }}/"+id+'/avg',
		method:"POST",
		enctype: "multipart/form-data",
		data:new FormData(form),
		dataType:'JSON',
		contentType: false,
		cache: false,
		processData: false,
		beforeSend: function(){
					sebelumKirim();
			},
			success: function(data){
					successMsg(data);
			},
			error: function(x, e){
				//	errorMsg(x.status);
			}
		})
	});

	$('#button_total').on('click', function(event){
		event.preventDefault();
		var form = $('#frmAdan-ubah')[0];

		$.ajax({
		headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
		url:"{{ url($url_admin.'/penilaian-indikator-kinerja/hitung') }}/"+id+'/sum',
		method:"POST",
		enctype: "multipart/form-data",
		data:new FormData(form),
		dataType:'JSON',
		contentType: false,
		cache: false,
		processData: false,
		beforeSend: function(){
				sebelumKirim();
			},
			success: function(data){
				successMsg(data);
			},
			error: function(x, e){
				//	errorMsg(x.status);
			}
		})
	});

</script>
