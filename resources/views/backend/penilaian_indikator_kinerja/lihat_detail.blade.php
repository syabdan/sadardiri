<div class="table-wrap">
    <div class="table-responsive tblHeightSet small-slimscroll-style">
        <table class="table table-striped table-bordered dt-responsive nowrap" id="table-detail" style="width:100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Prodi</th>
                    <th>Dokumen</th>
                    <th>Target</th>
                    <th>Capaian Tengah</th>
                    <th>Capaian Akhir</th>
                </tr>
            </thead>
            <tbody>
               
            </tbody>
        </table>
    </div>
</div>	

<input type="hidden" id="id" value="{{$id}}">
    <!-- data tables -->
    <script src="{{ URL::to('backend') }}/assets/plugins/datatables/jquery.dataTables.min.js" ></script>
    <script src="{{ URL::to('backend') }}/assets/plugins/datatables/editabletable.js" ></script>
	<script src="{{ URL::to('backend') }}/assets/js/pages/table/editable_table_data.js" ></script>
 	<script src="{{ URL::to('backend') }}/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
	<script src="{{ URL::to('backend') }}/assets/js/pages/table/table_data.js" ></script>
    <script src="{{ URL::to('resources') }}/vendor/datatables/js/dataTables.responsive.min.js" ></script>
    <script src="{{ URL::to('resources') }}/vendor/datatables/js/responsive.bootstrap4.min.js" ></script>

	<script src="{{ URL::asset('adan/penilaian_indikator_kinerja/ajax.js') }}"></script>
	<script src="{{ URL::asset('adan/includes/ajax.js') }}"></script>


<script>
    var table;
	var id = $('#id').val();

	// alert(id);
    $(document).ready( function () {
            $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
            });
        table = $('#table-detail').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
            url: '{!! route('penilaian-indikator-kinerja.data_detail') !!}'+'/'+id,
            type: 'POST'
            },
            columns: [
                { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false,searchable: false},
                { data: 'nama_unit' },
                { data: 'dokumen'},
                { data: 'target'},
                { data: 'capaianmid'},
                { data: 'capaian'},
            ]
        });
        table.on( 'draw', function () {
            $('.livicon').each(function(){
                $(this).updateLivicon();
            });
        } );
    });
    </script>