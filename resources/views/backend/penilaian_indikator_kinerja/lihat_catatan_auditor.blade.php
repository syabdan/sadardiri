<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <div class="col-lg-4">
                <label style="color: blue; font-size: 16px; font-weight: bold; ">Catatan Auditor</label>
            </div>
            <div class="col-lg-12">
                <textarea class = "mdl-textfield__input" rows =  "5" readonly>{!! $data_indikator_kinerja->catatan ?? '' !!}</textarea>
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="id" value="{{$id}}">
    <!-- data tables -->
    <script src="{{ URL::to('backend') }}/assets/plugins/datatables/jquery.dataTables.min.js" ></script>
    <script src="{{ URL::to('backend') }}/assets/plugins/datatables/editabletable.js" ></script>
	<script src="{{ URL::to('backend') }}/assets/js/pages/table/editable_table_data.js" ></script>
 	<script src="{{ URL::to('backend') }}/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
	<script src="{{ URL::to('backend') }}/assets/js/pages/table/table_data.js" ></script>
    <script src="{{ URL::to('resources') }}/vendor/datatables/js/dataTables.responsive.min.js" ></script>
    <script src="{{ URL::to('resources') }}/vendor/datatables/js/responsive.bootstrap4.min.js" ></script>

	<script src="{{ URL::asset('adan/penilaian_indikator_kinerja/ajax.js') }}"></script>
	<script src="{{ URL::asset('adan/includes/ajax.js') }}"></script>
