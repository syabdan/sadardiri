
<div class="table-wrap">
    <div class="table-responsive tblHeightSet small-slimscroll-style">
        <table class="table table-striped table-bordered dt-responsive nowrap" id="table-1" style="width:100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Indikator</th>
                    <th>Dokumen</th>
                    <th>Target</th>
                    <th>Capaian Tengah</th>
                    <th>Capaian Akhir</th>
                    <th>Persentase (%)</th>
                    <th>Nilai Capaian</th>
                    <th>Nilai Asessor</th>
                    @if(\Auth::user()->permissions_id == 4 || \Auth::user()->permissions_id == 3)
                    <th>Aksi</th>
                    @endif
                </tr>
            </thead>
            <tbody>
                <td colspan='10' style="color:blue; font-weight:bold">Indikator Bidang {!! $nama_indikator !!}</td>

                <?php $no = 1;?>


                @foreach($datas->where('indikator_id', $indikator_id)->whereIn('status_unit',[0, $groupUnit]) as $data)

					<tr>
                            <td>{{ $no.'.' }}</td>
                            <td>{!! strip_tags($data->isi_indikator_kinerja) ?? '' !!}</td>
                            <td>
                                 {{-- {{ dd($data->dataIndikator['iddata']) }} --}}
                            @if(!empty($data->dataIndikator['url_file_indikator']))
                                <a href="{{$data->dataIndikator['url_file_indikator']}}" target="_blank" class="btn btn-info btn-tbl-edit btn-xs"><i class="fa fa-download"></i> Unduh</a>
                                {{-- <a href="{{ url('penilaian-indikator-kinerja/ambil_file/'.$data->dataIndikator['file_indikator_kinerja'])}}" target="_blank" class="btn btn-info btn-tbl-edit btn-xs"><i class="fa fa-download"></i> Unduh</a> --}}
                            @else
                            <span class="btn btn-danger btn-xs">no file</span>
                            @endif

                            {{-- jika dosen --}}
                            @if(\Auth::user()->permissions_id == 4)
                            <a class="edit reupload btn btn-tbl-upload btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="reupload" data-id="{{$data->dataIndikator['iddata']}}" dekan-id ="{{ \Auth::user()->id}}" href="#reupload-{{$data->dataIndikator['iddata']}}/{{ \Auth::user()->id}}">
                               Reupload <i class="fa fa-upload"></i>
                            </a>
                            {{-- jika dosen --}}
                            @endif
                            </td>
                            <td>
                                @if(!empty($data->dataIndikator->target))
                                    {{ number_format((float)$data->dataIndikator->target, 2) }}
                                @else
                                    {{ '' }}
                                @endif
                            </td>
                            <td>{{ !empty($data->dataIndikator->capaianmid) ? number_format((float)$data->dataIndikator->capaianmid, 2) : '' }}</td>
                            <td>{{ !empty($data->dataIndikator->capaian) ? number_format((float)$data->dataIndikator->capaian, 2) : '' }}</td>
                            <td>{{ ($data->dataIndikator['persentase'] ?? '') ? number_format((float)$data->dataIndikator['persentase'], 2) : '' }}</td>
                            <td>{{ $data->dataIndikator->nilai_id ?? '' }}</td>
                            <td>{{ $data->dataIndikator->nilai_asessor_id ?? '' }}</td>

                            @if(\Auth::user()->permissions_id == 4 || \Auth::user()->permissions_id == 3)
                            <td>
                                @if(\Auth::user()->permissions_id == 4)
                                <a class="edit catatan" data-toggle="tooltip" data-placement="top" title="Catatan" data-id="{{$data->id}}" dekan-id ="{{ \Auth::user()->id}}" href="#catatan-{{$data->id}}/{{ \Auth::user()->id}}">
                                <i class="fa fa-comments-o text-primary"></i>
                                </a>
                                |
                                <a class="edit detail" data-toggle="tooltip" data-placement="top" title="Detail" data-id="{{$data->id}}" dekan-id ="{{ \Auth::user()->id}}" href="#detail-{{$data->id}}/{{ \Auth::user()->id}}">
                                <i class="fa fa-search text-success"></i>
                                </a>
                                |
                                <a class="edit ubah" data-toggle="tooltip" data-placement="top" title="Edit" data-id="{{$data->id}}" dekan-id ="{{ \Auth::user()->id}}" href="#edit-{{$data->id}}/{{ \Auth::user()->id}}">
                                <i class="fa fa-pencil text-warning"></i>
                                </a>
                                @endif

                                @if(\Auth::user()->permissions_id == 3 && !empty($data->dataIndikator['target']))
                                <a class="edit detail" data-toggle="tooltip" data-placement="top" title="Detail" data-id="{{$data->id}}" dekan-id ="{{ \Auth::user()->id}}" href="#detail-{{$data->id}}/{{ \Auth::user()->id}}">
                                <i class="fa fa-search text-success"></i>
                                </a>
                                |
                                <a class="edit nilai" data-toggle="tooltip" data-placement="top" title="Nilai" data-id="{{$data->id}}" assesor-id ="{{ \Auth::user()->id}}" href="#nilai-{{$data->id}}/{{ \Auth::user()->id}}">
                                    <i class="fa fa-pencil text-primary"></i>
                                </a>
                                @endif
                            </td>
                            @endif

					    <?php $no++; ?>

					</tr>
				@endforeach
            </tbody>
        </table>
    </div>
</div>
