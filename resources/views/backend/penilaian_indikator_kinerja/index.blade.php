@extends('backend.layout')
@section('style')
    <!-- data tables -->
    <link href="{{ URL::to('backend') }}/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::to('resources') }}/vendor/datatables/css/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
    <!--select2-->
    <link href="{{ URL::to('backend') }}/assets/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="{{ URL::to('backend') }}/assets/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Penilaian Indikator Kinerja</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Penilaian Indikator Kinerja</li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="card  card-box">
                    <div class="card-head">
                        <header>Penilaian Indikator Kinerja</header>
                        <div class="tools">
                            <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                            <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                            <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                        </div>
                    </div>

                            <div class="card card-box">
                                <div class="card-body " id="bar-parent">
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2 col-2">
                                            <ul class="nav nav-tabs tabs-left">
                                                <?php $no=1 ?>
                                                @foreach($indikators as $indikator)
                                                <li class="nav-item">
                                                    <a href="#tab_{{ $no }}" data-toggle="tab" class="{{ $no == 1 ? 'active' : ''}}"> Indikator {{$no}}</a>
                                                </li>
                                                <?php $no++ ?>
                                                @endforeach
                                            </ul>
                                        </div>
                                        <div class="col-md-10 col-sm-10 col-10">
                                            <div class="tab-content">
                                            <?php $no=1 ?>
                                                @foreach($indikators as $indikator)
                                                <div class="tab-pane {{ $no == 1 ? 'active' : 'fade'}}" id="tab_{{$indikator->indikator_id}}">
                                                   @include('backend.penilaian_indikator_kinerja.kriteria',['indikator_id' => $indikator->indikator_id, 'nama_indikator' => $indikator->keterangan])
                                                </div>
                                                <?php $no++ ?>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>
        <!-- end page content -->

@endsection

@section('script')
    <!-- data tables -->
    <script src="{{ URL::to('backend') }}/assets/plugins/datatables/jquery.dataTables.min.js" ></script>
    <script src="{{ URL::to('backend') }}/assets/plugins/datatables/editabletable.js" ></script>
	<script src="{{ URL::to('backend') }}/assets/js/pages/table/editable_table_data.js" ></script>
 	<script src="{{ URL::to('backend') }}/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
	<script src="{{ URL::to('backend') }}/assets/js/pages/table/table_data.js" ></script>
    <script src="{{ URL::to('resources') }}/vendor/datatables/js/dataTables.responsive.min.js" ></script>
    <script src="{{ URL::to('resources') }}/vendor/datatables/js/responsive.bootstrap4.min.js" ></script>
    <script src="{{ URL::to('/') }}/adan/penilaian_indikator_kinerja/jquery.js"></script>
@endsection
