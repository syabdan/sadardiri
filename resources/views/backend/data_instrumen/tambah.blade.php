@section('style')
<link href="{{ URL::to('backend') }}/assets/plugins/summernote/summernote.css" rel="stylesheet">
@endsection

{!! Form::open(array('id' => 'frmAdan', 'class' => 'form account-form', 'method' => 'post', 'files' => true)) !!}
<div class="form-group">
			{!! Form::label('Instrumen', 'Instrumen *', array('class' => 'form-label')) !!}
  			{!! Form::select('instrumen_id', $instrumen, NULL, array('id' => 'instrumen_id', 'class' => 'form-control select2', 'placeholder' => 'Pilih', 'style' => 'width:100%')) !!}
		</div>
		
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="card  card-box">
					<div class="card-body">
						<label for="isi_indikator_kinerja"> Narasi Instrumen / Pertanyaan *</label>
							<textarea name="isi_pertanyaan" class="summernote" cols="30" rows="10" ></textarea>

						{!! Form::label('Status unggah', 'Status unggah *', array('class' => 'form-label')) !!}
						{!! Form::select('status_unggah', $unggah, NULL, array('id' => 'status_unggah', 'class' => 'sidebar-pos-option form-control input-inline input-sm input-small select2', 'placeholder' => 'Pilih', 'style' => 'width:100%')) !!}
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
				<label for=""> Tambah Sub Narasi Instrumen / Pertanyaan</label>
				<a class="btn btn-flat btn-sm btn-success" id="tambah_sub_ask"><i class="fa fa-plus"  aria-hidden="true"></i></a>
			</div>
		</div>
		<br>
		<div id="indukwrap">
			
			
		</div>
		
		<br>
		

	
	{!! Form::hidden('url', URL::previous(), array('id' => 'url')) !!}

<div class="row">
	<div class="col-md-12">
		<span class="pesan"></span>
	</div>
</div>
{!! Form::close() !!}
<script src="{{ URL::asset('resources/vendor/jquery/jquery.enc.js') }}"></script>
<script src="{{ URL::asset('adan/pertanyaan/ajax.js') }}"></script>
<script src="{{ URL::asset('adan/includes/ajax.js') }}"></script>

<script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js" ></script>
<script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js" ></script>
<!-- summernote -->
<script src="{{ URL::to('backend') }}/assets/plugins/summernote/summernote.min.js" ></script>
<script src="{{ URL::to('backend') }}/assets/js/pages/summernote/summernote-data.js" ></script>

<!--select2-->
<script src="{{ URL::to('backend') }}/assets/plugins/select2/js/select2.js" ></script>

<script>
	$('.select2').select2();

	$('.summernote').summernote({
        placeholder: '',
        tabsize: 2,
        height: 200
      });

</script>

<script type="text/javascript">
		$(function() {
			var next = 0;
			var scntDiv = $('#indukwrap');
			$('#tambah_sub_ask').on('click', function() {
				 next = next + 1;
					$('<div class="row"><div class="col-md-12 col-sm-12"><div class="card  card-box"><div class="card-body"><div id="indukwrap'+next+'"> <div class="row"><div class="col-md-12 col-sm-12"><label for="isi_indikator_kinerja">Sub Narasi Instrumen / Pertanyaan  '+next+'</label> <a class="btn btn-sm btn-flat btn-danger" id="hapus"><i class="fa fa-trash"  aria-hidden="true"></i></a><textarea name="isi_pertanyaan_sub[]" class="summernote" cols="30" rows="10" ></textarea></div></div></div> {!! Form::label('Status unggah sub pertanyaan',"Status unggah sub pertanyaan", array('class' => 'form-label')) !!}{!! Form::select('status_unggah_sub[]', $unggah,NULL, array('id' => 'status_unggah_sub', 'class' => 'sidebar-pos-option form-control input-inline select2', 'placeholder' => 'Pilih', 'style' => 'width:100%')) !!}</div></div></div></div>').appendTo(scntDiv);

					$( '[class="summernote"]' ).summernote({
																				placeholder: '',
																				tabsize: 2,
																				height: 200
																			});

					// return false;
			});
			
			$('#indukwrap').on('click','#hapus',function() { 

			$('#indukwrap'+next).remove();
			next = next-1;
			});
		});
	</script>