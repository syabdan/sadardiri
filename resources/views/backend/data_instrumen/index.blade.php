@extends('backend.layout')
@section('style')
    <!-- data tables -->
    <link href="{{ URL::to('backend') }}/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::to('resources') }}/vendor/datatables/css/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
    <!--select2-->
    <link href="{{ URL::to('backend') }}/assets/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="{{ URL::to('backend') }}/assets/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">{{$halaman->nama}}</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">{{$halaman->nama}}</li>
                            </ol>
                        </div>
					</div>
					
                     <!-- start Payment Details -->
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="card  card-box">
                                <div class="card-head">
                                    <header>Data {{$halaman->nama}}</header>
                                    <div class="tools">
                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
	                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
	                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                    </div>
                                </div>
                                <div class="card-body ">
									<div class="row p-b-20">
                                        <div class="col-md-6 col-sm-6 col-6">
                                            <div class="btn-group">
                                                <a class="btn btn-info tambah">Tambah <i class="fa fa-plus"></i></a>
                                            </div>
                                        </div>
                                       
                                    </div>
                                  <div class="table-wrap">
										<div class="table-responsive tblHeightSet small-slimscroll-style">
											<table class="table table-striped table-bordered dt-responsive nowrap" id="table" style="width:100%">
												<thead>
													<tr>
														<th>No</th>
														<th>Instrumen</th>
														<th>Pertanyaan</th>
														<th>Status Unggah</th>
														<th>Aksi</th>
													</tr>
												</thead>
												<tbody>
													
												</tbody>
											</table>
										</div>
									</div>	
                                </div>
                            </div>
                        </div>
                    </div>
                   
                </div>
            </div>
        <!-- end page content -->
        
    </div>
@endsection

@section('script')
    <!-- data tables -->
    <script src="{{ URL::to('backend') }}/assets/plugins/datatables/jquery.dataTables.min.js" ></script>
    <script src="{{ URL::to('backend') }}/assets/plugins/datatables/editabletable.js" ></script>
	<script src="{{ URL::to('backend') }}/assets/js/pages/table/editable_table_data.js" ></script>
 	<script src="{{ URL::to('backend') }}/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
	<script src="{{ URL::to('backend') }}/assets/js/pages/table/table_data.js" ></script>
    <script src="{{ URL::to('resources') }}/vendor/datatables/js/dataTables.responsive.min.js" ></script>
    <script src="{{ URL::to('resources') }}/vendor/datatables/js/responsive.bootstrap4.min.js" ></script>

    <script src="{{ URL::to('/') }}/adan/pertanyaan/jquery.js"></script>

    <script>
    var table;
    $(document).ready( function () {
            $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
            });
        table = $('#table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
            url: '{!! route('narasi-instrumen.data') !!}',
            type: 'POST'
            },
            columns: [
                { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false,searchable: false},
                { data: 'instrumen.nama_instrumen' },
                { data: 'isi_pertanyaan'},
                { data: 'status_unggah'},
                { data: 'actions', name: 'actions', orderable: false, searchable: false }
            ]
        });
        table.on( 'draw', function () {
            $('.livicon').each(function(){
                $(this).updateLivicon();
            });
        } );
    });
    </script>
@endsection