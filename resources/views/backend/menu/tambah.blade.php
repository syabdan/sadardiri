{!! Form::open(array('id' => 'frmAdan', 'class' => 'form account-form', 'method' => 'post', 'files' => true)) !!}

		<div class="form-group row">
		{!! Form::label('Nama Menu', 'Nama Menu', array('class' => 'col-md-4 control-label')) !!}
			<div class="col-md-8">
			{!! Form::text('nama', NULL, array('id' => 'nama', 'class' => 'form-control', 'size' => 16, 'placeholder' => 'eg. Master')) !!}
			</div>
		</div>
		
		<div class="form-group row">
		{!! Form::label('Link', 'Link', array('class' => 'col-md-4 control-label')) !!}
			<div class="col-md-8">
			{!! Form::text('link', NULL, array('id' => 'link', 'class' => 'form-control', 'size' => 16, 'placeholder' => 'eg. master')) !!}
			</div>
		</div>
		
		<div class="form-group row">
		{!! Form::label('Icon', 'Icon', array('class' => 'col-md-4 control-label')) !!}
			<div class="col-md-8">
			{!! Form::text('icon', 'desktop_mac', array('id' => 'icon', 'class' => 'form-control', 'placeholder' => 'Icon')) !!}

			</div>
		</div>
		
		<div class="form-group row">
			{!! Form::label('Posisi', 'Posisi ', array('class' => 'col-md-4 form-label')) !!}
			<div class="col-md-8">
			<select name="posisi" id="posisi" class="form-control select2" style="width:100%">
			<option value="">Pilih</option>
			<?php 
				for($i=1;$i<=100;$i++){
					echo "<option value='".$i."'>$i</option>";
				}
			?>
			</select>
			</div>
		</div>
		
		<div class="form-group row">
			{!! Form::label('Parent', 'Parent Menu ', array('class' => 'col-md-4 form-label')) !!}
			<div class="col-md-8">
  			{!! Form::select('parent_menu_id', $parent_menu, NULL, array('id' => 'parent_menu_id', 'class' => 'form-control select2', 'placeholder' => 'Pilih', 'style' => 'width:100%')) !!}
			</div>
		</div>

		<div class="form-group row">
			{!! Form::label('Tampil', 'Tampil ', array('class' => 'col-md-4 form-label')) !!}
			<div class="col-md-8">
  			{!! Form::select('tampil', $tampil, NULL, array('id' => 'tampil', 'class' => 'form-control select2', 'placeholder' => 'Pilih', 'style' => 'width:100%')) !!}
			</div>
		</div>
		
	{!! Form::hidden('url', URL::previous(), array('id' => 'url')) !!}

<div class="row">
	<div class="col-md-12">
		<span class="pesan"></span>
	</div>
</div>
{!! Form::close() !!}
<script src="{{ URL::asset('resources/vendor/jquery/jquery.enc.js') }}"></script>
<script src="{{ URL::asset('adan/menu/ajax.js') }}"></script>
<script src="{{ URL::asset('adan/includes/ajax.js') }}"></script>

<script src="{{ URL::to('backend') }}/assets/plugins/select2/js/select2.js" ></script>
<script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js" ></script>
<script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js" ></script>

<script>
	$('.select2').select2();
</script>