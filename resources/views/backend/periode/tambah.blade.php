{!! Form::open(array('id' => 'frmAdan', 'class' => 'form account-form', 'method' => 'post', 'files' => true)) !!}

		
		<div class="form-group row">
		{!! Form::label('Nama', 'Nama', array('class' => 'col-md-4 control-label')) !!}
			<div class="col-md-8">
			{!! Form::text('nama_periode', NULL, array('id' => 'nama_periode', 'class' => 'form-control', 'size' => 16, 'placeholder' => 'eg. Periode 2020')) !!}
			</div>
		</div>

		<div class="card card-box">
			<div class="form-group row">
				<label class="col-md-4 control-label">Tanggal Target Awal
					<span class="required"> * </span>
				</label>
				<div class="col-md-8">
					<div class="input-group date form_date" data-date="" data-date-format="yyyy-mm-dd" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
						<input class="form-control" type="text" value="" name="tanggal_target_awal">
						<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
					</div>
				</div>
			</div>
			<div class="form-group row">
				<label class="control-label col-md-4">Tanggal Target Akhir
					<span class="required"> * </span>
				</label>
				<div class="col-md-8">
					<div class="input-group date form_date" data-date="" data-date-format="yyyy-mm-dd" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
						<input class="form-control" type="text" value="" name="tanggal_target_akhir">
						<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
					</div>
				</div>
			</div>
		</div>
		
        <div class="card card-box">
			<div class="form-group row">
				<label class="col-md-4 control-label">Tanggal Capaian Tengah Awal
					<span class="required"> * </span>
				</label>
				<div class="col-md-8">
					<div class="input-group date form_date" data-date="" data-date-format="yyyy-mm-dd" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
						<input class="form-control" type="text" value="" name="tanggal_capaianmid_awal">
						<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
					</div>
				</div>
			</div>
			<div class="form-group row">
				<label class="control-label col-md-4">Tanggal Capaian Tengah Akhir
					<span class="required"> * </span>
				</label>
				<div class="col-md-8">
					<div class="input-group date form_date" data-date="" data-date-format="yyyy-mm-dd" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
						<input class="form-control" type="text" value="" name="tanggal_capaianmid_akhir">
						<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
					</div>
				</div>
			</div>
		</div>
		
		<div class="card card-box">
			<div class="form-group row">
				<label class="col-md-4 control-label">Tanggal Capaian Awal
					<span class="required"> * </span>
				</label>
				<div class="col-md-8">
					<div class="input-group date form_date" data-date="" data-date-format="yyyy-mm-dd" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
						<input class="form-control" type="text" value="" name="tanggal_capaian_awal">
						<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
					</div>
				</div>
			</div>
			<div class="form-group row">
				<label class="control-label col-md-4">Tanggal Capaian Akhir
					<span class="required"> * </span>
				</label>
				<div class="col-md-8">
					<div class="input-group date form_date" data-date="" data-date-format="yyyy-mm-dd" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
						<input class="form-control" type="text" value="" name="tanggal_capaian_akhir">
						<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
					</div>
				</div>
			</div>
		</div>
	
	{!! Form::hidden('url', URL::previous(), array('id' => 'url')) !!}

<div class="row">
	<div class="col-md-12">
		<span class="pesan"></span>
	</div>
</div>
{!! Form::close() !!}
<script src="{{ URL::asset('resources/vendor/jquery/jquery.enc.js') }}"></script>
<script src="{{ URL::asset('adan/periode/ajax.js') }}"></script>
<script src="{{ URL::asset('adan/includes/ajax.js') }}"></script>

<script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js" ></script>
<script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js" ></script>
