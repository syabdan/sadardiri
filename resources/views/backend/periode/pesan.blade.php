<div class="inbox-body no-pad">
    <div class="mail-list">
        <div class="compose-mail">
        {!! Form::open(array('id' => 'frmAdan', 'class' => 'form account-form', 'method' => 'post', 'files' => true)) !!}


		<div class="row">
			<div class="col-md-12 col-sm-12">
			<label for="pesan"> Isi Pesan *</label>
				<textarea name="pesan" id="summernote" cols="30" rows="10" maxlength="250"></textarea>
			</div>
        </div>

        <span id="total-characters"></span>

        <span style="color: red"><i> Maksimal 250 karakter. Jika lebih, pesan akan terpotong</i></span>
	    {!! Form::hidden('url', URL::previous(), array('id' => 'url')) !!}

        <div class="row">
            <div class="col-md-12">
                <span class="pesan"></span>
            </div>
        </div>
        {!! Form::close() !!}

        </div>
    </div>
</div>

<script src="{{ URL::asset('resources/vendor/jquery/jquery.enc.js') }}"></script>
<script src="{{ URL::asset('adan/periode/ajax.js') }}"></script>
<script src="{{ URL::asset('adan/includes/ajax.js') }}"></script>

<script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js" ></script>
<script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js" ></script>
<!-- summernote -->
<script src="{{ URL::to('backend') }}/assets/plugins/summernote/summernote.min.js" ></script>
<script src="{{ URL::to('backend') }}/assets/js/pages/summernote/summernote-data.js" ></script>

<script>

</script>
