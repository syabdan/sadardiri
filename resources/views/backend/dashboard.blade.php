@extends('backend.layout')
@section('style')

@endsection
@section('content')
<div class="page-content-wrapper">
    <div class="page-content">

        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Dashboard
                        <button class="btn btn-xs btn-success" onclick="window.print()"><i class="fa fa-print"></i> Cetak</button>
                    </div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Dashboard</li>
                </ol>
            </div>
        </div>

        <div class="text-container">
        @if($running_text)
            <marquee direction="" onmouseover="this.stop();" onmouseout="this.start();">
                <h4 style="color:red">Pengisian {{ $running_text['nama'] }} Indikator Kinerja akan berakhir pada "{{ tanggal_indonesia($running_text['periode_akhir']) }}"</h4>
            </marquee>
        @endif
        </div>

        <!-- start widget -->

            @if(\Auth::user()->permissions_id != 1 && \Auth::user()->permissions_id != 2)
            <div class="row">
                <div class="col-lg-7 col-md-12 col-sm-12 col-12">
                    <div class="card card-topline-lightblue">
                        <div id="all_instrumen"></div><center>
                        <span class="highcharts-description">
                        1 = Belum, 2 = Sebagian, 3 = Memenuhi, 4 = Melampaui
                        </span></center>
                    </div>
                </div>
                <div class="col-lg-5 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <div class="panel-body">
                            <h4>Persentase Pengisian Evaluasi Diri</h4>
                            <div class="progressbar-xs progress-rounded progress-striped progress ng-isolate-scope active">
                                <div class="progress-bar progress-bar-green" style="width: {{ $presentase_evaluasi ?? '' }}%;" role="progressbar" aria-valuenow="{{ $presentase_evaluasi ?? ''}}" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <span class="text-small margin-top-10 full-width">{{ number_format($presentase_evaluasi ?? '', 2, ',', '') }} % sudah terisi</span>
                        </div>
                    </div>
                    <div class="card">
                        <div class="panel-body">
                            <h4>Persentase Unggah Dokumen Pendukung Evaluasi Diri</h4>
                            <div class="progressbar-xs progress-rounded progress-striped progress ng-isolate-scope active">
                                <div class="progress-bar progress-bar-orange" style="width: {{ $presentase_unggah }}%;" role="progressbar" aria-valuenow="{{ $presentase_unggah }}" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <span class="text-small margin-top-10 full-width">{{  number_format($presentase_unggah, 2, ',', '') }} % sudah terunggah </span>
                        </div>
                    </div>
                    <div class="card">
                        <div class="panel-body">
                            <h4>Persentase Pengisian Indikator Kinerja</h4>
                            <div class="progressbar-sm progress-rounded progress-striped progress ng-isolate-scope active" >
                                <div class="progress-bar progress-bar-purple " style="width: {{ $presentase_indikator }}%;" role="progressbar" aria-valuenow="{{ $presentase_indikator }}" aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                            <span class="text-small margin-top-10 full-width">{{ number_format($presentase_indikator, 2, ',', '') }} % sudah terisi</span>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            @if(\Auth::user()->permissions_id == 1 || \Auth::user()->permissions_id == 2)
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="card card-topline-lightblue">
                        <br><br>
                        <div id="all_instrumen"></div>

                        <center>
                            <span class="highcharts-description">
                            1 = Belum, 2 = Sebagian, 3 = Memenuhi, 4 = Melampaui
                            </span>
                            <br><br><br>
                        </center>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="card card-topline-lightblue">
                        <div class="panel-body">
                            <table class="table table-striped table-bordered dt-responsive nowrap" id="table" style="width:100%">
                                <thead>
                                    <th style="font-size: 8pt">Fakultas</th>
                                    <th style="font-size: 8pt">Evaluasi Diri</th>
                                    <th style="font-size: 8pt">Unggah Dokumen</th>
                                    <th style="font-size: 8pt">Pengisian Target</th>
                                    <th style="font-size: 8pt">Pengisian Capaian Tengah</th>
                                    <th style="font-size: 8pt">Pengisian Capaian Akhir</th>
                                </thead>
                                <tbody>
                                    @foreach($units as $unit)
                                    <tr>
                                        <td style="font-size: 8pt">{{ $unit->nama_unit }}</td>
                                        <td style="font-size: 8pt">{{ $persen_eva[$unit->id ]}} %</td>
                                        <td style="font-size: 8pt">{{ $persen_unggah[$unit->id ]}} %</td>
                                        <td style="font-size: 8pt">{{ $persen_target[$unit->id ]}} %</td>
                                        <td style="font-size: 8pt">{{ $persen_mid[$unit->id ]}} %</td>
                                        <td style="font-size: 8pt">{{ $persen_akhir[$unit->id ]}} %</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            <!-- end widget -->
        <div class="row">
            <div class="col-md-4 col-sm-12 col-12">
                <div class="card card-topline-lightblue">
                    <div id="instrumen_1" height=""></div>
                </div>
            </div>
            <div class="col-md-4 col-sm-12 col-12">
                <div class="card card-topline-lightblue">
                    <div id="instrumen_2" height=""></div>
                </div>
            </div>
            <div class="col-md-4 col-sm-12 col-12">
                <div class="card card-topline-lightblue">
                    <div id="instrumen_3" height=""></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-12 col-12">
                <div class="card card-topline-lightblue">
                    <div id="instrumen_4" height=""></div>
                </div>
            </div>
            <div class="col-md-4 col-sm-12 col-12">
                <div class="card card-topline-lightblue">
                    <div id="instrumen_5" height=""></div>
                </div>
            </div>
            <div class="col-md-4 col-sm-12 col-12">
                <div class="card card-topline-lightblue">
                    <div id="instrumen_6" height=""></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-12 col-12">
                <div class="card card-topline-lightblue">
                    <div id="instrumen_7" height=""></div>
                </div>
            </div>
            <div class="col-md-4 col-sm-12 col-12">
                <div class="card card-topline-lightblue">
                    <div id="instrumen_8" height=""></div>
                </div>
            </div>
            <div class="col-md-4 col-sm-12 col-12">
                <div class="card card-topline-lightblue">
                    <div id="instrumen_9" height=""></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
	<!-- <script src="{{ URL::to('backend') }}/assets/js/pages/chart/highchart/jquery-3.1.1.min.js"></script> -->
	<script src="{{ URL::to('backend') }}/assets/js/pages/chart/highchart/highcharts.js"></script>
	<script src="{{ URL::to('backend') }}/assets/js/pages/chart/highchart/highcharts-more.js"></script>
	<script src="{{ URL::to('backend') }}/assets/js/pages/chart/highchart/exporting.js"></script>
	<script src="{{ URL::to('backend') }}/assets/js/pages/chart/highchart/export-data.js"></script>
	<script src="{{ URL::to('backend') }}/assets/js/pages/chart/highchart/accessibility.js"></script>

	@if(\Auth::user()->permissions_id == 3 || \Auth::user()->permissions_id == 4 || \Auth::user()->permissions_id == 5)
        @include('backend.chart.script_unit')
    @endif

    @if(\Auth::user()->permissions_id == 1 || \Auth::user()->permissions_id == 2)
        @include('backend.chart.script_all')
    @endif

@endsection
