{!! Form::open(array('id' => 'frmAdan', 'class' => 'form account-form', 'method' => 'post', 'files' => true)) !!}
        <div class="form-group row">
            {!! Form::label('Fakultas', 'Fakultas', array('class' => 'col-md-4 form-label')) !!}
            <div class="col-md-8">
            {!! Form::select('unit_id', $unit, NULL, array('id' => 'unit_id', 'class' => 'form-control select2', 'placeholder' => 'Pilih', 'style' => 'width:100%')) !!}
            </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Jabatan', 'Jabatan', array('class' => 'col-md-4 control-label')) !!}
            <div class="col-md-8">
                {!! Form::select('jabatan', $jabatan, NULL, array('id' => 'jabatan', 'class' => 'form-control select2', 'placeholder' => 'Pilih', 'style' => 'width:100%')) !!}
            </div>
        </div>

        <div class="form-group row">
        {!! Form::label('Nama Dosen', 'Nama Dosen', array('class' => 'col-md-4 control-label')) !!}
            <div class="col-md-8">
            {!! Form::text('nama_pimpinan', NULL, array('id' => 'nama_pimpinan', 'class' => 'form-control')) !!}
            </div>
        </div>

        <div class="form-group row">
        {!! Form::label('Nomor Handphone', 'Nomor Handphone', array('class' => 'col-md-4 control-label')) !!}
            <div class="col-md-8">
            {!! Form::text('no_hp', NULL, array('id' => 'no_hp', 'class' => 'form-control')) !!}
            </div>
        </div>


	{!! Form::hidden('url', URL::previous(), array('id' => 'url')) !!}

<div class="row">
	<div class="col-md-12">
		<span class="pesan"></span>
	</div>
</div>
{!! Form::close() !!}
<script src="{{ URL::asset('resources/vendor/jquery/jquery.enc.js') }}"></script>
<script src="{{ URL::asset('adan/pimpinan/ajax.js') }}"></script>
<script src="{{ URL::asset('adan/includes/ajax.js') }}"></script>

<script src="{{ URL::to('backend') }}/assets/plugins/select2/js/select2.js" ></script>
<script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js" ></script>
<script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js" ></script>

<script>
	$('.select2').select2();
</script>
