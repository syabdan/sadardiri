<script>
		Highcharts.chart('all_instrumen', {
            chart: {
            polar: true
        },
        title: {
            text: 'Rata-rata Capaian Evaluasi Diri Fakultas'
        },
        subtitle: {
            // text: 'Rata-rata Capaian Evaluasi Diri Fakultas'
        },
        pane: {
            startAngle: 0,
            endAngle: 360
        },
        xAxis: {
            categories: {!! json_encode($chart_kriteria_all['category'])  !!},

        },
        yAxis: {
            min: 0,
            // max: 4 ,
            tickInterval: 1,
        },
        tooltip: {
            shared: true,
            pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.1f}</b><br/>'
        },
        credits: {
            enabled: false
        },
        legend: {
            align: 'right',
            verticalAlign: 'middle',
            layout: 'vertical'
        },

        series: [{
            name: 'Fak. Hukum',
            data: {!! json_encode($chart_kriteria_all['rata_nilai_hukum'])  !!},
            pointPlacement: 'on'
        },
        {
            name: 'Fak. Agama Islam',
            data: {!! json_encode($chart_kriteria_all['rata_nilai_fai'])  !!},
            pointPlacement: 'on'
        },
        {
            name: 'Fak. Teknik',
            data: {!! json_encode($chart_kriteria_all['rata_nilai_teknik'])  !!},
            pointPlacement: 'on'
        },
        {
            name: 'Fak. Pertanian',
            data: {!! json_encode($chart_kriteria_all['rata_nilai_pertanian'])  !!},
            pointPlacement: 'on'
        },
        {
            name: 'Fak. Ekonomi',
            data: {!! json_encode($chart_kriteria_all['rata_nilai_ekonomi'])  !!},
            pointPlacement: 'on'
        },
        {
            name: 'FKIP',
            data: {!! json_encode($chart_kriteria_all['rata_nilai_fkip'])  !!},
            pointPlacement: 'on'
        },
        {
            name: 'FISIPOL',
            data: {!! json_encode($chart_kriteria_all['rata_nilai_fisipol'])  !!},
            pointPlacement: 'on'
        },
        {
            name: 'Fak. Psikologi',
            data: {!! json_encode($chart_kriteria_all['rata_nilai_psikologi'])  !!},
            pointPlacement: 'on'
        },
        {
            name: 'Fak. I.Komunikasi',
            data: {!! json_encode($chart_kriteria_all['rata_nilai_komunikasi'])  !!},
            pointPlacement: 'on'
        },
        {
            name: 'Fak. Pascasarjana',
            data: {!! json_encode($chart_kriteria_all['rata_nilai_pascasarjana'])  !!},
            pointPlacement: 'on'
        },
        ],
        colors: ['#0CBD62', '#FFC300', '#C70039',  '#4DE5D5', '#6AB8F7', '#A38CC6', '#FFAC5B', '#e3fc03', '#eb03fc', '#fc0356'],

	    });
	</script>

    <script>
		Highcharts.chart('instrumen_1', {
            chart: {
            polar: true
        },
        title: {
            text: 'Kriteria 1'
        },
        subtitle: {
            // text: 'Rata-rata Capaian Evaluasi Diri Fakultas'
        },
        pane: {
            startAngle: 0,
            endAngle: 360
        },
        xAxis: {
            categories: {!! json_encode($chart_kriteria_1['category'])  !!}

        },
        yAxis: {
            min: 0,
            // max: 4 ,
            tickInterval: 1,
        },
        tooltip: {
            shared: true,
            pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.1f}</b><br/>'
        },
        credits: {
            enabled: false
        },
        series: [
                    {
                        name: 'Fakultas Hukum',
                        data: {!! json_encode($chart_kriteria_1['rata_nilai_hukum'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fak. Agama Islam',
                        data: {!! json_encode($chart_kriteria_1['rata_nilai_fai'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fak. Teknik',
                        data: {!! json_encode($chart_kriteria_1['rata_nilai_teknik'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fak. Pertanian',
                        data: {!! json_encode($chart_kriteria_1['rata_nilai_pertanian'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fekon',
                        data: {!! json_encode($chart_kriteria_1['rata_nilai_ekonomi'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'FKIP',
                        data: {!! json_encode($chart_kriteria_1['rata_nilai_fkip'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fakultas Fisipol',
                        data: {!! json_encode($chart_kriteria_1['rata_nilai_fisipol'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fakultas Psikologi',
                        data: {!! json_encode($chart_kriteria_1['rata_nilai_psikologi'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fak. Ilmu komunikasi',
                        data: {!! json_encode($chart_kriteria_1['rata_nilai_fikom'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Pascasarjana',
                        data: {!! json_encode($chart_kriteria_1['rata_nilai_pasca'])  !!},
                        /* pointPlacement: 'on' */
                    },

        ],
        colors: ['#0CBD62', '#FFC300', '#C70039',  '#4DE5D5', '#6AB8F7', '#A38CC6', '#FFAC5B', '#e3fc03', '#eb03fc', '#fc0356'],

	    });

	</script>
    <script>
		Highcharts.chart('instrumen_2', {
            chart: {
            polar: true
        },
        title: {
            text: 'Kriteria 2'
        },
        subtitle: {
            // text: 'Rata-rata Capaian Evaluasi Diri Fakultas'
        },
        pane: {
            startAngle: 0,
            endAngle: 360
        },
        xAxis: {
            categories:{!! json_encode($chart_kriteria_2['category'])  !!},

        },
        yAxis: {
            min: 0,
            // max: 4 ,
            tickInterval: 1,
        },
        tooltip: {
            shared: true,
            pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.1f}</b><br/>'
        },
        credits: {
            enabled: false
        },
        series: [{
                        name: 'Fakultas Hukum',
                        data: {!! json_encode($chart_kriteria_2['rata_nilai_hukum'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fak. Agama Islam',
                        data: {!! json_encode($chart_kriteria_2['rata_nilai_fai'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fak. Teknik',
                        data: {!! json_encode($chart_kriteria_2['rata_nilai_teknik'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fak. Pertanian',
                        data: {!! json_encode($chart_kriteria_2['rata_nilai_pertanian'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fekon',
                        data: {!! json_encode($chart_kriteria_2['rata_nilai_ekonomi'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'FKIP',
                        data: {!! json_encode($chart_kriteria_2['rata_nilai_fkip'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fakultas Fisipol',
                        data: {!! json_encode($chart_kriteria_2['rata_nilai_fisipol'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fakultas Psikologi',
                        data: {!! json_encode($chart_kriteria_2['rata_nilai_psikologi'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fak. Ilmu komunikasi',
                        data: {!! json_encode($chart_kriteria_2['rata_nilai_fikom'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Pascasarjana',
                        data: {!! json_encode($chart_kriteria_2['rata_nilai_pasca'])  !!},
                        /* pointPlacement: 'on' */
                    }],
        colors: ['#0CBD62', '#FFC300', '#C70039',  '#4DE5D5', '#6AB8F7', '#A38CC6', '#FFAC5B', '#e3fc03', '#eb03fc', '#fc0356'],

	    });

	</script>
    <script>
		Highcharts.chart('instrumen_3', {
            chart: {
            polar: true
        },
        title: {
            text: 'Kriteria 3'
        },
        subtitle: {
            // text: 'Rata-rata Capaian Evaluasi Diri Fakultas'
        },
        pane: {
            startAngle: 0,
            endAngle: 360
        },
        xAxis: {
            categories: {!! json_encode($chart_kriteria_3['category'])  !!}

        },
        yAxis: {
            min: 0,
            // max: 4 ,
            tickInterval: 1,
        },
        tooltip: {
            shared: true,
            pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.1f}</b><br/>'
        },
        credits: {
            enabled: false
        },
        series: [
                    {
                        name: 'Fakultas Hukum',
                        data: {!! json_encode($chart_kriteria_3['rata_nilai_hukum'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fak. Agama Islam',
                        data: {!! json_encode($chart_kriteria_3['rata_nilai_fai'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fak. Teknik',
                        data: {!! json_encode($chart_kriteria_3['rata_nilai_teknik'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fak. Pertanian',
                        data: {!! json_encode($chart_kriteria_3['rata_nilai_pertanian'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fekon',
                        data: {!! json_encode($chart_kriteria_3['rata_nilai_ekonomi'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'FKIP',
                        data: {!! json_encode($chart_kriteria_3['rata_nilai_fkip'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fakultas Fisipol',
                        data: {!! json_encode($chart_kriteria_3['rata_nilai_fisipol'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fakultas Psikologi',
                        data: {!! json_encode($chart_kriteria_3['rata_nilai_psikologi'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fak. Ilmu komunikasi',
                        data: {!! json_encode($chart_kriteria_3['rata_nilai_fikom'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Pascasarjana',
                        data: {!! json_encode($chart_kriteria_3['rata_nilai_pasca'])  !!},
                        /* pointPlacement: 'on' */
                    },

        ],
        colors: ['#0CBD62', '#FFC300', '#C70039',  '#4DE5D5', '#6AB8F7', '#A38CC6', '#FFAC5B', '#e3fc03', '#eb03fc', '#fc0356'],

	    });

	</script>
    <script>
		Highcharts.chart('instrumen_4', {
            chart: {
            polar: true
        },

        title: {
            text: 'Kriteria 4'
        },

        subtitle: {
            // text: 'Rata-rata Capaian Evaluasi Diri Fakultas'
        },

        pane: {
            startAngle: 0,
            endAngle: 360
        },

        xAxis: {
            categories: {!! json_encode($chart_kriteria_4['category'])  !!},

        },

        yAxis: {
            min: 0,
            // max: 4 ,
            tickInterval: 1,
        },

        tooltip: {
            shared: true,
            pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.1f}</b><br/>'
        },

        credits: {
            enabled: false
        },

        series: [
                    {
                        name: 'Fakultas Hukum',
                        data: {!! json_encode($chart_kriteria_4['rata_nilai_hukum'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fak. Agama Islam',
                        data: {!! json_encode($chart_kriteria_4['rata_nilai_fai'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fak. Teknik',
                        data: {!! json_encode($chart_kriteria_4['rata_nilai_teknik'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fak. Pertanian',
                        data: {!! json_encode($chart_kriteria_4['rata_nilai_pertanian'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fekon',
                        data: {!! json_encode($chart_kriteria_4['rata_nilai_ekonomi'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'FKIP',
                        data: {!! json_encode($chart_kriteria_4['rata_nilai_fkip'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fakultas Fisipol',
                        data: {!! json_encode($chart_kriteria_4['rata_nilai_fisipol'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fakultas Psikologi',
                        data: {!! json_encode($chart_kriteria_4['rata_nilai_psikologi'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fak. Ilmu komunikasi',
                        data: {!! json_encode($chart_kriteria_4['rata_nilai_fikom'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Pascasarjana',
                        data: {!! json_encode($chart_kriteria_4['rata_nilai_pasca'])  !!},
                        /* pointPlacement: 'on' */
                    },

        ],
        colors: ['#0CBD62', '#FFC300', '#C70039',  '#4DE5D5', '#6AB8F7', '#A38CC6', '#FFAC5B', '#e3fc03', '#eb03fc', '#fc0356'],

	    });

	</script>
    <script>
		Highcharts.chart('instrumen_5', {
            chart: {
            polar: true,
            type: 'line',
            // inverted: true,
        },

        title: {
            text: 'Kriteria 5'
        },

        subtitle: {
            // text: 'Rata-rata Capaian Evaluasi Diri Fakultas'
        },

        pane: {
            startAngle: 0,
            endAngle: 360
        },

        xAxis: {
            categories: {!! json_encode($chart_kriteria_5['category'])  !!},

        },

        yAxis: {
            min: 0,
            // max: 4 ,
            tickInterval: 1,
        },

        tooltip: {
            shared: true,
            pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.1f}</b><br/>'
        },

        credits: {
            enabled: false
        },
        series: [
                    {
                        name: 'Fakultas Hukum',
                        data: {!! json_encode($chart_kriteria_5['rata_nilai_hukum'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fak. Agama Islam',
                        data: {!! json_encode($chart_kriteria_5['rata_nilai_fai'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fak. Teknik',
                        data: {!! json_encode($chart_kriteria_5['rata_nilai_teknik'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fak. Pertanian',
                        data: {!! json_encode($chart_kriteria_5['rata_nilai_pertanian'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fekon',
                        data: {!! json_encode($chart_kriteria_5['rata_nilai_ekonomi'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'FKIP',
                        data: {!! json_encode($chart_kriteria_5['rata_nilai_fkip'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fakultas Fisipol',
                        data: {!! json_encode($chart_kriteria_5['rata_nilai_fisipol'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fakultas Psikologi',
                        data: {!! json_encode($chart_kriteria_5['rata_nilai_psikologi'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fak. Ilmu komunikasi',
                        data: {!! json_encode($chart_kriteria_5['rata_nilai_fikom'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Pascasarjana',
                        data: {!! json_encode($chart_kriteria_5['rata_nilai_pasca'])  !!},
                        /* pointPlacement: 'on' */
                    },

        ],
        colors: ['#0CBD62', '#FFC300', '#C70039',  '#4DE5D5', '#6AB8F7', '#A38CC6', '#FFAC5B', '#e3fc03', '#eb03fc', '#fc0356'],

	    });

	</script>
    <script>
		Highcharts.chart('instrumen_6', {
            chart: {
            polar: true
        },

        title: {
            text: 'Kriteria 6'
        },

        subtitle: {
            // text: 'Rata-rata Capaian Evaluasi Diri Fakultas'
        },

        pane: {
            startAngle: 0,
            endAngle: 360
        },

        xAxis: {
            categories: {!! json_encode($chart_kriteria_6['category'])  !!}

        },

        yAxis: {
            min: 0,
            // max: 4 ,
            tickInterval: 1,
        },

        tooltip: {
            shared: true,
            pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.1f}</b><br/>'
        },

        credits: {
            enabled: false
        },

        series: [
                    {
                        name: 'Fakultas Hukum',
                        data: {!! json_encode($chart_kriteria_6['rata_nilai_hukum'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fak. Agama Islam',
                        data: {!! json_encode($chart_kriteria_6['rata_nilai_fai'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fak. Teknik',
                        data: {!! json_encode($chart_kriteria_6['rata_nilai_teknik'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fak. Pertanian',
                        data: {!! json_encode($chart_kriteria_6['rata_nilai_pertanian'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fekon',
                        data: {!! json_encode($chart_kriteria_6['rata_nilai_ekonomi'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'FKIP',
                        data: {!! json_encode($chart_kriteria_6['rata_nilai_fkip'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fakultas Fisipol',
                        data: {!! json_encode($chart_kriteria_6['rata_nilai_fisipol'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fakultas Psikologi',
                        data: {!! json_encode($chart_kriteria_6['rata_nilai_psikologi'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fak. Ilmu komunikasi',
                        data: {!! json_encode($chart_kriteria_6['rata_nilai_fikom'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Pascasarjana',
                        data: {!! json_encode($chart_kriteria_6['rata_nilai_pasca'])  !!},
                        /* pointPlacement: 'on' */
                    },

        ],
        colors: ['#0CBD62', '#FFC300', '#C70039',  '#4DE5D5', '#6AB8F7', '#A38CC6', '#FFAC5B', '#e3fc03', '#eb03fc', '#fc0356'],

	    });

	</script>
    <script>
		Highcharts.chart('instrumen_7', {
            chart: {
            polar: true
        },

        title: {
            text: 'Kriteria 7'
        },

        subtitle: {
            // text: 'Rata-rata Capaian Evaluasi Diri Fakultas'
        },

        pane: {
            startAngle: 0,
            endAngle: 360
        },

        xAxis: {
            categories: {!! json_encode($chart_kriteria_7['category'])  !!},

        },

        yAxis: {
            min: 0,
            // max: 4 ,
            tickInterval: 1,
        },

        tooltip: {
            shared: true,
            pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.1f}</b><br/>'
        },

        credits: {
            enabled: false
        },

        series: [
                    {
                        name: 'Fakultas Hukum',
                        data: {!! json_encode($chart_kriteria_7['rata_nilai_hukum'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fak. Agama Islam',
                        data: {!! json_encode($chart_kriteria_7['rata_nilai_fai'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fak. Teknik',
                        data: {!! json_encode($chart_kriteria_7['rata_nilai_teknik'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fak. Pertanian',
                        data: {!! json_encode($chart_kriteria_7['rata_nilai_pertanian'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fekon',
                        data: {!! json_encode($chart_kriteria_7['rata_nilai_ekonomi'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'FKIP',
                        data: {!! json_encode($chart_kriteria_7['rata_nilai_fkip'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fakultas Fisipol',
                        data: {!! json_encode($chart_kriteria_7['rata_nilai_fisipol'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fakultas Psikologi',
                        data: {!! json_encode($chart_kriteria_7['rata_nilai_psikologi'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fak. Ilmu komunikasi',
                        data: {!! json_encode($chart_kriteria_7['rata_nilai_fikom'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Pascasarjana',
                        data: {!! json_encode($chart_kriteria_7['rata_nilai_pasca'])  !!},
                        /* pointPlacement: 'on' */
                    },

        ],
        colors: ['#0CBD62', '#FFC300', '#C70039',  '#4DE5D5', '#6AB8F7', '#A38CC6', '#FFAC5B', '#e3fc03', '#eb03fc', '#fc0356'],

	    });

	</script>
    <script>
		Highcharts.chart('instrumen_8', {
            chart: {
            polar: true
        },

        title: {
            text: 'Kriteria 8'
        },

        subtitle: {
            // text: 'Rata-rata Capaian Evaluasi Diri Fakultas'
        },

        pane: {
            startAngle: 0,
            endAngle: 360
        },

        xAxis: {
            categories: {!! json_encode($chart_kriteria_8['category'])  !!},

        },

        yAxis: {
            min: 0,
            // max: 4 ,
            tickInterval: 1,
        },

        tooltip: {
            shared: true,
            pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.1f}</b><br/>'
        },

        credits: {
            enabled: false
        },

        series: [
                    {
                        name: 'Fakultas Hukum',
                        data: {!! json_encode($chart_kriteria_8['rata_nilai_hukum'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fak. Agama Islam',
                        data: {!! json_encode($chart_kriteria_8['rata_nilai_fai'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fak. Teknik',
                        data: {!! json_encode($chart_kriteria_8['rata_nilai_teknik'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fak. Pertanian',
                        data: {!! json_encode($chart_kriteria_8['rata_nilai_pertanian'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fekon',
                        data: {!! json_encode($chart_kriteria_8['rata_nilai_ekonomi'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'FKIP',
                        data: {!! json_encode($chart_kriteria_8['rata_nilai_fkip'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fakultas Fisipol',
                        data: {!! json_encode($chart_kriteria_8['rata_nilai_fisipol'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fakultas Psikologi',
                        data: {!! json_encode($chart_kriteria_8['rata_nilai_psikologi'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fak. Ilmu komunikasi',
                        data: {!! json_encode($chart_kriteria_8['rata_nilai_fikom'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Pascasarjana',
                        data: {!! json_encode($chart_kriteria_8['rata_nilai_pasca'])  !!},
                        /* pointPlacement: 'on' */
                    },

        ],
        colors: ['#0CBD62', '#FFC300', '#C70039',  '#4DE5D5', '#6AB8F7', '#A38CC6', '#FFAC5B', '#e3fc03', '#eb03fc', '#fc0356'],

	    });

	</script>
    <script>
		Highcharts.chart('instrumen_9', {
            chart: {
            polar: true
        },

        title: {
            text: 'Kriteria 9'
        },

        subtitle: {
            // text: 'Rata-rata Capaian Evaluasi Diri Fakultas'
        },

        pane: {
            startAngle: 0,
            endAngle: 360
        },

        xAxis: {
            categories: {!! json_encode($chart_kriteria_9['category'])  !!}

        },

        yAxis: {
            min: 0,
            // max: 4 ,
            tickInterval: 1,
        },

        tooltip: {
            shared: true,
            pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.1f}</b><br/>'
        },

        credits: {
            enabled: false
        },
        series: [
                    {
                        name: 'Fakultas Hukum',
                        data: {!! json_encode($chart_kriteria_9['rata_nilai_hukum'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fak. Agama Islam',
                        data: {!! json_encode($chart_kriteria_9['rata_nilai_fai'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fak. Teknik',
                        data: {!! json_encode($chart_kriteria_9['rata_nilai_teknik'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fak. Pertanian',
                        data: {!! json_encode($chart_kriteria_9['rata_nilai_pertanian'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fekon',
                        data: {!! json_encode($chart_kriteria_9['rata_nilai_ekonomi'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'FKIP',
                        data: {!! json_encode($chart_kriteria_9['rata_nilai_fkip'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fakultas Fisipol',
                        data: {!! json_encode($chart_kriteria_9['rata_nilai_fisipol'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fakultas Psikologi',
                        data: {!! json_encode($chart_kriteria_9['rata_nilai_psikologi'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Fak. Ilmu komunikasi',
                        data: {!! json_encode($chart_kriteria_9['rata_nilai_fikom'])  !!},
                        /* pointPlacement: 'on' */
                    },
                    {
                        name: 'Pascasarjana',
                        data: {!! json_encode($chart_kriteria_9['rata_nilai_pasca'])  !!},
                        /* pointPlacement: 'on' */
                    },

        ],
        colors: ['#0CBD62', '#FFC300', '#C70039',  '#4DE5D5', '#6AB8F7', '#A38CC6', '#FFAC5B', '#e3fc03', '#eb03fc', '#fc0356'],

	    });

	</script>
