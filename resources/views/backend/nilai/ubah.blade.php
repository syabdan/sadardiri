{!! Form::open(array('id' => 'frmAdan-ubah', 'class' => 'form account-form', 'method' => 'PUT')) !!}
{{ csrf_field() }}
		<div class="form-group row">
		{!! Form::label('Nilai', 'Nilai', array('class' => 'col-md-4 control-label')) !!}
			<div class="col-md-8">
			{!! Form::text('nilai', $data->nilai, array('id' => 'nilai', 'class' => 'form-control', 'size' => 16, 'placeholder' => 'eg. 4')) !!}
			</div>
		</div>
		
		<div class="form-group row">
		{!! Form::label('Range Awal', 'Range Awal', array('class' => 'col-md-4 control-label')) !!}
			<div class="col-md-8">
			{!! Form::text('awal', $data->awal, array('id' => 'awal', 'class' => 'form-control', 'size' => 16, 'placeholder' => 'eg. 26')) !!}
			</div>
		</div>
		
		<div class="form-group row">
		{!! Form::label('Range Akhir', 'Range Akhir', array('class' => 'col-md-4 control-label')) !!}
			<div class="col-md-8">
			{!! Form::text('akhir', $data->akhir, array('id' => 'akhir', 'class' => 'form-control', 'size' => 16, 'placeholder' => 'eg. 50')) !!}
			</div>
		</div>

	{!! Form::hidden('id', $data->id, array('id' => 'id')) !!}
	{!! Form::hidden('url', URL::previous(), array('id' => 'url')) !!}
</div>
<div class="row">
		<div class="col-md-12">
			<span class="pesan"></span>
		</div>
</div>
{!! Form::close() !!}
<script src="{{ URL::asset('resources/vendor/jquery/jquery.enc.js') }}"></script>
<script src="{{ URL::asset('adan/nilai/ajax.js') }}"></script>
<script src="{{ URL::asset('adan/includes/ajax.js') }}"></script>

<script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js" ></script>
<script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js" ></script>