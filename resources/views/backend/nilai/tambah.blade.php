{!! Form::open(array('id' => 'frmAdan', 'class' => 'form account-form', 'method' => 'post', 'files' => true)) !!}

		
		<div class="form-group row">
		{!! Form::label('Nilai', 'Nilai', array('class' => 'col-md-4 control-label')) !!}
			<div class="col-md-8">
			{!! Form::text('nilai', NULL, array('id' => 'nilai', 'class' => 'form-control', 'size' => 16, 'placeholder' => 'eg. 4')) !!}
			</div>
		</div>
		
		<div class="form-group row">
		{!! Form::label('Range Awal', 'Range Awal', array('class' => 'col-md-4 control-label')) !!}
			<div class="col-md-8">
			{!! Form::text('awal', NULL, array('id' => 'awal', 'class' => 'form-control', 'size' => 16, 'placeholder' => 'eg. 26')) !!}
			</div>
		</div>
		
		<div class="form-group row">
		{!! Form::label('Range Akhir', 'Range Akhir', array('class' => 'col-md-4 control-label')) !!}
			<div class="col-md-8">
			{!! Form::text('akhir', NULL, array('id' => 'akhir', 'class' => 'form-control', 'size' => 16, 'placeholder' => 'eg. 50')) !!}
			</div>
		</div>

        
	{!! Form::hidden('url', URL::previous(), array('id' => 'url')) !!}

<div class="row">
	<div class="col-md-12">
		<span class="pesan"></span>
	</div>
</div>
{!! Form::close() !!}
<script src="{{ URL::asset('resources/vendor/jquery/jquery.enc.js') }}"></script>
<script src="{{ URL::asset('adan/nilai/ajax.js') }}"></script>
<script src="{{ URL::asset('adan/includes/ajax.js') }}"></script>

<script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js" ></script>
<script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js" ></script>
