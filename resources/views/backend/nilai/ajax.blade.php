$(document).on("click",".submit-tambah",function() {
	var dataString			= $('#frmAdan').serializeArray();
	var url					= "{{ url($url_admin.'/nilai') }}";
	goAjax(url, dataString);
});

$( ".submit-ubah" ).click(function() {
	var dataString			= $("#frmAdan-ubah").serializeArray();
	var url					= "{{ url($url_admin.'/nilai') }}/"+$("#id").val();
	goAjax(url, dataString);
});
$( ".submit-hapus" ).click(function() {
	var url					= "{{ url($url_admin.'/nilai') }}/"+$("#id").val();
	var dataString			= $('#frmAdan').serializeArray();
	goAjax(url, dataString);
});

