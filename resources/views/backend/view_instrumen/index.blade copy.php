@extends('backend.layout')
@section('style')
    <!-- data tables -->
    <link href="{{ URL::to('backend') }}/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::to('resources') }}/vendor/datatables/css/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
    <!--select2-->
    <link href="{{ URL::to('backend') }}/assets/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="{{ URL::to('backend') }}/assets/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">{{$halaman->nama}}</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">{{$halaman->nama}}</li>
                            </ol>
                        </div>
					</div>

                     <!-- start Payment Details -->
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="card  card-box">
                                <div class="card-body ">
									<div class="row p-b-20">
                                        <div class="col-md-12 col-sm-12 col-12">
                                            @if(\Auth::user()->permissions_id != 5)
                                            <div class="form-group">
                                                <select name="unit" id="unit" class="form-control select2" style="width:100%">
                                                    <option value="">Pilih Fakultas</option>
                                                    <!-- <option value="all">Semua Fakultas</option> -->
                                                    @foreach($units as $unit)
                                                        <option value="{{ $unit->id }}">{{ $unit->nama_unit }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                    <h3>Data Instrumen {{ $user->nama_user ?? ''}}</h3>

                                    <div class="table-wrap">
										<div class="table-responsive tblHeightSet small-slimscroll-style">
											<table class="table table-striped table-bordered" id="table_per_unit" style="width:100%">
												<thead>
													<tr>
														<th>No</th>
														<th width="45%">Pertanyaan</th>
														<th width="45%">Jawaban</th>
														<th>Nilai</th>
														<th>Dokumen</th>
														<!-- <th>Persentase (%)</th> -->
													</tr>
                                                </thead>
                                                @if(count($datas) > 0)

												<tbody>

                                                @php($no = 1)
                                                @foreach($datas as $data)
                                                    @if($data->parent_id == NULL && $data->as_parent == NULL)
                                                        <tr>
                                                            <td>{{$no}}</td>
                                                            <td><div style='border:1px solid #ccc; padding:10px; max-height:100px;overflow:scroll;'>{!! strip_tags($data->isi_pertanyaan) ?? '' !!}</div></td>
                                                            <td><div style='border:1px solid #ccc; padding:10px; max-height:100px;overflow:scroll;'>{!! strip_tags($data->jawaban) ?? '' !!}</div></td>
                                                            <td>{{$data->nilai ? $data->nilai : 'Belum ada nilai'}}</td>
                                                            <td>
                                                            @if($data->file_data_diri)
                                                                <a href="{{ url('penilaian-instrumen/ambil_file/'.$data->file_data_diri)}}" target="_blank" class="btn btn-info btn-tbl-edit btn-xs"><i class="fa fa-download"></i> Unduh</a>
                                                            @endif
                                                            </td>
                                                            {{-- <td>{{number_format($data->persentase,2,',','.')}}</td> --}}
                                                            @php($no++)
                                                        </tr>
                                                    @endif
                                                @endforeach
                                                @foreach($dataku as $d)
                                                        <tr>
                                                            <td colspan="5" style="color:blue; font-weight:bold">{!! strip_tags($d->isi_pertanyaan) ?? '' !!}</td>
                                                        </tr>
                                                        @php($no=1)
                                                        @foreach($datas as $data)

                                                            @if($data->parent_id == $d->id && $data->as_parent == NULL)
                                                                <tr>
                                                                    <td>{{$no}}</td>
                                                                    <td><div style='border:1px solid #ccc; padding:10px; max-height:100px;overflow:scroll;'>{!! strip_tags($data->isi_pertanyaan) ?? '' !!}</div></td>
                                                                    <td><div style='border:1px solid #ccc; padding:10px; max-height:100px;overflow:scroll;'>{!! strip_tags($data->jawaban) ?? '' !!}</div></td>
                                                                    <td>{{$data->nilai ? $data->nilai : 'Belum ada nilai'}}</td>
                                                                    <td>
                                                                    @if($data->file_data_diri)
                                                                        <a href="{{ url('penilaian-instrumen/ambil_file/'.$data->file_data_diri)}}" target="_blank" class="btn btn-info btn-tbl-edit btn-xs"><i class="fa fa-download"></i> Unduh</a>
                                                                    @endif
                                                                    </td>
                                                                    {{-- <td>{{number_format($data->persentase,2,',','.')}}</td> --}}
                                                                    @php($no++)
                                                                </tr>
                                                            @endif
                                                        @endforeach
                                                @endforeach
                                                </tbody>
                                                @else
                                                    <tbody>
                                                        <tr >
                                                            <td colspan="5" align="center">
                                                                <h1 >Tidak Ada Data</h1>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                @endif
											</table>
										</div>
									</div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        <!-- end page content -->

    </div>
@endsection

@section('script')
    <!-- data tables -->
    <script src="{{ URL::to('backend') }}/assets/plugins/datatables/jquery.dataTables.min.js" ></script>
    <script src="{{ URL::to('backend') }}/assets/plugins/datatables/editabletable.js" ></script>
	<script src="{{ URL::to('backend') }}/assets/js/pages/table/editable_table_data.js" ></script>
 	<script src="{{ URL::to('backend') }}/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
	<script src="{{ URL::to('backend') }}/assets/js/pages/table/table_data.js" ></script>
    <script src="{{ URL::to('resources') }}/vendor/datatables/js/dataTables.responsive.min.js" ></script>
    <script src="{{ URL::to('resources') }}/vendor/datatables/js/responsive.bootstrap4.min.js" ></script>

    <!-- <script src="{{ URL::to('/') }}/adan/indikator_kinerja/jquery.js"></script> -->
    <!--select2-->
    <script src="{{ URL::to('backend') }}/assets/plugins/select2/js/select2.js" ></script>
    <script>
    $('select2').select2();

    // var table;
    // $(document).ready( function () {
    //         $.ajaxSetup({
    //         headers: {
    //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //         }
    //         });
    //     table = $('.table').DataTable({
    //         processing: true,
    //         serverSide: true,
    //         ajax: {
    //         url: "{!! url('view-indikator-kinerja/data/all') !!}",
    //         // type: 'POST'
    //         },
    //         columns: [
    //             { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false,searchable: false},
    //             { data: 'isi_indikator_kinerja'},
    //             { data: 'target'},
    //             { data: 'capaianmid'},
    //             { data: 'capaian'},
    //             { data: 'nilai'},
    //             { data: 'persentase'},
    //         ]
    //     });
    //     table.on( 'draw', function () {
    //         $('.livicon').each(function(){
    //             $(this).updateLivicon();
    //         });
    //     } );
    // });
    </script>


    <script>
    $("#unit").change(function() {

        var id_unit = $("#unit").val();

		location.href = "{!! url('view-instrumen/"+id_unit+"') !!}";

    });
    </script>
@endsection
