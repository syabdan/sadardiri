@extends('backend.layout')
@section('style')
    <!-- data tables -->
    <link href="{{ URL::to('backend') }}/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::to('resources') }}/vendor/datatables/css/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
    <!--select2-->
    <link href="{{ URL::to('backend') }}/assets/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="{{ URL::to('backend') }}/assets/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">{{$halaman->nama}}</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">{{$halaman->nama}}</li>
                            </ol>
                        </div>
					</div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-12">
                            <div class="form-group">
                                <select name="periode" id="periode" class="form-control select2 periode" style="width:100%">
                                    <option value="">Pilih Periode</option>
                                    <!-- <option value="all">Semua Fakultas</option> -->
                                    @foreach($periodes as $periode)
                                        <option value="{{ $periode->id }}">{{ $periode->nama_periode }}</option>
                                    @endforeach
                                </select>
                            </div>

                            @if(\Auth::user()->permissions_id == 1 || \Auth::user()->permissions_id == 2)
                            <div class="form-group" id="form-unit" style="display: none">
                                <select name="unit" id="unit" class="form-control select2 unit" style="width:100%;">
                                    <option value="">Pilih Fakultas</option>
                                    <!-- <option value="all">Semua Fakultas</option> -->
                                    @foreach($units as $unit)
                                        <option value="{{ $unit->id }}">{{ $unit->nama_unit }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <h3>Data Instrumen Evaluasi Diri {{ $fakultas->nama_unit ?? ''}}</h3>


                            @endif
                            {{-- Set id_unit untuk UPM --}}
                            {{--  @php
                                $id_unit = \Auth::user()->unit_id;
                            @endphp  --}}
                            {{-- #Set id_unit untuk UPM --}}

                            <div id="cetak"  style="display: {{ $id_unit ? 'show' : 'none' }}">
                                <form action="{{ url('cetak/instrumen')}}" method="post" target="_blank" >
                                    @csrf
                                    <input type="hidden" name="unit" class="unit" value="{{ $id_unit }}">
                                    <input type="hidden" name="periode" class="periode" value=" {{ $periode_id }}">
                                    <button type="submit" class="fa fa-print btn-success btn-lg pull-right" style="cursor: pointer;"> Cetak</button>
                                </form>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12">
                            <div class="card card-box">
                                <div class="card-body " id="bar-parent">
                                    <div class="row">
                                        <div class="col-md-2 col-sm-2 col-2">
                                            <ul class="nav nav-tabs tabs-left">
                                                <?php $no=1 ?>
                                                @foreach($instrumens as $instrumen)
                                                <li class="nav-item">
                                                    <a href="#tab_{{ $instrumen->id }}" data-toggle="tab" class="{{ $no == 1 ? 'active' : ''}}"> {{ $instrumen->nama_instrumen }} </a>
                                                </li>
                                                <?php $no++ ?>
                                                @endforeach
                                            </ul>
                                        </div>
                                        <div class="col-md-10 col-sm-10 col-10">
                                            <div class="tab-content">
                                            <?php $no=1 ?>
                                                @foreach($instrumens as $instrumen)
                                                <div class="tab-pane {{ $no == 1 ? 'active' : 'fade'}}" id="tab_{{$instrumen->id}}">
                                                   @include('backend.penilaian_instrumen.kriteria',['id' => $instrumen->id])
                                                </div>
                                                <?php $no++ ?>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        <!-- end page content -->

    </div>
@endsection

@section('script')
    <!-- data tables -->
    <script src="{{ URL::to('backend') }}/assets/plugins/datatables/jquery.dataTables.min.js" ></script>
    <script src="{{ URL::to('backend') }}/assets/plugins/datatables/editabletable.js" ></script>
	<script src="{{ URL::to('backend') }}/assets/js/pages/table/editable_table_data.js" ></script>
 	<script src="{{ URL::to('backend') }}/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
	<script src="{{ URL::to('backend') }}/assets/js/pages/table/table_data.js" ></script>
    <script src="{{ URL::to('resources') }}/vendor/datatables/js/dataTables.responsive.min.js" ></script>
    <script src="{{ URL::to('resources') }}/vendor/datatables/js/responsive.bootstrap4.min.js" ></script>

    <script src="{{ URL::to('/') }}/adan/penilaian_instrumen/jquery.js"></script>
    <!--select2-->
    <script src="{{ URL::to('backend') }}/assets/plugins/select2/js/select2.js" ></script>
    <script>
    $('.select2').select2();

    $(".unit").change(function() {

        var id_unit = $(".unit").val();
        var periode = $(".periode").val();
        $('.periode').val(periode);
        $('.unit').val(id_unit);

		location.href = "{!! url('view-instrumen/"+id_unit+"/"+periode+"') !!}";

    });

    $(".periode").change(function() {
        var periode = $(".periode").val();
        $('.periode').val(periode);
        @if(\Auth::user()->permissions_id != 5)
            $("#form-unit").show(1000);
        @else
		    location.href = "{!! url('view-instrumen/"+null+"/"+periode+"') !!}";
        @endif
    });

    </script>

@endsection
