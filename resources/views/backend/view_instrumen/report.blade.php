<style>
    table {
        border-collapse: collapse;
    }

    .center-dinas  { text-align: center;
            font-size: 130%;
            font-weight: bold;
       }

       .center-pekanbaru  {
            text-align: center;
            font-size: 110%;
            font-weight: bold;
            padding-bottom: 0;
       }
</style>
<table width="100%" border="0" style="border-bottom:3px solid #000;tr:margin-top:-25px">
    <tbody>
      <tr>
        <td rowspan="3" width="80"><center><img src="{{ asset('backend/assets/img/uir.png') }}" width="80%"></center></td>
        <td class="center-dinas">SISTEM DARING EVALUASI DIRI</td>
      </tr>
      <tr>
        <td class="center-dinas">UNIVERSITAS ISLAM RIAU</td>
      </tr>
      <tr style="margin-bottom: -10;">
        <td class="center-pekanbaru">Jl. Kaharudin Nasution No. 113 Pekanbaru</td>
      </tr>
    </tbody>
</table>
  <br>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-12">

        </div>
        <div class="col-md-12 col-sm-12">
            <div class="card card-box">
                <div class="card-body " id="bar-parent">
                <center><h3>Laporan Instrumen Evaluasi Diri {{ $fakultas->nama_unit ?? ''}}</h3></center>


                    <div class="row">

                        <div class="col-md-10 col-sm-10 col-10">
                            <div class="tab-content">
                            <?php $no=1 ?>
                                @foreach($instrumens as $instrumen)
                                <h4>{{ $instrumen->nama_instrumen }} : {{ $instrumen->keterangan }}  </h4>
                                <div class="table-wrap">
                                    <div class="table-responsive tblHeightSet small-slimscroll-style">
                                        <table style="width:100%; border-collapse: collapse;" border="0.01">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Instrumen</th>
                                                    <th>Jawaban</th>
                                                    <th>Nilai</th>
                                                    <th>Nilai Auditor</th>
                                                    <th>Catatan Auditor</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                    $no = 1;
                                                ?>
                                                @foreach($datas->where('instrumen_id', $instrumen->id) as $data)
                                                    <tr >
                                                        @if($data->as_parent == NULL)
                                                            <td style="text-align: center;">{{ $no.'.' }}</td>
                                                            <td><div style='#ccc; padding:5px;'>{!! strip_tags($data->isi_pertanyaan) ?? '' !!}</div></td>
                                                            <td><div style='#ccc; padding:5px;'>{!! strip_tags($data->jawaban) ?? '' !!}</div></td>

                                                            <td style="text-align: center;">{{ $data->nilai_id ?? ''}}</td>
                                                            <td style="text-align: center;">{{ $data->nilai_asessor_id ?? ''}}</td>
                                                            <td style="text-align: center;">{!! strip_tags($data->catatan ?? '') !!}</td>

                                                        <?php $no++; ?>
                                                        @else
                                                        <?php $no = 1;?>
                                                            <td colspan='2' style="color:blue; font-weight:bold">{!! strip_tags($data->isi_pertanyaan) ?? '' !!}</td>
                                                            <td colspan="4"></td>
                                                        @endif
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <br>
                                @foreach ($narasis->where('periode_id', $periode_id)->where('unit_id', $fakultas->id)->where('instrumen_id', $instrumen->id)->where('publish', 1) as $narasi)

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="card-box">
                                            <div class="col-lg-4">
                                                <label style="font-size: 16px; font-weight: bold; ">Narasi Strategi</label>
                                            </div>
                                            <table style="border: 1px solid; margin-bottom: 10px; width:100%">
                                                <tr>
                                                    <td style="padding: 10px 5px 10px 5px;">
                                                        {!! strip_tags($narasi->narasi_1 ?? '') !!}
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="card-box">
                                            <div class="col-lg-4">
                                                <label style="font-size: 16px; font-weight: bold; ">Narasi Mekanisme</label>
                                            </div>
                                            <table style="border: 1px solid; margin-bottom: 10px; width:100%">
                                                <tr>
                                                    <td style="padding: 10px 5px 10px 5px;">
                                                        {!! strip_tags($narasi->narasi_2 ?? '') !!}
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="card-box">
                                            <div class="col-lg-4">
                                                <label style="font-size: 16px; font-weight: bold; ">Narasi Praktik Baik</label>
                                            </div>
                                            <table style="border: 1px solid; margin-bottom: 10px; width:100%">
                                                <tr>
                                                    <td style="padding: 10px 5px 10px 5px;">
                                                        {!! strip_tags($narasi->narasi_3 ?? '') !!}
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="card-box">
                                            <div class="col-lg-4">
                                                <label style="font-size: 16px; font-weight: bold; ">Narasi Hambatan</label>
                                            </div>
                                            <table style="border: 1px solid; margin-bottom: 10px; width:100%">
                                                <tr>
                                                    <td style="padding: 10px 5px 10px 5px;">
                                                        {!! strip_tags($narasi->narasi_4 ?? '') !!}
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="card-box">
                                            <div class="col-lg-4">
                                                <label style="font-size: 16px; font-weight: bold; ">Narasi Luaran</label>
                                            </div>
                                            <table style="border: 1px solid; margin-bottom: 10px; width:100%">
                                                <tr>
                                                    <td style="padding: 10px 5px 10px 5px;">
                                                        {!! strip_tags($narasi->narasi_5 ?? '') !!}
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                @endforeach

                                <?php $no++ ?>
                                @endforeach

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <table style="width: 100%; text-align:center; margin-top:50px">
        <tr style="margin-bottom: 50px">
            <td></td>
            <td >Pekanbaru, {{ tanggal_indonesia( date('Y-m-d'), false ) }}</td>
        </tr>
        <tr>
            <td style="width: 50%">Auditor</td>
            <td style="width: 50%">Dekan {{ $fakultas->nama_unit ?? ''}}</td>
        </tr>
        <tr>
            <td colspan="2">
                <br><br><br><br><br><br>
            </td>
        </tr>
        <tr style="font-weight: 600">
            <td>(......................................................)</td>
            <td>{{ $pimpinan->nama_pimpinan }} </td>
        </tr>
    </table>
