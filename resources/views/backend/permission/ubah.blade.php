{!! Form::open(array('id' => 'frmAdan-ubah', 'class' => 'form account-form', 'method' => 'PUT')) !!}
{{ csrf_field() }}
	<div class="form-group row">
	{!! Form::label('Nama Permission', 'Nama Permission', array('class' => 'col-md-4 control-label')) !!}
		<div class="col-md-8">
		{!! Form::text('permission_name', $data->permission_name, array('id' => 'permission_name', 'class' => 'form-control', 'size' => 16, 'placeholder' => 'eg. Root')) !!}
		</div>
	</div>
	
	<div class="form-group row">
		<label class="col-md-4 control-label">Menu</label>
		<div class="col-md-8">
			<input type="hidden" value="{{ $data->data_menus}}" id="data_menu">
			<select class="form-control select2-multiple" id="menu" name="menu[]" multiple style="width:100%" >
			
					<option value="">Pilih Menu</option>
					@foreach($menus as $menu)
						<option value="{{ $menu->id}}">{{ $menu->nama }}</option>
					@endforeach
			</select>
		</div>
	</div>

	{!! Form::hidden('id', $data->id, array('id' => 'id')) !!}
	{!! Form::hidden('url', URL::previous(), array('id' => 'url')) !!}

<div class="row">
		<div class="col-md-12">
			<span class="pesan"></span>
		</div>
</div>
{!! Form::close() !!}
<script src="{{ URL::asset('resources/vendor/jquery/jquery.enc.js') }}"></script>
<script src="{{ URL::asset('adan/permission/ajax.js') }}"></script>
<script src="{{ URL::asset('adan/includes/ajax.js') }}"></script>

<script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js" ></script>
<script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js" ></script>
<script src="{{ URL::to('backend') }}/assets/plugins/select2/js/select2.js" ></script>

<script>
	$('.select2, .select2-multiple').select2({
		theme: "bootstrap"
	});

	$(document).ready(function(){
		
		var values= $('#data_menu').val();
		$.each(values.split(", "), function(i,e){
			$("#menu option[value='" + e + "']").prop("selected", true).trigger('change');
		});
			
	});
</script>