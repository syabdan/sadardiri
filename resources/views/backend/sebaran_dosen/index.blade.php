@extends('backend.layout')
@section('style')
    <!-- data tables -->
    <link href="{{ URL::to('backend') }}/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::to('resources') }}/vendor/datatables/css/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
    <!--select2-->
    <link href="{{ URL::to('backend') }}/assets/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="{{ URL::to('backend') }}/assets/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">{{$halaman->nama}}</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">{{$halaman->nama}}</li>
                            </ol>
                        </div>
					</div>

                     <!-- start Payment Details -->
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="card  card-box">
                                <div class="card-head">
                                    <header>Data {{$halaman->nama}}</header>
                                    <div class="tools">
                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
	                                    <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
	                                    <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                    </div>
                                </div>
                                <div class="card-body ">
									<div class="row p-b-20">
                                        <div class="col-md-6 col-sm-6 col-6">
                                            <div class="btn-group">
                                            @if(\Auth::user()->permissions_id == 1)
                                                <a class="btn btn-info tambah">Tambah <i class="fa fa-plus"></i></a>
                                            @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="table-wrap">
										<div class="table-responsive tblHeightSet small-slimscroll-style">
											<table class="table table-striped table-bordered dt-responsive nowrap" id="table" style="width:100%">
												<thead>
													<tr>
														<th rowspan="2">No</th>
														<th rowspan="2">Program Studi</th>
														<th rowspan="2">Jenjang Pendidikan</th>
														<th colspan="4">Magister (S2)</th>
                                                        <th colspan="5">Doktor (S3)</th>
                                                        <th rowspan="2">Jumlah</th>
                                                        @if(\Auth::user()->permissions_id == 1)
                                                        <th rowspan="2">Aksi</th>
                                                        @endif
                                                    </tr>
                                                    <tr>
														<th>Non Fungsional</th>
														<th>Asisten Ahli</th>
														<th>Lektor</th>
														<th>Lektor Kepala</th>
														<th>Non Fungsional</th>
														<th>Asisten Ahli</th>
														<th>Lektor</th>
														<th>Lektor Kepala</th>
                                                        <th>Guru Besar</th>
                                                    </tr>

												</thead>
												<tbody>
                                                    @php
                                                        $no = 1;
                                                        $total_non_fungsional_s2 = 0;
                                                        $total_asisten_ahli_s2 = 0;
                                                        $total_lektor_s2 = 0;
                                                        $total_lektor_kepala_s2 = 0;
                                                        $total_non_fungsional_s3 = 0;
                                                        $total_asisten_ahli_s3 = 0;
                                                        $total_lektor_s3 = 0;
                                                        $total_lektor_kepala_s3 = 0;
                                                        $total_guru_besar_s3 = 0;
                                                        $total_jumlah = 0;
                                                    @endphp
                                                    @foreach ($datas as $data)
                                                    <tr>
                                                        <td>{{ $no }}</td>
                                                        @if(\Auth::user()->permissions_id == 1 || \Auth::user()->permissions_id == 2)
                                                        <td>{{ $data->unit->nama_unit ?? ''}}</td>
                                                        <td>{{ $data->unit->jenjang_pendidikan ?? ''}}</td>
                                                        @endif
                                                        @if(\Auth::user()->permissions_id == 3 || \Auth::user()->permissions_id == 4 || \Auth::user()->permissions_id == 5)
                                                        <td>{{ $data->nama_unit ?? ''}}</td>
                                                        <td>{{ $data->jenjang_pendidikan ?? ''}}</td>
                                                        @endif
                                                        <td>{{ $data->non_fungsional_s2 ?? 0}}</td>
                                                        <td>{{ $data->asisten_ahli_s2 ?? 0}}</td>
                                                        <td>{{ $data->lektor_s2 ?? 0}}</td>
                                                        <td>{{ $data->lektor_kepala_s2 ?? 0}}</td>
                                                        <td>{{ $data->non_fungsional_s3 ?? 0}}</td>
                                                        <td>{{ $data->asisten_ahli_s3 ?? 0}}</td>
                                                        <td>{{ $data->lektor_s3 ?? 0}}</td>
                                                        <td>{{ $data->lektor_kepala_s3 ?? 0}}</td>
                                                        <td>{{ $data->guru_besar_s3 ?? 0}}</td>
                                                        <td>{{ $data->jumlah ?? 0}}</td>
                                                        @if(\Auth::user()->permissions_id == 1)
                                                        <td>
                                                            <a data-id="{{ $data->id }}"  class="btn btn-tbl-edit btn-xs ubah"><i class="fa fa-pencil"></i></a>
                                                            <a  data-id="{{ $data->id }}" class="btn btn-tbl-delete btn-xs hapus"><i class="fa fa-trash-o"></i></a>
                                                        </td>
                                                        @endif
                                                    </tr>
                                                        @php
                                                            $no++;
                                                            $total_non_fungsional_s2 = $total_non_fungsional_s2 + $data->non_fungsional_s2;
                                                            $total_asisten_ahli_s2 = $total_asisten_ahli_s2 + $data->asisten_ahli_s2;
                                                            $total_lektor_s2 = $total_lektor_s2 + $data->lektor_s2;
                                                            $total_lektor_kepala_s2 = $total_lektor_kepala_s2 + $data->lektor_kepala_s2;
                                                            $total_non_fungsional_s3 = $total_non_fungsional_s3 + $data->non_fungsional_s3;
                                                            $total_asisten_ahli_s3 = $total_asisten_ahli_s3 + $data->asisten_ahli_s3;
                                                            $total_lektor_s3 = $total_lektor_s3 + $data->lektor_s3;
                                                            $total_lektor_kepala_s3 = $total_lektor_kepala_s3 + $data->lektor_kepala_s3;
                                                            $total_guru_besar_s3 = $total_guru_besar_s3 + $data->guru_besar_s3;
                                                            $total_jumlah = $total_jumlah + $data->jumlah;
                                                        @endphp
                                                    @endforeach
                                                    @if(\Auth::user()->permissions_id == 4 || \Auth::user()->permissions_id == 5)

                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <th >Total</th>
                                                            <td >{{ $total_non_fungsional_s2 ?? 0}}</td>
                                                            <td >{{ $total_asisten_ahli_s2 ?? 0}}</td>
                                                            <td >{{ $total_lektor_s2 ?? 0}}</td>
                                                            <td >{{ $total_lektor_kepala_s2 ?? 0}}</td>
                                                            <td >{{ $total_non_fungsional_s3 ?? 0}}</td>
                                                            <td >{{ $total_asisten_ahli_s3 ?? 0}}</td>
                                                            <td >{{ $total_lektor_s3 ?? 0}}</td>
                                                            <td >{{ $total_lektor_kepala_s3 ?? 0}}</td>
                                                            <td >{{ $total_guru_besar_s3 ?? 0}}</td>
                                                            <td >{{ $total_jumlah ?? 0}}</td>

                                                        </tr>
                                                        {{--  <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <th >Presentase</th>
                                                            <td >{{ number_format($total_non_fungsional_s2 ?? 0 / $total_jumlah * 100, 2) }} %</td>
                                                            <td >{{ number_format($total_asisten_ahli_s2 ?? 0 / $total_jumlah * 100, 2)}} %</td>
                                                            <td >{{ number_format($total_lektor_s2 ?? 0 / $total_jumlah * 100, 2)}} %</td>
                                                            <td >{{ number_format($total_lektor_kepala_s2 ?? 0 / $total_jumlah * 100, 2)}} %</td>
                                                            <td >{{ number_format($total_non_fungsional_s3 ?? 0 / $total_jumlah* 100, 2) }} %</td>
                                                            <td >{{ number_format($total_asisten_ahli_s3 ?? 0 / $total_jumlah * 100, 2)}} %</td>
                                                            <td >{{ number_format($total_lektor_s3 ?? 0 / $total_jumlah * 100, 2)}} %</td>
                                                            <td >{{ number_format($total_lektor_kepala_s3 ?? 0 / $total_jumlah * 100, 2) }} %</td>
                                                            <td >{{ number_format($total_guru_besar_s3 ?? 0 / $total_jumlah * 100, 2)}} %</td>
                                                            <td></td>
                                                        </tr>  --}}

                                                    @endif
												</tbody>
                                            </table>
                                            @if(\Auth::user()->permissions_id == 4 ||\Auth::user()->permissions_id == 5)

                                            <span>Persentase Jabatan Fungsional</span><br>
                                            <p>
                                                a. Non Fungsional =<b> {{ number_format(($total_non_fungsional_s2 + $total_non_fungsional_s3)  / $total_jumlah * 100, 2) }} % </b><br>
                                                b. Asisten Ahli =<b> {{ number_format(($total_asisten_ahli_s2 + $total_asisten_ahli_s3)  / $total_jumlah * 100, 2) }} % </b><br>
                                                c. Lektor =<b> {{ number_format(($total_lektor_s2 + $total_lektor_s3)  / $total_jumlah * 100, 2) }} % </b><br>
                                                d. Lektor Kepala =<b> {{ number_format(($total_lektor_kepala_s2 + $total_lektor_kepala_s3) / $total_jumlah * 100, 2) }} % </b><br>
                                                e. Guru Besar =<b> {{ number_format($total_guru_besar_s3 / $total_jumlah * 100, 2) }} % </b><br>
                                            </p>

                                            @endif

										</div>
									</div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        <!-- end page content -->

    </div>
@endsection

@section('script')
    <!-- data tables -->
    <script src="{{ URL::to('backend') }}/assets/plugins/datatables/jquery.dataTables.min.js" ></script>
    <script src="{{ URL::to('backend') }}/assets/plugins/datatables/editabletable.js" ></script>
	<script src="{{ URL::to('backend') }}/assets/js/pages/table/editable_table_data.js" ></script>
 	<script src="{{ URL::to('backend') }}/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
	<script src="{{ URL::to('backend') }}/assets/js/pages/table/table_data.js" ></script>
    <script src="{{ URL::to('resources') }}/vendor/datatables/js/dataTables.responsive.min.js" ></script>
    <script src="{{ URL::to('resources') }}/vendor/datatables/js/responsive.bootstrap4.min.js" ></script>

    <script src="{{ URL::to('/') }}/adan/sebaran_dosen/jquery.js"></script>

    {{--  <script>
    var table;
    $(document).ready( function () {
            $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
            });
        table = $('#table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
            url: '{!! route('sebaran_dosen.data') !!}',
            type: 'POST'
            },
            columns: [
                { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false,searchable: false},
                { data: 'unit.nama_unit' },
                { data: 'unit.jenjang_pendidikan' },
                { data: 'non_fungsional_s2' },
                { data: 'asisten_ahli_s2' },
                { data: 'lektor_s2' },
                { data: 'lektor_kepala_s2' },
                { data: 'non_fungsional_s3' },
                { data: 'asisten_ahli_s3' },
                { data: 'lektor_s3' },
                { data: 'lektor_kepala_s3' },
                { data: 'guru_besar_s3' },
                { data: 'jumlah' },
                { data: 'actions', name: 'actions', orderable: false, searchable: false }
            ]
        });
        table.on( 'draw', function () {
            $('.livicon').each(function(){
                $(this).updateLivicon();
            });
        } );
    });
    </script>  --}}
@endsection
