{!! Form::open(array('id' => 'frmAdan', 'class' => 'form account-form', 'method' => 'post', 'files' => true)) !!}
        <div class="form-group row">
            <label class="col-lg-3 col-md-4 control-label">Program Studi
            </label>
            <div class="col-lg-9 col-md-8">
                <select class="form-control  select2" style="width:100%" name="unit_id">
                    <option value="">Pilih Program Studi</option>
                    @foreach($unit as $data)
                        <option value="{{$data->id}}">{{$data->nama_unit}} - {{ $data->jenjang_pendidikan }}</option>
                    @endforeach
                </select>
            </div>
        </div>


        <fieldset class="container-fluid" style="border:1px solid #cecece; padding-top:10px">
            <legend style="color:blue;">Master (S2)</legend>
            <div class="form-group row">
            {!! Form::label('Non Fungsional', 'Non Fungsional', array('class' => 'col-md-4 control-label')) !!}
                <div class="col-md-8">
                {!! Form::number('non_fungsional_s2', NULL, array('id' => 'non_fungsional_s2', 'class' => 'form-control', 'min' => 0)) !!}
                </div>
            </div>

            <div class="form-group row">
            {!! Form::label('Asisten Ahli', 'Asisten Ahli', array('class' => 'col-md-4 control-label')) !!}
                <div class="col-md-8">
                {!! Form::number('asisten_ahli_s2', NULL, array('id' => 'asisten_ahli_s2', 'class' => 'form-control', 'min' => 0)) !!}
                </div>
            </div>

            <div class="form-group row">
            {!! Form::label('Lektor', 'Lektor', array('class' => 'col-md-4 control-label')) !!}
                <div class="col-md-8">
                {!! Form::number('lektor_s2', NULL, array('id' => 'lektor_s2', 'class' => 'form-control', 'min' => 0)) !!}
                </div>
            </div>

            <div class="form-group row">
            {!! Form::label('Lektor Kepala', 'Lektor Kepala', array('class' => 'col-md-4 control-label')) !!}
                <div class="col-md-8">
                {!! Form::number('lektor_kepala_s2', NULL, array('id' => 'lektor_kepala_s2', 'class' => 'form-control', 'min' => 0)) !!}
                </div>
            </div>
        </fieldset>

        <fieldset class="container-fluid" style="border:1px solid #cecece; padding-top:10px">
            <legend style="color:blue;">Doktor (S3)</legend>
            <div class="form-group row">
            {!! Form::label('Non Fungsional', 'Non Fungsional', array('class' => 'col-md-4 control-label')) !!}
                <div class="col-md-8">
                {!! Form::number('non_fungsional_s3', NULL, array('id' => 'non_fungsional_s3', 'class' => 'form-control', 'min' => 0)) !!}
                </div>
            </div>

            <div class="form-group row">
            {!! Form::label('Asisten Ahli', 'Asisten Ahli', array('class' => 'col-md-4 control-label')) !!}
                <div class="col-md-8">
                {!! Form::number('asisten_ahli_s3', NULL, array('id' => 'asisten_ahli_s3', 'class' => 'form-control', 'min' => 0)) !!}
                </div>
            </div>

            <div class="form-group row">
            {!! Form::label('Lektor', 'Lektor', array('class' => 'col-md-4 control-label')) !!}
                <div class="col-md-8">
                {!! Form::number('lektor_s3', NULL, array('id' => 'lektor_s3', 'class' => 'form-control', 'min' => 0)) !!}
                </div>
            </div>

            <div class="form-group row">
            {!! Form::label('Lektor Kepala', 'Lektor Kepala', array('class' => 'col-md-4 control-label')) !!}
                <div class="col-md-8">
                {!! Form::number('lektor_kepala_s3', NULL, array('id' => 'lektor_kepala_s3', 'class' => 'form-control', 'min' => 0)) !!}
                </div>
            </div>
            <div class="form-group row">
            {!! Form::label('Guru Besar', 'Guru Besar', array('class' => 'col-md-4 control-label')) !!}
                <div class="col-md-8">
                {!! Form::number('guru_besar_s3', NULL, array('id' => 'guru_besar_s3', 'class' => 'form-control', 'min' => 0)) !!}
                </div>
            </div>
        </fieldset>


	{!! Form::hidden('url', URL::previous(), array('id' => 'url')) !!}

<div class="row">
	<div class="col-md-12">
		<span class="pesan"></span>
	</div>
</div>
{!! Form::close() !!}
<script src="{{ URL::asset('resources/vendor/jquery/jquery.enc.js') }}"></script>
<script src="{{ URL::asset('adan/sebaran_dosen/ajax.js') }}"></script>
<script src="{{ URL::asset('adan/includes/ajax.js') }}"></script>

<script src="{{ URL::to('backend') }}/assets/plugins/select2/js/select2.js" ></script>
<script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js" ></script>
<script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js" ></script>

<script>
	$('.select2').select2();
</script>
