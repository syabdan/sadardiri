{!! Form::open(array('id' => 'frmAdan-ubah', 'class' => 'form account-form', 'method' => 'PUT')) !!}
{{ csrf_field() }}
		<div class="form-group row">
			{!! Form::label('Program Studi', 'Program Studi ', array('class' => 'col-md-4 form-label')) !!}
			<div class="col-md-8">
  			{!! Form::select('unit_id', $unit, $data->unit_id, array('id' => 'unit_id', 'class' => 'form-control select2', 'placeholder' => 'Pilih', 'style' => 'width:100%')) !!}
			</div>
		</div>

        <fieldset class="container-fluid" style="border:1px solid #cecece; padding-top:10px">
            <legend style="color:blue;">Master (S2)</legend>
            <div class="form-group row">
            {!! Form::label('Non Fungsional', 'Non Fungsional', array('class' => 'col-md-4 control-label')) !!}
                <div class="col-md-8">
                {!! Form::number('non_fungsional_s2', $data->non_fungsional_s2, array('id' => 'non_fungsional_s2', 'class' => 'form-control', 'min' => 0)) !!}
                </div>
            </div>

            <div class="form-group row">
            {!! Form::label('Asisten Ahli', 'Asisten Ahli', array('class' => 'col-md-4 control-label')) !!}
                <div class="col-md-8">
                {!! Form::number('asisten_ahli_s2', $data->asisten_ahli_s2, array('id' => 'asisten_ahli_s2', 'class' => 'form-control', 'min' => 0)) !!}
                </div>
            </div>

            <div class="form-group row">
            {!! Form::label('Lektor', 'Lektor', array('class' => 'col-md-4 control-label')) !!}
                <div class="col-md-8">
                {!! Form::number('lektor_s2', $data->lektor_s2, array('id' => 'lektor_s2', 'class' => 'form-control', 'min' => 0)) !!}
                </div>
            </div>

            <div class="form-group row">
            {!! Form::label('Lektor Kepala', 'Lektor Kepala', array('class' => 'col-md-4 control-label')) !!}
                <div class="col-md-8">
                {!! Form::number('lektor_kepala_s2', $data->lektor_kepala_s2, array('id' => 'lektor_kepala_s2', 'class' => 'form-control', 'min' => 0)) !!}
                </div>
            </div>
        </fieldset>

        <fieldset class="container-fluid" style="border:1px solid #cecece; padding-top:10px">
            <legend style="color:blue;">Doktor (S3)</legend>
            <div class="form-group row">
            {!! Form::label('Non Fungsional', 'Non Fungsional', array('class' => 'col-md-4 control-label')) !!}
                <div class="col-md-8">
                {!! Form::number('non_fungsional_s3', $data->non_fungsional_s3, array('id' => 'non_fungsional_s3', 'class' => 'form-control', 'min' => 0)) !!}
                </div>
            </div>

            <div class="form-group row">
            {!! Form::label('Asisten Ahli', 'Asisten Ahli', array('class' => 'col-md-4 control-label')) !!}
                <div class="col-md-8">
                {!! Form::number('asisten_ahli_s3', $data->asisten_ahli_s3, array('id' => 'asisten_ahli_s3', 'class' => 'form-control', 'min' => 0)) !!}
                </div>
            </div>

            <div class="form-group row">
            {!! Form::label('Lektor', 'Lektor', array('class' => 'col-md-4 control-label')) !!}
                <div class="col-md-8">
                {!! Form::number('lektor_s3', $data->lektor_s3, array('id' => 'lektor_s3', 'class' => 'form-control', 'min' => 0)) !!}
                </div>
            </div>

            <div class="form-group row">
            {!! Form::label('Lektor Kepala', 'Lektor Kepala', array('class' => 'col-md-4 control-label')) !!}
                <div class="col-md-8">
                {!! Form::number('lektor_kepala_s3', $data->lektor_kepala_s3, array('id' => 'lektor_kepala_s3', 'class' => 'form-control', 'min' => 0)) !!}
                </div>
            </div>
            <div class="form-group row">
            {!! Form::label('Guru Besar', 'Guru Besar', array('class' => 'col-md-4 control-label')) !!}
                <div class="col-md-8">
                {!! Form::number('guru_besar_s3', $data->guru_besar_s3, array('id' => 'guru_besar_s3', 'class' => 'form-control', 'min' => 0)) !!}
                </div>
            </div>
        </fieldset>

	{!! Form::hidden('id', $data->id, array('id' => 'id')) !!}
	{!! Form::hidden('url', URL::previous(), array('id' => 'url')) !!}
</div>
<div class="row">
		<div class="col-md-12">
			<span class="pesan"></span>
		</div>
</div>
{!! Form::close() !!}
<script src="{{ URL::asset('resources/vendor/jquery/jquery.enc.js') }}"></script>
<script src="{{ URL::asset('adan/sebaran_dosen/ajax.js') }}"></script>
<script src="{{ URL::asset('adan/includes/ajax.js') }}"></script>

<script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js" ></script>
<script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js" ></script>


<!--select2-->
<script src="{{ URL::to('backend') }}/assets/plugins/select2/js/select2.js" ></script>

<script>
	$('.select2').select2();
</script>
