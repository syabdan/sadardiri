{!! Form::open(array('id' => 'frmAdan-ubah', 'class' => 'form account-form', 'method' => 'PUT')) !!}
{{ csrf_field() }}
	<div class="form-group row">
		{!! Form::label('Program Studi', 'Program Studi ', array('class' => 'col-md-4 form-label')) !!}
		<div class="col-md-8">
		{!! Form::select('unit_id', $unit, $data->unit_id, array('id' => 'unit_id', 'class' => 'form-control select2', 'placeholder' => 'Pilih', 'style' => 'width:100%')) !!}
		</div>
	</div>	

	<div class="form-group row">
	{!! Form::label('Jumlah Mahasiswa Aktif', 'Jumlah Mahasiswa Aktif', array('class' => 'col-md-4 control-label')) !!}
		<div class="col-md-8">
		{!! Form::text('jumlah_mhs_aktif', $data->jumlah_mhs_aktif, array('id' => 'jumlah_mhs_aktif', 'class' => 'form-control', 'size' => 16)) !!}
		</div>
	</div>	

	<div class="form-group row">
	{!! Form::label('Jumlah Mahasiswa Tidak Aktif', 'Jumlah Mahasiswa Tidak Aktif', array('class' => 'col-md-4 control-label')) !!}
		<div class="col-md-8">
		{!! Form::text('jumlah_mhs_nonaktif', $data->jumlah_mhs_nonaktif, array('id' => 'jumlah_mhs_nonaktif', 'class' => 'form-control', 'size' => 16)) !!}
		</div>
	</div>	
	
	<div class="form-group row">
	{!! Form::label('Jumlah Mahasiswa Asing Aktif', 'Jumlah Mahasiswa Asing Aktif', array('class' => 'col-md-4 control-label')) !!}
		<div class="col-md-8">
		{!! Form::text('jumlah_mhsasing_aktif', $data->jumlah_mhsasing_aktif, array('id' => 'jumlah_mhsasing_aktif', 'class' => 'form-control', 'size' => 16)) !!}
		</div>
	</div>	
	
	<div class="form-group row">
	{!! Form::label('Jumlah Mahasiswa Asing Tidak Aktif', 'Jumlah Mahasiswa Asing Tidak Aktif', array('class' => 'col-md-4 control-label')) !!}
		<div class="col-md-8">
		{!! Form::text('jumlah_mhsasing_nonaktif', $data->jumlah_mhsasing_nonaktif, array('id' => 'jumlah_mhsasing_nonaktif', 'class' => 'form-control', 'size' => 16)) !!}
		</div>
	</div>

	{!! Form::hidden('id', $data->id, array('id' => 'id')) !!}
	{!! Form::hidden('url', URL::previous(), array('id' => 'url')) !!}
</div>
<div class="row">
		<div class="col-md-12">
			<span class="pesan"></span>
		</div>
</div>
{!! Form::close() !!}
<script src="{{ URL::asset('resources/vendor/jquery/jquery.enc.js') }}"></script>
<script src="{{ URL::asset('adan/sebaran_mahasiswa/ajax.js') }}"></script>
<script src="{{ URL::asset('adan/includes/ajax.js') }}"></script>

<script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js" ></script>
<script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js" ></script>


<!--select2-->
<script src="{{ URL::to('backend') }}/assets/plugins/select2/js/select2.js" ></script>

<script>
	$('.select2').select2();
</script>