{!! Form::open(array('id' => 'frmAdan', 'class' => 'form account-form', 'method' => 'post', 'files' => true)) !!}
        <div class="form-group row">
            {!! Form::label('Fakultas', 'Fakultas', array('class' => 'col-md-4 form-label')) !!}
            <div class="col-md-8">
            {!! Form::select('unit_id', $unit, NULL, array('id' => 'unit_id', 'class' => 'form-control select2', 'placeholder' => 'Pilih', 'style' => 'width:100%')) !!}
            </div>
        </div>

        <div class="form-group row">
            {!! Form::label('Tingkat Pendidikan', 'Tingkat Pendidikan', array('class' => 'col-md-4 control-label')) !!}
            <div class="col-md-8">
                {!! Form::select('tingkat_pendidikan', $tingkat_pendidikan, NULL, array('id' => 'tingkat_pendidikan', 'class' => 'form-control select2', 'placeholder' => 'Pilih', 'style' => 'width:100%')) !!}
            </div>
        </div>

        <fieldset class="container-fluid" style="border:1px solid #cecece; padding-top:10px">
            <legend style="color:blue;">Status Kepegawaian</legend>
            <div class="form-group row">
            {!! Form::label('Tetap', 'Tetap', array('class' => 'col-md-4 control-label')) !!}
                <div class="col-md-8">
                {!! Form::number('tetap', NULL, array('id' => 'tetap', 'class' => 'form-control', 'min' => 0)) !!}
                </div>
            </div>

            <div class="form-group row">
            {!! Form::label('80 %', '80 %', array('class' => 'col-md-4 control-label')) !!}
                <div class="col-md-8">
                {!! Form::number('hampir_tetap', NULL, array('id' => 'hampir_tetap', 'class' => 'form-control', 'min' => 0)) !!}
                </div>
            </div>

            <div class="form-group row">
            {!! Form::label('Kontrak', 'Kontrak', array('class' => 'col-md-4 control-label')) !!}
                <div class="col-md-8">
                {!! Form::number('kontrak', NULL, array('id' => 'kontrak', 'class' => 'form-control', 'min' => 0)) !!}
                </div>
            </div>

        </fieldset>

	{!! Form::hidden('url', URL::previous(), array('id' => 'url')) !!}

<div class="row">
	<div class="col-md-12">
		<span class="pesan"></span>
	</div>
</div>
{!! Form::close() !!}
<script src="{{ URL::asset('resources/vendor/jquery/jquery.enc.js') }}"></script>
<script src="{{ URL::asset('adan/sebaran_pegawai/ajax.js') }}"></script>
<script src="{{ URL::asset('adan/includes/ajax.js') }}"></script>

<script src="{{ URL::to('backend') }}/assets/plugins/select2/js/select2.js" ></script>
<script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js" ></script>
<script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js" ></script>

<script>
	$('.select2').select2();
</script>
