{!! Form::open(array('id' => 'frmAdan-ubah', 'class' => 'form account-form', 'method' => 'PUT')) !!}

<div class="row">
	<div class="col-md-12">
		<div class="form-group row">
			{!! Form::label('Nama', 'Nama', array('class' => 'col-md-4 control-label')) !!}
			<div class="col-md-8">
			{!! Form::text('nama_user', $data->nama_user, array('id' => 'nama_user', 'class' => 'form-control', 'placeholder' => 'eg. Dekan Fakultas Teknik')) !!}
			</div>
		</div>
		<div class="form-group row">
			{!! Form::label('Email', 'Email', array('class' => 'col-md-4 control-label')) !!}
			<div class="col-md-8">
			{!! Form::text('email', $data->email, array('id' => 'email', 'class' => 'form-control', 'placeholder' => 'eg. eng.uir.ac.id')) !!}
			</div>
		</div>
		<div class="form-group row">
			{!! Form::label('Password', 'Password', array('class' => 'col-md-4 control-label')) !!}
			<div class="col-md-8">
			<input type="password" name="password" id="password" class="form-control" placeholder="Password"/>
			<span class="required"><i> Kosongkan jika tidak perlu </i></span>
			</div>
		</div>
		<div class="form-group row">
			{!! Form::label('Level', 'Level *', array('class' => 'col-md-4 form-label')) !!}
			<div class="col-md-8">
  			{!! Form::select('permissions_id', $permission, $data->permissions_id, array('id' => 'permissions_id', 'class' => 'form-control select2', 'placeholder' => 'Pilih', 'style' => 'width:100%')) !!}
			</div>
		</div>
		<div class="form-group row">
			{!! Form::label('Fakultas', 'Fakultas *', array('class' => 'col-md-4 form-label')) !!}
			<div class="col-md-8">
			  {!! Form::select('unit_id', $unit, $data->unit_id, array('id' => 'unit_id', 'class' => 'form-control select2', 'placeholder' => 'Pilih', 'style' => 'width:100%')) !!}
			</div>

		</div>
		<div class="form-group row">
			{!! Form::label('Dosen', 'Dosen *', array('class' => 'col-md-4 form-label')) !!}
			<div class="col-md-8">
			  {!! Form::select('dosen_id', $dosen, $data->dosen_id, array('id' => 'dosen_id', 'class' => 'form-control select2', 'placeholder' => 'Pilih', 'style' => 'width:100%')) !!}
			</div>

		</div>
		<div class="form-group row">
			<label class="col-md-4 control-label">Tanggal Awal Menjabat
			</label>
			<div class="col-md-8">
				<div class="input-group date form_date" data-date="" data-date-format="yyyy-mm-dd" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
					<input class="form-control" type="text" value="{{ $data->tanggal_awal }}" name="tanggal_awal">
					<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
					<!-- <span class="required"><i> Kosongkan jika tidak perlu </i></span> -->
				</div>
			</div>
		</div>

		<div class="form-group row">
			<label class="control-label col-md-4">Tanggal Akhir Menjabat
			</label>
			<div class="col-md-8">
				<div class="input-group date form_date" data-date="" data-date-format="yyyy-mm-dd" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
					<input class="form-control" type="text" value="{{ $data->tanggal_akhir }}" name="tanggal_akhir">
					<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
					<!-- <span class="required"><i>	Kosongkan jika tidak perlu </i></span> -->
				</div>
			</div>
		</div>
		<div class="form-group row">
			{!! Form::label('Foto', 'Foto', array('class' => 'col-md-4 form-label')) !!}
			<div class="col-md-8">
			{!! Form::file('pic', array('id' => 'pic', 'class' => 'form-control')) !!}
			</div>
		</div>
		{!! Form::hidden('id', $data->id, array('id' => 'id')) !!}
		{!! Form::hidden('url', URL::previous(), array('id' => 'url')) !!}
		<div class="form-group row">
			<div class="col-md-12">
				<span class="pesan"></span>
			</div>
		</div>

	</div>
</div>

{!! Form::close() !!}
<script src="{{ URL::asset('resources/vendor/jquery/jquery.enc.js') }}"></script>
<script src="{{ URL::asset('adan/users/ajax.js') }}"></script>
<script src="{{ URL::asset('adan/includes/ajax.js') }}"></script>
<script src="{{ URL::to('backend') }}/assets/plugins/select2/js/select2.js" ></script>
<script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js" ></script>
<script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js" ></script>

<script>
    $('.select2').select2();
</script>
