@section('style')
<link href="{{ URL::to('backend') }}/assets/plugins/summernote/summernote.css" rel="stylesheet">
@endsection

{!! Form::open(array('id' => 'frmAdan-ubah', 'class' => 'form account-form', 'method' => 'PUT')) !!}
{{ csrf_field() }}
        <div class="form-group">
			{!! Form::label('indikator', 'Indikator *', array('class' => 'form-label')) !!}
  			{!! Form::select('indikator_id', $indikator, $data->indikator_id, array('id' => 'indikator_id', 'class' => 'form-control select2', 'placeholder' => 'Pilih', 'style' => 'width:100%')) !!}
		</div>

        <p>
			{!! Form::label('Group Unit', 'Group Unit *', array('class' => 'form-label col-md-3')) !!}
  			{!! Form::select('status_unit', $status_units, $data->status_unit, array('id' => 'status_unit', 'class' => 'sidebar-pos-option form-control input-inline input-sm input-small select2 col-md-9', 'style' => 'width:100%')) !!}
        </p>

		<div class="row">
			<div class="col-md-12 col-sm-12">
			<label for="isi_indikator_kinerja"> Narasi Indikator Kinerja *</label>
				<textarea name="isi_indikator_kinerja" id="summernote" cols="30" rows="10" >{{ $data->isi_indikator_kinerja }}</textarea>
			</div>
		</div>
		<p>
			{!! Form::label('Status unggah', 'Status unggah *', array('class' => 'form-label')) !!}
  			{!! Form::select('status_unggah', $unggah, $data->status_unggah, array('id' => 'status_unggah', 'class' => 'sidebar-pos-option form-control input-inline input-sm input-small select2', 'placeholder' => 'Pilih', 'style' => 'width:100%')) !!}
		</p>

        <p>
			{!! Form::label('Status aktif', 'Status aktif *', array('class' => 'form-label')) !!}
  			{!! Form::select('deleted_at', $aktif, $data->deleted_at, array('id' => 'deleted_at', 'class' => 'sidebar-pos-option form-control input-inline input-sm input-small select2', 'style' => 'width:100%')) !!}
		</p>
	{!! Form::hidden('id', $data->id, array('id' => 'id')) !!}
	{!! Form::hidden('url', URL::previous(), array('id' => 'url')) !!}
</div>
<div class="row">
		<div class="col-md-12">
			<span class="pesan"></span>
		</div>
</div>
{!! Form::close() !!}
<script src="{{ URL::asset('resources/vendor/jquery/jquery.enc.js') }}"></script>
<script src="{{ URL::asset('adan/indikator_kinerja/ajax.js') }}"></script>
<script src="{{ URL::asset('adan/includes/ajax.js') }}"></script>

<script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js" ></script>
<script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js" ></script>

<!-- summernote -->
<script src="{{ URL::to('backend') }}/assets/plugins/summernote/summernote.min.js" ></script>
<script src="{{ URL::to('backend') }}/assets/js/pages/summernote/summernote-data.js" ></script>

<!--select2-->
<script src="{{ URL::to('backend') }}/assets/plugins/select2/js/select2.js" ></script>

<script>
	$('.select2').select2();
</script>
