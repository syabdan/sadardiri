@section('style')
<link href="{{ URL::to('backend') }}/assets/plugins/summernote/summernote.css" rel="stylesheet">
@endsection

{!! Form::open(array('id' => 'frmAdan', 'class' => 'form account-form', 'method' => 'post', 'files' => true)) !!}

		<div class="form-group">
			{!! Form::label('indikator', 'Indikator *', array('class' => 'form-label')) !!}
  			{!! Form::select('indikator_id', $indikator, NULL, array('id' => 'indikator_id', 'class' => 'form-control select2', 'placeholder' => 'Pilih', 'style' => 'width:100%')) !!}
		</div>

        <p>
			{!! Form::label('Group Unit', 'Group Unit *', array('class' => 'form-label col-md-3')) !!}
  			{!! Form::select('status_unit', $status_units, NULL, array('id' => 'status_unit', 'class' => 'sidebar-pos-option form-control input-inline input-sm input-small select2 col-md-9', 'style' => 'width:100%')) !!}
        </p>

		<div class="row">
			<div class="col-md-12 col-sm-12">
			<label for="isi_indikator_kinerja"> Narasi Indikator Kinerja *</label>
				<textarea name="isi_indikator_kinerja" id="summernote" cols="30" rows="10"></textarea>
			</div>
		</div>
		<p>
			{!! Form::label('Status unggah', 'Status unggah *', array('class' => 'form-label col-md-3')) !!}
  			{!! Form::select('status_unggah', $unggah, NULL, array('id' => 'status_unggah', 'class' => 'sidebar-pos-option form-control input-inline input-sm input-small select2 col-md-9', 'placeholder' => 'Pilih', 'style' => 'width:100%')) !!}
		</p>

		<div class="row">
		{{-- <div class="col-sm-7"></div>
			<div class="col-sm-5">
				<label for=""> Tambah Sub Indikator Kinerja</label>
				<a class="btn btn-flat btn-sm btn-success" id="tambah_sub_ask"><i class="fa fa-plus"  aria-hidden="true"></i></a><span id="nowrap"></span>
			</div>
		</div> --}}
		<br>
		<div id="indukwrap">

		</div>
		<br>

	{!! Form::hidden('url', URL::previous(), array('id' => 'url')) !!}

<div class="row">
	<div class="col-md-12">
		<span class="pesan"></span>
	</div>
</div>
{!! Form::close() !!}
<script src="{{ URL::asset('resources/vendor/jquery/jquery.enc.js') }}"></script>
<script src="{{ URL::asset('adan/indikator_kinerja/ajax.js') }}"></script>
<script src="{{ URL::asset('adan/includes/ajax.js') }}"></script>

<script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js" ></script>
<script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js" ></script>
<!-- summernote -->
<script src="{{ URL::to('backend') }}/assets/plugins/summernote/summernote.min.js" ></script>
<script src="{{ URL::to('backend') }}/assets/js/pages/summernote/summernote-data.js" ></script>

<!--select2-->
<script src="{{ URL::to('backend') }}/assets/plugins/select2/js/select2.js" ></script>

<script>
	$('.select2').select2();
</script>

<script type="text/javascript">
		$(function() {
			var next = 0;
			var scntDiv = $('#indukwrap');
			var nomor = $('#nowrap');
			$('#tambah_sub_ask').on('click', function() {
				 next = next + 1;
					$('<div class="row"><div class="col-md-12 col-sm-12"><div id="indukwrap'+next+'"><div class="row"><div class="col-md-12 col-sm-12"><div class="card  card-box"><div class="card-body"> <div class="row"><div class="col-md-12 col-sm-12"><label for="isi_indikator_sub"><b>Sub Indikator Kinerja  '+next+'</b></label> <a class="btn btn-sm btn-flat btn-danger" id="hapus"><i class="fa fa-trash"  aria-hidden="true"></i></a><textarea name="isi_indikator_sub[]" class="summernote" cols="30" rows="2" ></textarea></div></div></div> {!! Form::label('Status unggah sub indikator kinerja',"Status Unggah Sub Indikator Kinerja", array('class' => 'form-label')) !!}{!! Form::select('status_unggah_sub[]', $unggah,NULL, array('id' => 'status_unggah_sub', 'class' => 'sidebar-pos-option form-control input-inline select2', 'placeholder' => 'Pilih', 'style' => 'width:100%')) !!}</div></div></div></div></div></div>').appendTo(scntDiv);

					$(nomor).remove();
					$('<b>'+next+' </b>').appendTo(nomor);

					$( '[class="summernote"]' ).summernote({
																placeholder: '',
																tabsize: 2,
																height: 200
															});
					// return false;
			});

			$('#indukwrap').on('click','#hapus',function() {

			$('#indukwrap'+next).remove();
			next = next-1;
			});
		});
	</script>
