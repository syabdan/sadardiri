<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-TYHDMD24T2"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-TYHDMD24T2');
    </script>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta name="description" content="Sistem Evaluasi Diri Universitas Islam Riau, sadardiri UIR" />
    <meta name="author" content="Syabdan Dalimunthe" />
	<title>Sadardiri UIR</title>
	<!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
	@include('backend.includes.css')
	@yield('style')

 </head>
 <!-- END HEAD -->
 <body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md page-full-width header-white dark-sidebar-color logo-white">
    <div class="page-wrapper">
        <!-- start header -->
        <div class="page-header navbar navbar-fixed-top">
           <div class="page-header-inner ">
                <!-- logo start -->
                <div class="page-logo">
                    <a href="{{ url('/')}}">
                    <img alt="" src="{{ asset('backend/assets/img/uir.png')}}" style="width:30px ">
                    <span class="logo-default" >Sadardiri</span> </a>
                </div>
                <!-- logo end -->
				<ul class="nav navbar-nav navbar-left in">
					<li><a href="#" class="menu-toggler sidebar-toggler"><i class="icon-menu"></i></a></li>
				</ul>

                <!-- start mobile menu -->
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                    <span></span>
                </a>
               <!-- end mobile menu -->
                <!-- start header menu -->
                <div class="top-menu">
                    <ul class="nav navbar-nav pull-right">
 						<!-- start manage user dropdown -->
 						<li class="dropdown dropdown-user">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                @if(Auth::user()->pic)
                                <img src="{{ asset('backend/assets/img/user/'.Auth::user()->pic)}}">
                                @else
                                <img class="img-circle " src="{{ asset('backend/assets/img/user/user.png')}}" />
                                @endif
                                <span class="username username-hide-on-mobile"> {{ Auth::user()->nama_user }} </span>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-default animated jello">
                                <li>
                                    <a user-id="{{ \Auth::user()->id }}" class="ubah_user">
                                        <i class="icon-user"></i> Profile </a>
                                </li>
                                </li>
                                <li>
                                    <a href="{{ route('logout')  }}">
                                        <i class="icon-logout"></i> Log Out </a>
                                </li>
                            </ul>
                        </li>
                        <!-- end manage user dropdown -->
                    </ul>
                </div>
            </div>

        @include('backend.includes.menu')


        </div>
        <!-- end header -->
	<!-- start page container -->
	<div class="page-container">
		<!-- start sidebar menu -->
			@include('backend.includes.menu_mobile')
        <!-- end sidebar menu -->
		<!-- start page content -->
            @yield('content')
            @yield('modal')
		<!-- end page container -->
		<!-- start footer -->
    		@include('backend.includes.footer')
    	<!-- end footer -->
			@include('backend.includes.js')
            @yield('script')

            <script>
                $(document).on("click",".ubah_user",function() {
                adanLoadingFadeIn();
                    var id = $(this).attr('user-id');
                    $.loadmodal({
                        url: "{{ url($url_admin.'/users') }}/"+ id +"/edit",
                id: 'responsive',
                        dlgClass: 'fade',
                        bgClass: 'warning',
                        title: 'Ubah',
                        width: 'whatever',
                        modal: {
                            keyboard: true,
                            // any other options from the regular $().modal call (see Bootstrap docs)
                            },
                    ajax: {
                            dataType: 'html',
                            method: 'GET',
                            success: function(data, status, xhr){
                        adanLoadingFadeOut();
                            },
                        },
                    });
                });
            </script>
  </body>
</html>
