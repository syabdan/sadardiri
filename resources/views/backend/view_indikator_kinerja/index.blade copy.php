@extends('backend.layout')
@section('style')
    <!-- data tables -->
    <link href="{{ URL::to('backend') }}/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::to('resources') }}/vendor/datatables/css/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
    <!--select2-->
    <link href="{{ URL::to('backend') }}/assets/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="{{ URL::to('backend') }}/assets/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">{{$halaman->nama}}</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">{{$halaman->nama}}</li>
                            </ol>
                        </div>
					</div>

                     <!-- start Payment Details -->
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="card  card-box">
                                <div class="card-body ">
									<div class="row p-b-20">
                                        <div class="col-md-12 col-sm-12 col-12">
                                            <div class="form-group">
                                                <select name="jenis" id="jenis" class="form-control select2" style="width:100%">
                                                    <option value="">Pilih Jenis Indikator</option>
                                                    <option value="1">Indikator Kinerja Utama</option>
                                                    <option value="2">Indikator Kinerja Tambahan</option>

                                                </select>
                                            </div>
                                            <div class="form-group" id="form_unit" style="display: none">
                                                <select name="unit" id="unit" class="form-control select2" style="width:100%">
                                                    <option value="">Pilih Fakultas</option>
                                                    <!-- <option value="all">Semua Fakultas</option> -->
                                                    @foreach($units as $unit)
                                                        <option value="{{ $unit->id }}">{{ $unit->nama_unit }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <h3>Indikator Kinerja {{ $status }} {{ $user->nama_user ?? ''}}</h3>

                                    <div class="table-wrap">
										<div class="table-responsive tblHeightSet small-slimscroll-style">
											<table class="table table-striped table-bordered" id="table_per_unit" style="width:100%">
												<thead>
													<tr>
														<th>No</th>
														<th>Indikator Kinerja</th>
														<th>Target</th>
														<th>Capaian Tengah</th>
														<th>Capaian Akhir</th>
														<th>Nilai</th>
														<th>Persentase (%)</th>
													</tr>
                                                </thead>
                                                @if(count($datas) > 0)
												<tbody>
                                                    @php($parent = 0)
                                                    @php($nilai = 0)
                                                    @php($persen = 0)
                                                    @php($count = 0)


                                                        @foreach($datas as $d)

                                                            @if($d->p_id != $parent)
                                                                @php($nilai = 0)
                                                                @php($persen = 0)
                                                                @php($count = 0)

                                                                @foreach($datas as $data)
                                                                    @if($d->p_id == $data->p_id)

                                                                    @php($nilai = $nilai + $data->nilai)
                                                                    @php($persen = $persen + $data->persentase)
                                                                    @php($count++)

                                                                    @endif
                                                                @endforeach

                                                                <tr>
                                                                    <td colspan="2" style="color:blue; font-weight:bold"> {{strip_tags($d->isi_parent) ?? ''}}</td>
                                                                    <td colspan="4"><center> <div style="color:brown; font-weight:bold"> Rata-rata: {{ number_format($nilai / $count,2,',',',')}}</div></center></td>
                                                                    <td><center><div style="color:brown; font-weight:bold">{{ number_format($persen / $count, 2,',',',')}} %</div></center></td>
                                                                </tr>
                                                                @php($no = 1)
                                                                @foreach($datas as $data)
                                                                    @if($d->p_id == $data->p_id)
                                                                    <tr>
                                                                        <td>{{$no}}</td>
                                                                        <!-- <td></td> -->
                                                                        <td><div style='border:1px solid #ccc; padding:10px; max-height:100px;overflow:scroll;'>{{strip_tags($data->isi_child) ?? ''}}</div></td>
                                                                        <td>{{$data->target}}</td>
                                                                        <td>{{$data->capaianmid}}</td>
                                                                        <td>{{$data->capaian}}</td>
                                                                        <td>{{$data->nilai}}</td>
                                                                        <td>{{number_format($data->persentase,2,',','.')}}</td>
                                                                        @php($no++)
                                                                    </tr>
                                                                    @php($nilai = $nilai + $data->nilai)
                                                                    @php($persen = $persen + $data->persentase)
                                                                    @php($count++)
                                                                    @endif
                                                                @endforeach

                                                            @endif
                                                            @php($parent = $d->p_id)
                                                        @endforeach
                                                        <?php if($count > 0 ) {?>

                                                        <tr style="color:red; font-weight:bold">
                                                        <td colspan="5" ><center>Total Rata - rata</center></td>
                                                        <td>{{number_format($nilai / $count,2,',',',')}}</td>
                                                        <td>{{number_format($persen / $count, 2,',',',')}} %</td>
                                                        </tr>

                                                        <?php
                                                        }
                                                        ?>

                                                </tbody>
                                                @else
                                                    <tbody>
                                                        <tr >
                                                            <td colspan="7" align="center">
                                                                <h1 >Tidak Ada Data</h1>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                @endif
											</table>
										</div>
									</div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        <!-- end page content -->

    </div>
@endsection

@section('script')
    <!-- data tables -->
    <script src="{{ URL::to('backend') }}/assets/plugins/datatables/jquery.dataTables.min.js" ></script>
    <script src="{{ URL::to('backend') }}/assets/plugins/datatables/editabletable.js" ></script>
	<script src="{{ URL::to('backend') }}/assets/js/pages/table/editable_table_data.js" ></script>
 	<script src="{{ URL::to('backend') }}/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
	<script src="{{ URL::to('backend') }}/assets/js/pages/table/table_data.js" ></script>
    <script src="{{ URL::to('resources') }}/vendor/datatables/js/dataTables.responsive.min.js" ></script>
    <script src="{{ URL::to('resources') }}/vendor/datatables/js/responsive.bootstrap4.min.js" ></script>

    <!-- <script src="{{ URL::to('/') }}/adan/indikator_kinerja/jquery.js"></script> -->
    <!--select2-->
    <script src="{{ URL::to('backend') }}/assets/plugins/select2/js/select2.js" ></script>
    <script>
    $('select2').select2();

    // var table;
    // $(document).ready( function () {
    //         $.ajaxSetup({
    //         headers: {
    //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //         }
    //         });
    //     table = $('.table').DataTable({
    //         processing: true,
    //         serverSide: true,
    //         ajax: {
    //         url: "{!! url('view-indikator-kinerja/data/all') !!}",
    //         // type: 'POST'
    //         },
    //         columns: [
    //             { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false,searchable: false},
    //             { data: 'isi_indikator_kinerja'},
    //             { data: 'target'},
    //             { data: 'capaianmid'},
    //             { data: 'capaian'},
    //             { data: 'nilai'},
    //             { data: 'persentase'},
    //         ]
    //     });
    //     table.on( 'draw', function () {
    //         $('.livicon').each(function(){
    //             $(this).updateLivicon();
    //         });
    //     } );
    // });
    </script>


    <script>
    $('#jenis').on('change', function() {
        {{--  alert('masuk jenis');  --}}
        $("#form_unit").show();
    });



    $("#unit").change(function() {

        var id_unit = $("#unit").val();
        var jenis = $("#jenis").val();

		location.href = "{!! url('view-indikator-kinerja/"+id_unit+"/"+jenis+"') !!}";

        // var _token = "{{ csrf_token() }}";

        // $('#table_per_unit').dataTable( {
        //     processing: true,
        //     serverSide: true,
        //     bDestroy: true,
        //     ajax: "{!! url('view-indikator-kinerja/data/"+id_unit+"') !!}",
        //     // data: { 'unit_id':id_unit, '_token': _token},
        //     // type: "POST",
        //     columns: [
        //         { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false,searchable: false},
        //         { data: 'isi_indikator_kinerja'},
        //         { data: 'target'},
        //         { data: 'capaianmid'},
        //         { data: 'capaian'},
        //         { data: 'nilai'},
        //         { data: 'persentase'},
        //     ],
        //     dom: 'Bfrtip',
        //     buttons: [
        //         'copy', 'csv', 'excel', 'pdf', 'print'
        //     ]
        // });

    });
    </script>
@endsection
