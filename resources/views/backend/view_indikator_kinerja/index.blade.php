@extends('backend.layout')
@section('style')
    <!-- data tables -->
    <link href="{{ URL::to('backend') }}/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::to('resources') }}/vendor/datatables/css/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
    <!--select2-->
    <link href="{{ URL::to('backend') }}/assets/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="{{ URL::to('backend') }}/assets/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">{{$halaman->nama}}</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">{{$halaman->nama}}</li>
                            </ol>
                        </div>
					</div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="card  card-box">
                                <div class="card-body ">
									<div class="row p-b-20">
                                        <div class="col-md-12 col-sm-12 col-12">
                                            <div class="form-group">
                                                <select name="jenis" id="jenis" class="form-control select2" style="width:100%">
                                                    <option value="">Pilih Jenis Indikator</option>
                                                    <option value="1">Indikator Kinerja Utama</option>
                                                    <option value="2">Indikator Kinerja Tambahan</option>
                                                </select>
                                            </div>

                                            @if(\Auth::user()->permissions_id == 1 || \Auth::user()->permissions_id == 2)
                                            <div class="form-group" id="form_unit" style="display: none">
                                                <select name="unit" id="unit" class="form-control select2" style="width:100%">
                                                    <option value="">Pilih Fakultas</option>
                                                    <!-- <option value="all">Semua Fakultas</option> -->
                                                    @foreach($units as $unit)
                                                        <option value="{{ $unit->id }}">{{ $unit->nama_unit }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            @endif

                                            <div class="form-group" id="form-periode" style="display: none">
                                                <select name="periode" id="periode" class="form-control select2" style="width:100%" style="display: none">
                                                    <option value="">Pilih Periode</option>
                                                    @foreach($periodes as $periode)
                                                        <option value="{{ $periode->id }}">{{ $periode->nama_periode }}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                        </div>
                                    </div>

                                    <div id="cetak"  style="display: {{ $id_unit ? 'show' : 'none' }}">
                                        <form action="{{ url('cetak/indikator_kinerja')}}" method="post" target="_blank" >
                                            @csrf
                                            <input type="hidden" name="unit" value="{{ $id_unit }}">
                                            <input type="hidden" name="jenis" value="{{ $jenis }}">
                                            <input type="hidden" name="periode_id" value="{{ $periode_id }}">
                                            <button type="submit" class="fa fa-print btn-success btn-lg pull-right" style="cursor: pointer;"> Cetak</button>
                                        </form>
                                    </div>
                                    <h3>Indikator Kinerja {{ $status }} {{ $fakultas->nama_unit ?? ''}}</h3>

                                    <div class="table-wrap">
										<div class="table-responsive tblHeightSet small-slimscroll-style">
											<table class="table table-striped table-bordered" id="table_per_unit" style="width:100%">
												<thead>
													<tr>
														<th>No</th>
														<th>Indikator Kinerja</th>
														<th>Target</th>
														<th>Capaian Tengah</th>
														<th>Capaian Akhir</th>
														<th>Nilai</th>
														<th>Persentase (%)</th>
													</tr>
                                                </thead>
                                                @if(count($datas) > 0)
												<tbody>
                                                    @php($parent = 0)
                                                    @php($nilai2 = 0)
                                                    @php($persen2 = 0)
                                                    @php($count2 = 0)


                                                        @foreach($dataku as $d)
                                                                @php($nilai = 0)
                                                                @php($persen = 0)
                                                                @php($count = 0)

                                                                @foreach($datas as $data)
                                                                @if($d->indikator_id == $data->indikator_id)

                                                                    @php($nilai = $nilai + $data->nilai)
                                                                    @php($persen = $persen + $data->persentase)
                                                                    @php($count++)

                                                                @endif
                                                                @endforeach

                                                                <tr>
                                                                    <td colspan="2" style="color:blue; font-weight:bold"> {{strip_tags($d->keterangan) ?? ''}}</td>
                                                                    <td colspan="4"><center> <div style="color:brown; font-weight:bold"> Rata-rata: {{ number_format($nilai / $count,2,',',',')}}</div></center></td>
                                                                    <td><center><div style="color:brown; font-weight:bold">{{ number_format($persen / $count, 2,',',',')}} %</div></center></td>
                                                                </tr>
                                                                @php($no = 1)
                                                                @foreach($datas as $data)
                                                                    @if($d->indikator_id == $data->indikator_id)
                                                                    <tr>
                                                                        <td>{{$no}}</td>
                                                                        <td><div style='border:1px solid #ccc; padding:10px; max-height:100px;overflow:scroll;'>{{strip_tags($data->isi_child) ?? ''}}</div></td>
                                                                        <td>{{$data->target}}</td>
                                                                        <td>{{$data->capaianmid}}</td>
                                                                        <td>{{$data->capaian}}</td>
                                                                        <td>{{$data->nilai}}</td>
                                                                        <td>{{number_format($data->persentase,2,',','.')}}</td>
                                                                        @php($no++)
                                                                    </tr>
                                                                    @php($nilai2 = $nilai2 + $data->nilai)
                                                                    @php($persen2 = $persen2 + $data->persentase)
                                                                    @php($count2++)
                                                                    @endif

                                                                @endforeach
                                                        @endforeach

                                                        <?php if($count > 0 ) {?>

                                                        <tr style="color:red; font-weight:bold">
                                                        <td colspan="5" ><center>Total Rata - rata</center></td>
                                                        <td>{{number_format($nilai2 / $count2, 2,',',',')}}</td>
                                                        <td>{{number_format($persen2 / $count2, 2,',',',')}} %</td>
                                                        </tr>

                                                        <?php
                                                        }
                                                        ?>

                                                </tbody>
                                                @else
                                                    <tbody>
                                                        <tr >
                                                            <td colspan="7" align="center">
                                                                <h1 >Tidak Ada Data</h1>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                @endif
											</table>
										</div>
									</div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        <!-- end page content -->

    </div>
@endsection

@section('script')
    <!-- data tables -->
    <script src="{{ URL::to('backend') }}/assets/plugins/datatables/jquery.dataTables.min.js" ></script>
    <script src="{{ URL::to('backend') }}/assets/plugins/datatables/editabletable.js" ></script>
	<script src="{{ URL::to('backend') }}/assets/js/pages/table/editable_table_data.js" ></script>
 	<script src="{{ URL::to('backend') }}/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
	<script src="{{ URL::to('backend') }}/assets/js/pages/table/table_data.js" ></script>
    <script src="{{ URL::to('resources') }}/vendor/datatables/js/dataTables.responsive.min.js" ></script>
    <script src="{{ URL::to('resources') }}/vendor/datatables/js/responsive.bootstrap4.min.js" ></script>

    <!-- <script src="{{ URL::to('/') }}/adan/indikator_kinerja/jquery.js"></script> -->
    <!--select2-->
    <script src="{{ URL::to('backend') }}/assets/plugins/select2/js/select2.js" ></script>
    <script>
    $('.select2').select2();


    $('#jenis').on('change', function() {

        $("#form_unit").show(800);
        $("#form-periode").show(1000);
    });



    // $("#unit").change(function() {

    //     var id_unit = $("#unit").val();
    //     var jenis   = $("#jenis").val();

	// 	// location.href = "{!! url('view-indikator-kinerja/"+id_unit+"/"+jenis+"') !!}";

    // });

    $("#periode").change(function() {

        var id_unit     = $("#unit").val();
        var jenis       = $("#jenis").val();
        var periode     = $("#periode").val();
        @if(\Auth::user()->permissions_id == 5 || \Auth::user()->permissions_id == 3 || \Auth::user()->permissions_id == 4)
            location.href = "{!! url('view-indikator-kinerja/"+null+"/"+jenis+"/"+periode+"') !!}";
        @else
		    location.href = "{!! url('view-indikator-kinerja/"+id_unit+"/"+jenis+"/"+periode+"') !!}";
        @endif

    });
</script>
@endsection
