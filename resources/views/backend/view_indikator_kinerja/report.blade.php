<style>
    table {
        border-collapse: collapse;
    }

    .center-dinas  { text-align: center;
            font-size: 130%;
            font-weight: bold;
       }

       .center-pekanbaru  {
            text-align: center;
            font-size: 110%;
            font-weight: bold;
            padding-bottom: 0;
       }
</style>
<table width="100%" border="0" style="border-bottom:3px solid #000;tr:margin-top:-25px">
    <tbody>
      <tr>
        <td rowspan="3" width="80"><center><img src="{{ asset('backend/assets/img/uir.png') }}" width="80%"></center></td>
        <td class="center-dinas">SISTEM DARING EVALUASI DIRI</td>
      </tr>
      <tr>
        <td class="center-dinas">UNIVERSITAS ISLAM RIAU</td>
      </tr>
      <tr style="margin-bottom: -10;">
        <td class="center-pekanbaru">Jl. Kaharudin Nasution No. 113 Pekanbaru</td>
      </tr>
    </tbody>
</table>
<br>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="card  card-box">
            <div class="card-body ">
                <center>
                    <h3>Laporan Indikator Kinerja {{ $status }} {{ $fakultas->nama_unit ?? ''}}</h3>
                </center>

                <div class="table-wrap">
                    <div class="table-responsive tblHeightSet small-slimscroll-style">
                        <table style="width:100%; border-collapse: collapse;" border="0.01">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Indikator Kinerja</th>
                                    <th>Target</th>
                                    <th>Capaian Tengah</th>
                                    <th>Capaian Akhir</th>
                                    <th>Nilai</th>
                                    <th>Persentase (%)</th>
                                </tr>
                            </thead>
                            @if(count($datas) > 0)
                            <tbody>
                                @php($parent = 0)
                                @php($nilai2 = 0)
                                @php($persen2 = 0)
                                @php($count2 = 0)


                                    @foreach($dataku as $d)
                                            @php($nilai = 0)
                                            @php($persen = 0)
                                            @php($count = 0)

                                            @foreach($datas as $data)
                                            @if($d->indikator_id == $data->indikator_id)

                                                @php($nilai = $nilai + $data->nilai)
                                                @php($persen = $persen + $data->persentase)
                                                @php($count++)

                                            @endif
                                            @endforeach

                                            <tr>
                                                <td colspan="2" style="color:blue; padding-left: 15px"> {{strip_tags($d->keterangan) ?? ''}}</td>
                                                <td colspan="4"><center> <div style="color:brown; font-weight:bold"> Rata-rata: {{ number_format($nilai / $count,2,',','.')}}</div></center></td>
                                                <td><center><div style="color:brown; font-weight:bold">{{ number_format($persen / $count, 2,',','.')}} %</div></center></td>
                                            </tr>
                                            @php($no = 1)
                                            @foreach($datas as $data)
                                                @if($d->indikator_id == $data->indikator_id)
                                                <tr style="text-align: center;">
                                                    <td>{{$no}}</td>
                                                    <td style="text-align: left;"><div style='border:1px solid #ccc; padding:5px; max-height:100px;overflow:scroll;'>{{strip_tags($data->isi_child) ?? ''}}</div></td>
                                                    <td>{{ number_format($data->target, 2,',','.')}}</td>
                                                    <td>{{ number_format($data->capaianmid, 2,',','.')}}</td>
                                                    <td>{{ number_format($data->capaian, 2,',','.')}}</td>
                                                    <td>{{ number_format($data->nilai, 2,',','.')}}</td>
                                                    <td>{{number_format($data->persentase,2,',','.')}}</td>
                                                    @php($no++)
                                                </tr>
                                                @php($nilai2 = $nilai2 + $data->nilai)
                                                @php($persen2 = $persen2 + $data->persentase)
                                                @php($count2++)
                                                @endif


                                            @endforeach


                                    @endforeach

                                    <?php if($count > 0 ) {?>

                                    <tr style="font-weight:bold; text-align:center">
                                    <td colspan="5" >Total Rata - rata</td>
                                    <td>{{number_format($nilai2 / $count2, 2,',',',')}}</td>
                                    <td>{{number_format($persen2 / $count2, 2,',',',')}} %</td>
                                    </tr>

                                    <?php
                                    }
                                    ?>

                            </tbody>
                            @else
                                <tbody>
                                    <tr >
                                        <td colspan="7" align="center">
                                            <h1 >Tidak Ada Data</h1>
                                        </td>
                                    </tr>
                                </tbody>
                            @endif
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<table style="width: 100%; text-align:center; margin-top:50px">
    <tr style="margin-bottom: 50px">
        <td></td>
        <td >Pekanbaru, {{ tanggal_indonesia( date('Y-m-d'), false ) }}</td>
    </tr>
    <tr>
        <td style="width: 50%">Auditor</td>
        <td style="width: 50%">Dekan {{ $fakultas->nama_unit ?? ''}}</td>
    </tr>
    <tr>
        <td colspan="2">
            <br><br><br><br><br><br>
        </td>
    </tr>
    <tr style="font-weight: 600">
        <td>(......................................................)</td>
        <td>{{ $pimpinan->nama_pimpinan }} </td>
    </tr>
</table>
