    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="{{ URL::to('backend') }}/assets/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ URL::to('backend') }}/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<!--bootstrap -->
	<link href="{{ URL::to('backend') }}/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="{{ URL::to('backend') }}/assets/plugins/summernote/summernote.css" rel="stylesheet">
	
    <!-- Material Design Lite CSS -->
	<link rel="stylesheet" href="{{ URL::to('backend') }}/assets/plugins/material/material.min.css">
	<link rel="stylesheet" href="{{ URL::to('backend') }}/assets/css/material_style.css">
	<!-- animation -->
    <link href="{{ URL::to('backend') }}/assets/css/pages/animate_page.css" rel="stylesheet">
	<link rel="stylesheet" href="{{ URL::to('backend') }}/assets/plugins/sweet-alert/sweetalert.min.css">
    
	<!-- Template Styles -->
    <link href="{{ URL::to('backend') }}/assets/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ URL::to('backend') }}/assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="{{ URL::to('backend') }}/assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="{{ URL::to('backend') }}/assets/css/theme-color.css" rel="stylesheet" type="text/css" />
	<!-- favicon -->
    <link rel="shortcut icon" href="{{ URL::to('backend') }}/assets/img/favicon.ico" /> 
    <!--select2-->
    <link href="{{ URL::to('backend') }}/assets/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="{{ URL::to('backend') }}/assets/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

    <!-- dropzone -->
    <link href="{{ URL::to('backend') }}/assets/plugins/dropzone/dropzone.css" rel="stylesheet" media="screen">