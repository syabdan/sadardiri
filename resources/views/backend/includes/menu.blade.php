<div class="navbar-custom">
	<div class="hor-menu hidden-sm hidden-xs">
		<ul class="nav navbar-nav">
		<?php
		$data_sections_arr = explode(",", Auth::user()->permissionsGroup->data_menus);
		$user			= \Auth::user();
        $current	=	explode(".", \Route::currentRouteName());
        use App\Menu;
		$submenu	=	Menu::where('link', $current[0])->orderBy('id','asc')->first();
		// dd($submenu);
		?>

		@foreach($menu as $row)
		@if(in_array($row->id,$data_sections_arr))
		<li class="mega-menu-dropdown {{($submenu == url($row->link)) ? 'active' : '' }}">
				<a href="{{ url($row->link) }}" class="dropdown-toggle"> <i class="material-icons">{{ $row->icon}}</i> {{ $row->nama }}
				@if($row->link == '#')
					<i class="fa fa-angle-down"></i>
					<span class="arrow "></span>
				@endif
				</a>

			<ul class="dropdown-menu">
			@if($row->link == '#')
			<li>
				<div class="mega-menu-content">
					<div class="row">
						<div class="col-md-12">
			@endif
                                @foreach($row->subMenus->where('tampil',1) as $rowSub)
                                @if(in_array($rowSub->id,$data_sections_arr))
                                <ul class="mega-menu-submenu">
                                    <li class="nav-item">
                                        <a href="{{url($rowSub->link)}}" class="nav-link "> <span class="title">{{ $rowSub->nama}}</span>
                                        </a>
                                    </li>
                                </ul>
                                @endif
                                @endforeach

			@if($row->link == '#')
						</div>
					</div>
				</div>
			</li>
			@endif
			</ul>

		</li>
		@endif
		@endforeach

		</ul>

	</div>
</div>
