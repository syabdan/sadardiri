<div class="sidebar-container">
			<div class="sidemenu-container navbar-collapse collapse fixed-menu">
				<div id="remove-scroll">
					<ul class="sidemenu page-header-fixed p-t-20" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
						<li class="sidebar-toggler-wrapper hide">
							<div class="sidebar-toggler">
								<span></span>
							</div>
						</li>
						<li class="sidebar-user-panel">
							<div class="user-panel">
								<div class="row">
										<div class="sidebar-userpic">
										@if(Auth::user()->pic)
											<img src="{{ asset('backend/assets/img/user/'.Auth::user()->pic)}}" class="img-responsive">
										@else
											<img class="img-responsive" src="{{ asset('backend/assets/img/user/user.png')}}" />
										@endif
									</div>
									<div class="profile-usertitle">
										<div class="sidebar-userpic-name"> {{ \Auth::user()->nama_user }} </div>
										<div class="profile-usertitle-job"> {{ \Auth::user()->email }} </div>
									</div>

							</div>
						</li>

						<?php
						$data_sections_arr = explode(",", Auth::user()->permissionsGroup->data_menus);
						$user			= \Auth::user();
						$current	=	explode(".", Route::currentRouteName());
						use App\Menu;
						$submenu	=	Menu::where('link', $current[0])->orderBy('id','asc')->first();
						// dd($submenu);
						?>

						@foreach($menu as $row)
						@if(in_array($row->id,$data_sections_arr))
						<li class="nav-item">
							<a href="{{ url($row->link) }}" class="nav-link nav-toggle"> <i class="material-icons">{{ $row->icon}}</i>
								<span class="title">{{ $row->nama }}</span>
								@if($row->link == '#' || $row->link == '')
								<span class="arrow"></span>
								@endif
                            </a>
							<ul class="sub-menu">

							@foreach($row->subMenus->where('tampil',1) as $rowSub)
							@if(in_array($rowSub->id,$data_sections_arr))
								<li class="nav-item">
									<a href="{{url($rowSub->link)}}" class="nav-link "> <span class="title">{{ $rowSub->nama}}</span>
									</a>
								</li>
							@endif
                            @endforeach
							</ul>

						</li>
						@endif
						@endforeach

					</ul>
				</div>
			</div>
			</div>
		</div>
