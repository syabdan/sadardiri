<div class="page-footer">
        <div class="page-footer-inner">{{ date('Y')}} &copy; Universitas Islam Riau - 
            <a href="uir.ac.id" target="_blank" class="makerCss">UIR</a>
        </div>
        <div class="scroll-to-top">
            <i class="icon-arrow-up"></i>
        </div>
    </div>