function adanLoadingFadeIn() {
      loadingBlock();
    }
function adanLoadingFadeOut() {
      $.unblockUI();
    }
$(document).keyup(function(e) {
     if (e.keyCode == 27) { // escape key maps to keycode `27`
        adanLoadingFadeOut()
    }
});


function loadingBlock() { 
	$.blockUI({
		css: { 
			border: 'none', 
			padding: '10px', 
			width: '150px', 
			top:"40%",left:"45%",
			textAlign:"center",
			backgroundColor: '#000', 
			'-webkit-border-radius': '10px', 
			'-moz-border-radius': '10px', 
			opacity: .5, 
			color: '#fff' 
		},
		message: '<h4>Loading... <i class="fa fa-refresh fa-spin"></i></h4>',
		onOverlayClick: $.unblockUI
	}); 
}

$(document).ajaxError(function (event, jqxhr, settings, exception) {
	if (jqxhr.status == 500) {
		$(".alertbottom").fadeToggle(350);
	}
});

$(function () {
	"use strict";


	//Alerts
	$(".myadmin-alert .closed").click(function (event) {
		$(this).parents(".myadmin-alert").fadeToggle(350);
		return false;
	});
	

}); // End of use strict


