    <!-- start js include path -->
    <script src="{{ URL::to('backend') }}/assets/plugins/jquery/jquery.min.js" ></script>
    <script src="{{ URL::to('backend') }}/assets/plugins/popper/popper.min.js" ></script>
    <script src="{{ URL::to('backend') }}/assets/plugins/jquery-blockui/jquery.blockui.min.js" ></script>
	<script src="{{ URL::to('backend') }}/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- bootstrap -->
    <script src="{{ URL::to('backend') }}/assets/plugins/bootstrap/js/bootstrap.min.js" ></script>
    <script src="{{ URL::to('backend') }}/assets/plugins/sparkline/jquery.sparkline.min.js" ></script>
	<script src="{{ URL::to('backend') }}/assets/js/pages/sparkline/sparkline-data.js" ></script>
    <!-- Common js-->
	<script src="{{ URL::to('backend') }}/assets/js/app.js" ></script>
    <script src="{{ URL::to('backend') }}/assets/js/layout.js" ></script>
    <script src="{{ URL::to('backend') }}/assets/js/theme-color.js" ></script>
    <!-- material -->
    <script src="{{ URL::to('backend') }}/assets/plugins/material/material.min.js"></script>
    <!-- animation -->
    <script src="{{ URL::to('backend') }}/assets/js/pages/ui/animations.js" ></script>
    <!-- Sweet Alert -->
    <script src="{{ URL::to('backend') }}/assets/plugins/sweet-alert/sweetalert.min.js" ></script>
    <script src="{{ URL::to('backend') }}/assets/js/pages/sweet-alert/sweet-alert-data.js" ></script>
    <!-- end js include path -->
    
    
    <script src="{{ asset('resources/vendor/jquery/blockUI.js') }}"></script>
    <script src="{{ asset('resources/vendor/jquery/jquery.loadmodal.js') }}"></script>
    <script type="text/javascript" src="{{ URL::to('/') }}/adan/includes/jquery.js"></script>
