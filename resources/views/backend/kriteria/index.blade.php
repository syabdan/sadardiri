@extends('backend.layout')
@section('style')
    <!-- data tables -->
    <link href="{{ URL::to('backend') }}/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::to('resources') }}/vendor/datatables/css/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
    <!--select2-->
    <link href="{{ URL::to('backend') }}/assets/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="{{ URL::to('backend') }}/assets/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

    <link href="{{ URL::to('backend') }}/assets/plugins/summernote/summernote.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">

	<style>
    .template_faq {
    background: #edf3fe none repeat scroll 0 0;
    }
    .panel-group {
        background: #fff none repeat scroll 0 0;
        border-radius: 3px;
        box-shadow: 0 5px 30px 0 rgba(0, 0, 0, 0.04);
        margin-bottom: 0;
        padding: 30px;
    }
    #accordion .panel {
        border: medium none;
        border-radius: 0;
        box-shadow: none;
        margin: 0 0 15px 10px;
    }
    #accordion .panel-heading {
        border-radius: 30px;
        padding: 0;
    }
    #accordion .panel-title a {
        background: #fff2e5 none repeat scroll 0 0;
        border: 1px solid transparent;
        border-radius: 30px;
        color: #000;
        display: block;
        font-size: 18px;
        font-weight: 600;
        padding: 12px 20px 12px 50px;
        position: relative;
        transition: all 0.3s ease 0s;
    }
    #accordion .panel-title a.collapsed {
        background: #fff none repeat scroll 0 0;
        border: 1px solid #ddd;
        color: #333;
    }
    #accordion .panel-title a::after, #accordion .panel-title a.collapsed::after {
        background: #93e6a9 none repeat scroll 0 0;
        border: 1px solid transparent;
        border-radius: 50%;
        box-shadow: 0 3px 10px rgba(0, 0, 0, 0.58);
        color: #fff;
        content: "";
        font-family: fontawesome;
        font-size: 25px;
        height: 55px;
        left: -20px;
        line-height: 55px;
        position: absolute;
        text-align: center;
        top: -5px;
        transition: all 0.3s ease 0s;
        width: 55px;
    }
    #accordion .panel-title a.collapsed::after {
        background: #fff none repeat scroll 0 0;
        border: 1px solid #ddd;
        box-shadow: none;
        color: #333;
        content: "";
    }
    #accordion .panel-body {
        background: transparent none repeat scroll 0 0;
        border-top: none none;
        padding: 20px 25px 10px 9px;
        position: relative;
    }
    #accordion .panel-body p {
        border-left: 1px dashed #8c8c8c;
        padding-left: 25px;
    }
    </style>


@endsection
@section('content')
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">{{$title}}</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">{{$title}}</li>
                </ol>
            </div>
        </div>
        {!! Form::open(['id' => 'form-kriteria', 'class' => 'form-kriteria', 'files' => true ]) !!}

        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="card card-box">
                    <div class="card-body ">
                        <div class="row p-b-20">
                            <div class="col-md-12 col-sm-12 col-12">
                                <div class="btn-group float-right">
                                    <a class="btn btn-info"  id="button-publish-data">Publish <i class="fa fa-plane"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="section-title text-center wow zoomIn">
                                        <h2>{{$title}}</h2>
                                        <!-- <span></span> -->
                                        <h3>{{ $kriteria->keterangan}}</h3>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                        <?php $no=1; ?>
                                        <?php $nosub=1; ?>
                                        <?php $anak=0; ?>

                                            @foreach($pertanyaan->where('instrumen_id', $id)->whereNull('parent_id') as $ask)
                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading_{{ $ask->id}}">
                                                    <h4 class="panel-title">
                                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_{{ $ask->id}}" aria-expanded="{{ $no == 1 ? 'true' : 'false' }}" aria-controls="collapse_{{ $ask->id}}">

                                                        <?php $anak = count($ask->subPertanyaan) ?> @if($anak > 0) <h4> {{strip_tags($ask->isi_pertanyaan)}} <span class="mdl-badge " data-badge="{{$anak }}"></span> </h4>  @else <h5> {{strip_tags($ask->isi_pertanyaan)}}</h5> @endif
                                                            @if($ask->as_parent != 1) @if($ask->dataDiri['jawaban'] != NULL) {{ '' }} @else <span class="btn btn-sm btn-danger" >   belum dijawab</span> @endif @endif
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapse_{{ $ask->id}}" class="panel-collapse collapse {{ $no == 1 ? 'in' : 'collapse' }}" role="tabpanel" aria-labelledby="heading_{{ $ask->id}}">
                                                <?php $nosub=1; ?>

                                                    <!-- Code untuk isi capaian -->
                                                    @foreach($ask->subPertanyaan as $ask_sub)
                                                        <div class="card bg-light mb-3" >
                                                            <div class="card-header">Capaian {{ $nosub }}</div>
                                                            <div class="card-body">
                                                                <h3 class="card-title">{!! $ask_sub->isi_pertanyaan !!}</h3>

                                                                    @foreach($capaians->where('pertanyaan_id', $ask_sub->id) as $capaian)
                                                                    <div class="checkbox">
                                                                    <input type="hidden" value="{{$ask_sub->id}}" name="pertanyaan_id[]">

                                                                        <?php
                                                                            $list = $capaian->pilihan;
                                                                            for($i=0;$i<count($list);$i++){ ?>
                                                                                <label style="cursor:pointer;">
                                                                                    <input type="radio" value="{{ $list[$i]['nilai'] }}" class="hover" name="jawaban_nilai_{{$ask_sub->id}}" >
                                                                                    <input type="hidden" value="{{ $list[$i]['isi_pilihan'] }}" name="jawaban_isi_{{$ask_sub->id}}">
                                                                                    <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                                                    {{ $list[$i]['isi_pilihan'] }}
                                                                                </label><br>
                                                                        <?php
                                                                            }
                                                                        ?>
                                                                    </div>
                                                                    @endforeach

                                                                    <div class="form-group row">
                                                                        {!! Form::label('Link Bukti Dokumen', 'Link Bukti Dokumen', array('class' => 'col-md-2 control-label')) !!}
                                                                        <div class="col-md-10">
                                                                        {!! Form::text('url_file_datadiri_'.$ask_sub->id, NULL, array('id' => 'url_file_datadiri_'.$ask_sub->id, 'class' => 'form-control', 'size' => 16, 'placeholder' => 'eg. https://drive.google.com/file/d/1prmuzkpeRoD7UeqTvQSmWV__yAhjp96C/view?usp=sharing')) !!}
                                                                        </div>
                                                                    </div>
                                                            </div>




                                                                {{-- @if($ask_sub->status_unggah == 1)
                                                                <div class="card-body">
                                                                    <p>Dokumen yang telah di unggah : <span id="namafile_{{$ask_sub->id}}"></span> </p>
                                                                </div>
                                                                <div class="card-body dropzone" >
                                                                <center>
                                                                <span style="color:red">(file dokumen harus berekstensi .pdf )</span>
                                                                <input type="file" name="dokumen_{{$ask_sub->id}}" required accept="application/pdf" /><br>
                                                                <input type="hidden" name="dokumen_id[]" value="{{ $ask_sub->id }}" />
                                                                </center>
                                                                </div>
                                                                @endif --}}

                                                        </div>


                                                    <?php $nosub++; ?>
                                                    @endforeach
                                                    <!-- #Code untuk isi capaian -->


                                                </div>
                                            </div>



                                            <?php $no++; ?>
                                            @endforeach

                                        <input type="hidden" name="id_pertanyaan[]" value="{{ $ask_sub->id ?? ''}}">
                                        <input type="hidden" name="instrumen_id" value="{{ $kriteria->id ?? ''}}">
                                        <div class="card card-box">
                                            <div class="card-body ">
                                                <?php
                                                    $narasi     = array();
                                                    $narasi   = [
                                                                    '1' => 'Strategi',
                                                                    '2' => 'Mekanisme',
                                                                    '3' => 'Praktik Baik',
                                                                    '4' => 'Hambatan',
                                                                    '5' => 'Luaran'
                                                                ];
                                                for($a=1;$a<6;$a++){
                                                ?>
                                                <div class="panel panel-default">
                                                    <div class="panel-heading" role="tab" id="heading_{{ $a}}">
                                                        <h4 class="panel-title">
                                                            <a role="button" data-toggle="collapse" data-parent="#collapse_narasi_{{ $a}}" href="#collapse_narasi_{{ $a }}" aria-expanded="false" aria-controls="collapse_narasi_{{ $a}}">
                                                            Narasi <span style="color:red;">{{ $narasi[$a]}}</span>
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapse_narasi_{{ $a}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_narasi_{{ $a}}">
                                                        <div class="panel-body">
                                                            {{-- @if($data_narasi->where('instrumen_id', $kriteria->id)->count() > 0)
                                                                @foreach($data_narasi->where('instrumen_id', $kriteria->id) as $jawab)
                                                                    @php($jawaban = "narasi_$a")
                                                                    <textarea
                                                                    name="narasi_{{$a}}" id="narasi_{{$a}}" class="summernote" cols="30" rows="10">
                                                                    {{ $jawab->$jawaban }}
                                                                    </textarea>
                                                                @endforeach
                                                            @else --}}

                                                                <textarea
                                                                name="narasi_{{$a}}" id="" class="summernote" cols="30" rows="10">

                                                                </textarea>
                                                            {{-- @endif --}}
                                                        </div>
                                                    </div>
                                                </div>

                                                <?php $nosub++;
                                                }
                                                ?>
                                            <input type="hidden" name="instrumen_id" value="{{ $kriteria->id}}">

                                            </div>
                                        </div>

                                    </div>
                                </div><!--- END COL -->
                            </div><!--- END ROW -->
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row p-b-20">
            <div class="col-md-12 col-sm-12 col-12">
                <center>
                    <div class="btn-group">
                        <a class="btn btn-info"  id="button-simpan"><i class="fa fa-save"> </i> Simpan</a>
                    </div>
                </center>
            </div>
        </div>

        {!! Form::close() !!}

    </div>
</div>
<!-- end page content -->

@endsection

@section('script')
    <!-- data tables -->
    <script src="{{ URL::to('backend') }}/assets/plugins/datatables/jquery.dataTables.min.js" ></script>
    <script src="{{ URL::to('backend') }}/assets/plugins/datatables/editabletable.js" ></script>
	<script src="{{ URL::to('backend') }}/assets/js/pages/table/editable_table_data.js" ></script>
 	<script src="{{ URL::to('backend') }}/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
	<script src="{{ URL::to('backend') }}/assets/js/pages/table/table_data.js" ></script>
    <script src="{{ URL::to('resources') }}/vendor/datatables/js/dataTables.responsive.min.js" ></script>
    <script src="{{ URL::to('resources') }}/vendor/datatables/js/responsive.bootstrap4.min.js" ></script>

    <script src="{{ URL::to('/') }}/adan/pertanyaan/jquery.js"></script>

    <!-- summernote -->
    <script src="{{ URL::to('backend') }}/assets/plugins/summernote/summernote.min.js" ></script>
    <script src="{{ URL::to('backend') }}/assets/js/pages/summernote/summernote-data.js" ></script>
    <script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js" ></script>
    <script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker-init.js" ></script>

    <!-- bootstrap tree -->
    <script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-treeview/bootstrap-treeview.js" ></script>
    <script src="{{ URL::to('backend') }}/assets/js/pages/treeview/treeview-data.js" ></script>

    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
    <script>
    $('.summernote').summernote({
        placeholder: '',
        tabsize: 2,
        height: 200,
        toolbar: [
                    ['font', ['bold', 'italic', 'underline', 'clear']],
                    ['fontname', ['fontname']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']],
                    ['help', ['help']]
                ],
        callbacks: {
            // onKeyup: function(e) {
            // var name =   $(this).attr('name');
            // var data =   $(this).val();
    		// var _token = "{{ csrf_token() }}";

            // setTimeout(function(){
            //     $.ajax({
            //         type: "POST",
            //         url: "{{ url($url_admin.'instrumen/simpan_capaian') }}",
            //         data: {'id_jawaban' : name, 'jawaban': data, '_token': _token},
            //         dataType: 'json',
            //         cache: false,

            //         success: function(data){
            //         },error:function(x, e){

            //         }
            //     })
            // },10000);
            // }
        }
      });


    $(document).ready(function (){

    // Disable after publish
    // var _token = "{{ csrf_token() }}";
    //   $.ajax({
    //     type: "POST",
    //     url: "{{ url($url_admin.'/instrumen/disable') }}",
    //     data: { '_token': _token},
    //     dataType: 'json',
    //     cache: false,

    //         success: function(data){
    //             $.each(data, function(key, value)
    //             {
    //                 if(value.publish == 1){
    //                     $('#narasi_'+value.id).summernote('disable');
    //                 }
    //             });
    //         }
    //     });
    // #Disable after publish

    // ambil jawaban
    var _token = "{{ csrf_token() }}";
      $.ajax({
        type: "POST",
        url: "{{ url($url_admin.'/instrumen/ambil_pilihan') }}",
        data: { '_token': _token},
        dataType: 'json',
        cache: false,

            success: function(data){
                $.each(data, function(key, value)
                {

                    $("input[name=jawaban_nilai_"+value.pertanyaan_id+"][value=" + value.nilai_id + "]").prop('checked', true);
                    $("#url_file_datadiri_"+value.pertanyaan_id).val(value.url_file_datadiri);

                    // var url = "{{ url('ambil_file') }}";
                    // $('#namafile_'+value.pertanyaan_id).html("<a target='_blank' href="+url+'/'+value.file_data_diri+">"+value.file_data_diri+"</a>");
                });
            }
        });
    // #ambil


    // ambil narasi
    $.ajax({
        type: "GET",
        url: "{{ url($url_admin.'/instrumen/ambil_narasi') }}"+"/"+{{ $id }},
        data: { '_token': _token},
        dataType: 'json',
        cache: false,

            success: function(data){
                @for($i=1;$i<6;$i++)
                var narasi = 'narasi_'+{{ $i }};
                    $("textarea[name="+narasi+"]").summernote('code', data.narasi_{{ $i }});
                @endfor
            }
        });
    // #ambil narasi
    });


    $('#button-publish-data').on("click", function(){
        swal({
            title: 'Apakah anda yakin untuk mempublish data ini ?',
			text: "Setelah Anda mempublish, data tidak akan dapat diubah kembali ...!",
			type: 'warning',
			showCancelButton: true,
			cancelButtonColor: '#d33',
			confirmButtonColor: '#3085d6',
			confirmButtonText: 'Ya, Publish',
			cancelButtonText: 'Tidak',
            closeOnConfirm: false,
            closeOnCancel: true
    }, function (isConfirm) {
            if (isConfirm) {
                var publish = "{{ $publish }}";
				if(publish > 0){
					swal({
						title: 'Maaf...',
						text: 'Harap menginput data untuk semua Narasi pada Kriteria ini !',
						type: 'error',
						showConfirmButton: true
					});
				} else {
					url = "{{ url('instrumen/publish') }}";
                    var _token = "{{ csrf_token() }}";
					$.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
						url : url,
						type : "POST",
                        // data: { '_token': _token, 'id_pertanyaan' : id_pertanyaan},
                        data: $(".form-kriteria").serializeArray(),
						success : function(data) {
							if(data.status == true){
								swal({
								type: "success",
								title: 'Berhasil...',
								text: 'Data telah dipublish..!',
								showConfirmButton: false
								});
								setTimeout(function(){
									window.location.reload();
								}, 1500);
							} else {
								swal({
								type: "error",
								title: 'Gagal...',
								text: 'Maaf, terjadi kesalahan..!',
								showConfirmButton: false
								});
								setTimeout(function(){
									window.location.reload();
								}, 1500);
							}

						},
						error : function(){
							alert('gagal');
						}
					});
				}
            }
        });
	});

    $('#button-simpan').on("click", function(event){
        swal({
            title: 'Apakah anda yakin untuk menyimpan data ini ?',
			text: "Silahkan klik tombol 'Ya' untuk menyimpan data",
			type: 'warning',
			showCancelButton: true,
			cancelButtonColor: '#d33',
			confirmButtonColor: '#3085d6',
			confirmButtonText: 'Ya, Simpan',
			cancelButtonText: 'Tidak',
            closeOnConfirm: false,
            closeOnCancel: true
    }, function (isConfirm) {
            if (isConfirm) {
	        {{-- event.preventDefault(); --}}
                var url = "{{ url('instrumen/simpan_capaian') }}";
                var form = $('#form-kriteria')[0];
                var formData = new FormData(form);
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url : url,
					type : "POST",
                    enctype: 'multipart/form-data',
                    data : formData,
                    processData: false,
                    contentType: false,
                    cache: false,
                    dataType : 'JSON',
                    success : function(data) {
                            swal({
                            type: "success",
                            title: 'Berhasil...',
                            text: 'Data telah disimpan..!',
                            showConfirmButton: false
                            });
                            setTimeout(function(){
                                window.location.reload();
                            }, 1500);
                    },
                    error : function(){
                        alert('gagal');
                    }
                });

            }
        });
	});


    $(document).on("change", "input[type=checkbox]", function(){

        //Jika Ingin ambil jawaban sebelumnya
        var check = $("input[name='check_ambil_lagi']:checked").val();
        if(check== 'on'){
            var _token            = "{{ csrf_token() }}";
            var  id_pertanyaan    =   $(this).attr('id');

        $.ajax({
            type: "POST",
            url: "{{ url($url_admin.'/instrumen/ambil_jawaban') }}",
            data: { '_token': _token, 'id_pertanyaan' : id_pertanyaan},
            dataType: 'json',
            cache: false,

                success: function(data){
                    $("textarea[name="+data.pertanyaan_id+"]").summernote('code', data.jawaban);
                }
            });
        }else{
        // alert('else ini');
            $('.check_ambil_lagi').html('');
        }
    });


    </script>

@endsection
