$(document).ready(function(){
   $('.tambah').click(function(){
		adanLoadingFadeIn();
		$.loadmodal({
			url: "{{ url($url_admin.'/narasi-instrumen/create') }}",
			id: 'responsive',
			dlgClass: 'fade',
			bgClass: 'primary',
			title: 'Tambah',
			width: 'whatever',
			modal: {
				keyboard: true,
				// any other options from the regular $().modal call (see Bootstrap docs)
				},
          ajax: {
				dataType: 'html',
				method: 'GET',
				success: function(data, status, xhr){
            adanLoadingFadeOut();
				},

			},
        });
  });

	$(document).on("click",".ubah",function() {
    adanLoadingFadeIn();
		var id = $(this).attr('data-id');
		$.loadmodal({
			url: "{{ url($url_admin.'/narasi-instrumen') }}/"+ id +"/edit",
      id: 'responsive',
			dlgClass: 'fade',
			bgClass: 'warning',
			title: 'Ubah',
			width: 'whatever',
			modal: {
				keyboard: true,
				// any other options from the regular $().modal call (see Bootstrap docs)
				},
          ajax: {
				dataType: 'html',
				method: 'GET',
				success: function(data, status, xhr){
            adanLoadingFadeOut();
				},
			},
        });
	});

	$(document).on("click",".hapus",function() {
    adanLoadingFadeIn();
		var id = $(this).attr('data-id');
		$.loadmodal({
			url: "{{ url($url_admin.'/narasi-instrumen/hapus') }}/"+ id,
      id: 'responsive',
			dlgClass: 'fade',
			bgClass: 'danger',
			title: 'Hapus',
			width: '300px',
			modal: {
				keyboard: true,
				// any other options from the regular $().modal call (see Bootstrap docs)
				},
          ajax: {
				dataType: 'html',
				method: 'GET',
				success: function(data, status, xhr){
            adanLoadingFadeOut();
				},
			},
        });
	});



});
