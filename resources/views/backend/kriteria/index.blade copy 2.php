@extends('backend.layout')
@section('style')
    <!-- data tables -->
    <link href="{{ URL::to('backend') }}/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::to('resources') }}/vendor/datatables/css/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
    <!--select2-->
    <link href="{{ URL::to('backend') }}/assets/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="{{ URL::to('backend') }}/assets/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

    <link href="{{ URL::to('backend') }}/assets/plugins/summernote/summernote.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">

	<style>
    .template_faq {
    background: #edf3fe none repeat scroll 0 0;
    }
    .panel-group {
        background: #fff none repeat scroll 0 0;
        border-radius: 3px;
        box-shadow: 0 5px 30px 0 rgba(0, 0, 0, 0.04);
        margin-bottom: 0;
        padding: 30px;
    }
    #accordion .panel {
        border: medium none;
        border-radius: 0;
        box-shadow: none;
        margin: 0 0 15px 10px;
    }
    #accordion .panel-heading {
        border-radius: 30px;
        padding: 0;
    }
    #accordion .panel-title a {
        background: #d9d20d none repeat scroll 0 0;
        border: 1px solid transparent;
        border-radius: 30px;
        color: #fff;
        display: block;
        font-size: 18px;
        font-weight: 600;
        padding: 12px 20px 12px 50px;
        position: relative;
        transition: all 0.3s ease 0s;
    }
    #accordion .panel-title a.collapsed {
        background: #fff none repeat scroll 0 0;
        border: 1px solid #ddd;
        color: #333;
    }
    #accordion .panel-title a::after, #accordion .panel-title a.collapsed::after {
        background: #93e6a9 none repeat scroll 0 0;
        border: 1px solid transparent;
        border-radius: 50%;
        box-shadow: 0 3px 10px rgba(0, 0, 0, 0.58);
        color: #fff;
        content: "";
        font-family: fontawesome;
        font-size: 25px;
        height: 55px;
        left: -20px;
        line-height: 55px;
        position: absolute;
        text-align: center;
        top: -5px;
        transition: all 0.3s ease 0s;
        width: 55px;
    }
    #accordion .panel-title a.collapsed::after {
        background: #fff none repeat scroll 0 0;
        border: 1px solid #ddd;
        box-shadow: none;
        color: #333;
        content: "";
    }
    #accordion .panel-body {
        background: transparent none repeat scroll 0 0;
        border-top: none none;
        padding: 20px 25px 10px 9px;
        position: relative;
    }
    #accordion .panel-body p {
        border-left: 1px dashed #8c8c8c;
        padding-left: 25px;
    }
    </style>
@endsection
@section('content')
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">{{$title}}</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">{{$title}}</li>
                </ol>
            </div>
        </div>	
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="card card-box">
                    <div class="card-body ">
                        <div class="row p-b-20">
                            <div class="col-md-12 col-sm-12 col-12">
                                <div class="btn-group float-right">
                                    <a class="btn btn-info"  id="button-publish-data">Publish <i class="fa fa-plane"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="section-title text-center wow zoomIn">
                                        <h1>Data {{$title}}</h1>
                                        <span></span>
                                        <p>Silahkan jawab pertanyaan dibawah ini.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="row">				
                                <div class="col-md-12">
                                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                        <?php $no=1; ?>
                                        <?php $nosub=1; ?>
                                        <?php $anak=0; ?>
                                        
                                        @foreach($pertanyaan->where('instrumen_id', $id)->whereNull('parent_id') as $ask)
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="heading_{{ $ask->id}}">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_{{ $ask->id}}" aria-expanded="{{ $no == 1 ? 'true' : 'false' }}" aria-controls="collapse_{{ $ask->id}}">
                                                    
                                                    <?php $anak = count($ask->subPertanyaan) ?> @if($anak > 0) <h4> {{$ask->isi_pertanyaan}} <span class="mdl-badge " data-badge="{{$anak }}"></span> </h4>  @else <h4> {{$ask->isi_pertanyaan}}</h4> @endif
                                                        @if($ask->as_parent != 1) @if($ask->dataDiri['jawaban'] != NULL) {{ '' }} @else <span class="btn btn-sm btn-danger" >   belum dijawab</span> @endif @endif
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapse_{{ $ask->id}}" class="panel-collapse collapse {{ $no == 1 ? 'in' : 'collapse' }}" role="tabpanel" aria-labelledby="heading_{{ $ask->id}}">
                                            @if(($ask->as_parent) != 1)
                                            <div class="panel-body">
                                            @if($data_jawaban->where('pertanyaan_id', $ask->id)->count() > 0)
                                                @foreach($data_jawaban->where('pertanyaan_id', $ask->id) ?? '' as $jawaban)
                                                    <textarea  
                                                    name="{{$ask->id}}" id="jawaban_{{$jawaban->id}}"  class="summernote" cols="30" rows="10">
                                                    {{ $jawaban->jawaban }}
                                                    </textarea>
                                                 @endforeach 
                                            @else
                                                    <textarea  
                                                    name="{{$ask->id}}"   class="summernote" cols="30" rows="10">
                                                    </textarea>
                                            @endif
                                                    @if($ask->status_ambil_lagi == 1) 
                                                        <div class="col-md-12">
                                                            <div class="checkbox checkbox-icon-aqua">
                                                                <input id="checkbox_{{$ask->id }}" type="checkbox" name="check_ambil_lagi" class="check_ambil_lagi" data-toggle="toggle" data-onstyle="outline-info" data-offstyle="outline-danger">
                                                                <label for="checkbox4">
                                                                    Ambil Jawaban Tahun Sebelumnya
                                                                </label>
                                                            </div>
                                                        </div>
                                                    @endif
                                                    <br>
                                                    @if($ask->status_unggah == 1) 
                                                    <div class="col-md-8" >
                                                        {!! Form::open([ 'route' => 'instrumen.simpan_dokumen', 'files' => true, 'enctype' => 'multipart/form-data', 'class' => 'dropzone', 'id' => 'image-upload_'.$ask->id , 'method' => 'PUT']) !!}
                                                        {{ csrf_field() }}   
                                                        <span style="color:red">(file dokumen harus berekstensi .pdf )</span>
                                                        <input type="file" value="" name="dokumen" required accept="application/pdf">
                                                        <input type="hidden" name="dokumen_id" value="{{ $ask->id }}">
                                                        <button type="submit" class="btn btn-info btn-large"><i class="fa fa-save"></i> Upload</button>

                                                        {!! Form::close() !!}
                                                    </div>
                                                    @endif
                                            </div>
                                            @else
                                            <div class="card card-box">
                                            <div class="card-body ">
                                                @foreach($ask->subPertanyaan as $ask_sub)
                                                <div class="panel panel-default">
                                                    <div class="panel-heading" role="tab" id="heading_{{ $ask_sub->id}}">
                                                        <h4 class="panel-title">
                                                            <a role="button" data-toggle="collapse" data-parent="#collapse_{{ $ask->id}}" href="#collapse_{{ $ask_sub->id}}" aria-expanded="{{ $nosub == 1 ? 'true' : 'false' }}" aria-controls="collapse_{{ $ask_sub->id}}">
                                                            <h4>{{$ask_sub->isi_pertanyaan}} </h4>@if($ask_sub->dataDiri['jawaban'] != NULL ) {{ '' }} @else <span class="btn btn-sm btn-danger" >   belum dijawab</span> @endif
                                                            </a>
                                                        </h4>
                                                    </div>
                                                    <div id="collapse_{{ $ask_sub->id}}" class="panel-collapse {{ $nosub == 1 ? 'in' : 'collapse' }}" role="tabpanel" aria-labelledby="heading_{{ $ask_sub->id}}">
                                                        <div class="panel-body">
                                                            @if($data_jawaban->where('pertanyaan_id', $ask_sub->id)->count() > 0)
                                                                @foreach($data_jawaban->where('pertanyaan_id', $ask_sub->id) as $jawaban)
                                                                    <textarea  
                                                                    name="{{$ask_sub->id}}" id="jawaban_{{$jawaban->id}}"  class="summernote" cols="30" rows="10">
                                                                    {{ $jawaban->jawaban }}
                                                                    </textarea>
                                                                @endforeach 
                                                            @else
                                                                <textarea  
                                                                name="{{$ask_sub->id}}" id=""  class="summernote" cols="30" rows="10">
                                                                </textarea>
                                                            @endif
                                                                @if($ask_sub->status_unggah == 1) 
                                                                <div class="col-md-8">
                                                                    {!! Form::open([ 'route' => 'instrumen.simpan_dokumen', 'files' => true, 'enctype' => 'multipart/form-data', 'class' => 'dropzone', 'id' => 'image-upload_'.$ask_sub->id , 'method' => 'PUT']) !!}
                                                                    {{ csrf_field() }}    
                                                                    <span style="color:red">(file dokumen harus berekstensi .pdf )</span>
                                                                    <input type="file" value="" name="dokumen" required accept="application/pdf">
                                                                    <input type="hidden" name="dokumen_id" value="{{ $ask_sub->id }}">
                                                                    <button type="submit" class="btn btn-info btn-large"><i class="fa fa-save"></i> Upload</button>

                                                                    {!! Form::close() !!}
                                                                </div>
                                                                @endif

                                                                @if($ask->status_ambil_lagi == 1) 
                                                                    <div class="col-md-12">
                                                                        <div class="checkbox checkbox-icon-aqua">
                                                                            <input id="checkbox_{{$ask_sub->id }}" type="checkbox" name="check_ambil_lagi" class="check_ambil_lagi">
                                                                            <label for="checkbox4">
                                                                                Ambil Jawaban Tahun Sebelumnya
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <form action="" class="form-instrumen">
                                                    <input type="hidden" name="id_pertanyaan[]" value="{{ $ask_sub->id }}">
                                                </form>
                                                <?php $nosub++; ?>
                                                @endforeach
                                            </div>
                                            </div>
                                            @endif
                                            </div>
                                        </div>
                                        <form action="" class="form-instrumen">
                                            <input type="hidden" name="id_pertanyaan[]" value="{{ $ask->id }}">
                                        </form>
                                        <?php $no++; ?>
                                        @endforeach
                                    </div>
                                </div><!--- END COL -->		
                            </div><!--- END ROW -->			
                        </div>
                    </div>
                </div>
            </div>
        </div>     
    </div>
</div>
<!-- end page content -->

@endsection

@section('script')
    <!-- data tables -->
    <script src="{{ URL::to('backend') }}/assets/plugins/datatables/jquery.dataTables.min.js" ></script>
    <script src="{{ URL::to('backend') }}/assets/plugins/datatables/editabletable.js" ></script>
	<script src="{{ URL::to('backend') }}/assets/js/pages/table/editable_table_data.js" ></script>
 	<script src="{{ URL::to('backend') }}/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
	<script src="{{ URL::to('backend') }}/assets/js/pages/table/table_data.js" ></script>
    <script src="{{ URL::to('resources') }}/vendor/datatables/js/dataTables.responsive.min.js" ></script>
    <script src="{{ URL::to('resources') }}/vendor/datatables/js/responsive.bootstrap4.min.js" ></script>

    <script src="{{ URL::to('/') }}/adan/pertanyaan/jquery.js"></script>

    <!-- summernote -->
    <script src="{{ URL::to('backend') }}/assets/plugins/summernote/summernote.min.js" ></script>
    <script src="{{ URL::to('backend') }}/assets/js/pages/summernote/summernote-data.js" ></script>
    <script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js" ></script>
    <script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker-init.js" ></script>
    
    <!-- bootstrap tree -->
    <script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-treeview/bootstrap-treeview.js" ></script>
    <script src="{{ URL::to('backend') }}/assets/js/pages/treeview/treeview-data.js" ></script>

    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
    <script>
    $('.summernote').summernote({
        placeholder: '',
        tabsize: 2,
        height: 200,
        callbacks: {
            onKeyup: function(e) {
            var name =   $(this).attr('name');
            var data =   $(this).val();
    		var _token = "{{ csrf_token() }}";

            setTimeout(function(){
            // $("#result").html($('.description').val());
            // alert('namenya '+name+ ' datanya '+data);
                $.ajax({
                    type: "POST",
                    url: "{{ url($url_admin.'instrumen/simpan_jawaban') }}",
                    data: {'id_jawaban' : name, 'jawaban': data, '_token': _token},
                    dataType: 'json',
                    cache: false,
                
                    success: function(data){
                    // alert('tersimpan');
                    },error:function(x, e){

                    }
                })
            },10000);
            }
        }
      });

   
    $(document).ready(function (){
    var _token = "{{ csrf_token() }}";
      $.ajax({
        type: "POST",
        url: "{{ url($url_admin.'/instrumen/disable') }}",
        data: { '_token': _token},
        dataType: 'json',
        cache: false,
    
            success: function(data){
                $.each(data, function(key, value) 
                {
                    if(value.publish == 1){
                        $('#jawaban_'+value.id).summernote('disable');
                    }
                });
            }
        });
    });


    $('#button-publish-data').on("click", function(){
        swal({
            title: 'Apakah anda yakin untuk mempublish data ini ?',
			text: "Setelah Anda mempublish, data tidak akan dapat diubah kembali ...!",
			type: 'warning',
			showCancelButton: true,
			cancelButtonColor: '#d33',
			confirmButtonColor: '#3085d6',
			confirmButtonText: 'Ya, Publish',
			cancelButtonText: 'Tidak',
            closeOnConfirm: false,
            closeOnCancel: true
    }, function (isConfirm) {
            if (isConfirm) {
                var publish = "{{ $publish }}";
				if(publish > 0){
					swal({
						title: 'Maaf...',
						text: 'Harap menginput data untuk semua Narasi pada Kriteria ini !',
						type: 'error',
						showConfirmButton: true
					});
				} else {
					url = "{{ url('instrumen/publish') }}";
                    var _token = "{{ csrf_token() }}";
					$.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
						url : url,
						type : "POST",
                        // data: { '_token': _token, 'id_pertanyaan' : id_pertanyaan},
                        data: $(".form-instrumen").serializeArray(),
						success : function(data) {
							if(data.status == true){
								swal({
								type: "success",
								title: 'Berhasil...',
								text: 'Data telah dipublish..!',
								showConfirmButton: false
								});
								setTimeout(function(){
									window.location.reload();
								}, 1500);
							} else {
								swal({
								type: "error",
								title: 'Gagal...',
								text: 'Maaf, terjadi kesalahan..!',
								showConfirmButton: false
								});
								setTimeout(function(){
									window.location.reload();
								}, 1500);
							}
							
						},
						error : function(){
							alert('gagal');
						}
					});
				}
            }
        });
	});


    $(document).on("change", "input[type=checkbox]", function(){

        //Jika Ingin ambil jawaban sebelumnya
        var check = $("input[name='check_ambil_lagi']:checked").val();
        if(check== 'on'){
            var _token            = "{{ csrf_token() }}";
            var  id_pertanyaan    =   $(this).attr('id');

        $.ajax({
            type: "POST",
            url: "{{ url($url_admin.'/instrumen/ambil_jawaban') }}",
            data: { '_token': _token, 'id_pertanyaan' : id_pertanyaan},
            dataType: 'json',
            cache: false,
        
                success: function(data){ 
                    $("textarea[name="+data.pertanyaan_id+"]").summernote('code', data.jawaban);
                }
            });
        }else{
        // alert('else ini');
            $('.check_ambil_lagi').html('');
        }
    });
		
    // var timeoutId;
    // $('form input, form textarea').on('input propertychange change', function() {
    //     console.log('Textarea Change');
        
    //     clearTimeout(timeoutId);
    //     timeoutId = setTimeout(function() {
    //         // Runs 1 second (1000 ms) after the last change    
    //         saveToDB();
    //     }, 1000);
    // });

    // function saveToDB()
    // {
    //     console.log('Saving to the db');
        
    //     // Now show them we saved and when we did
    //     var d = new Date();
    //     $('.form-status-holder').html('Saved! Last: ' + d.toLocaleTimeString());
    // }

    </script>

@endsection