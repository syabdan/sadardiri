@extends('backend.layout')
@section('style')
    <!-- data tables -->
    <link href="{{ URL::to('backend') }}/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::to('resources') }}/vendor/datatables/css/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
    <!--select2-->
    <link href="{{ URL::to('backend') }}/assets/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
    <link href="{{ URL::to('backend') }}/assets/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

    <link href="{{ URL::to('backend') }}/assets/plugins/summernote/summernote.css" rel="stylesheet">
	
@endsection
@section('content')

<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">{{$title}}</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">{{$title}}</li>
                </ol>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="card  card-box">
                    <div class="card-head">
                        <header>Instrumen {{$title}}</header>
                        <button type="button" class="btn btn-success submit_publish" id="button-publish-data" style="float: right;"><i class="fa fa-save"></i> Publish</button>
                        
                    </div>

                    <div class="card-body ">

                            <?php $no=1; ?>
                            <?php $no_sub=1; ?>
                            @foreach($pertanyaan->where('instrumen_id', $id)->whereNull('parent_id') as $ask)
                            <div class="panel-group accordion" id="accordion{{$id}}">
                                <div class="panel faq panel-default">
                                    <div class="panel-heading faq-heading panel-heading-gray">
                                        <h5 class="panel-title">
                                            <a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion{{$id}}" href="#collapse_{{ $ask->id}}">{{$ask->isi_pertanyaan}}</a>
                                        </h5>
                                    </div>
                                    <div id="collapse_{{ $ask->id}}" class="panel-collapse  {{ $no == 1 ? 'in' : 'collapse' }}">     
                                        @if(($ask->as_parent) != 1)
                                        <div class="panel-body">
                                        @foreach($data_jawaban->where('pertanyaan_id', $ask->id) as $jawaban)
                                            <textarea  
                                            name="{{$ask->id}}" id="jawaban_{{$jawaban->id}}"  class="summernote" cols="30" rows="10">
                                            {{ $jawaban->jawaban }}
                                            </textarea>
                                            @if($ask->status_unggah == 1) 
                                            <div class="col-md-12">
                                                {!! Form::open([ 'route' => 'instrumen.simpan_dokumen', 'files' => true, 'enctype' => 'multipart/form-data', 'class' => 'dropzone', 'id' => 'image-upload_'.$ask->id , 'method' => 'PUT']) !!}
                                                {{ csrf_field() }}    
                                                <input type="file" value="" name="dokumen">
                                                <input type="hidden" name="dokumen_id" value="{{ $ask->id }}">
                                                <button type="submit">Upload</button>

                                                {!! Form::close() !!}
                                            </div>
                                            @endif

                                            @if($ask->status_ambil_lagi == 1) 
                                                <div class="col-md-12">
                                                    <div class="checkbox checkbox-icon-aqua">
                                                        <input id="checkbox_{{$ask->id }}" type="checkbox" name="check_ambil_lagi" class="check_ambil_lagi">
                                                        <label for="checkbox4">
                                                            Ambil Jawaban Tahun Sebelumnya
                                                        </label>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                        
                                        </div>
                                        @endif
                                        <div class="card-body ">

                                            @foreach($ask->subPertanyaan as $ask_sub)
                                            <div class="panel-group accordion" id="accordion{{$ask_sub->id}}">
                                                <div class="panel faq panel-default">
                                                    <div class="panel-heading faq-heading panel-heading-gray">
                                                        <h5 class="panel-title">
                                                            <a class="accordion-toggle accordion-toggle-styled" data-toggle="collapse" data-parent="#accordion{{$ask->id}}" href="#collapse_sub_{{ $ask_sub->id}}">{{$ask_sub->isi_pertanyaan}}</a>
                                                        </h5>
                                                        <span>
                                                        @foreach($data_jawaban->where('pertanyaan_id', $ask_sub->id) as $jawaban)
                                                        @if($jawaban->jawaban == NULL) {{ 'belum diisi' }} @endif
                                                        @endforeach
                                                        </span>
                                                    </div>
                                                    <div id="collapse_sub_{{$ask_sub->id}}" class="panel-collapse {{$no_sub == 1 ? 'in' : 'collapse' }}">
                                                        @foreach($data_jawaban->where('pertanyaan_id', $ask_sub->id) as $jawaban)
                                                        <div class="panel-body">
                                                            <textarea name="{{$ask_sub->id}}" id="jawaban_{{$jawaban->id}}" class="summernote" cols="30" rows="10" >
                                                            {{ $jawaban->jawaban }}
                                                            </textarea>
                                                            
                                                            @if($ask_sub->status_unggah == 1) 
                                                            <div class="col-md-12">
                                                                {!! Form::open([ 'route' => 'instrumen.simpan_dokumen', 'files' => true, 'enctype' => 'multipart/form-data', 'class' => 'dropzone',  'method' => 'PUT' ]) !!}
                                                                {{ csrf_field() }}     
                                                                    <input type="file" value="" name="dokumen" required>
                                                                    <input type="hidden" name="dokumen_id" value="{{ $ask_sub->id }}">
                                                                    <button type="submit" id="button_{{ $ask_sub->id}}">Upload </button>
                                                                {!! Form::close() !!} 
                                                            </div>
                                                            @endif
                                                            
                                                            @if($ask_sub->status_ambil_lagi == 1) 
                                                            <div class="col-md-12">
                                                                <div class="checkbox checkbox-icon-aqua">
                                                                    <input id="checkbox_{{$ask_sub->id }}" type="checkbox" name="check_ambil_lagi" class="check_ambil_lagi">
                                                                    <label for="checkbox4">
                                                                        Ambil Jawaban Tahun Sebelumnya
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            @endif
                                                        </div>
                                                        @endforeach

                                                    </div>
                                                </div>
                                                <form action="" class="form-instrumen">
                                                    <input type="hidden" name="id_pertanyaan[]" value="{{ $ask_sub->id }}">
                                                </form>

                                                <?php $no_sub++; ?>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <form action="" class="form-instrumen">
                                    <input type="hidden" name="id_pertanyaan[]"  value="{{ $ask->id }}">
                                </form>

                                <?php $no++; ?>
                            </div>

                            @endforeach
                    </div>

                </div>
            </div>
        </div>
</div>
        </div>
    </div>
</div>
        <!-- end page content -->
@endsection

@section('script')
    <!-- data tables -->
    <script src="{{ URL::to('backend') }}/assets/plugins/datatables/jquery.dataTables.min.js" ></script>
    <script src="{{ URL::to('backend') }}/assets/plugins/datatables/editabletable.js" ></script>
	<script src="{{ URL::to('backend') }}/assets/js/pages/table/editable_table_data.js" ></script>
 	<script src="{{ URL::to('backend') }}/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
	<script src="{{ URL::to('backend') }}/assets/js/pages/table/table_data.js" ></script>
    <script src="{{ URL::to('resources') }}/vendor/datatables/js/dataTables.responsive.min.js" ></script>
    <script src="{{ URL::to('resources') }}/vendor/datatables/js/responsive.bootstrap4.min.js" ></script>

    <script src="{{ URL::to('/') }}/adan/pertanyaan/jquery.js"></script>

    <!-- summernote -->
    <script src="{{ URL::to('backend') }}/assets/plugins/summernote/summernote.min.js" ></script>
    <script src="{{ URL::to('backend') }}/assets/js/pages/summernote/summernote-data.js" ></script>
    <script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js" ></script>
    <script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker-init.js" ></script>
    
    
    <script>
    $('.summernote').summernote({
        placeholder: '',
        tabsize: 2,
        height: 200,
        callbacks: {
            onKeyup: function(e) {
            var name =   $(this).attr('name');
            var data =   $(this).val();
    		var _token = "{{ csrf_token() }}";

            setTimeout(function(){
            // $("#result").html($('.description').val());
            // alert('namenya '+name+ ' datanya '+data);
                $.ajax({
                    type: "POST",
                    url: "{{ url($url_admin.'instrumen/simpan_jawaban') }}",
                    data: {'id_jawaban' : name, 'jawaban': data, '_token': _token},
                    dataType: 'json',
                    cache: false,
                
                    success: function(data){
                    // alert('tersimpan');
                    },error:function(x, e){

                    }
                })
            },1000);
            }
        }
      });

   
    $(document).ready(function (){
    var _token = "{{ csrf_token() }}";
      $.ajax({
        type: "POST",
        url: "{{ url($url_admin.'/instrumen/disable') }}",
        data: { '_token': _token},
        dataType: 'json',
        cache: false,
    
            success: function(data){
                $.each(data, function(key, value) 
                {
                    if(value.publish == 1){
                    dd(value.id);

                        $('#jawaban_'+value.id).summernote('disable');
                    }
                });
            }
        });
    });


    $('#button-publish-data').on("click", function(){
        swal({
            title: 'Apakah anda yakin untuk mempublish data ini ?',
			text: "Setelah Anda mempublish, data tidak akan dapat diubah kembali ...!",
			type: 'warning',
			showCancelButton: true,
			cancelButtonColor: '#d33',
			confirmButtonColor: '#3085d6',
			confirmButtonText: 'Ya, Publish',
			cancelButtonText: 'Tidak',
            closeOnConfirm: false,
            closeOnCancel: true
    }, function (isConfirm) {
            if (isConfirm) {
                var publish = "{{ $publish }}";
				if(publish > 0){
					swal({
						title: 'Maaf...',
						text: 'Harap menginput data untuk semua Narasi pada Kriteria ini !',
						type: 'error',
						showConfirmButton: true
					});
				} else {
					url = "{{ url('instrumen/publish') }}";
                    var _token = "{{ csrf_token() }}";
					$.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
						url : url,
						type : "POST",
                        // data: { '_token': _token, 'id_pertanyaan' : id_pertanyaan},
                        data: $(".form-instrumen").serializeArray(),
						success : function(data) {
							if(data.status == true){
								swal({
								type: "success",
								title: 'Berhasil...',
								text: 'Data telah dipublish..!',
								showConfirmButton: false
								});
								setTimeout(function(){
									window.location.reload();
								}, 1500);
							} else {
								swal({
								type: "error",
								title: 'Gagal...',
								text: 'Maaf, terjadi kesalahan..!',
								showConfirmButton: false
								});
								setTimeout(function(){
									window.location.reload();
								}, 1500);
							}
							
						},
						error : function(){
							alert('gagal');
						}
					});
				}
            }
        });
	});


    $(document).on("change", "input[type=checkbox]", function(){

        //Jika Ingin ambil jawaban sebelumnya
        var check = $("input[name='check_ambil_lagi']:checked").val();
        if(check== 'on'){
            var _token            = "{{ csrf_token() }}";
            var  id_pertanyaan    =   $(this).attr('id');

        $.ajax({
            type: "POST",
            url: "{{ url($url_admin.'/instrumen/ambil_jawaban') }}",
            data: { '_token': _token, 'id_pertanyaan' : id_pertanyaan},
            dataType: 'json',
            cache: false,
        
                success: function(data){ 
                    $("textarea[name="+data.pertanyaan_id+"]").summernote('code', data.jawaban);
                }
            });
        }else{
        alert('else ini');

            $('.check_ambil_lagi').html('');
        }
    });
		
    // var timeoutId;
    // $('form input, form textarea').on('input propertychange change', function() {
    //     console.log('Textarea Change');
        
    //     clearTimeout(timeoutId);
    //     timeoutId = setTimeout(function() {
    //         // Runs 1 second (1000 ms) after the last change    
    //         saveToDB();
    //     }, 1000);
    // });

    // function saveToDB()
    // {
    //     console.log('Saving to the db');
        
    //     // Now show them we saved and when we did
    //     var d = new Date();
    //     $('.form-status-holder').html('Saved! Last: ' + d.toLocaleTimeString());
    // }

    </script>

@endsection