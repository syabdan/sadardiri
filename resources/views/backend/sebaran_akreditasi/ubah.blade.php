{!! Form::open(array('id' => 'frmAdan-ubah', 'class' => 'form account-form', 'method' => 'PUT')) !!}
{{ csrf_field() }}
		<div class="form-group row">
			{!! Form::label('Program Studi', 'Program Studi ', array('class' => 'col-md-4 form-label')) !!}
			<div class="col-md-8">
  			{!! Form::select('unit_id', $unit, $data->unit_id, array('id' => 'unit_id', 'class' => 'form-control select2', 'placeholder' => 'Pilih', 'style' => 'width:100%')) !!}
			</div>
		</div>	

		<div class="form-group row">
		{!! Form::label('SK Akreditasi', 'SK Akreditasi', array('class' => 'col-md-4 control-label')) !!}
			<div class="col-md-8">
			{!! Form::text('sk_akreditasi', $data->sk_akreditasi, array('id' => 'sk_akreditasi', 'class' => 'form-control', 'size' => 16)) !!}
			</div>
		</div>	
		
		<div class="form-group row">
			{!! Form::label('Status Akreditasi', 'Status Akreditasi ', array('class' => 'col-md-4 form-label')) !!}
			<div class="col-md-8">
			{!! Form::select('akreditasi', $akreditasi, $data->akreditasi, array('id' => 'akreditasi', 'class' => 'sidebar-pos-option form-control input-inline input-sm input-small select2', 'style' => 'width:100%', 'required')) !!}
			</div>
		</div>

		<div class="form-group row">
			<label class="col-md-4 control-label">Tanggal Penetapan
				<span class="required"> * </span>
			</label>
			<div class="col-md-8">
				<div class="input-group date form_date" data-date="" data-date-format="yyyy-mm-dd" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
					<input class="form-control" type="text" value="{{$data->tanggal_penetapan}}" name="tanggal_penetapan">
					<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
				</div>
			</div>
		</div>

		<div class="form-group row">
			<label class="control-label col-md-4">Tanggal Kadaluarsa
				<span class="required"> * </span>
			</label>
			<div class="col-md-8">
				<div class="input-group date form_date" data-date="" data-date-format="yyyy-mm-dd" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
					<input class="form-control" type="text" value="{{$data->tanggal_kadaluarsa}}" name="tanggal_kadaluarsa">
					<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
				</div>
			</div>
		</div>

		<div class="form-group row">
			{!! Form::label('Dokumen', 'Dokumen Akreditasi *(PDF)', array('class' => 'col-md-4 form-label')) !!}
			<div class="col-md-8">
			{!! Form::file('file_akreditasi', array('id' => 'file_akreditasi', 'class' => 'form-control')) !!}
			</div>
		</div>

	{!! Form::hidden('id', $data->id, array('id' => 'id')) !!}
	{!! Form::hidden('url', URL::previous(), array('id' => 'url')) !!}
</div>
<div class="row">
		<div class="col-md-12">
			<span class="pesan"></span>
		</div>
</div>
{!! Form::close() !!}
<script src="{{ URL::asset('resources/vendor/jquery/jquery.enc.js') }}"></script>
<script src="{{ URL::asset('adan/sebaran_akreditasi/ajax.js') }}"></script>
<script src="{{ URL::asset('adan/includes/ajax.js') }}"></script>

<script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js" ></script>
<script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js" ></script>


<!--select2-->
<script src="{{ URL::to('backend') }}/assets/plugins/select2/js/select2.js" ></script>

<script>
	$('.select2').select2();
</script>