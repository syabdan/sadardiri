{!! Form::open(array('id' => 'frmAdan', 'class' => 'form account-form', 'method' => 'post', 'files' => true)) !!}
		<div class="form-group row">
			{!! Form::label('Program Studi', 'Program Studi ', array('class' => 'col-md-4 form-label')) !!}
			<div class="col-md-8">
  			{!! Form::select('unit_id', $unit, NULL, array('id' => 'unit_id', 'class' => 'form-control select2', 'placeholder' => 'Pilih', 'style' => 'width:100%')) !!}
			</div>
		</div>

		<div class="form-group row">
		{!! Form::label('SK Akreditasi', 'SK Akreditasi', array('class' => 'col-md-4 control-label')) !!}
			<div class="col-md-8">
			{!! Form::text('sk_akreditasi', NULL, array('id' => 'sk_akreditasi', 'class' => 'form-control', 'size' => 16)) !!}
			</div>
		</div>

		<div class="form-group row">
			{!! Form::label('Status Akreditasi', 'Status Akreditasi ', array('class' => 'col-md-4 form-label')) !!}
			<div class="col-md-8">
				<select name="akreditasi" id="akreditasi" class="form-control select2" style="width:100%">
				<option value="">Pilih</option>
				<option value="A">A</option>
				<option value="B">B</option>
				<option value="C">C</option>
				<option value="Unggul">Unggul</option>
				<option value="Sangat Baik">Baik Sekali</option>
				<option value="Baik">Baik</option>
				<option value="Izin Operasional">Izin Operasional</option>
				</select>
			</div>
		</div>

		<div class="form-group row">
			<label class="col-md-4 control-label">Tanggal Penetapan
				<span class="required"> * </span>
			</label>
			<div class="col-md-8">
				<div class="input-group date form_date" data-date="" data-date-format="yyyy-mm-dd" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
					<input class="form-control" type="text" value="" name="tanggal_penetapan">
					<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
				</div>
			</div>
		</div>

		<div class="form-group row">
			<label class="control-label col-md-4">Tanggal Kadaluarsa
				<span class="required"> * </span>
			</label>
			<div class="col-md-8">
				<div class="input-group date form_date" data-date="" data-date-format="yyyy-mm-dd" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
					<input class="form-control" type="text" value="" name="tanggal_kadaluarsa">
					<span class="input-group-addon"><span class="fa fa-calendar"></span></span>
				</div>
			</div>
		</div>

		<div class="form-group row">
			{!! Form::label('Dokumen', 'Dokumen Akreditasi *(PDF)', array('class' => 'col-md-4 form-label')) !!}
			<div class="col-md-8">
			{!! Form::file('file_akreditasi', array('id' => 'file_akreditasi', 'class' => 'form-control')) !!}
			</div>
		</div>

	{!! Form::hidden('url', URL::previous(), array('id' => 'url')) !!}

<div class="row">
	<div class="col-md-12">
		<span class="pesan"></span>
	</div>
</div>
{!! Form::close() !!}
<script src="{{ URL::asset('resources/vendor/jquery/jquery.enc.js') }}"></script>
<script src="{{ URL::asset('adan/sebaran_akreditasi/ajax.js') }}"></script>
<script src="{{ URL::asset('adan/includes/ajax.js') }}"></script>

<script src="{{ URL::to('backend') }}/assets/plugins/select2/js/select2.js" ></script>
<script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js" ></script>
<script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js" ></script>

<script>
	$('.select2').select2();
</script>
