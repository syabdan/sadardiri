$(document).on("click",".submit-tambah",function() {
	$('#frmAdan').submit();
});

$( ".submit-ubah" ).click(function() {
	$('#frmAdan-ubah').submit();
});

$( ".submit-hapus" ).click(function() {
	var url					= "{{ url($url_admin.'/sebaran_akreditasi') }}/"+$("#id").val();
	var dataString			= $('#frmAdan').serializeArray();
	goAjax(url, dataString);
});

$('#frmAdan').on('submit', function(event){
	event.preventDefault();
	$.ajax({
	url:"{{ url($url_admin.'/sebaran_akreditasi') }}",
	method:"POST",
	enctype: "multipart/form-data",
	data:new FormData(this),
	dataType:'JSON',
	contentType: false,
	cache: false,
	processData: false,
	beforeSend: function(){
				sebelumKirim();
		},
		success: function(data){
				successMsg(data);
		},
		error: function(x, e){
			//	errorMsg(x.status);
		}
	})
});

var id = $("#id").val();
$('#frmAdan-ubah').on('submit', function(event){
	event.preventDefault();
	$.ajax({
	url:"{{ url($url_admin.'/sebaran_akreditasi') }}/"+id,
	method:"POST",
	enctype: "multipart/form-data",
	data:new FormData(this),
	dataType:'JSON',
	contentType: false,
	cache: false,
	processData: false,
	beforeSend: function(){
				sebelumKirim();
		},
		success: function(data){
				successMsg(data);
		},
		error: function(x, e){
			//	errorMsg(x.status);
		}
	})
});