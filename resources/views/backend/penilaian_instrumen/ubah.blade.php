@section('style')
<link href="{{ URL::to('backend') }}/assets/plugins/summernote/summernote.css" rel="stylesheet">
@endsection

{!! Form::open(array('id' => 'frmAdan-ubah', 'class' => 'form account-form', 'method' => 'PUT')) !!}
{{ csrf_field() }}

	{{--  <div class="form-group row">
		<label class="col-lg-3 col-md-4 control-label">Nilai
		</label>
		<div class="col-lg-9 col-md-8">
			<select class="form-control  select2" style="width:100%" name="nilai_id">
				<option value="">PIlih Nilai</option>
				<option value="1">1 - Belum Memenuhi</option>
				<option value="2">2 - Sebagian Memenuhi</option>
				<option value="3">3 - Memenuhi Standar</option>
				<option value="4">4 - Melampaui Standar</option>
			</select>
		</div>
    </div>  --}}

    <div class="form-group">
        {!! Form::label('Nilai Auditor', 'Nilai Auditor *', array('class' => 'form-label')) !!}
          {!! Form::select('nilai_asessor_id', $nilai, $datas->nilai_asessor_id, array('id' => 'nilai_asessor_id', 'class' => 'form-control select2', 'placeholder' => 'Pilih', 'style' => 'width:100%')) !!}
    </div>

    <div class="row">
        <div class="col-md-12 col-sm-12">
        <label for="catatan"> Catatan Auditor</label>
            <textarea name="catatan" id="summernote" cols="30" rows="10" >{{ $datas->catatan }}</textarea>
        </div>
    </div>

	{!! Form::hidden('id', $datas->id, array('id' => 'id')) !!}
	{!! Form::hidden('id_diri', $datas->id_diri, array('id_diri' => 'id_diri')) !!}
	{!! Form::hidden('url', URL::previous(), array('id' => 'url')) !!}
</div>
<div class="row">
		<div class="col-md-12">
			<span class="pesan"></span>
		</div>
</div>
{!! Form::close() !!}
<script src="{{ URL::asset('resources/vendor/jquery/jquery.enc.js') }}"></script>
<script src="{{ URL::asset('adan/penilaian_instrumen/ajax.js') }}"></script>
<script src="{{ URL::asset('adan/includes/ajax.js') }}"></script>

<script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js" ></script>
<script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js" ></script>

<!-- summernote -->
<script src="{{ URL::to('backend') }}/assets/plugins/summernote/summernote.min.js" ></script>
<script src="{{ URL::to('backend') }}/assets/js/pages/summernote/summernote-data.js" ></script>

<!--select2-->
<script src="{{ URL::to('backend') }}/assets/plugins/select2/js/select2.js" ></script>

<script>
	$('.select2').select2();
</script>
