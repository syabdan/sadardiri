@section('style')
<link href="{{ URL::to('backend') }}/assets/plugins/summernote/summernote.css" rel="stylesheet">
@endsection

{!! Form::open(array('id' => 'frmAdan-reupload', 'class' => 'form account-form', 'method' => 'post', 'files' => true)) !!}

    <div class="form-group row">
        {!! Form::label('Link Bukti Dokumen', 'Link Bukti Dokumen', array('class' => 'col-md-2 control-label')) !!}
        <div class="col-md-10">
        {!! Form::text('url_file_datadiri', NULL, array('id' => 'url_file_datadiri', 'class' => 'form-control', 'size' => 16, 'placeholder' => 'eg. https://drive.google.com/file/d/1prmuzkpeRoD7UeqTvQSmWV__yAhjp96C/view?usp=sharing')) !!}
        </div>
    </div>

    {{-- <div class="form-group row">
        {!! Form::label('Dokumen', 'Dokumen', array('class' => 'col-md-4 form-label')) !!}
        <div class="col-md-8">
        {!! Form::file('file', array('id' => 'file', 'class' => 'form-control')) !!}
        </div>
    </div> --}}

	{!! Form::hidden('id', $datas->id, array('id' => 'id')) !!}
	{!! Form::hidden('url', URL::previous(), array('id' => 'url')) !!}
</div>
<div class="row">
		<div class="col-md-12">
			<span class="pesan"></span>
		</div>
</div>
{!! Form::close() !!}
<script src="{{ URL::asset('resources/vendor/jquery/jquery.enc.js') }}"></script>
<script src="{{ URL::asset('adan/penilaian_instrumen/ajax.js') }}"></script>
<script src="{{ URL::asset('adan/includes/ajax.js') }}"></script>

<script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js" ></script>
<script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js" ></script>

<!-- summernote -->
<script src="{{ URL::to('backend') }}/assets/plugins/summernote/summernote.min.js" ></script>
<script src="{{ URL::to('backend') }}/assets/js/pages/summernote/summernote-data.js" ></script>

<!--select2-->
<script src="{{ URL::to('backend') }}/assets/plugins/select2/js/select2.js" ></script>

<script>
	$('.select2').select2();
</script>
