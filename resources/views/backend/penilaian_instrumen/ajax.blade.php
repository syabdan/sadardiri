$(document).on("click",".submit-tambah",function() {
	$('#frmAdan').submit();
});

$( ".submit-ubah" ).click(function() {
	$('#frmAdan-ubah').submit();
});

$( ".submit-reupload" ).click(function() {
	$('#frmAdan-reupload').submit();
});

$( ".submit-lihat-detail" ).click(function() {
	var url					= "{{ url($url_admin.'/penilaian-instrumen/view_detail') }}/"+$("#id").val();
	var dataString			= $('#frmAdan-detail').serializeArray();
	goAjax(url, dataString);
});

$('#frmAdan').on('submit', function(event){
	event.preventDefault();
	$.ajax({
	url:"{{ url($url_admin.'/penilaian-instrumen') }}",
	method:"POST",
	enctype: "multipart/form-data",
	data:new FormData(this),
	dataType:'JSON',
	contentType: false,
	cache: false,
	processData: false,
	beforeSend: function(){
				sebelumKirim();
		},
		success: function(data){
				successMsg(data);
		},
		error: function(x, e){
			//	errorMsg(x.status);
		}
	})
});

var id = $("#id").val();
$('#frmAdan-ubah').on('submit', function(event){
	event.preventDefault();
	$.ajax({
	url:"{{ url($url_admin.'/penilaian-instrumen') }}/"+id,
	method:"POST",
	enctype: "multipart/form-data",
	data:new FormData(this),
	dataType:'JSON',
	contentType: false,
	cache: false,
	processData: false,
	beforeSend: function(){
				sebelumKirim();
		},
		success: function(data){
				successMsg(data);
		},
		error: function(x, e){
			//	errorMsg(x.status);
		}
	})
});

var id = $("#id").val();
$('#frmAdan-reupload').on('submit', function(event){
	event.preventDefault();
	$.ajax({
	url:"{{ url($url_admin.'/penilaian-instrumen/submit_reupload') }}/"+id,
	method:"POST",
	enctype: "multipart/form-data",
	data:new FormData(this),
	dataType:'JSON',
	contentType: false,
	cache: false,
	processData: false,
	beforeSend: function(){
				sebelumKirim();
		},
		success: function(data){
				successMsg(data);
		},
		error: function(x, e){
			//	errorMsg(x.status);
		}
	})
});
