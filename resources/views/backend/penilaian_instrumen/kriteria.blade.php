<div class="table-wrap">
    <div class="table-responsive tblHeightSet small-slimscroll-style">
        <table class="table table-striped table-bordered dt-responsive nowrap" id="table-1" style="width:100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Instrumen</th>
                    <th>Jawaban</th>
                    <th>Dokumen</th>
                    <th>Nilai</th>
                    <th>Nilai Asessor</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $no = 1;
                ?>
				@foreach($datas->where('instrumen_id', $id) as $data)
					<tr>
                        @if($data->as_parent == NULL)
                            <td>{{ $no.'.' }}</td>
                            <td><div style='border:1px solid #ccc; padding:10px; max-height:100px;overflow:scroll;'>{!! strip_tags($data->isi_pertanyaan) ?? '' !!}</div>
                            </td>
                            <td><div style='border:1px solid #ccc; padding:10px; max-height:500px;overflow:scroll;'>{!! strip_tags($data->jawaban) ?? '' !!}</div></td>
                            <td>
                            @if($data->url_file_datadiri)
                                <a href="{{$data->url_file_datadiri}}" target="_blank" class="btn btn-tbl-edit btn-xs btn-primary"><i class="fa fa-download"></i> Unduh</a>
                                {{-- <a href="{{ url('penilaian-instrumen/ambil_file/'.$data->file_data_diri)}}" target="_blank" class="btn btn-tbl-edit btn-xs btn-primary"><i class="fa fa-download"></i> Unduh</a> --}}
                            @else
                                <span class="btn btn-danger btn-xs">no file</span>
                            @endif
                            {{-- jika dosen --}}
                            @if(\Auth::user()->permissions_id == 4)
                            <a class="edit reupload btn btn-tbl-upload btn-warning btn-xs" data-toggle="tooltip" data-placement="top" title="reupload" data-id="{{$data->id}}" dekan-id ="{{ \Auth::user()->id}}" href="#reupload-{{$data->id}}/{{ \Auth::user()->id}}">
                               Reupload <i class="fa fa-upload"></i>
                            </a>
                            @endif

                            {{-- jika dosen --}}

                            </td>
                            <td>{{ $data->nilai_id ?? ''}}</td>
                            <td>{{ $data->nilai_asessor_id ?? ''}}</td>

                                <td>
                                    {{--  <a class="edit catatan" data-toggle="tooltip" data-placement="top" title="Catatan" data-id="{{$data->id}}" dekan-id ="{{ \Auth::user()->id}}" href="#catatan-{{$data->id}}/{{ \Auth::user()->id}}">
                                        <i class="fa fa-comments-o text-primary"></i>
                                        </a>
                                        |  --}}
                                    <a class="edit detail" data-toggle="tooltip" data-placement="top" title="Detail" data-id="{{$id}}" dekan-id ="{{ \Auth::user()->id}}" href="#detail-{{$data->id}}/{{ \Auth::user()->id}}">
                                    <i class="fa fa-search text-success"></i>
                                    </a>
                                    @if(\Auth::user()->permissions_id == 3)
                                    |
                                    <a class="edit ubah" data-toggle="tooltip" data-placement="top" title="Edit" data-id="{{$data->id}}" dekan-id ="{{ \Auth::user()->id}}" href="#edit-{{$data->id}}/{{ \Auth::user()->id}}">
                                        <i class="fa fa-pencil text-warning"></i>
                                    </a>
                                    @endif
                                </td>
					    <?php $no++; ?>
                        @else
                        <?php $no = 1;?>
                            <td colspan='2' style="color:blue; font-weight:bold">{!! strip_tags($data->isi_pertanyaan) ?? '' !!}</td>
                            <td colspan="4"></td>
                        @endif
					</tr>
				@endforeach
            </tbody>
        </table>
    </div>
</div>
