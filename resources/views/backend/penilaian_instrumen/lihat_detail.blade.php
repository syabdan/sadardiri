
<div class="row">
    <div class="col-sm-12">
        <div class="card-box">
            <div class="col-lg-4">
                <label style="color: blue; font-size: 16px;">Narasi Strategi</label>
            </div>
            <div class="col-lg-12">
                <textarea class = "mdl-textfield__input" rows =  "5" readonly>{!! strip_tags($narasi->narasi_1 ?? '') !!}</textarea>
            </div>
        </div>
        <div class="card-box">
            <div class="col-lg-4">
                <label style="color: blue; font-size: 16px;">Narasi Mekanisme</label>
            </div>
            <div class="col-lg-12">
                <textarea class = "mdl-textfield__input" rows =  "5" readonly>{!! strip_tags($narasi->narasi_2 ?? '') !!}</textarea>
            </div>
        </div>
        <div class="card-box">
            <div class="col-lg-4">
                <label style="color: blue; font-size: 16px;">Narasi Praktik Baik</label>
            </div>
            <div class="col-lg-12">
                <textarea class = "mdl-textfield__input" rows =  "5" readonly>{!! strip_tags($narasi->narasi_3 ?? '') !!}</textarea>
            </div>
        </div>
        <div class="card-box">
            <div class="col-lg-4">
                <label style="color: blue; font-size: 16px;">Narasi Hambatan</label>
            </div>
            <div class="col-lg-12">
                <textarea class = "mdl-textfield__input" rows =  "5" readonly>{!! strip_tags($narasi->narasi_4 ?? '') !!}</textarea>
            </div>
        </div>
        <div class="card-box">
            <div class="col-lg-4">
                <label style="color: blue; font-size: 16px;">Narasi Luaran</label>
            </div>
            <div class="col-lg-12">
                <textarea class = "mdl-textfield__input" rows =  "5" readonly>{!! strip_tags($narasi->narasi_5 ?? '') !!}</textarea>
            </div>
        </div>
    </div>
</div>

<input type="hidden" id="id" value="{{$id}}">
    <!-- data tables -->
    <script src="{{ URL::to('backend') }}/assets/plugins/datatables/jquery.dataTables.min.js" ></script>
    <script src="{{ URL::to('backend') }}/assets/plugins/datatables/editabletable.js" ></script>
	<script src="{{ URL::to('backend') }}/assets/js/pages/table/editable_table_data.js" ></script>
 	<script src="{{ URL::to('backend') }}/assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js" ></script>
	<script src="{{ URL::to('backend') }}/assets/js/pages/table/table_data.js" ></script>
    <script src="{{ URL::to('resources') }}/vendor/datatables/js/dataTables.responsive.min.js" ></script>
    <script src="{{ URL::to('resources') }}/vendor/datatables/js/responsive.bootstrap4.min.js" ></script>

	<script src="{{ URL::asset('adan/penilaian_instrumen/ajax.js') }}"></script>
	<script src="{{ URL::asset('adan/includes/ajax.js') }}"></script>

