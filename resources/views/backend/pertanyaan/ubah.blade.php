@section('style')
<link href="{{ URL::to('backend') }}/assets/plugins/summernote/summernote.css" rel="stylesheet">
@endsection

{!! Form::open(array('id' => 'frmAdan-ubah', 'class' => 'form account-form', 'method' => 'PUT')) !!}
{{ csrf_field() }}
<div class="form-group">
			{!! Form::label('Instrumen', 'Instrumen *', array('class' => 'form-label')) !!}
  			{!! Form::select('instrumen_id', $instrumen, $data->instrumen_id, array('id' => 'instrumen_id', 'class' => 'form-control select2', 'placeholder' => 'Pilih', 'style' => 'width:100%')) !!}
		</div>

		<div class="row">
			<div class="col-md-12 col-sm-12">
			<label for="isi_indikator_kinerja"> Narasi Instrumen / Pertanyaan *</label>
				<textarea name="isi_pertanyaan" id="summernote" cols="30" rows="10" >{{ $data->isi_pertanyaan }}</textarea>
			</div>
        </div>

        <?php
        if(!is_null($capaian)){
            $list = $capaian->pilihan;
            for($i=0;$i<count($list);$i++){
                 $no=$i+1; ?>

                <div class="form-group row">
                    {!! Form::label("Pilihan $no", "Pilihan Jawaban $no", array('class' => 'col-md-2 control-label')) !!}
                    <div class="col-md-10">
                        {!! Form::textarea("pilihan$no", $list[$i]['isi_pilihan'], array('id' => "pilihan$no", 'class' => 'form-control', 'rows' => '2', 'required' => 'required')) !!}
                    </div>
                </div>
                <input type="hidden" name="capaian_id" value="{{ $capaian->id }}">

        <?php
            }
        }
        ?>
		<p>
			{!! Form::label('Status unggah', 'Status unggah *', array('class' => 'form-label')) !!}
  			{!! Form::select('status_unggah', $unggah, $data->status_unggah, array('id' => 'status_unggah', 'class' => 'sidebar-pos-option form-control input-inline input-sm input-small select2', 'placeholder' => 'Pilih', 'style' => 'width:100%')) !!}
		</p>

		<div class="form-group">
			{!! Form::label('Boleh Ambil Kembali Jawaban Sebelumnya ? *', 'Boleh Ambil Kembali Jawaban Sebelumnya ? * *', array('class' => 'form-label')) !!}
			{!! Form::select('status_ambil_lagi', $ambil_lagi, NULL, array('id' => 'status_ambil_lagi', 'class' => 'sidebar-pos-option form-control input-inline input-sm input-small select2', 'placeholder' => 'Pilih', 'style' => 'width:100%')) !!}
		</div>
	{!! Form::hidden('id', $data->id, array('id' => 'id')) !!}
	{!! Form::hidden('url', URL::previous(), array('id' => 'url')) !!}
</div>
<div class="row">
		<div class="col-md-12">
			<span class="pesan"></span>
		</div>
</div>
{!! Form::close() !!}
<script src="{{ URL::asset('resources/vendor/jquery/jquery.enc.js') }}"></script>
<script src="{{ URL::asset('adan/pertanyaan/ajax.js') }}"></script>
<script src="{{ URL::asset('adan/includes/ajax.js') }}"></script>

<script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js" ></script>
<script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js" ></script>

<!-- summernote -->
<script src="{{ URL::to('backend') }}/assets/plugins/summernote/summernote.min.js" ></script>
<script src="{{ URL::to('backend') }}/assets/js/pages/summernote/summernote-data.js" ></script>

<!--select2-->
<script src="{{ URL::to('backend') }}/assets/plugins/select2/js/select2.js" ></script>

<script>
	$('.select2').select2();
</script>
