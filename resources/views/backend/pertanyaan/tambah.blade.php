@section('style')
<link href="{{ URL::to('backend') }}/assets/plugins/summernote/summernote.css" rel="stylesheet">
@endsection

{!! Form::open(array('id' => 'frmAdan', 'class' => 'form account-form', 'method' => 'post', 'files' => true)) !!}
		<div class="form-group">
			{!! Form::label('Instrumen', 'Instrumen *', array('class' => 'form-label')) !!}
  			{!! Form::select('instrumen_id', $instrumen, NULL, array('id' => 'instrumen_id', 'class' => 'form-control select2', 'placeholder' => 'Pilih', 'style' => 'width:100%')) !!}
		</div>
		
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<div class="card  card-box">
					<div class="card-body">
						<label for=""> Capaian Kinerja *</label>
							<textarea name="isi_pertanyaan" class="summernote" cols="30" rows="3" ></textarea>

					{{--	{!! Form::label('Status unggah', 'Status unggah *', array('class' => 'form-label')) !!}
						{!! Form::select('status_unggah', $unggah, NULL, array('id' => 'status_unggah', 'class' => 'sidebar-pos-option form-control input-inline input-sm input-small select2', 'placeholder' => 'Pilih', 'style' => 'width:100%')) !!}
						<br>
						<br>
						{!! Form::label('Boleh Ambil Kembali Jawaban Sebelumnya ? ', 'Boleh Ambil Kembali Jawaban Sebelumnya ? * ', array('class' => 'form-label')) !!}
						{!! Form::select('status_ambil_lagi', $ambil_lagi, NULL, array('id' => 'status_ambil_lagi', 'class' => 'sidebar-pos-option form-control select2', 'placeholder' => 'Pilih', 'style' => 'width:100%')) !!} --}}
					</div>


				</div>
			</div>
		</div>

		

		<div class="row">
			<div class="col-sm-7"></div>
			<div class="col-sm-5">		
				<label for=""> Tambah Sub Capaian Kinerja</label>
				<a class="btn btn-flat btn-sm btn-success tambah_sub_ask" id="tambah_sub_ask"><i class="fa fa-plus"  aria-hidden="true"></i></a>
			</div>
		</div>
		<br>
		<div id="indukwrap">
		</div>
		
		<br>
	{!! Form::hidden('url', URL::previous(), array('id' => 'url')) !!}

<div class="row">
	<div class="col-md-12">
		<span class="pesan"></span>
	</div>
</div>
{!! Form::close() !!}
<script src="{{ URL::asset('resources/vendor/jquery/jquery.enc.js') }}"></script>
<script src="{{ URL::asset('adan/pertanyaan/ajax.js') }}"></script>
<script src="{{ URL::asset('adan/includes/ajax.js') }}"></script>

<script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js" ></script>
<script src="{{ URL::to('backend') }}/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker-init.js" ></script>
<!-- summernote -->
<script src="{{ URL::to('backend') }}/assets/plugins/summernote/summernote.min.js" ></script>
<script src="{{ URL::to('backend') }}/assets/js/pages/summernote/summernote-data.js" ></script>

<!--select2-->
<script src="{{ URL::to('backend') }}/assets/plugins/select2/js/select2.js" ></script>

<script>
	$('.select2').select2();

	$('.summernote').summernote({
        placeholder: '',
        tabsize: 2,
        height: 200
      });

</script>

<script type="text/javascript">
		$(function() {
			var next = 0;
			var scntDiv = $('#indukwrap');
			$('.tambah_sub_ask').on('click', function() {
				 next = next + 1;
					$('<div class="row"><div class="col-md-12 col-sm-12"><div id="indukwrap'+next+'"><div class="card  card-box"><div class="card-body"> <div class="row"><div class="col-md-12 col-sm-12"><label for="isi_indikator_kinerja"><b>Sub Capaian Kinerja  '+next+'</b></label> <a class="btn btn-sm btn-flat btn-danger" id="hapus"><i class="fa fa-trash"  aria-hidden="true"></i></a><textarea name="isi_pertanyaan_sub[]" class="summernote" cols="30" rows="10" ></textarea></div></div></div><div class="form-group row">{!! Form::label('Pilihan 1', 'Pilihan Jawaban 1', array('class' => 'col-md-2 control-label')) !!}<div class="col-md-10">{!! Form::textarea('pilihan1[]', NULL, array('id' => 'pilihan1[]', 'class' => 'form-control', 'rows' => '2', 'required' => 'required')) !!}</div></div><div class="form-group row">{!! Form::label('Pilihan 2', 'Pilihan Jawaban 2', array('class' => 'col-md-2 control-label')) !!}<div class="col-md-10">{!! Form::textarea('pilihan2[]', NULL, array('id' => 'pilihan2[]', 'class' => 'form-control', 'rows' => '2', 'required' => 'required')) !!}</div></div><div class="form-group row">{!! Form::label('Pilihan 3', 'Pilihan Jawaban 3', array('class' => 'col-md-2 control-label')) !!}<div class="col-md-10">{!! Form::textarea('pilihan3[]', NULL, array('id' => 'pilihan3[]', 'class' => 'form-control', 'rows' => '2', 'required' => 'required')) !!}</div></div><div class="form-group row">{!! Form::label('Pilihan 4', 'Pilihan Jawaban 4', array('class' => 'col-md-2 control-label')) !!}<div class="col-md-10">{!! Form::textarea('pilihan4[]', NULL, array('id' => 'pilihan4[]', 'class' => 'form-control', 'rows' => '2', 'required' => 'required')) !!}</div></div>{!! Form::label('Status unggah',"Status unggah", array('class' => 'form-label')) !!}{!! Form::select('status_unggah_sub[]', $unggah,NULL, array('id' => 'status_unggah_sub', 'class' => 'sidebar-pos-option form-control input-inline select2', 'placeholder' => 'Pilih', 'style' => 'width:100%')) !!}<br>{!! Form::label('Boleh Ambil Kembali Jawaban Sebelumnya ? ', 'Boleh Ambil Kembali Jawaban Sebelumnya ? * ', array('class' => 'form-label')) !!}{!! Form::select('status_ambil_lagi_sub[]', $ambil_lagi, NULL, array('id' => 'status_ambil_lagi_sub', 'class' => 'sidebar-pos-option form-control select2', 'placeholder' => 'Pilih', 'style' => 'width:100%')) !!}</div></div></div></div>').appendTo(scntDiv);

					$( '[class="summernote"]' ).summernote({
													placeholder: '',
													tabsize: 2,
													height: 200
												});

					// return false;
			});
			
			$('#indukwrap').on('click','#hapus',function() { 

			$('#indukwrap'+next).remove();
			next = next-1;
			});
		});
	</script>