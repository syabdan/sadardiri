@extends('frontend.layout')
@section('style')

@endsection
@section('content')
<div class="page-content-wrapper">

    <div class="container" style="margin-top: -100px;">
        <div class="jumbotron">
          <h1>Selamat Datang di Statistik {{$unit->nama_unit}} Sadar Diri UIR</h1>
          <h4> Sistem sadar diri bertujuan sebagai evaluasi internal terhadap pencapaian kinerja Akademik dan non Akademik fakultas di lingkungan UIR sehingga tercipta siklus perbaikan yang berkelanjutan</h4>
        </div>
      </div>

    <div class="page-content" >

        <h2 style="margin-top: -20px;">{{$unit->nama_unit}}</h2>
        <!-- start widget -->
        <div class="row">
            <div class="col-lg-6 col-md-12 col-sm-12 col-12">
                <div class="card card-topline-lightblue">
                    <div id="all_indikator"></div>
                    <center>
                        <span class="highcharts-description">

                        </span>
                    </center>
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 col-12">
                <div class="card card-topline-lightblue">
                    <div id="data_chart_bar_kriteria"></div>
                    <center>
                        <span class="highcharts-description">

                        </span>
                    </center>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-lg-6 col-md-12 col-sm-12 col-12">
                <div class="card card-topline-lightblue">
                    <div id="pie_akreditasi"></div>
                    <center>
                        <span class="highcharts-description">

                        </span>
                    </center>
                </div>
            </div>

            <div class="col-lg-6 col-md-12 col-sm-12 col-12">
                <div class="card card-topline-lightblue">
                    <div id="pie_dosen"></div>
                    <center>
                        <span class="highcharts-description">

                        </span>
                    </center>
                </div>
            </div>
        </div>
        <!-- end widget -->

    </div>
</div>
@endsection
@section('script')
	<!-- <script src="{{ URL::to('backend') }}/assets/js/pages/chart/highchart/jquery-3.1.1.min.js"></script> -->
	<script src="{{ URL::to('backend') }}/assets/js/pages/chart/highchart/highcharts.js"></script>
	<script src="{{ URL::to('backend') }}/assets/js/pages/chart/highchart/highcharts-more.js"></script>
	<script src="{{ URL::to('backend') }}/assets/js/pages/chart/highchart/exporting.js"></script>
	<script src="{{ URL::to('backend') }}/assets/js/pages/chart/highchart/export-data.js"></script>
	<script src="{{ URL::to('backend') }}/assets/js/pages/chart/highchart/accessibility.js"></script>
    <script src="https://code.highcharts.com/highcharts-3d.js"></script>
    <script>
		Highcharts.chart('all_indikator', {
            chart: {
            polar: true
        },
        title: {
            text: 'Rata-rata Capaian Evaluasi Diri Fakultas'
        },
        subtitle: {
            // text: 'Rata-rata capaian evaluasi diri Dekan UIR'
        },
        pane: {
            startAngle: 0,
            endAngle: 360,
        },
        xAxis: {
            categories: {!! json_encode($chart_kriteria_all['category'])  !!},
        },
        yAxis: {
            min: 0,
            // max: 4 ,
            tickInterval: 1,
        },
        tooltip: {
            shared: true,
            pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.1f}</b><br/>'
        },
        credits: {
            enabled: false
        },
        legend: {
            align: 'right',
            verticalAlign: 'middle',
            layout: 'vertical'
        },

        series: [{
            name: 'Fakultas',
            data: {!! json_encode($chart_kriteria_all['rata_nilai_dekan'])  !!},
            pointPlacement: 'on'
        },
        {
            name: 'Auditor',
            data: {!! json_encode($chart_kriteria_all['rata_nilai_asessor'])  !!},
            pointPlacement: 'on'
        }],
        colors: ['#0CBD62', '#FFC300', '#C70039'],

	    });
	</script>

    <script>
        // Chart
        var chart = Highcharts.chart('data_chart_bar_kriteria', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Grafik Indikator Kinerja Utama',
                // style: {
                //     fontSize: '25px'
                // }
            },
            // subtitle: {
            //     text: 'Source: Dinas Kesehatan Provinsi Riau'
            // },
            xAxis: {
                categories: {!! json_encode($indikator['category'])  !!},
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Nilai'
                },
                allowDecimals: false
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true,
            },
                credits: {
                enabled: false
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                },
                series: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            series: {!! json_encode($indikator['series'])  !!},
            colors: ['#13D5E8', '#0CBD62', '#FFC300', '#C70039'],
        });

        // #Chart
    </script>

    <script>
        Highcharts.chart('pie_akreditasi', {

            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'Rasio Sebaran Akreditasi Per Program Studi {{$unit->nama_unit}}'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            accessibility: {
                point: {
                    valueSuffix: '%'
                }
            },
                credits: {
                enabled: false
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                    }
                }
            },

            series: [{
                type: 'pie',
                allowPointSelect: true,
                keys: ['name', 'y', 'selected', 'sliced'],
                data: {!! json_encode($sebaran['data'])  !!},
                showInLegend: true
            }],
        colors: ['#8E44AD', '#4B77BE', '#4DAF7C ', '#F5D76E', '#6C7A89', '#C91F37'],

        });
    </script>

    <script>
        Highcharts.chart('pie_dosen', {

            chart: {
                type: 'pie',
                options3d: {
                    enabled: true,
                    alpha: 45
                }
            },
            title: {
                text: 'Rasio Sebaran Dosen Per Jabatan Fungsional di {{$unit->nama_unit}}'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            accessibility: {
                point: {
                    valueSuffix: '%'
                }
            },
                credits: {
                enabled: false
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    innerSize: 100,
                    depth: 45,
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                    }
                }
            },

            series: [{
                type: 'pie',
                allowPointSelect: true,
                keys: ['name', 'y', 'selected', 'sliced'],
                data: {!! json_encode($sebaran_dosen['data'])  !!},
                showInLegend: true
            }],
        colors: ['#F58F84', '#875F9A', '#F62459 ', '#BDC3C7', '#C3272B'],


        });
    </script>
@endsection
