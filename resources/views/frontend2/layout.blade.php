<!DOCTYPE html>
<html lang="en">
<!-- BEGIN HEAD -->
<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-TYHDMD24T2"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-TYHDMD24T2');
    </script>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta name="description" content="Sistem Evaluasi Diri Universitas Islam Riau, sadardiri UIR" />
    <meta name="author" content="Syabdan Dalimunthe" />
	<title>Sadardiri UIR</title>
	<!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
	@include('backend.includes.css')
    @yield('style')

 </head>
 <!-- END HEAD -->
 <body class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md page-full-width header-white dark-sidebar-color logo-white">
    <div class="page-wrapper">
        <!-- start header -->
        <div class="page-header navbar navbar-fixed-top">
           <div class="page-header-inner ">
                <!-- logo start -->
                <div class="page-logo">
                    <a href="{{ url('/')}}">
                    <img alt="" src="{{ asset('backend/assets/img/uir.png')}}" style="width:30px ">
                    <span class="logo-default" >Sadardiri</span> </a>
                </div>
                <!-- logo end -->

                <!-- start mobile menu -->
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                    <span></span>
                </a>
               <!-- end mobile menu -->
                <!-- start header menu -->
                <div class="top-menu">
                    <ul class="nav navbar-nav pull-right">
 						<!-- start manage user dropdown -->
 						<li class="dropdown dropdown-user">
                            <a href="{{ route('login')}}" class="btn btn-sm btn-outline-success"><i class="fa fa-sign-in"></i> Login</a>
                        </li>
                        <!-- end manage user dropdown -->
                    </ul>
                </div>

            </div>
        </div>
        <!-- end header -->
	<!-- start page container -->
	<div class="page-container">

		<!-- start sidebar menu -->

        <!-- end sidebar menu -->
		<!-- start page content -->
            @yield('content')
            @yield('modal')
		<!-- end page container -->
		<!-- start footer -->
    		@include('backend.includes.footer')
    	<!-- end footer -->
            @yield('script')

  </body>
</html>
