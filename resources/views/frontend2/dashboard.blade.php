@extends('frontend.layout')
@section('style')
<style>
    .rounded {
        border-radius: 6px!important;
        border-top-left-radius: 6px !important;
        border-top-right-radius: 6px !important;
        border-bottom-right-radius: 6px !important;
        border-bottom-left-radius: 6px !important;
    }
</style>
@endsection
@section('content')
    <!-- start page content -->
    <div class="page-content-wrapper">
        <div class="container" style="margin-top: -100px;">
            <div class="jumbotron">
                <h1>Selamat Datang di Sistem Daring Evaluasi Diri (Sadar Diri) UIR</h1>
                <h4> Sistem sadar diri bertujuan sebagai evaluasi internal terhadap pencapaian kinerja Akademik dan non
                    Akademik fakultas di lingkungan UIR sehingga tercipta siklus perbaikan yang berkelanjutan</h4>
            </div>
        </div>

        <div class="page-content">

            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box ">
                        <div class="card-head">
                            <header>Sistem Daring Evaluasi Diri di Lingkungan UIR</header>
                        </div>
                        <div class="card-body ">
                            <div class="tab-pane" id="tab2">
                                <div class="row">
                                    @foreach ($users as $user)
                                        <div class="col-md-4">
                                            <div class="card">
                                                <div class="m-b-20">
                                                    <div class="doctor-profile">
                                                        <div class="profile-header {{ $user->unit['warna'] }}">
                                                            <div class="user-name">{{ $user->unit['nama_unit'] }}</div>
                                                            <div class="name-center">
                                                                {{-- @if ($user->pic)
                                                                    <img src="{{ asset('backend/assets/img/user/' . $user->pic) }}"
                                                                        style="width:74px;height:84px" class="rounded img-fluid">
                                                                @else --}}
                                                                    <img class="img-circle"
                                                                        src="{{ asset('backend/assets/img/user/user.png') }}" />
                                                                {{-- @endif --}}
                                                            </div>
                                                        </div>

                                                        <p>
                                                            {{ $user->email }}
                                                        </p>

                                                        <div class="profile-userbuttons">
                                                            <a href="{{ url('frontend/lihat/' . $user->unit['id']) }}"
                                                                class="btn btn-circle deepPink-bgcolor btn-sm">Lihat
                                                                Statistik</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page content -->
@endsection
@section('script')
@endsection
