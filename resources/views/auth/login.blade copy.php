<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta name="description" content="Sistem Evaluasi Diri Dekan Universitas Islam Riau" />
    <meta name="author" content="Syabdan Dalimunthe" />
    <title>Login | sadardiri UIR</title>
    <!-- google font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
	<!-- icons -->
    <link href="{{ URL::to('backend') }}/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="{{ URL::to('backend') }}/assets/plugins/iconic/css/material-design-iconic-font.min.css">
    <!-- bootstrap -->
	<link href="{{ URL::to('backend') }}/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- style -->
    <link rel="stylesheet" href="{{ URL::to('backend') }}/assets/css/pages/extra_pages.css">
	<!-- favicon -->
    <link rel="shortcut icon" href="{{ URL::to('backend') }}/assets/img/favicon.ico" /> 
</head>
<body>
    <div class="limiter">
		<div class="container-login100 page-background">
			<div class="wrap-login100">
                <form method="POST" action="{{ route('login') }}" class="login100-form validate-form" >
                        @csrf
					<span class="login100-form-logo">
						<i class="zmdi zmdi-flower"></i>
					</span>
					<span class="login100-form-title p-b-34 p-t-27">
						Log in
                    </span>
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
					<div class="wrap-input100 validate-input" data-validate = "Enter Email">
						<input type="email" id="email" placeholder="E-mail" class="input100 @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
						<span class="focus-input100" data-placeholder="&#xf207;"></span>
                    </div>
                    
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
					<div class="wrap-input100 validate-input" data-validate="Enter password">
						<input type="password" id="password" placeholder="Password" class="input100  @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
						<span class="focus-input100" data-placeholder="&#xf191;"></span>
					</div>
					<div class="container-login100-form-btn">
						<button class="login100-form-btn">
							Login
						</button>
					</div>
				
				</form>
			</div>
		</div>
	</div>
    <!-- start js include path -->
    <script src="{{ URL::to('backend') }}/assets/plugins/jquery/jquery.min.js" ></script>
    <!-- bootstrap -->
    <script src="{{ URL::to('backend') }}/assets/plugins/bootstrap/js/bootstrap.min.js" ></script>
    <script src="{{ URL::to('backend') }}/assets/js/pages/extra_pages/login.js" ></script>
    <!-- end js include path -->
</body>
</html>