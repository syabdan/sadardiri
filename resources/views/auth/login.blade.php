<!DOCTYPE html>
<html lang="en">
<head>
	<title>{{ config('master.aplikasi.nama') }}</title>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1" name="viewport" />
	<meta name="description" content="Sistem Daring Evaluasi Diri Dekan Universitas Islam Riau" />
	<meta name="author" content="Syabdan Dalimunthe" />
	<meta name="csrf-token" content="{{ csrf_token() }}">

<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ URL::to('backend/login') }}/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ URL::to('backend/login') }}/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ URL::to('backend/login') }}/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ URL::to('backend/login') }}/vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ URL::to('backend/login') }}/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ URL::to('backend/login') }}/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ URL::to('backend/login') }}/vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ URL::to('backend/login') }}/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ URL::to('backend/login') }}/css/util.css">
	<link rel="stylesheet" type="text/css" href="{{ URL::to('backend/login') }}/css/main.css">
<!--===============================================================================================-->

</head>
<body style="background-color: #666666;">

	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
			<form method="POST" action="{{ route('login') }}" class="login100-form validate-form" >
			@csrf

				@if ($errors->any())
					<div class="alert alert-danger">
							@foreach ($errors->all() as $error)
							<span class="help-block">{{ $error }}</span>
							@endforeach

					</div>
				@endif
				<span class="login100-form-title p-b-43">
				<img src="{{ URL::to('backend/login') }}/images/uir.png" alt="" width="50px">
					<a href="{{ url('/') }}" style="font-size:25px">	Login {{ config('master.aplikasi.singkatan') }} </a>
				</span>

				<div class="wrap-input100 validate-input" data-validate = "Valid email is required: example@uir.ac.id">
					<input class="input100  @error('email') is-invalid @enderror" type="text" name="email" id="email" value="{{ old('email') }}"  autofocus>
					<span class="focus-input100"></span>
					<span class="label-input100">Email</span>
				</div>

				<div class="wrap-input100 validate-input" data-validate="Password is required">
					<input class="input100 @error('password') is-invalid @enderror" type="password" name="password" id="password" value="{{ old('password') }}">
					<span class="focus-input100"></span>
					<span class="label-input100">Password</span>
				</div>
	<!--
				<div class="flex-sb-m w-full p-t-3 p-b-32">
					<div class="contact100-form-checkbox">
						<input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
						<label class="label-checkbox100" for="ckb1">
							Remember me
						</label>
					</div>

					<div>
						<a href="#" class="txt1">
							Forgot Password?
						</a>
					</div>
				</div> -->

				<div class="container-login100-form-btn">
					<button class="login100-form-btn" type="submit">
						Login
					</button>
				</div>

			</form>

        <div class="login100-more img-fluid" style="background-image: url('{{ URL::to('backend/login') }}/images/sadardiri.png'); background-size:90%;">
        {{-- <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
        <br>
            <div class="container">
            <div class="jumbotron" >
                <h2 style="margin-top: -50px">Selamat Datang di Sistem Daring Evaluasi Diri (Sadar Diri) UIR</h2>
                <p style="margin-bottom: -50px">Sadar diri adalah sistem evaluasi pencapaian kinerja Akademik dan non akademik di lingkungan UIR.
    Tujuan sadar diri adalah sebagai evaluasi internal sehingga tercipta siklus perbaikan yang berkelanjutan</p>
            </div>
            </div> --}}
				</div>
			</div>
		</div>
	</div>

<!--===============================================================================================-->
	<script src="{{ URL::to('backend/login') }}/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="{{ URL::to('backend/login') }}/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="{{ URL::to('backend/login') }}/vendor/bootstrap/js/popper.js"></script>
	<script src="{{ URL::to('backend/login') }}/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="{{ URL::to('backend/login') }}/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="{{ URL::to('backend/login') }}/vendor/daterangepicker/moment.min.js"></script>
	<script src="{{ URL::to('backend/login') }}/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="{{ URL::to('backend/login') }}/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="{{ URL::to('backend/login') }}/js/main.js"></script>

</body>
</html>
