<?php
use Illuminate\Support\Facades\Route;
use App\User;

Route::get('off_akreditasi', function () {
	\Artisan::call('off:akreditasi');
});

Route::get('off_user', function () {
	\Artisan::call('off:user');
});

Route::get('insert_mahasiswa', function () {
	\Artisan::call('tarik:total_mahasiswa');
});

Route::get('update_dosen', function () {
	\Artisan::call('db:seed --class=DosenSeeder');
});

// Frontend
Route::get('/', 'FrontendController@index');
Route::get('/frontend/lihat/{id_unit}', 'FrontendController@lihat');
// #Frontend

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');

Auth::routes();

Route::get('ambil_file/{file}', 'backend\PenilaianInstrumenController@ambilFile')->name('penilaian-instrumen.ambil_file');


Route::group(['middleware' => [ 'auth','aksesmenu']], function () {
	Route::get('/dashboard', 'backend\dashboardController@index')->name('dashboard');
	Route::get('adan/{folder}/{file}', 'backend\jsController@backend');
	Route::get('adan/{folder}/{id}/{file}', 'backend\jsController@backendWithId');
	#users
	Route::group([ 'prefix' => 'users'], function () {
		Route::get('hapus/{id}', 'backend\UsersController@hapus')->name('users.hapus');
		Route::any('data', 'backend\UsersController@data')->name('users.data');
	});
	Route::resource('users', 'backend\UsersController');

	#periode
	Route::group([ 'prefix' => 'periode'], function () {
		Route::get('hapus/{id}', 'backend\PeriodeController@hapus')->name('periode.hapus');
		Route::get('pesan', 'backend\PeriodeController@pesan')->name('periode.pesan');
		Route::post('kirim_pesan', 'backend\PeriodeController@kirim_pesan')->name('periode.kirim_pesan');
		Route::any('data', 'backend\PeriodeController@data')->name('periode.data');
	});
    Route::resource('periode', 'backend\PeriodeController');

	#nilai
	Route::group([ 'prefix' => 'nilai'], function () {
		Route::get('hapus/{id}', 'backend\NilaiController@hapus')->name('nilai.hapus');
		Route::any('data', 'backend\NilaiController@data')->name('nilai.data');
	});
    Route::resource('nilai', 'backend\NilaiController');

	#narasi-instrumen
	Route::group([ 'prefix' => 'narasi-instrumen'], function () {
		Route::get('hapus/{id}', 'backend\InstrumenController@hapus')->name('narasi-instrumen.hapus');
		Route::any('data', 'backend\InstrumenController@data')->name('narasi-instrumen.data');
	});
	Route::resource('narasi-instrumen', 'backend\InstrumenController');

	#narasi-capaian
	Route::group([ 'prefix' => 'capaian'], function () {
		Route::get('hapus/{id}', 'backend\CapaianController@hapus')->name('narasi-instrumen.hapus');
		Route::any('data', 'backend\CapaianController@data')->name('capaian.data');
	});
	Route::resource('capaian', 'backend\CapaianController');

	#penilaian-instrumen
	Route::group([ 'prefix' => 'penilaian-instrumen'], function () {
		Route::get('ambil_file/{file}', 'backend\PenilaianInstrumenController@ambilFile')->name('penilaian-instrumen.ambil_file');
        Route::get('detail/{id}', 'backend\PenilaianInstrumenController@detail')->name('penilaian-instrumen.detail');
        Route::get('catatan/{id}', 'backend\PenilaianInstrumenController@catatan')->name('penilaian-instrumen.catatan');
        Route::get('reupload/{id}', 'backend\PenilaianInstrumenController@reupload')->name('penilaian-instrumen.reupload');
        Route::any('submit_reupload/{id}', 'backend\PenilaianInstrumenController@submit_reupload')->name('penilaian-instrumen.submit_reupload');
		Route::any('data_pertanyaan/{id?}', 'backend\PenilaianInstrumenController@data_pertanyaan')->name('penilaian-instrumen.data_detail');

	});
	Route::resource('penilaian-instrumen', 'backend\PenilaianInstrumenController');

	#penilaian-indikator-kinerja
	Route::group([ 'prefix' => 'penilaian-indikator-kinerja'], function () {
        Route::get('ambil_file/{id}', 'backend\PenilaianIndikatorKinerjaController@ambilFile')->name('penilaian-indikator-kinerja.ambil_file');
        Route::any('hitung/{id}/{pilihan}', 'backend\PenilaianIndikatorKinerjaController@hitung')->name('penilaian-indikator-kinerja.hitung');
        Route::get('nilai/{id}', 'backend\PenilaianIndikatorKinerjaController@nilai')->name('penilaian-indikator-kinerja.nilai');
        Route::get('detail/{id}', 'backend\PenilaianIndikatorKinerjaController@detail')->name('penilaian-indikator-kinerja.detail');
        Route::get('catatan/{id}', 'backend\PenilaianIndikatorKinerjaController@catatan')->name('penilaian-indikator-kinerja.catatan');
        Route::get('reupload/{id}', 'backend\PenilaianIndikatorKinerjaController@reupload')->name('penilaian-instrumen.reupload');
        Route::any('submit_reupload/{id}', 'backend\PenilaianIndikatorKinerjaController@submit_reupload')->name('penilaian-instrumen.submit_reupload');
		Route::post('update_nilai/{id}', 'backend\PenilaianIndikatorKinerjaController@update_nilai')->name('penilaian-indikator-kinerja.update_nilai');
		Route::any('data_prodi/{id?}', 'backend\PenilaianIndikatorKinerjaController@data_prodi')->name('penilaian-indikator-kinerja.data_detail');

	});
	Route::resource('penilaian-indikator-kinerja', 'backend\PenilaianIndikatorKinerjaController');

	#isi-indikator-kinerja
    Route::get('isi-indikator-kinerja/{jenis}', 'backend\PenilaianIndikatorKinerjaController@jenis')->name('isi-indikator-kinerja.jenis');

	#view-indikator-kinerja
    Route::get('view-indikator-kinerja/{id_unit?}/{jenis?}/{periode?}', 'backend\PenilaianIndikatorKinerjaController@view')->name('view-indikator-kinerja');
	Route::get('view-indikator-kinerja/data/{id_unit}/{jenis?}/{periode?}', 'backend\PenilaianIndikatorKinerjaController@data')->name('view-indikator-kinerja.data');

	#view-instrumen
	Route::get('view-instrumen/{id_unit?}/{periode?}', 'backend\PenilaianInstrumenController@view')->name('view-instrumen');
	Route::get('view-instrumen/data/{id_unit}/{periode?}', 'backend\PenilaianInstrumenController@data')->name('view-instrumen.data');

    Route::get('view-instrumen', 'backend\PenilaianInstrumenController@view')->name('view-instrumen');

	#kriteria
	Route::group([ 'prefix' => 'instrumen'], function () {
		Route::get('kriteria/{file}', 'backend\InstrumenController@kriteria')->name('instrumen.kriteria');
		Route::post('simpan_jawaban', 'backend\InstrumenController@simpan_jawaban')->name('instrumen.simpan');
		Route::post('simpan_dokumen', 'backend\InstrumenController@simpan_dokumen')->name('instrumen.simpan_dokumen');
		Route::post('disable', 'backend\InstrumenController@disable')->name('instrumen.disable');
		Route::post('ambil_jawaban', 'backend\InstrumenController@ambil_jawaban')->name('instrumen.ambil_jawaban');
		Route::post('ambil_pilihan', 'backend\InstrumenController@ambil_pilihan')->name('instrumen.ambil_pilihan');
		Route::get('ambil_narasi/{kriteria_id}', 'backend\InstrumenController@ambil_narasi')->name('instrumen.ambil_narasi');
		Route::post('publish', 'backend\InstrumenController@publish')->name('instrumen.publish');
		Route::any('simpan_capaian', 'backend\InstrumenController@simpan_capaian')->name('instrumen.simpan_capaian');
	});

	#indikator-kinerja
	Route::group([ 'prefix' => 'indikator-kinerja'], function () {
		Route::get('hapus/{id}', 'backend\Indikator_kinerjaController@hapus')->name('indikator-kinerja.hapus');
		Route::get('tambahan', 'backend\Indikator_kinerjaController@tambahan')->name('indikator-kinerja.tambahan');
		Route::any('data_tambahan_utama', 'backend\Indikator_kinerjaController@data_tambahan_utama')->name('indikator-kinerja.data_tambahan_utama');
		Route::get('utama', 'backend\Indikator_kinerjaController@utama')->name('indikator-kinerja.utama');
		Route::post('data', 'backend\Indikator_kinerjaController@data')->name('indikator-kinerja.data');
	});
	Route::resource('indikator-kinerja', 'backend\Indikator_kinerjaController');

	#menu
	Route::group([ 'prefix' => 'menu'], function () {
		Route::get('hapus/{id}', 'backend\MenuController@hapus')->name('menu.hapus');
		Route::any('data', 'backend\MenuController@data')->name('menu.data');
	});
	Route::resource('menu', 'backend\MenuController');

	#permission
	Route::group([ 'prefix' => 'permission'], function () {
		Route::get('hapus/{id}', 'backend\PermissionController@hapus')->name('permission.hapus');
		Route::any('data', 'backend\PermissionController@data')->name('permission.data');
	});
	Route::resource('permission', 'backend\PermissionController');

	#pimpinan
	Route::group([ 'prefix' => 'pimpinan'], function () {
		Route::get('hapus/{id}', 'backend\PimpinanController@hapus')->name('pimpinan.hapus');
		Route::any('data', 'backend\PimpinanController@data')->name('pimpinan.data');
	});
	Route::resource('pimpinan', 'backend\PimpinanController');

    #unggah
	Route::group([ 'prefix' => 'unggah'], function () {
		Route::get('hapus/{id}', 'backend\DokumenController@hapus')->name('unggah.hapus');
		Route::any('data', 'backend\DokumenController@data')->name('unggah.data');
		Route::get('ambil_file/{file}', 'backend\DokumenController@ambilFile')->name('unggah.ambil_file');

	});
	Route::resource('unggah', 'backend\DokumenController');

    #sebaran_pegawai
	Route::group([ 'prefix' => 'sebaran_pegawai'], function () {
		Route::get('hapus/{id}', 'backend\SebaranPegawaiController@hapus')->name('sebaran_pegawai.hapus');
		Route::any('data', 'backend\SebaranPegawaiController@data')->name('sebaran_pegawai.data');
	});
	Route::resource('sebaran_pegawai', 'backend\SebaranPegawaiController');

    #sebaran_dosen
	Route::group([ 'prefix' => 'sebaran_dosen'], function () {
		Route::get('hapus/{id}', 'backend\SebaranDosenController@hapus')->name('sebaran_dosen.hapus');
		Route::any('data', 'backend\SebaranDosenController@data')->name('sebaran_dosen.data');
	});
	Route::resource('sebaran_dosen', 'backend\SebaranDosenController');

    #sebaran_akreditasi
	Route::group([ 'prefix' => 'sebaran_akreditasi'], function () {
        Route::get('hapus/{id}', 'backend\SebaranAkreditasiController@hapus')->name('sebaran_akreditasi.hapus');
		Route::get('ambil_file/{file}', 'backend\SebaranAkreditasiController@ambilFile')->name('penilaian-sebaran_akreditasi.ambil_file');
		Route::any('data', 'backend\SebaranAkreditasiController@data')->name('sebaran_akreditasi.data');
	});
	Route::resource('sebaran_akreditasi', 'backend\SebaranAkreditasiController');

	#sebaran_mahasiswa
	Route::group([ 'prefix' => 'sebaran_mahasiswa'], function () {
		Route::get('hapus/{id}', 'backend\SebaranMahasiswaController@hapus')->name('sebaran_mahasiswa.hapus');
		Route::any('data', 'backend\SebaranMahasiswaController@data')->name('sebaran_mahasiswa.data');
	});
	Route::resource('sebaran_mahasiswa', 'backend\SebaranMahasiswaController');

    #CetakLaporan
	Route::group([ 'prefix' => 'cetak'], function () {
        Route::post('indikator_kinerja', 'backend\CetakController@cetak_indikator_kinerja')->name('cetak.indikator_kinerja');
		Route::get('indikator_kinerja', 'backend\PenilaianIndikatorKinerjaController@view')->name('cetak.index_indikator');
        Route::post('instrumen', 'backend\CetakController@cetak_instrumen')->name('cetak.instrumen');
		Route::get('instrumen', 'backend\PenilaianInstrumenController@view')->name('cetak.index_instrumen');
    });

	// Route::get('test', function(){
	// 	$user           =   with(new User)::select('id')
    //                         ->where('tanggal_awal', '<=', date('Y-m-d'))
    //                         ->where('tanggal_akhir','>=', date('Y-m-d'))
    //                         ->get();
    //     // $json_user      = json_encode($user);
    //     $data_user      = json_decode($user);
    //     dd($data_user[1]);
	// });

	Route::get('test_api', function(){
		$sumber = api_total_mahasiswa_aktif_lokal();
		$data=array();
		$data2=array();
		foreach($sumber as $key => $val){
			$data[] = $key;
			$data2[] = $val[0];
		}

		return $sumber;
	});


});
