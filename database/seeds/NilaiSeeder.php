<?php

use Illuminate\Database\Seeder;

class NilaiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
		DB::table('nilais')->truncate();

        $isi = 	[
					[
						'nilai'                 => 1,
						'awal'                  => 0,
						'akhir'                 => 25
					],	
					[
						'nilai'                 => 2,
						'awal'                  => 26,
						'akhir'                 => 50
					],	
					[
						'nilai'                 => 3,
						'awal'                  => 51,
						'akhir'                 => 75
					],	
					[
						'nilai'                 => 4,
						'awal'                  => 76,
						'akhir'                 => 100
					]
				];
		DB::table('nilais')->insert($isi);
        Schema::enableForeignKeyConstraints();
    }
}
