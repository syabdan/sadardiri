<?php

use Illuminate\Database\Seeder;
use App\Pertanyaan;

class PertanyaanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('pertanyaans')->truncate();
			$isi = [
            [
                'instrumen_id'        => '1',
                'isi_pertanyaan'      => 'Bagaimana kesesuaian Visi, Misi, Tujuan dan Strategi (VMTS) Unit Pengelola Program Studi (UPPS) terhadap VMTS Perguruan Tinggi (PT) dan Program Studi (PS) yang dikelolanya',
                'parent_id'           => NULL,

            ],
            [
                'instrumen_id'        => '1',
                'isi_pertanyaan'      => 'Bagaimana Mekanisme dan keterlibatan pemangku kepentingan dalam penyusunan VMTS UPPS',
                'parent_id'           => NULL,

            ],
            [
                'instrumen_id'        => '1',
                'isi_pertanyaan'      => 'Bagaimana strategi pencapaian tujuan disusun berdasarkan analisis yang sistematis, serta pada pelaksanaannya dilakukan pemantauan dan evaluasi yang ditindaklanjuti',
                'parent_id'           => NULL,

            ],
            [
                'instrumen_id'        => '2',
                'isi_pertanyaan'      => 'Sistem Tata Pamong',
                'parent_id'           => NULL,
                'as_parent'           => 1

            ],
            [
                'instrumen_id'        => '2',
                'isi_pertanyaan'      => 'Jelaskan kelengkapan struktur organisasi dan keefektifan penyelenggaraan organisasi',
                'parent_id'           => '4',
            ],
            [
                'instrumen_id'        => '2',
                'isi_pertanyaan'      => 'Perwujudan good governance dan pemenuhan lima pilar sistem tata pamong (kredibel, akuntabel, bertanggung jawab dan adil)',
                'parent_id'           => '4',
            ],
            [
                'instrumen_id'        => '2',
                'isi_pertanyaan'      => 'Kepemimpinan dan kemampuan manajerial',
                'parent_id'           => NULL,

            ],
            [
                'instrumen_id'        => '2',
                'isi_pertanyaan'      => 'Jelaskan bagaimana komitmen pimpinan UPPS. ',
                'parent_id'           => '7',
            ],
            [
                'instrumen_id'        => '2',
                'isi_pertanyaan'      => 'Jelaskan kapabilitas pimpinan UPPS mencakup aspek: perencanaan, pengorganisasian, penempatan personel, pelaksanaan, pengendalian dan pengawasan, dan pelaporan yang menjadi dasar tindak lanjut',
                'parent_id'           => '7',
            ],
            [
                'instrumen_id'        => '2',
                'isi_pertanyaan'      => 'Kerjasama',
                'parent_id'           => NULL,

            ],
            [
                'instrumen_id'        => '2',
                'isi_pertanyaan'      => 'Bagaimana pimpinan UPPS menggambarkan:
                i.	Mutu kerjasama
                ii.	manfaat kerjasama bagi PS dalam pemenuhan proses pembelajaran, penelitian, PkM, 
                iii.	Tingkat Kepuasan mitra kerjasama
                iv.	Keberlanjutan kerjasama pada bidang pendidikan, penelitian dan PkM yang relevan dengan PS.
                ',
                'parent_id'           => '10',
            ],
            [
                'instrumen_id'        => '2',
                'isi_pertanyaan'      => 'Apa saja realisasi dari kerjasama pendidikan, penelitian, dan PkM yang relevan dengan PS dan dikelola oleh UPPS pada tahun berjalan',
                'parent_id'           => '10',
            ],
            [
                'instrumen_id'        => '2',
                'isi_pertanyaan'      => 'Apa saja realisasi kerjasama tingkat internasional, nasional, wilayah/lokal yang relevan dengan PS dan dikelola oleh UPPS pada tahun berjalan',
                'parent_id'           => '10',
            ],
            [
                'instrumen_id'        => '2',
                'isi_pertanyaan'      => 'Jelaskan apa saja Indikator kinerja tambahan yang ditetapkan oleh perguruan tinggi dan/atau UPPS berdasarkan standar pendidikan tinggi yang telah ditetapkan oleh perguruan tinggi untuk melampaui SN-DIKTI',
                'parent_id'           => NULL,

            ],
            [
                'instrumen_id'        => '2',
                'isi_pertanyaan'      => 'Bagaimana evaluasi capaian kinerja: analisis keberhasilan dan/atau ketidakberhasilan pencapaian kinerja UPPS yang telah ditetapkan di tiap kriteria (capaian kinerja diukur dengan metoda yang tepat, dan hasilnya dianalisis serta dievaluasi, dan analisis terhadap capaian kinerja mencakup identifikasi akar masalah, faktor pendukung keberhasilan dan faktor penghambat ketercapaian standard, dan deskripsi singkat tindak lanjut yang akan dilakukan).',
                'parent_id'           => NULL,

            ],
            [
                'instrumen_id'        => '2',
                'isi_pertanyaan'      => 'Bagaimana keterlaksanaan Sistem Penjaminan Mutu Internal (akademik dan non akademik) yang dibuktikan dengan keberadaan 4 aspek: 1) dokumen legal pembentukan unsur pelaksana penjaminan mutu; 2) ketersediaan dokumen mutu: kebijakan SPMI, manual SPMI, standar SPMI, dan formulir SPMI; 3) terlaksananya siklus penjaminan mutu (siklus PPEPP); 4) bukti sahih efektivitas pelaksanaan penjaminan mutu',
                'parent_id'           => NULL,

            ],
            [
                'instrumen_id'        => '2',
                'isi_pertanyaan'      => 'Bagaimana pengukuran kepuasan layanan manajemen terhadap para pemangku kepentingan, seperti: mahasiswa, dosen, tenaga kependidikan, lulusan, pengguna dan mitra yang memenuhi aspek-aspek berikut: 1) menggunakan instrumen kepuasan yang sahih, andal, mudah digunakan; 2) dilaksanakan secara berkala, serta datanya terekam secara komprehensif; 3) dianalisis dengan metode yang tepat serta bermanfaat untuk pengambilan keputusan; 4) tingkat kepuasan dan umpan balik ditindaklanjuti untuk perbaikan dan peningkatan mutu luaran secara berkala dan tersistem; 5) review terhadap pelaksanaan pengukuran kepuasan dosen dan mahasiswa; dan 6) hasilnya dipublikasikan dan mudah diakses oleh dosen dan mahasiswa',
                'parent_id'           => NULL,

            ],
            [
                'instrumen_id'        => '3',
                'isi_pertanyaan'      => 'Kualitas input mahasiswa',
                'parent_id'           => NULL,

            ],
            [
                'instrumen_id'        => '3',
                'isi_pertanyaan'      => 'Bagaimana metoda rekrutmen mahasiswa baru pada tahun ini',
                'parent_id'           => '18',

            ],
            [
                'instrumen_id'        => '3',
                'isi_pertanyaan'      => 'Bagaimana keketatan seleksi penerimaan mahasiswa baru pada tahun ini',
                'parent_id'           => '18',

            ],
            [
                'instrumen_id'        => '3',
                'isi_pertanyaan'      => 'Daya Tarik program studi',
                'parent_id'           => NULL,


            ],
            [
                'instrumen_id'        => '3',
                'isi_pertanyaan'      => 'Bagaimana peningkatan jumlah calon mahasiswa pada tahun ini',
                'parent_id'           => '21',

            ],
            [
                'instrumen_id'        => '3',
                'isi_pertanyaan'      => 'Bagaimana peningkatan keberadaan mahasiswa asing',
                'parent_id'           => '21',

            ],
            [
                'instrumen_id'        => '3',
                'isi_pertanyaan'      => 'Layanan kemahasiswaan',
                'parent_id'           => NULL,

            ],
            [
                'instrumen_id'        => '3',
                'isi_pertanyaan'      => 'Bagaimana ketersediaan layanan kemahasiswaan.',
                'parent_id'           => '24',

            ],
            [
                'instrumen_id'        => '3',
                'isi_pertanyaan'      => 'Bagaimana Akses dan mutu layanan kemahasiswaan yang tersedia ',
                'parent_id'           => '24',

            ],
            [
                'instrumen_id'        => '4',
                'isi_pertanyaan'      => 'Profil Dosen',
                'parent_id'           => NULL,


            ],
            [
                'instrumen_id'        => '4',
                'isi_pertanyaan'      => 'Bagaimana kecukupan jumlah dosen tetap perguruan tinggi yang ditugaskan sebagai pengampu mata kuliah di program studi yang diakreditasi. ',
                'parent_id'           => '27',

            ],
            [
                'instrumen_id'        => '4',
                'isi_pertanyaan'      => 'Kualifikasi akademik dosen tetap.',
                'parent_id'           => '27',

            ],
            [
                'instrumen_id'        => '4',
                'isi_pertanyaan'      => 'Kualifikasi akademik dosen tetap.',
                'parent_id'           => '27',

            ],
            [
                'instrumen_id'        => '4',
                'isi_pertanyaan'      => 'Sertifikasi profesi/kompetensi/industri dosen tetap. ',
                'parent_id'           => '27',

            ],
            [
                'instrumen_id'        => '4',
                'isi_pertanyaan'      => 'Jabatan akademik dosen tetap. ',
                'parent_id'           => '27',

            ],
            [
                'instrumen_id'        => '4',
                'isi_pertanyaan'      => 'Rasio jumlah mahasiswa Program Studi terhadap jumlah dosen tetap.',
                'parent_id'           => '27',

            ],
            [
                'instrumen_id'        => '4',
                'isi_pertanyaan'      => 'Beban kerja dosen tetap sebagai pembimbing tugas akhir mahasiswa.',
                'parent_id'           => '27',

            ],
            [
                'instrumen_id'        => '4',
                'isi_pertanyaan'      => 'Ekuivalensi Waktu Mengajar Penuh (EWMP) dosen tetap. ',
                'parent_id'           => '27',
            ],
            [
                'instrumen_id'        => '4',
                'isi_pertanyaan'      => 'Jumlah Dosen tidak tetap. ',
                'parent_id'           => '27',
            ],
            [
                'instrumen_id'        => '4',
                'isi_pertanyaan'      => 'Keterlibatan dosen industri/praktisi',
                'parent_id'           => '27',
            ],
            [
                'instrumen_id'        => '4',
                'isi_pertanyaan'      => 'Kinerja dosen',
                'parent_id'           => NULL,

            ],
            [
                'instrumen_id'        => '4',
                'isi_pertanyaan'      => 'Rekognisi / Pengakuan atas prestasi/kinerja dosen tetap. ',
                'parent_id'           => '38',
            ],
            [
                'instrumen_id'        => '4',
                'isi_pertanyaan'      => 'Kegiatan penelitian dosen tetap yang relevan dengan bidang program studi',
                'parent_id'           => '38',
            ],
            [
                'instrumen_id'        => '4',
                'isi_pertanyaan'      => 'Pagelaran/pameran/presentasi/publikasi karya ilmiah dengan tema yang relevan dengan bidang program studi',
                'parent_id'           => '38',
            ],
            [
                'instrumen_id'        => '4',
                'isi_pertanyaan'      => 'Luaran penelitian dan PkM yang dihasilkan dosen tetap. ',
                'parent_id'           => '38',
            ],
            [
                'instrumen_id'        => '4',
                'isi_pertanyaan'      => 'Artikel karya ilmiah dosen tetap yang disitasi',
                'parent_id'           => '38',
            ],
            [
                'instrumen_id'        => '4',
                'isi_pertanyaan'      => 'Bagaimana upaya yang dilakukan UPPS dalam pengembangan dosen unit pengelola dan program studi. ',
                'parent_id'           => NULL,

            ],
            [
                'instrumen_id'        => '4',
                'isi_pertanyaan'      => 'Tenaga kependidikan', 
                'parent_id'           => NULL,

            ],
            [
                'instrumen_id'        => '4',
                'isi_pertanyaan'      => 'Kualifikasi dan kecukupan tenaga kependidikan berdasarkan jenis pekerjaannya (administrasi, pustakawan, teknisi, dll.).',
                'parent_id'           => '45',
            ],
            [
                'instrumen_id'        => '4',
                'isi_pertanyaan'      => 'Kualifikasi dan kecukupan laboran untuk mendukung proses pembelajaran sesuai dengan kebutuhan program studi',
                'parent_id'           => '45',
            ],
            [
                'instrumen_id'        => '4',
                'isi_pertanyaan'      => 'Sebutkan indikator kinerja SDM lain yang berlaku di UPPS berdasarkan standar pendidikan tinggi yang ditetapkan perguruan tinggi untuk melampaui SN-DIKTI.',
                'parent_id'           => NULL,

            ],
            [
                'instrumen_id'        => '4',
                'isi_pertanyaan'      => 'Bagaimana mengukur, memonitor, mengkaji, dan menganalisis data indikator kinerja tambahan yang sahih untuk perbaikan berkelanjutan. ',
                'parent_id'           => NULL,

            ],
            [
                'instrumen_id'        => '4',
                'isi_pertanyaan'      => 'Bagaimana analisis keberhasilan dan/atau ketidakberhasilan atas ketercapaian indikator kinerja yang berlaku di UPPS berdasarkan standar pendidikan tinggi yang ditetapkan perguruan tinggi',
                'parent_id'           => NULL,
                
            ],
            [
                'instrumen_id'        => '4',
                'isi_pertanyaan'      => 'Jelaskan dengan bukti sahih tentang implementasi sistem penjaminan mutu di UPPS yang sesuai dengan standar pendidikan tinggi yang ditetapkan perguruan tinggi terkait SDM, yang mengikuti siklus penetapan, pelaksanaan, evaluasi, pengendalian, dan perbaikan berkelanjutan (PPEPP).',
                'parent_id'           => NULL,

            ],
            [
                'instrumen_id'        => '4',
                'isi_pertanyaan'      => 'Jelaskan mengenai hasil kepuasan dosen dan tenaga kependidikan terhadap layanan pengelolaan dan pengembangan SDM',
                'parent_id'           => NULL,

            ],
            [
                'instrumen_id'        => '4',
                'isi_pertanyaan'      => 'Jelaskan pemosisian, masalah dan akar masalah, serta rencana perbaikan dan pengembangan yang akan dilakukan UPPS pada tahun berikut terkait sumber daya manusia',
                'parent_id'           => NULL,

            ],
            [
                'instrumen_id'        => '4',
                'isi_pertanyaan'      => 'Jelaskan pemosisian, masalah dan akar masalah, serta rencana perbaikan dan pengembangan yang akan dilakukan UPPS pada tahun berikut terkait sumber daya manusia',
                'parent_id'           => NULL,

            ],
            [
                'instrumen_id'        => '5',
                'isi_pertanyaan'      => 'Keuangan',
                'parent_id'           => NULL,

            ],
            [
                'instrumen_id'        => '5',
                'isi_pertanyaan'      => 'Bagaimana realisasi investasi (SDM, sarana dan prasarana) yang mendukung penyelenggaraan tridharma',
                'parent_id'           => '55',
            ],
            [
                'instrumen_id'        => '5',
                'isi_pertanyaan'      => 'Bagaimana kecukupan dana untuk menjamin pencapaian capaian pembelajaran',
                'parent_id'           => '55',
            ],
            [
                'instrumen_id'        => '5',
                'isi_pertanyaan'      => 'Sarana dan prasarana',
                'parent_id'           => NULL,

            ],
            [
                'instrumen_id'        => '5',
                'isi_pertanyaan'      => 'Bagaimana kecukupan, aksesibilitas dan mutu sarana dan prasarana untuk menjamin pencapaian capaian pembelajaran dan meningkatkan suasana akademik',
                'parent_id'           => '58',
            ],
            [
                'instrumen_id'        => '5',
                'isi_pertanyaan'      => 'Jelaskan Indikator kinerja tambahan keuangan, sarana dan prasarana lain yang berlaku di UPPS berdasarkan standar pendidikan tinggi yang ditetapkan perguruan tinggi untuk melampaui SN-DIKTI',
                'parent_id'           => NULL,

            ],
            [
                'instrumen_id'        => '5',
                'isi_pertanyaan'      => 'Bagaimana analisis keberhasilan dan/atau ketidakberhasilan atas ketercapaian indikator kinerja yang berlaku di UPPS berdasarkan standar pendidikan tinggi yang ditetapkan perguruan tinggi',
                'parent_id'           => NULL,

            ],
            [
                'instrumen_id'        => '5',
                'isi_pertanyaan'      => 'Jelaskan dengan bukti sahih tentang implementasi sistem penjaminan mutu di UPPS yang sesuai dengan standar pendidikan tinggi yang ditetapkan perguruan tinggi terkait keuangan, yang mengikuti siklus penetapan, pelaksanaan, evaluasi, pengendalian, dan perbaikan berkelanjutan (PPEPP).',
                'parent_id'           => NULL,

            ],
            [
                'instrumen_id'        => '5',
                'isi_pertanyaan'      => 'Jelaskan mengenai pengukuran kepuasan sivitas akademika terhadap layanan pengelolaan keuangan maupun sarana dan prasarana',
                'parent_id'           => NULL,

            ],
            [
                'instrumen_id'        => '5',
                'isi_pertanyaan'      => 'Jelaskan pemosisian, masalah dan akar masalah, serta rencana perbaikan dan pengembangan yang akan dilakukan UPPS pada tahun berikut terkait keuangan',
                'parent_id'           => NULL,

            ],
        ];
		DB::table('pertanyaans')->insert($isi);

    }
}
