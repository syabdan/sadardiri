<?php

use Illuminate\Database\Seeder;
use App\Indikator;


class IndikatorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('indikators')->truncate();
        for($a=1; $a <= 9; $a++){
			Indikator::updateOrCreate([
                'nama_indikator'        => 'Indikator '.$a
            ]);
		}
    }
}
