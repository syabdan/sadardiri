<?php

use Illuminate\Database\Seeder;
use App\Menu;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

		DB::table('menus')->truncate();

        $menus = new Menu();
        $menus->posisi = 1;
        $menus->nama = "Dashboard";
        $menus->link = "dashboard";
        $menus->icon = "dashboard";
        $menus->status = 1;
        $menus->tampil = 1;
        $menus->parent_menu_id = NULL;
        $menus->save();
        
        $menus = new Menu();
        $menus->posisi = 2;
        $menus->nama = "Instrumen";
        $menus->link = "#";
        $menus->icon = "grain";
        $menus->status = 1;
        $menus->tampil = 1;
        $menus->parent_menu_id = NULL;
        $menus->save();
        
        $menus = new Menu();
        $menus->posisi = 3;
        $menus->nama = "Indikator Kinerja";
        $menus->link = "#";
        $menus->icon = "dvr";
        $menus->status = 1;
        $menus->tampil = 1;
        $menus->parent_menu_id = NULL;
        $menus->save();

        $menus = new Menu();
        $menus->posisi = 4;
        $menus->nama = "Kriteria 1";
        $menus->link = "instrumen/kriteria/1";
        $menus->icon = "grain";
        $menus->status = 1;
        $menus->tampil = 1;
        $menus->parent_menu_id = 2;
        $menus->save();
        
        $menus = new Menu();
        $menus->posisi = 5;
        $menus->nama = "Kriteria 2";
        $menus->link = "instrumen/kriteria/2";
        $menus->icon = "grain";
        $menus->status = 1;
        $menus->tampil = 1;
        $menus->parent_menu_id = 2;
        $menus->save();
        
        $menus = new Menu();
        $menus->posisi = 6;
        $menus->nama = "Kriteria 3";
        $menus->link = "instrumen/kriteria/3";
        $menus->icon = "grain";
        $menus->status = 1;
        $menus->tampil = 1;
        $menus->parent_menu_id = 2;
        $menus->save();
        
        $menus = new Menu();
        $menus->posisi = 7;
        $menus->nama = "Kriteria 4";
        $menus->link = "instrumen/kriteria/4";
        $menus->icon = "grain";
        $menus->status = 1;
        $menus->tampil = 1;
        $menus->parent_menu_id = 2;
        $menus->save();
        
        $menus = new Menu();
        $menus->posisi = 8;
        $menus->nama = "Kriteria 5";
        $menus->link = "instrumen/kriteria/5";
        $menus->icon = "grain";
        $menus->status = 1;
        $menus->tampil = 1;
        $menus->parent_menu_id = 2;
        $menus->save();
        
        $menus = new Menu();
        $menus->posisi = 9;
        $menus->nama = "Kriteria 6";
        $menus->link = "instrumen/kriteria/6";
        $menus->icon = "grain";
        $menus->status = 1;
        $menus->tampil = 1;
        $menus->parent_menu_id = 2;
        $menus->save();
        
        $menus = new Menu();
        $menus->posisi = 10;
        $menus->nama = "Kriteria 7";
        $menus->link = "instrumen/kriteria/7";
        $menus->icon = "grain";
        $menus->status = 1;
        $menus->tampil = 1;
        $menus->parent_menu_id = 2;
        $menus->save();
        
        $menus = new Menu();
        $menus->posisi = 11;
        $menus->nama = "Kriteria 8";
        $menus->link = "instrumen/kriteria/8";
        $menus->icon = "grain";
        $menus->status = 1;
        $menus->tampil = 1;
        $menus->parent_menu_id = 2;
        $menus->save();
        
        $menus = new Menu();
        $menus->posisi = 12;
        $menus->nama = "Kriteria 9";
        $menus->link = "instrumen/kriteria/9";
        $menus->icon = "grain";
        $menus->status = 1;
        $menus->tampil = 1;
        $menus->parent_menu_id = 2;
        $menus->save();
        
        $menus = new Menu();
        $menus->posisi = 13;
        $menus->nama = "Narasi Indikator Kinerja Utama";
        $menus->link = "indikator-kinerja/utama";
        $menus->icon = "desktop_mac";
        $menus->status = 1;
        $menus->tampil = 1;
        $menus->parent_menu_id = 17;
        $menus->save();

        
        $menus = new Menu();
        $menus->posisi = 14;
        $menus->nama = "Narasi Indikator Kinerja Tambahan";
        $menus->link = "indikator-kinerja/tambahan";
        $menus->icon = "desktop_mac";
        $menus->status = 1;
        $menus->tampil = 1;
        $menus->parent_menu_id = 17;
        $menus->save();
        $menus = new Menu();
        
        $menus = new Menu();
        $menus->posisi = 15;
        $menus->nama = "Penilaian";
        $menus->link = "#";
        $menus->icon = "desktop_mac";
        $menus->status = 1;
        $menus->tampil = 1;
        $menus->parent_menu_id = NULL;
        $menus->save();
        
        $menus = new Menu();
        $menus->posisi = 100;
        $menus->nama = "Cetak Dashboard";
        $menus->link = "cetak-dashboard";
        $menus->icon = "print";
        $menus->status = 1;
        $menus->tampil = 1;
        $menus->parent_menu_id = NULL;
        $menus->save();
        
        $menus = new Menu();
        $menus->posisi = 17;
        $menus->nama = "Master";
        $menus->link = "#";
        $menus->icon = "settings";
        $menus->status = 1;
        $menus->tampil = 1;
        $menus->parent_menu_id = NULL;
        $menus->save();
        
        $menus = new Menu();
        $menus->posisi = 18;
        $menus->nama = "Narasi Instrumen";
        $menus->link = "narasi-instrumen";
        $menus->icon = "time";
        $menus->status = 1;
        $menus->tampil = 1;
        $menus->parent_menu_id = 17;
        $menus->save();
        
        $menus = new Menu();
        $menus->posisi = 19;
        $menus->nama = "Narasi Indikator Kinerja";
        $menus->link = "narasi-indikator-kinerja";
        $menus->icon = "time";
        $menus->status = 1;
        $menus->tampil = 0;
        $menus->parent_menu_id = 17;
        $menus->save();
        
        $menus = new Menu();
        $menus->posisi = 20;
        $menus->nama = "Setting Periode";
        $menus->link = "periode";
        $menus->icon = "time";
        $menus->status = 1;
        $menus->tampil = 1;
        $menus->parent_menu_id = 17;
        $menus->save();

        $menus = new Menu();
        $menus->posisi = 21;
        $menus->nama = "Data Users";
        $menus->link = "users";
        $menus->icon = "time";
        $menus->status = 1;
        $menus->tampil = 1;
        $menus->parent_menu_id = 17;
        $menus->save();
        $menus = new Menu();

        $menus->posisi = 22;
        $menus->nama = "Penilaian Indikator Kinerja";
        $menus->link = "penilaian-indikator-kinerja";
        $menus->icon = "desktop_mac";
        $menus->status = 1;
        $menus->tampil = 1;
        $menus->parent_menu_id = 15;
        $menus->save();

        $menus = new Menu();
        $menus->posisi = 23;
        $menus->nama = "Penilaian Instrumen";
        $menus->link = "penilaian-instrumen";
        $menus->icon = "desktop_mac";
        $menus->status = 1;
        $menus->tampil = 1;
        $menus->parent_menu_id = 15;
        $menus->save();
        
        
        $menus->posisi = 24;
        $menus->nama = "Indikator Kinerja Tambahan";
        $menus->link = "indikator-kinerja/isi-tambahan";
        $menus->icon = "desktop_mac";
        $menus->status = 1;
        $menus->tampil = 1;
        $menus->parent_menu_id = 3;
        $menus->save();

                
        $menus = new Menu();
        $menus->posisi = 25;
        $menus->nama = "Indikator Kinerja Utama";
        $menus->link = "indikator-kinerja/isi-utama";
        $menus->icon = "desktop_mac";
        $menus->status = 1;
        $menus->tampil = 1;
        $menus->parent_menu_id = 3;
        $menus->save();
    }
}
