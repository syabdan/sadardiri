<?php

use Illuminate\Database\Seeder;
use App\Instrumen;
class InstrumenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            
        \DB::table('instrumens')->truncate();
        for($a=1; $a <= 9; $a++){
			Instrumen::updateOrCreate([
                'nama_instrumen'        => 'Kriteria '.$a
            ]);
		}
    }
}
