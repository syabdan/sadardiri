<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('users')->truncate();

        $newuser = new User();
        $newuser->nama_user = "Admin LPM";
        $newuser->email = "lpm@uir.ac.id";
        $newuser->password = bcrypt('secret');
        $newuser->permissions_id = 1;
        $newuser->tanggal_awal = NULL;
        $newuser->tanggal_akhir = NULL;
        $newuser->status = 1;
        $newuser->created_by = 1;
        $newuser->save();
        
        $newuser = new User();
        $newuser->nama_user = "UIR";
        $newuser->email = "pimpinan@uir.ac.id";
        $newuser->password = bcrypt('secret');
        $newuser->permissions_id = 2;
        $newuser->tanggal_awal = NULL;
        $newuser->tanggal_akhir = NULL;
        $newuser->status = 1;
        $newuser->created_by = 1;
        $newuser->save();
        
        $newuser = new User();
        $newuser->nama_user = "Asessor Teknik";
        $newuser->email = "asessor.eng@uir.ac.id";
        $newuser->password = bcrypt('secret');
        $newuser->permissions_id = 3;
        $newuser->tanggal_awal = '2020-05-01';
        $newuser->tanggal_akhir = '2021-05-01';
        $newuser->status = 1;
        $newuser->created_by = 1;
        $newuser->save();
        
        $newuser = new User();
        $newuser->nama_user = "Asessor Pertanian";
        $newuser->email = "asessor.pertanian@uir.ac.id";
        $newuser->password = bcrypt('secret');
        $newuser->permissions_id = 3;
        $newuser->tanggal_awal = '2020-05-01';
        $newuser->tanggal_akhir = '2021-05-01';
        $newuser->status = 1;
        $newuser->created_by = 1;
        $newuser->save();
        
        $newuser = new User();
        $newuser->nama_user = "Dekan Teknik";
        $newuser->email = "dekan.eng@uir.ac.id";
        $newuser->password = bcrypt('secret');
        $newuser->permissions_id = 4;
        $newuser->tanggal_awal = '2020-05-01';
        $newuser->tanggal_akhir = '2024-05-01';
        $newuser->status = 1;
        $newuser->created_by = 1;
        $newuser->save();
        
        $newuser = new User();
        $newuser->nama_user = "Dekan Pertanian";
        $newuser->email = "dekan.pertanian@uir.ac.id";
        $newuser->password = bcrypt('secret');
        $newuser->permissions_id = 4;
        $newuser->tanggal_awal = '2020-05-01';
        $newuser->tanggal_akhir = '2024-05-01';
        $newuser->status = 1;
        $newuser->created_by = 1;
        $newuser->save();
    }
}
