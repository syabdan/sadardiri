<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataIndikatorKinerjasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_indikator_kinerjas', function (Blueprint $table) {
            $table->id();
            $table->integer('indikator_kinerja_id');
            $table->string('file_indikator_kinerja')->nullable();
            $table->float('target')->nullable();
            $table->float('capaianmid')->nullable();
            $table->float('capaian')->nullable();
            $table->integer('nilai_id')->nullable();
            $table->integer('periode_id');
            $table->integer('unit_id');
            $table->integer('persentase');
            $table->integer('publish')->default(0);
            $table->integer('status_target')->default(0);
            $table->integer('status_capaianmid')->default(0);
            $table->integer('status_capaian')->default(0);
            $table->integer('rated_by')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_indikator_kinerjas');
    }
}
