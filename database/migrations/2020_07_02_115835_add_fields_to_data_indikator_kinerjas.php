<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToDataIndikatorKinerjas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('data_indikator_kinerjas', function (Blueprint $table) {
            $table->text('catatan')->nullable()->after('rated_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('data_indikator_kinerjas', function (Blueprint $table) {
            $table->dropIfExists('catatan');
        });
    }
}
