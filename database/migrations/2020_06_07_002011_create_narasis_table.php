<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNarasisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('narasis', function (Blueprint $table) {
            $table->id();
            $table->integer('unit_id');
            $table->integer('user_id');
            $table->integer('instrumen_id');
            $table->text('narasi_1');
            $table->text('narasi_2');
            $table->text('narasi_3');
            $table->text('narasi_4');
            $table->text('narasi_5');
            $table->integer('publish');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('narasis');
    }
}
