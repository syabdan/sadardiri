<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToSebaranDosen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sebaran_dosens', function (Blueprint $table) {
            $table->integer('guru_besar_s3')->after('lektor_kepala_s3');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sebaran_dosens', function (Blueprint $table) {
            $table->dropIfExists('guru_besar_s3');
        });
    }
}
