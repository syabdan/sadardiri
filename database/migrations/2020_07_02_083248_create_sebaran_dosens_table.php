<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSebaranDosensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sebaran_dosens', function (Blueprint $table) {
            $table->id();
            $table->integer('unit_id');
            $table->integer('non_fungsional_s2');
            $table->integer('asisten_ahli_s2');
            $table->integer('lektor_s2');
            $table->integer('lektor_kepala_s2');
            $table->integer('non_fungsional_s3');
            $table->integer('asisten_ahli_s3');
            $table->integer('lektor_s3');
            $table->integer('lektor_kepala_s3');
            $table->integer('jumlah');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sebaran_dosens');
    }
}
