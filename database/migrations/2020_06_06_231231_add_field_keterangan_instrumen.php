<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldKeteranganInstrumen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('instrumens', function (Blueprint $table) {
            $table->string('keterangan')->nullable()->after('nama_instrumen');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('instrumens', function (Blueprint $table) {
            $table->dropColumn('keterangan');
        });
    }
}
