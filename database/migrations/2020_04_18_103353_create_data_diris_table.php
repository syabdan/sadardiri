<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataDirisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_diris', function (Blueprint $table) {
            $table->id();
            $table->integer('pertanyaan_id');
            $table->integer('user_id')->nullable();
            $table->text('jawaban')->nullable();
            $table->string('file_data_diri')->nullable();
            $table->integer('nilai_id')->nullable();
            $table->float('persentase')->nullable();
            $table->integer('publish')->default(0);
            $table->integer('asessor_id')->nullable();
            $table->integer('unit_id');
            $table->integer('periode_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_diris');
    }
}
