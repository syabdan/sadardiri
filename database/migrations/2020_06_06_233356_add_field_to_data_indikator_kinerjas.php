<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldToDataIndikatorKinerjas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('data_indikator_kinerjas', function (Blueprint $table) {
            $table->integer('nilai_asessor_id');
            $table->float('nilai_total');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('data_indikator_kinerjas', function (Blueprint $table) {
            $table->dropColumn('nilai_asessor_id');
            $table->dropColumn('nilai_total');
        });
    }
}
