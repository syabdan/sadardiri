<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIndikatorKinerjasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('indikator_kinerjas', function (Blueprint $table) {
            $table->id();
            $table->integer('indikator_id');
            $table->text('isi_indikator_kinerja');
            $table->integer('status_unggah')->default('0');
            $table->integer('status_utama')->default('1');
            $table->integer('user_id');
            $table->integer('parent_id');
            $table->integer('as_parent')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('indikator_kinerjas');
    }
}
