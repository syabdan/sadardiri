<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSebaranAkreditasisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sebaran_akreditasis', function (Blueprint $table) {
            $table->id();
            $table->integer('unit_id');
            $table->string('sk_akreditasi');
            $table->string('akreditasi');
            $table->date('tanggal_penetapan');
            $table->date('tanggal_kadaluarsa');
            $table->string('status_kadaluarsa')->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sebaran_akreditasis');
    }
}
