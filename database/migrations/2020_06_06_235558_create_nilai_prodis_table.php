<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNilaiProdisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nilai_prodis', function (Blueprint $table) {
            $table->id();
            $table->integer('unit_id');
            // $table->integer('nilai_id');
            $table->integer('indikator_kinerja_id');
            $table->float('target')->nullable();
            $table->float('capaianmid')->nullable();
            $table->float('capaian')->nullable();
            $table->string('dokumen')->nullable();
            $table->integer('tahun')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nilai_prodis');
    }
}
