<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldCatatanToDataDiris extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('data_diris', function (Blueprint $table) {
            $table->text('catatan')->nullable()->after('nilai_asessor_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('data_diris', function (Blueprint $table) {
            $table->dropIfExists('catatan');
        });
    }
}
