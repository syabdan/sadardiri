<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSebaranMahasiswasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sebaran_mahasiswas', function (Blueprint $table) {
            $table->id();
            $table->integer('unit_id');
            $table->integer('jumlah_mhs_aktif')->nullable();
            $table->integer('jumlah_mhs_nonaktif')->nullable();
            $table->integer('jumlah_mhsasing_aktif')->nullable();
            $table->integer('jumlah_mhsasing_nonaktif')>nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sebaran_mahasiswas');
    }
}
