<?php
return [

    /*
    |--------------------------------------------------------------------------
    | MataBansos
    |--------------------------------------------------------------------------
    |
    | Untuk Pengaturan standar MataBansos
    |
    */
    'aplikasi' =>   [
                        'nama'          => 'Sistem Daring Evaluasi Diri',
                        'singkatan'     => 'Sadardiri',
                        'logo'		    => env('APP_URL').'/img/logo.png',
                    ],


    'url'   =>  [
                    'admin'     => '',
                    'public'    => '',
                ],

    'akreditasi' => [
                        ''                  => 'Unknown',
                        'A'                 => 'A',
                        'B'                 => 'B',
                        'C'                 => 'C',
                        'Unggul'            => 'Unggul',
                        'Sangat Baik'       => 'Sangat Baik',
                        'Baik'              => 'Baik',
                        "Izin Operasional"  => 'Izin Operasional',
                    ],

    'tingkat_pendidikan' => [
                        ''                              => 'Unknown',
                        'Magister'                      => 'Magister',
                        'Sarjana'                       => 'Sarjana',
                        'Diploma 3'                     => 'Diploma 3',
                        'Diploma 2'                     => 'Diploma 2',
                        'Diploma 1'                     => 'Diploma 1',
                        'SLTA atau kurang'              => 'SLTA atau kurang',
                        ],

    'jabatan' => [
                        ''                           => 'Unknown',
                        'Dekan'                      => 'Dekan',
                        'Wakil Dekan I'              => 'Wakil Dekan I',
                        'Wakil Dekan II'             => 'Wakil Dekan II',
                        'Wakil Dekan III'            => 'Wakil Dekan III',
                        ],

    'jenis_dokumen' => [
                        ''                          => 'Unknown',
                        '1'                         => 'Instrumen Evaluasi Diri',
                        '2'                         => 'Indikator Kinerja',
                        ],

    'status_unit' => [
                        ''                          => 'Unknown',
                        '0'                         => 'Semua Unit',
                        '10'                        => 'Pascasarjana',
                        '11'                        => 'Fakultas',
                        ],
    //artisan password untuk validasi melakukan sintak di command laravel
    'artisan_password'   =>  env('PASSWORD_ARTISAN', FALSE),
];
