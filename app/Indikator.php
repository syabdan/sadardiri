<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Indikator extends Model
{
    protected $dates    =   ['deleted_at'];
    protected $fillable = [
        'nama_indikator', 'keterangan'
    ];
}
