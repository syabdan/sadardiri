<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Instrumen extends Model
{
    protected $dates    =   ['deleted_at'];
    protected $fillable = [
        'nama_instrumen', 'keterangan'
    ];
}
