<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Pertanyaan;

class DataDiri extends Model
{

    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $dates    =   ['deleted_at'];
    protected $fillable = [
        'pertanyaan_id', 'user_id','jawaban', 'file_data_diri', 'url_file_datadiri', 'nilai_id','asessor_id', 'publish','unit_id', 'nilai_asessor_id', 'catatan', 'periode_id'
    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model){
            \Auth::user()->permissions_id == 4 ? ($model->user_id = \Auth::user()->id) : 0;
            \Auth::user()->permissions_id == 3 ? ($model->asessor_id = \Auth::user()->id) : 0;
            $model->unit_id = \Auth::user()->unit_id;
        });

        static::updating(function ($model){
            \Auth::user()->permissions_id == 4 ? ($model->user_id = \Auth::user()->id) : 0;
            \Auth::user()->permissions_id == 3 ? ($model->asessor_id = \Auth::user()->id) : 0;
        });

    }

    public function instrumen()
    {
        return $this->belongsTo('App\Instrumen', 'instrumen_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function cekPublish($id)
    {
        // dd($id);
        if(!empty($id)){
            $sts = \DB::table('pertanyaans')->where('pertanyaans.instrumen_id', $id)->whereNull('as_parent');
                $sts->whereNotIn('pertanyaans.id', function($q) use($id){
                    $q  ->select('pertanyaan_id')->from('data_diris')
                        ->where('data_diris.user_id', \Auth::user()->id)
                        ->whereNotNull('jawaban');
                    });
            return $sts->count();
        }
    }

    // #kriteria all
    public function instrumen_array($unit_id)
    {
        $query = DataDiri::groupBy('pertanyaans.instrumen_id')
                ->join('pertanyaans','pertanyaans.id','data_diris.pertanyaan_id')
                ->join('instrumens','instrumens.id','pertanyaans.instrumen_id')
                ->join('nilais','nilais.id','data_diris.nilai_id')
                ->select(array('instrumens.nama_instrumen as kriteria', \DB::Raw('avg(nilai) as rata_nilai')))
                ->orderBy('instrumens.id')
                ->where('data_diris.unit_id', $unit_id)
                ->get();
        // dd($query);
        if($query){
            $data =[];
            foreach($query as $row){
                $data[$row->kriteria] = $row->rata_nilai;
            }
        }
        // dd($data);
        return $data;
    }

    public function instrumen_array_asessor($unit_id)
    {
        $query = DataDiri::groupBy('pertanyaans.instrumen_id')
                ->join('pertanyaans','pertanyaans.id','data_diris.pertanyaan_id')
                ->join('instrumens','instrumens.id','pertanyaans.instrumen_id')
                ->join('nilais','nilais.id','data_diris.nilai_asessor_id')
                ->select(array('instrumens.nama_instrumen as kriteria', \DB::Raw('avg(nilai) as rata_nilai')))
                ->orderBy('instrumens.id')
                ->where('data_diris.unit_id', $unit_id)
                ->get();
        // dd($query);
        if($query){
            $data =[];
            foreach($query as $row){
                $data[$row->kriteria] = $row->rata_nilai;
            }
        }
        // dd($data);
        return $data;
    }

    public function instrumen_array_unit($id)
    {
        $query = DataDiri::groupBy('pertanyaans.instrumen_id')
                ->join('pertanyaans','pertanyaans.id','data_diris.pertanyaan_id')
                ->join('instrumens','instrumens.id','pertanyaans.instrumen_id')
                ->join('nilais','nilais.id','data_diris.nilai_id')
                ->select(array('pertanyaans.instrumen_id as kriteria', \DB::Raw('avg(nilai) as rata_nilai')))
                ->orderBy('instrumens.id')
                ->where('data_diris.unit_id', $id)
                ->get();
        // dd($query);
        if($query){
            $data =[];
            foreach($query as $row){
                $data[$row->kriteria] = $row->rata_nilai;
            }
        }
        // dd($data);
        return $data;
    }

    // Frontend
    public function frontend_chart_kriteria_all($unit_id){
        $category   = array();
        $rata_nilai_dekan = array();
        $rata_nilai_asessor = array();

            $unit = with (new Unit)->where('id', $unit_id)->first();
            foreach (with(new DataDiri)->instrumen_array($unit_id) as $key => $value) {
                $category[]             = $key;
                $rata_nilai_dekan[]     = (int) $value;
            }

            foreach (with(new DataDiri)->instrumen_array_asessor($unit_id) as $key => $value) {
                $rata_nilai_asessor[]   = (int) $value;
            }
        	return ['category' => $category, 'rata_nilai_dekan' => $rata_nilai_dekan, 'rata_nilai_asessor' => $rata_nilai_asessor,'series_nama' => $unit->nama_unit];

    }

    public function frontend_bar_chart($unit_id){
        $instrumen = Instrumen::all();
        // $tanggal = date('Y-m-d');

            $series[0]['name'] = 'Fakultas';
            $series[1]['name'] = 'Asessor';
            $kriteria = array();
            foreach ($instrumen as $ins) {
                $kriteria[] = "Indikator ".$ins->id;
                $series[0]['data'][] = (int) with(new DataDiri)->array_dekan($unit_id, $ins->id);
                $series[1]['data'][] = (int) with(new DataDiri)->array_asessor($unit_id, $ins->id);
            }

            // dd($series);
        return ['category' => $kriteria, 'series' =>  $series];
    }

    public function array_asessor($unit_id, $id_instrumen)
    {
        $query = DataDiri::
                join('pertanyaans','pertanyaans.id','data_diris.pertanyaan_id')
                ->join('instrumens','instrumens.id','pertanyaans.instrumen_id')
                ->join('nilais','nilais.id','data_diris.nilai_asessor_id')
                ->select(array('instrumens.nama_instrumen as kriteria', \DB::Raw('avg(nilai) as rata_nilai')))
                ->orderBy('instrumens.id')
                ->where('data_diris.unit_id', $unit_id)
                ->where('pertanyaans.instrumen_id', $id_instrumen)
                ->first();
        // dd($query);
        // if($query){
        //     $data =[];
        //     foreach($query as $row){
        //         $data[$row->kriteria] = $row->rata_nilai;
        //     }
        // }
        // dd($data);
        return $query->rata_nilai ?? 0;
    }

    public function array_dekan($id, $id_instrumen)
    {
        $query = DataDiri::groupBy('pertanyaans.instrumen_id')
                ->join('pertanyaans','pertanyaans.id','data_diris.pertanyaan_id')
                ->join('instrumens','instrumens.id','pertanyaans.instrumen_id')
                ->join('nilais','nilais.id','data_diris.nilai_id')
                ->select(array('pertanyaans.instrumen_id as kriteria', \DB::Raw('avg(nilai) as rata_nilai')))
                ->orderBy('instrumens.id')
                ->where('data_diris.unit_id', $id)
                ->where('pertanyaans.instrumen_id', $id_instrumen)
                ->first();
        // dd($query);
        // if($query){
        //     $data =[];
        //     foreach($query as $row){
        //         $data[$row->kriteria] = $row->rata_nilai;
        //     }
        // }
        // dd($data);
        return $query->rata_nilai ?? 0;
    }

    // #Frontend

    public function chart_kriteria_all(){
        $category   = array();
        $rata_nilai_dekan = array();
        $rata_nilai_asessor = array();
        $rata_nilai_hukum = array();
        $rata_nilai_teknik = array();
        $rata_nilai_fai = array();
        $rata_nilai_pertanian = array();
        $rata_nilai_ekonomi = array();
        $rata_nilai_komunikasi = array();
        $rata_nilai_fkip = array();
        $rata_nilai_fisipol = array();
        $rata_nilai_psikologi = array();
        $rata_nilai_pascasarjana = array();

        if(\Auth::user()->permissions_id == 3 || \Auth::user()->permissions_id == 4 || \Auth::user()->permissions_id == 5)
        {
            $unit = with (new Unit)->where('id', \Auth::user()->unit_id)->first();
            foreach (with(new DataDiri)->instrumen_array(\Auth::user()->unit_id) as $key => $value) {
                $category[]             = $key;
                $rata_nilai_dekan[]     = (int) $value;
            }

            foreach (with(new DataDiri)->instrumen_array_asessor(\Auth::user()->unit_id) as $key => $value) {
                // $category[]             = $key;
                $rata_nilai_asessor[]   = (int) $value;
            }
        	return ['category' => $category, 'rata_nilai_dekan' => $rata_nilai_dekan, 'rata_nilai_asessor' => $rata_nilai_asessor,'series_nama' => $unit->nama_unit];
        }

        if(\Auth::user()->permissions_id == 1 || \Auth::user()->permissions_id == 2)
        {
            $series[0]['name'] = 'F. Hukum';
            $series[1]['name'] = 'F. Agama Islam';
            $series[2]['name'] = 'F. Teknik';
            $series[3]['name'] = 'F. Pertanian';
            $series[4]['name'] = 'F. Ekonomi';
            $series[5]['name'] = 'F. KIP ';
            $series[6]['name'] = 'F. Sospol ';
            $series[7]['name'] = 'F. Psikologi ';
            $series[8]['name'] = 'F. Komunikasi ';
            $series[9]['name'] = 'Pascasarjana ';

                foreach (with(new DataDiri)->instrumen_array(1) as $key => $value) { //fak. hukum
                    $category[]             = $key;
                    $rata_nilai_hukum[]     = (int) $value;
                }

                foreach (with(new DataDiri)->instrumen_array(2) as $key => $value) { //fak. AI
                    $rata_nilai_fai[]       = (int) $value;
                }

                foreach (with(new DataDiri)->instrumen_array(3) as $key => $value) { //fak. Teknik
                    $rata_nilai_teknik[]     = (int) $value;
                }

                foreach (with(new DataDiri)->instrumen_array(4) as $key => $value) { //fak. Pertanian
                    $rata_nilai_pertanian[]     = (int) $value;
                }

                foreach (with(new DataDiri)->instrumen_array(5) as $key => $value) { //fak. Ekonomi
                    $rata_nilai_ekonomi[]       = (int) $value;
                }

                foreach (with(new DataDiri)->instrumen_array(6) as $key => $value) { //fak. KIP
                    $rata_nilai_fkip[]       = (int) $value;
                }

                foreach (with(new DataDiri)->instrumen_array(7) as $key => $value) { //fak. sipol
                    $rata_nilai_fisipol[]       = (int) $value;
                }

                foreach (with(new DataDiri)->instrumen_array(8) as $key => $value) { //fak. sikologi
                    $rata_nilai_psikologi[]     = (int) $value;
                }

                foreach (with(new DataDiri)->instrumen_array(9) as $key => $value) { //fak. komunikasi
                    $rata_nilai_komunikasi[]    = (int) $value;
                }

                foreach (with(new DataDiri)->instrumen_array(10) as $key => $value) { //fak. pasca
                    $rata_nilai_pascasarjana[]    = (int) $value;
                }



            return ['category' => $category, 'series' =>  $series,
                    'rata_nilai_pascasarjana' => $rata_nilai_pascasarjana,
                    'rata_nilai_komunikasi' => $rata_nilai_komunikasi,
                    'rata_nilai_psikologi' => $rata_nilai_psikologi,
                    'rata_nilai_fisipol' => $rata_nilai_fisipol,
                    'rata_nilai_fkip' => $rata_nilai_fkip,
                    'rata_nilai_ekonomi' => $rata_nilai_ekonomi,
                    'rata_nilai_pertanian' => $rata_nilai_pertanian,
                    'rata_nilai_teknik' => $rata_nilai_teknik,
                    'rata_nilai_fai' => $rata_nilai_fai,
                    'rata_nilai_hukum' => $rata_nilai_hukum,

                ];

        }
    }
    // #kriteria all

    public function instrumen_item_array($id)
    {
        $query = DataDiri::groupBy('data_diris.pertanyaan_id')
                ->join('pertanyaans','pertanyaans.id','data_diris.pertanyaan_id')
                ->join('instrumens','instrumens.id','pertanyaans.instrumen_id')
                ->join('nilais','nilais.id','data_diris.nilai_id')
                ->select(array('pertanyaan_id', \DB::Raw('avg(nilai) as rata_nilai')))
                ->orderBy('instrumens.id')
                // ->orderBy('data_diris.updated_at')
                ->where('data_diris.unit_id', \Auth::user()->unit_id)
                ->where('pertanyaans.instrumen_id', $id)
                ->get();
        // dd($query);
        if($query){
            $data =[];
            $no=1;
            foreach($query as $row){
                $data[$no] = $row->rata_nilai;
                $no++;
            }
        }
        // dd($data);
        return $data;
    }

    public function instrumen_item_array_asessor($id)
    {
        $query = DataDiri::groupBy('data_diris.pertanyaan_id')
                ->join('pertanyaans','pertanyaans.id','data_diris.pertanyaan_id')
                ->join('instrumens','instrumens.id','pertanyaans.instrumen_id')
                ->join('nilais','nilais.id','data_diris.nilai_asessor_id')
                ->select(array('pertanyaan_id', \DB::Raw('avg(nilai) as rata_nilai')))
                ->orderBy('instrumens.id')
                // ->orderBy('data_diris.updated_at')
                ->where('data_diris.unit_id', \Auth::user()->unit_id)
                ->where('pertanyaans.instrumen_id', $id)
                ->get();
        // dd($query);
        if($query){
            $data =[];
            $no=1;
            foreach($query as $row){
                $data[$no] = $row->rata_nilai;
                $no++;
            }
        }
        // dd($data);
        return $data;
    }

    public function instrumen_item_array_unit($unit,$id)
    {

        $query = DataDiri::groupBy('data_diris.pertanyaan_id')
                ->join('pertanyaans','pertanyaans.id','data_diris.pertanyaan_id')
                ->join('instrumens','instrumens.id','pertanyaans.instrumen_id')
                ->join('nilais','nilais.id','data_diris.nilai_id')
                ->select(array('pertanyaan_id', \DB::Raw('avg(nilai) as rata_nilai')))
                ->orderBy('instrumens.id')
                // ->orderBy('data_diris.updated_at')
                ->where('data_diris.unit_id', $unit)
                ->where('pertanyaans.instrumen_id', $id)
                ->get();
        // dd($query);
        if($query){
            $data =[];
            $no=1;
            foreach($query as $row){
                $data[$no] = $row->rata_nilai;
                $no++;
            }
        }
        // dd($data);
        return $data;
    }

    public function chart_kriteria($id){
        $category   = array();
        $rata_nilai_hukum = array();
        $rata_nilai_fai = array();
        $rata_nilai_teknik = array();
        $rata_nilai_pertanian = array();
        $rata_nilai_ekonomi = array();
        $rata_nilai_fkip = array();
        $rata_nilai_fisipol = array();
        $rata_nilai_psikologi = array();
        $rata_nilai_fikom = array();
        $rata_nilai_pasca = array();
        $rata_nilai_dekan = array();
        $rata_nilai_asessor = array();

        if(\Auth::user()->permissions_id == 3 || \Auth::user()->permissions_id == 4 || \Auth::user()->permissions_id == 5)
        {
            $unit = with (new Unit)->where('id', \Auth::user()->unit_id)->first();
            foreach ($this->instrumen_item_array($id) as $key => $value) {
                $category[]     = "Item ".$key;
                $rata_nilai_dekan[]   = (int) $value;
            }
            foreach ($this->instrumen_item_array_asessor($id) as $key => $value) {
                // $category[]     = "Item ".$key;
                $rata_nilai_asessor[]   = (int) $value;
            }
            return ['category' => $category, 'rata_nilai_dekan' => $rata_nilai_dekan, 'rata_nilai_asessor' => $rata_nilai_asessor, 'series_nama' => $unit->nama_unit];
        }

        if(\Auth::user()->permissions_id == 1 || \Auth::user()->permissions_id == 2)
        {
            $series[0]['name'] = 'F. Hukum';
            $series[1]['name'] = 'F. Agama Islam';
            $series[2]['name'] = 'F. Teknik';
            $series[3]['name'] = 'F. Pertanian';
            $series[4]['name'] = 'F. Ekonomi';
            $series[5]['name'] = 'F. KIP ';
            $series[6]['name'] = 'F. Sospol ';
            $series[7]['name'] = 'F. Psikologi ';
            $series[8]['name'] = 'F. Komunikasi ';
            $series[9]['name'] = 'Pascasarjana ';
            $fakultas = array();
            $units    = Unit::whereNull('parent_id')->get();

            // $i = 0;
            // foreach ($units as $unit) {
            //     $fakultas[] = $unit->nama;
            //     $series[$i]['data'][] = with(new DataDiri)->instrumen_item_array_unit($unit->id);
            //     $i++;
            // }
            // dd($series);

            foreach ($this->instrumen_item_array_unit(1,$id) as $key => $value) {
                $category[]     = "Item ".$key;
                $rata_nilai_hukum[]   = (int) $value;
            }
            foreach ($this->instrumen_item_array_unit(2,$id) as $key => $value) {
                $rata_nilai_fai[]   = (int) $value;
            }
            foreach ($this->instrumen_item_array_unit(3,$id) as $key => $value) {
                $rata_nilai_teknik[]   = (int) $value;
            }
            foreach ($this->instrumen_item_array_unit(4,$id) as $key => $value) {
                $rata_nilai_pertanian[]   = (int) $value;
            }
            foreach ($this->instrumen_item_array_unit(5,$id) as $key => $value) {
                $rata_nilai_ekonomi[]   = (int) $value;
            }
            foreach ($this->instrumen_item_array_unit(6,$id) as $key => $value) {
                $rata_nilai_fkip[]   = (int) $value;
            }
            foreach ($this->instrumen_item_array_unit(7,$id) as $key => $value) {
                $rata_nilai_fisipol[]   = (int) $value;
            }
            foreach ($this->instrumen_item_array_unit(8,$id) as $key => $value) {
                $rata_nilai_psikologi[]   = (int) $value;
            }
            foreach ($this->instrumen_item_array_unit(9,$id) as $key => $value) {
                $rata_nilai_fikom[]   = (int) $value;
            }
            foreach ($this->instrumen_item_array_unit(10,$id) as $key => $value) {
                $rata_nilai_pasca[]   = (int) $value;
            }

        return [
            'category' => $category,
            'rata_nilai_pasca' =>  $rata_nilai_pasca,
            'rata_nilai_fikom' =>  $rata_nilai_fikom,
            'rata_nilai_psikologi' =>  $rata_nilai_psikologi,
            'rata_nilai_fisipol' =>  $rata_nilai_fisipol,
            'rata_nilai_fkip' =>  $rata_nilai_fkip,
            'rata_nilai_ekonomi' =>  $rata_nilai_ekonomi,
            'rata_nilai_pertanian' =>  $rata_nilai_pertanian,
            'rata_nilai_teknik' =>  $rata_nilai_teknik,
            'rata_nilai_fai' =>  $rata_nilai_fai,
            'rata_nilai_hukum' =>  $rata_nilai_hukum,
            ];
        }
    }

    public function cek_progres_evaluasi(){
        $periode        = Periode::whereStatus('1')->first();
        $data           = DataDiri::join('pertanyaans','pertanyaans.id','data_diris.pertanyaan_id')
                            ->where('data_diris.periode_id', $periode->id)
                            ->whereNotNull('jawaban');
                            if(\Auth::user()->permissions_id == 3 || \Auth::user()->permissions_id == 4 || \Auth::user()->permissions_id == 5){
                                $data->where('data_diris.unit_id', \Auth::user()->unit_id);
                            }
        // $data_diri      = $data->count();
        // $presentase     = ($data_diri / $pertanyaan) * 100;
        return $data;
    }

    public function cek_progres_unggah(){
        $periode        = Periode::whereStatus('1')->first();
        $data           = DataDiri::join('pertanyaans','pertanyaans.id','data_diris.pertanyaan_id')
                            ->whereNotNull('file_data_diri')
                            ->where('data_diris.periode_id', $periode->id);
                            if(\Auth::user()->permissions_id == 3 || \Auth::user()->permissions_id == 4 || \Auth::user()->permissions_id == 5){
                                $data->where('data_diris.unit_id', \Auth::user()->unit_id);
                            }
        // $data_diri      = $data->count();

        // $presentase     = ($data_diri / $pertanyaan) * 100;
        return $data;
    }
}
