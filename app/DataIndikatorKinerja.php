<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\IndikatorKinerja;

class DataIndikatorKinerja extends Model
{
    protected $dates    =   ['deleted_at'];
    protected $fillable = [
        'id','indikator_kinerja_id', 'rated_by','file_indikator_kinerja', 'url_file_indikator',  'capaian', 'capaianmid','target','nilai_id','nilai_asessor_id','status','created_by','updated_by','unit_id', 'persentase','periode_id', 'catatan'
    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model){
            \Auth::user()->permissions_id == 4 ? ($model->created_by = \Auth::user()->id) : 0;
            \Auth::user()->permissions_id == 3 ? ($model->rated_by = \Auth::user()->id) : 0;
            $model->unit_id = \Auth::user()->unit_id;
        });

        static::updating(function ($model){
            \Auth::user()->permissions_id == 4 ? ($model->created_by = \Auth::user()->id) : 0;
            \Auth::user()->permissions_id == 3 ? ($model->rated_by = \Auth::user()->id) : 0;
            $model->unit_id = \Auth::user()->unit_id;
        });

    }

    public function indikator_kinerja()
    {
        return $this->belongsTo('App\IndikatorKinerja', 'indikator_kinerja_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function periode()
    {
        return $this->belongsTo('App\User', 'periode_id');
    }

    public function cek_progres_indikator(){
        // $jumlah_indikator     = IndikatorKinerja::whereNull('as_parent')->count();
        $periode              = Periode::whereStatus('1')->first();

        $data_indikator      = $this::groupBy('indikator_kinerja_id')
                            ->join('indikator_kinerjas','indikator_kinerjas.id','data_indikator_kinerjas.indikator_kinerja_id')
                            ->where('data_indikator_kinerjas.periode_id', $periode->id);
                            if(\Auth::user()->permissions_id == 3 || \Auth::user()->permissions_id == 4 || \Auth::user()->permissions_id == 5){
                                $data_indikator->where('created_by', \Auth::user()->id);
                            }
        // $presentase     = ($data_indikator / $jumlah_indikator) * 100;
        // dd($data_indikator->get());
        return $data_indikator;
    }

    public function jumlah_indikator(){
        $jumlah_indikator     = IndikatorKinerja::whereNull('as_parent')->count();
        return $jumlah_indikator;
    }

    public function frontend_bar_chart($unit_id){
        $indikator = Indikator::all();
        // $tanggal = date('Y-m-d');

            $series[0]['name'] = 'Fakultas';
            // $series[1]['name'] = 'Asessor';
            $kriteria = array();
            foreach ($indikator as $ins) {
                $kriteria[] = $ins->keterangan;
                $series[0]['data'][] = (int) with(new DataIndikatorKinerja)->array_dekan($unit_id, $ins->id);
                // $series[1]['data'][] = (int) with(new DataIndikatorKinerja)->array_asessor($unit_id, $ins->id);
            }

            // dd($series);
        return ['category' => $kriteria, 'series' =>  $series];
    }

    public function array_asessor($unit_id, $id_indikator)
    {
        $query = DataIndikatorKinerja::groupBy('indikator_kinerjas.indikator_id')
                ->join('indikator_kinerjas','indikator_kinerjas.id','data_indikator_kinerjas.indikator_kinerja_id')
                ->join('indikators','indikators.id','indikator_kinerjas.indikator_id')
                ->join('nilais','nilais.id','data_indikator_kinerjas.nilai_asessor_id')
                ->select( \DB::Raw('avg(nilai) as rata_nilai'))
                ->orderBy('data_indikator_kinerjas.updated_at')
                ->where('status_utama', 1)
                ->where('data_indikator_kinerjas.unit_id', $unit_id)
                ->where('indikator_kinerjas.indikator_id', $id_indikator)
                ->first();

        return $query->rata_nilai ?? 0;
    }

    public function array_dekan($id, $id_indikator)
    {
        $query = DataIndikatorKinerja::groupBy('indikator_kinerjas.indikator_id')
                ->join('indikator_kinerjas','indikator_kinerjas.id','data_indikator_kinerjas.indikator_kinerja_id')
                ->join('indikators','indikators.id','indikator_kinerjas.indikator_id')
                ->join('nilais','nilais.id','data_indikator_kinerjas.nilai_id')
                ->select( \DB::Raw('avg(nilai) as rata_nilai'))
                ->orderBy('data_indikator_kinerjas.updated_at')
                ->where('status_utama', 1)
                ->where('data_indikator_kinerjas.unit_id', $id)
                ->where('indikator_kinerjas.indikator_id', $id_indikator)
                ->first();

        return $query->rata_nilai ?? 0;
    }

}
