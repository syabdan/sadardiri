<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dosen extends Model
{   
    protected $fillable = [
        'nidn', 'nama_dosen','gelar_depan', 'gelar_belakang','prodi','fakultas'
    ];

    public function unit()
    {
        return $this->belongsTo('App\Unit', 'dosen_id');
    }
}
