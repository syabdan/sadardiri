<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class nilaiProdi extends Model
{
    protected $fillable = [
        'unit_id', 'indikator_kinerja_id','target','capaianmid','capaian','dokumen','tahun'
    ];

    public function unit()
    {
        return $this->belongsTo('App\Unit', 'unit_id');
    }
}
