<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;

class check_user extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'off:user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Non Aktif kan User yang lewat masa Periode';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Proses Check User ...');
        $start = microtime(true);

        $user           =    User::select('id')
                            ->where('tanggal_awal', '<=', date('Y-m-d'))
                            ->where('tanggal_akhir','>=', date('Y-m-d'))
                            ->get();
        // $json_user      = json_encode($user);
        $data_user      = json_decode($user);

        $arr_user = array();
        foreach($data_user as $val){
            $arr_user[] = $val->id;
        }

        // Untuk Sementara di off kan dulu
        // $usr            = User::whereNotIn('id',$arr_user)->whereIn('permissions_id',[3,4])->delete();


        $time_elapsed_secs = microtime(true) - $start;
        $this->info('Selesai...... '.$time_elapsed_secs);

    }
}
