<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\SebaranAkreditasi;

class check_akreditasi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'off:akreditasi';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Non aktifkan akreditasi lewat tanggal kadaluarsa';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Proses Check Akreditasi ...');
        $start = microtime(true);

        $data           =    SebaranAkreditasi::select('id')
                            ->where('tanggal_penetapan', '<=', date('Y-m-d'))
                            ->where('tanggal_kadaluarsa','>=', date('Y-m-d'))
                            ->get();
        // $json_user      = json_encode($user);
        $data_json      = json_decode($data);

        $arr_data = array();
        foreach($data_json as $val){
            $arr_data[] = $val->id;
        }
        // dd($arr_user);

        $usr            = SebaranAkreditasi::whereNotIn('id',$arr_data)->delete();
        // dd($usr);
        $time_elapsed_secs = microtime(true) - $start;
        $this->info('Selesai...... '.$time_elapsed_secs);


    }
}
