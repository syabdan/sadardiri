<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\SebaranMahasiswa;

class tarikTotalMahasiswa extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tarik:total_mahasiswa';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Tarik data total mahasiswa dari API Sikad';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("\n" .'Tarik total mahasiswa');

        \DB::table('sebaran_mahasiswas')->truncate();

        $data1 = api_total_mahasiswa_aktif_lokal();
        $list = json_decode($data1, true);

		for($a=0; $a < count($list); $a++){
                SebaranMahasiswa::updateOrCreate(['unit_id' => $list[$a]['kode_prodi']],[
                    'unit_id'                     => $list[$a]['kode_prodi'],
                    'jumlah_mhs_aktif'            => $list[$a]['total']
                ]);
        }

        $data2 = api_total_mahasiswa_aktif_asing();
        $list2 = json_decode($data2, true);

		for($a=0; $a < count($list2); $a++){
                SebaranMahasiswa::updateOrCreate(['unit_id' => $list2[$a]['kode_prodi']],[
                    'jumlah_mhsasing_aktif'       => $list2[$a]['total']
                ]);
        }

        $this->info("\n" .'Selesai');

    }
}
