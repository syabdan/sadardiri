<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SebaranPegawai extends Model
{

    protected $fillable = [
        'tingkat_pendidikan', 'tetap', 'hampir_tetap', 'kontrak', 'jumlah'
    ];

    public function unit()
    {
        return $this->belongsTo('App\Unit', 'unit_id');
    }
}
