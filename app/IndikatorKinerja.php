<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class IndikatorKinerja extends Model
{
    protected $dates    =   ['deleted_at'];
    protected $fillable = [
        'indikator_id', 'isi_indikator_kinerja','status_unggah','status_unit','deleted_at'
    ];

    public static  function boot()
    {
        parent::boot();
        static::creating(function ($model){
            \Auth::user()->permissions_id == 1 ? ($model->status_utama = 1) : ($model->status_utama = 0);
            $model->user_id = \Auth::user()->id;
        });
    }

    public function indikator()
    {
        return $this->belongsTo('App\Indikator', 'indikator_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function dataIndikator()
    {
        return $this->hasOne('App\DataIndikatorKinerja')->select('data_indikator_kinerjas.id as iddata','file_indikator_kinerja','rated_by','url_file_indikator', 'capaian', 'capaianmid','target','nilai_id','nilai_asessor_id','status','created_by','updated_by','unit_id', 'persentase','periode_id', 'catatan')
        ->join('periodes', 'periodes.id', 'data_indikator_kinerjas.periode_id')
        ->leftjoin('nilais','nilais.id','data_indikator_kinerjas.nilai_id')
        ->where('data_indikator_kinerjas.unit_id', \Auth::user()->unit_id)
        ->where('periodes.status',1)
        ->orderBy('data_indikator_kinerjas.id', 'DESC');

    }

    // public function data ($unit_id, $jenis)
    // {
    //     $unit_id == 'NULL' ? $unit_id = 'all' : $unit_id = $unit_id;

    //     $data = \DB::table('indikator_kinerjas AS p_indikator')
    //                 ->leftjoin('indikator_kinerjas AS c_indikator', 'p_indikator.id', '=', 'c_indikator.parent_id')
    //                 ->leftjoin('data_indikator_kinerjas','c_indikator.id','data_indikator_kinerjas.indikator_kinerja_id')
    //                 ->join('users','users.unit_id','data_indikator_kinerjas.unit_id')
    //                 ->join('nilais','nilais.id','data_indikator_kinerjas.nilai_id');
    //                 if($jenis == 1){
    //                     $data->where('c_indikator.status_utama', 1);
    //                 }
    //                 if($jenis == 2){
    //                     $data->where('c_indikator.status_utama', '<>', 1);
    //                 }
    //                 $data->where('data_indikator_kinerjas.unit_id', $unit_id)
    //                 ->where('users.permissions_id', 4)
    //                 ->select(['c_indikator.id as id'
    //                 ,'c_indikator.isi_indikator_kinerja as isi_child'
    //                 ,'c_indikator.parent_id as c_parent'
    //                 ,'p_indikator.id as p_id'
    //                 ,'p_indikator.isi_indikator_kinerja as isi_parent'
    //                 ,'p_indikator.parent_id as p_parent'
    //                 ,'target'
    //                 ,'capaianmid'
    //                 ,'capaian'
    //                 ,'nilai'
    //                 ,'persentase'
    //                 ,'data_indikator_kinerjas.unit_id as unit_id'
    //                 ,'nama_user'
    //                 ])->groupBy('c_indikator.id');

    //     // dd(json_encode($data->get()));

    //     return $data->get();
    // }

    public function data($unit_id, $jenis, $periode_id=NULL)
    {
        $unit_id == 'NULL' ? $unit_id = 'all' : $unit_id = $unit_id;

        $data = \DB::table('indikator_kinerjas')
                    ->join('indikators', 'indikators.id', '=', 'indikator_kinerjas.indikator_id')
                    ->leftjoin('data_indikator_kinerjas','indikator_kinerjas.id','data_indikator_kinerjas.indikator_kinerja_id')
                    ->join('users','users.unit_id','data_indikator_kinerjas.unit_id')
                    ->join('nilais','nilais.id','data_indikator_kinerjas.nilai_asessor_id');
                    if($jenis == 1){
                        $data->where('indikator_kinerjas.status_utama', 1);
                    }
                    if($jenis == 2){
                        $data->where('indikator_kinerjas.status_utama', 0);
                    }
                    if($periode_id){
                        $data->where('periode_id', $periode_id);
                    }
                    $data->where('data_indikator_kinerjas.unit_id', $unit_id)
                    ->where('users.permissions_id', 4)
                    ->select([
                    'indikator_kinerjas.id as p_id'
                    ,'indikator_id'
                    ,'keterangan'
                    ,'indikators.keterangan as isi_parent'
                    ,'indikator_kinerjas.isi_indikator_kinerja as isi_child'
                    ,'indikator_kinerjas.parent_id as p_parent'
                    ,'target'
                    ,'capaianmid'
                    ,'capaian'
                    ,'nilai'
                    ,'persentase'
                    ,'data_indikator_kinerjas.unit_id as unit_id'
                    ,'nama_user'
                    ]);

        // dd(json_encode($data->get()));

        return $data;
    }

}
