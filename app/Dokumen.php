<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dokumen extends Model
{

    protected $fillable = [
        'unit_id', 'user_id','periode_id', 'jenis','file'];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model){
            $model->unit_id = \Auth::user()->unit_id;
            $model->user_id = \Auth::user()->id;
        });

    }

    public function periode()
    {
        return $this->belongsTo('App\Periode', 'periode_id');
    }
}
