<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{

    protected $fillable = [
        'permission_name', 'alias','detail', 'view_status','add_status','edit_status','delete_status','data_menus', 'status'
    ];

    public function users()
    {
        return $this->hasMany('App\User', 'permission_id');
    }
}
