<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Narasi extends Model
{
    protected $dates    =   ['deleted_at'];
    protected $fillable = [
        'instrumen_id','user_id','unit_id','narasi_1', 'narasi_2', 'narasi_3', 'narasi_4', 'narasi_5','publish','periode_id'
    ];
    public static  function boot()
    {
        parent::boot();
        static::creating(function ($model){
            $model->unit_id = \Auth::user()->unit_id;
            $model->user_id = \Auth::user()->id;
        });
    }
}
