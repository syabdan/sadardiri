<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $dates    =   ['deleted_at'];
    protected $fillable = [
        'nama_user', 'email','hp', 'password','permissions_id','tanggal_awal','tanggal_akhir','unit_id','pic','dosen_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // public function setPasswordAttribute($value)
    // {
    //     if ($value != "") {
    //         $this->attributes['password'] = bcrypt(base64_decode(trim($value)));
    //     }
    // }

    public function permissionsGroup()
    {
        return $this->belongsTo('App\Permission', 'permissions_id');
    }

    public function unit()
    {
        return $this->belongsTo('App\Unit', 'unit_id');
    }

    public function groupUnit()
    {
        (\Auth::user()->unit_id == 10) ? $data = 10 : $data = 11;
        // dd($data);
        return $data;
    }

    public function periode()
    {
        return $this->belongsTo('App\Periode', 'periode_id');
    }
}
