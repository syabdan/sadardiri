<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    protected $fillable = [
        'nama_unit', 'jenjang_pendidikan','parent_id', 'warna'
    ];

    public function dosen()
    {
        return $this->belongsTo('App\Dosen', 'dosen_id');
    }

    public function child()
    {
        return $this->hasMany('App\Unit', 'parent_unit_id')->with('parent');
    }
    
    public function parent()
    {
        return $this->belongsTo('App\Unit', 'parent_unit_id');
    }
}
