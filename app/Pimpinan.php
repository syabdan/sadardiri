<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Pimpinan extends Model
{
    use Notifiable;
    use SoftDeletes;
    protected $dates    =   ['deleted_at'];
    protected $fillable = [
        'id','unit_id', 'jabatan', 'nama_pimpinan', 'no_hp'
    ];

    public function unit()
    {
        return $this->belongsTo('App\Unit', 'unit_id');
    }
}
