<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $fillable = [
        'posisi', 'nama','link', 'icon','status','tampil','parent_menu_id'
    ];

    public function subMenus()
    {
        return $this->hasMany(static::class, 'parent_menu_id')->orderby('posisi', 'asc');
    }
}
