<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Capaian extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];  
    protected $casts = ['pilihan'=> 'array'];
    protected $fillable = [
        'pertanyaan_id','pilihan'
    ];

    public function pertanyaan()
    {
        return $this->belongsTo('App\Pertanyaan', 'pertanyaan_id');
    }
}
