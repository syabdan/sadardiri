<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Periode extends Model
{
    protected $dates    =   ['deleted_at'];
    protected $fillable = [
        'nama_periode', 'tanggal_target_awal','tanggal_target_akhir', 'tanggal_capaianmid_awal','tanggal_capaianmid_akhir','tanggal_capaian_awal','tanggal_capaian_akhir',
    ];

    public function cek_periode(){

        $data_target      = $this::where('tanggal_target_awal', '<=', date('Y-m-d'))
                            ->where('tanggal_target_akhir','>=', date('Y-m-d'))
                            ->where('status',1)
                            ->first();

        $data_capaian_mid      = $this::where('tanggal_capaianmid_awal', '<=', date('Y-m-d'))
                            ->where('tanggal_capaianmid_akhir','>=', date('Y-m-d'))
                            ->where('status',1)
                            ->first();

        $data_capaian      = $this::where('tanggal_capaian_awal', '<=', date('Y-m-d'))
                            ->where('tanggal_capaian_akhir','>=', date('Y-m-d'))
                            ->where('status',1)
                            ->first();

        $periode = array();
        if($data_target){
            $periode = ['nama' => 'Target', 'periode_akhir' => $data_target->tanggal_target_akhir, 'alias' => 'c_target'];
        }
        else

        if($data_capaian_mid){
            $periode = ['nama' => 'Capaian Tengah Semester', 'periode_akhir' => $data_capaian_mid->tanggal_capaianmid_akhir, 'alias' => 'c_mid'];
        }else

        if($data_capaian){
            $periode = ['nama' => 'Capaian Akhir', 'periode_akhir' => $data_capaian->tanggal_capaian_akhir, 'alias' => 'c_akhir'];
        }

        // dd($periode);
        return $periode ?? ['nama' => '', 'periode_akhir' => '', 'alias' => ''];
    }
}
