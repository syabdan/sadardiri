<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class SebaranAkreditasi extends Model
{

    use Notifiable;
    use SoftDeletes;

    protected $dates    =   ['deleted_at'];
    protected $fillable = [
        'unit_id', 'sk_akreditasi', 'akreditasi', 'tanggal_penetapan', 'tanggal_kadaluarsa', 'status_kadaluarsa', 'file_akreditasi'
    ];

    public function unit()
    {
        return $this->belongsTo('App\Unit', 'unit_id');
    }

    // Code for pie chart akreditasi
    public function rasio_sebaran($unit_id)
    {
        $query = \DB::table('units AS p_unit')
        ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_id')
        ->join('sebaran_akreditasis','sebaran_akreditasis.unit_id','c_unit.id')
        ->select('akreditasi', \DB::Raw('count(akreditasi) as total'))
        ->where('p_unit.id', $unit_id)
        ->whereNull('sebaran_akreditasis.deleted_at')
        ->groupBy('akreditasi')
        ->get();

        if($query){
            $data =[];
            foreach($query as $row){
                $data[$row->akreditasi] = $row->total;
            }
        }

        return $data;
    }

    public function frontend_pie_chart($unit_id)
    {
        $data = array();
        foreach ($this->rasio_sebaran($unit_id) as $key => $value) {
            $data[] = ['Akreditasi '.$key, (int) $value, false];
        }
        // dd(json_encode($data));

            return ['data' =>  $data];

    }


    // #Code for pie chart akreditasi

}
