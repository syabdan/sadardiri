<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pertanyaan extends Model
{
    protected $dates    =   ['deleted_at'];
    protected $fillable = [
        'instrumen_id', 'isi_pertanyaan','parent_id', 'status_unggah', 'status_ambil_lagi','as_parent','persentase'
    ];

    public function instrumen()
    {
        return $this->belongsTo('App\Instrumen', 'instrumen_id');
    }

    public function subPertanyaan()
    {
        return $this->hasMany(static::class, 'parent_id');
    }

    public function dataDiri()
    {
        return $this->hasOne('App\DataDiri')->where('data_diris.user_id', \Auth::user()->id);

    }

    public function data($unit_id)
    {
        $unit_id == 'NULL' ? $unit_id = 'all' : $unit_id = $unit_id;

        $data = \DB::table('pertanyaans')
                    ->join('instrumens', 'instrumens.id', '=', 'pertanyaans.instrumen_id')
                    ->join('data_diris','pertanyaans.id','data_diris.pertanyaan_id')
                    ->join('users','users.unit_id','data_diris.unit_id')
                    ->leftjoin('nilais','nilais.id','data_diris.nilai_id')
                    ->where('data_diris.unit_id', $unit_id)
                    ->where('data_diris.publish', 1)
                    ->where('users.permissions_id', 4)
                    ->whereNull('users.deleted_at')
                    ->select('pertanyaans.id as id'
                    ,'as_parent'
                    ,'parent_id'
                    ,'isi_pertanyaan'
                    ,'jawaban'
                    ,'file_data_diri'
                    ,'publish'
                    ,'nilai'
                    ,'persentase'
                    ,'data_diris.unit_id as unit_id'
                    ,'nama_user'
                    ,'nama_instrumen'
                    );

        // dd(json_encode($data->get()));

        return $data;
    }

}
