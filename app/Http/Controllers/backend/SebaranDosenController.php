<?php

namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use App\Permission;
use App\User;
use App\SebaranDosen;
use App\Unit;
use Auth;
use Illuminate\Config;
use Illuminate\Http\Request;
use View;
use Yajra\DataTables\DataTables;
use Validator;
use DB;
use Illuminate\Support\Arr;

class SebaranDosenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = SebaranDosen::all();

        if(\Auth::user()->permissions_id == 3 || Auth::user()->permissions_id == 4 || \Auth::user()->permissions_id == 5){
            $datas = \DB::table('units AS p_unit')
                ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_id')
                ->join('sebaran_dosens','sebaran_dosens.unit_id','c_unit.id')
                ->select('sebaran_dosens.*', 'c_unit.nama_unit as nama_unit', 'c_unit.jenjang_pendidikan as jenjang_pendidikan')
                ->where('p_unit.id', \Auth::user()->unit_id)
                ->whereNull('sebaran_dosens.deleted_at')
                ->get();
        }
        return view('backend.sebaran_dosen.index', compact('datas'));
    }

    public function data(Request $request)
    {
        $data = SebaranDosen::orderBy('sebaran_dosens.id', 'DESC')->with('unit');
        if(\Auth::user()->permissions_id == 3 || \Auth::user()->permissions_id == 4 || \Auth::user()->permissions_id == 5){
            $data->join('units','units.id','sebaran_dosens.unit_id')
            ->where('parent_id', \Auth::user()->unit_id);
        }

        return DataTables::of($data->get())

            ->addColumn('actions',function($data) {
                $actions = '<a data-id="'.$data->id.'"  class="btn btn-tbl-edit btn-xs ubah"><i class="fa fa-pencil"></i></a>';
                $actions .= '<a  data-id="'.$data->id.'" class="btn btn-tbl-delete btn-xs hapus"><i class="fa fa-trash-o"></i></a>';
                return $actions;
            })

            ->addIndexColumn()
            ->rawColumns(['actions'])
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data           = SebaranDosen::all();
        // $unit           = Unit::whereNotNull('parent_id')->pluck('nama_unit','id');
        $unit           = Unit::whereNotNull('parent_id')->get();

        if(\Auth::user()->permissions_id == 3 || \Auth::user()->permissions_id == 4){
            $unit           = Unit::where('parent_id', \Auth::user()->unit_id)->pluck('nama_unit','id');
        }

        return view('backend.sebaran_dosen.tambah', compact('data','unit'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [

        ]);
        if ($validator->fails()) {
            $respon = array('status'=>false, 'pesan' => $validator->messages());
        } else {
        $data = new SebaranDosen;

        $data->unit_id                  = $request->unit_id;
        $data->non_fungsional_s2        = $request->non_fungsional_s2;
        $data->asisten_ahli_s2          = $request->asisten_ahli_s2;
        $data->lektor_s2                = $request->lektor_s2;
        $data->lektor_kepala_s2         = $request->lektor_kepala_s2;
        $data->non_fungsional_s3        = $request->non_fungsional_s3;
        $data->asisten_ahli_s3          = $request->asisten_ahli_s3;
        $data->lektor_s3                = $request->lektor_s3;
        $data->lektor_kepala_s3         = $request->lektor_kepala_s3;
        $data->guru_besar_s3            = $request->guru_besar_s3;

        $jumlah = $request->non_fungsional_s2 + $request->asisten_ahli_s2 + $request->lektor_s2 + $request->lektor_kepala_s2 + $request->non_fungsional_s3 + $request->asisten_ahli_s3 + $request->lektor_s3 + $request->lektor_kepala_s3 + $request->guru_besar_s3;

        $data->jumlah                   = $jumlah;

        $data =  $data->save() ? 1 : 0;

        if ($data) {
            $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil disimpan']);
        } else {
            $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal disimpan']);
        }
    }
    return response()->json($respon);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data           = SebaranDosen::find($id);

        $unit = Unit::whereNotNull('parent_id')->select("id", DB::raw("CONCAT(units.nama_unit,' ',units.jenjang_pendidikan) as full_name"))
        ->pluck('full_name', 'id');

        if(\Auth::user()->permissions_id == 3 || \Auth::user()->permissions_id == 4){
            $unit = Unit::whereNotNull('parent_id')->where('parent_id', \Auth::user()->unit_id)->select("id", DB::raw("CONCAT(units.nama_unit,' ',units.jenjang_pendidikan) as full_name"))
        ->pluck('full_name', 'id');

        }

        return view('backend.sebaran_dosen.ubah', compact('data','unit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [

       ]);
        if ($validator->fails()) {
            $respon = array('status'=>false, 'pesan' => $validator->messages());
        } else {
            $data   = SebaranDosen::find($id);

        $data->unit_id                  = $request->unit_id;
        $data->non_fungsional_s2        = $request->non_fungsional_s2;
        $data->asisten_ahli_s2          = $request->asisten_ahli_s2;
        $data->lektor_s2                = $request->lektor_s2;
        $data->lektor_kepala_s2         = $request->lektor_kepala_s2;
        $data->non_fungsional_s3        = $request->non_fungsional_s3;
        $data->asisten_ahli_s3          = $request->asisten_ahli_s3;
        $data->lektor_s3                = $request->lektor_s3;
        $data->lektor_kepala_s3         = $request->lektor_kepala_s3;
        $data->guru_besar_s3            = $request->guru_besar_s3;

        $jumlah = $request->non_fungsional_s2 + $request->asisten_ahli_s2 + $request->lektor_s2 + $request->lektor_kepala_s2 + $request->non_fungsional_s3 + $request->asisten_ahli_s3 + $request->lektor_s3 + $request->lektor_kepala_s3 + $request->guru_besar_s3;

        $data->jumlah                   = $jumlah;

        $data =  $data->update() ? 1 : 0;

            if ($data) {
                $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil diubah']);
            } else {
                $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal diubah']);
            }
        }
        return response()->json($respon);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function hapus($id)
    {
        $data = SebaranDosen::find($id);
        return view('backend.sebaran_dosen.hapus', ['data' => $data]);
    }

    public function destroy($id)
    {
        $data = SebaranDosen::find($id);

        if ($data->delete()) {
            $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil dihapus']);
        } else {
            $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal dihapus']);
        }
        return response()->json($respon);
    }
}
