<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Permission;
use App\Instrumen;
use App\Pertanyaan;
use App\DataDiri;
use App\Capaian;
use App\Narasi;
use App\Periode;
use Auth;
use Illuminate\Config;
use Illuminate\Http\Request;
use Redirect;
use View;
use Yajra\DataTables\DataTables;
use Validator;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;

class InstrumenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.pertanyaan.index');
    }

    public function kriteria($id)
    {
        // $id_kriteria    = $id;
        $pertanyaan     = Pertanyaan::all();
        $data_jawaban   = \DB::table('data_diris')->groupBy('pertanyaan_id')->where('user_id', Auth::user()->id)->orderBy('id','desc')->get();
        $title          = 'Kriteria '.$id;
        $kriteria       = Instrumen::whereId($id)->first();
        $publish        = with(new DataDiri)->cekPublish($id);
        $capaians       = Capaian::get();
        $data_narasi    = Narasi::join('periodes', 'periodes.id', 'narasis.periode_id')
                            ->where('periodes.status', 1)
                            ->where('user_id', Auth::user()->id)
                            ->get();
        // dd($capaians);
        return view('backend.kriteria.index', compact('id','pertanyaan','title','data_jawaban','publish', 'kriteria', 'capaians', 'data_narasi'));
    }

    public function data(Request $request)
    {
        $data = Pertanyaan::with(['instrumen'])->orderBy('id', 'DESC')->get();
        return DataTables::of($data)

            ->addColumn('actions',function($data) {
                $actions = '<a href="#edit-'.$data->id.'" data-id="'.$data->id.'"  class="btn btn-tbl-edit btn-xs ubah"><i class="fa fa-pencil"></i></a>';
                $actions .= '<a  data-id="'.$data->id.'" class="btn btn-tbl-delete btn-xs hapus"><i class="fa fa-trash-o"></i></a>';
                return $actions;
            })
            ->addColumn('status_unggah',function($data) {
                $data->status_unggah == 1 ? $return = '<span class="label label-sm label-success">Aktif</span>' : $return = '<span class="label label-sm label-danger">Tidak Aktif</span>';
                return $return;
            })
            ->addColumn('isi_pertanyaan',function($data) {
                return "<div style='border:1px solid #ccc; padding:10px; max-height:100px;overflow:scroll;'>$data->isi_pertanyaan</div>";
            })
            ->addIndexColumn()
            ->rawColumns(['actions','status_unggah','isi_pertanyaan'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data           = Pertanyaan::all();
        $instrumen      = Instrumen::pluck('nama_instrumen','id');
        $unggah         = array(0 => 'Tidak Aktif', 1 => 'Aktif');
        $ambil_lagi     = array(NULL => 'Tidak', 1 => 'Ya');
        $data_jawaban   = DataDiri::where('user_id', Auth::user()->id)->get();
        return view('backend.pertanyaan.tambah', compact('data','instrumen','unggah','data_jawaban', 'ambil_lagi'));
    }

    public function disable()
    {
        $data_jawaban   = DataDiri::where('user_id', Auth::user()->id)->get();
        return response()->json($data_jawaban);
    }

    public function ambil_jawaban(Request $request)
    {
        $id_pertanyaan = str_replace("checkbox_","",$request->id_pertanyaan);
        $data_jawaban   = DataDiri::join('periodes', 'periodes.id', 'data_diris.periode_id')
                            ->where('periodes.status', 1)
                            ->where('user_id', Auth::user()->id)
                            ->where('pertanyaan_id', $id_pertanyaan)
                            ->where('publish', 1)
                            ->first();
        return response()->json($data_jawaban);
    }

    public function ambil_pilihan(Request $request)
    {
        $data   = DataDiri::join('periodes', 'periodes.id', 'data_diris.periode_id')
                        ->where('periodes.status', 1)
                        ->where('user_id', Auth::user()->id)->get();
        return response()->json($data);
    }

    public function ambil_narasi($idkriteria)
    {
        $data   = narasi::where('periode_id', getPeriodeId())
                        ->where('user_id', Auth::user()->id)
                        ->where('instrumen_id', $idkriteria)->orderBy('narasis.id', 'ASC')->first();

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'instrumen_id'               => 'required',
            'isi_pertanyaan'             => 'required',
        ]);
        if ($validator->fails()) {
            $respon = array('status'=>false, 'pesan' => $validator->messages());
        } else {
            $data = Pertanyaan::create($request->all());

            if($request->has('isi_pertanyaan_sub')){
                $id_baru_disimpan = $data->id;
                Pertanyaan::find($id_baru_disimpan)->update(['as_parent' => 1]); // update status as_parent

                $nourut = 1;
                for($i=0;$i<count($request->isi_pertanyaan_sub);$i++)
                {

                    $parent = new Pertanyaan;
                    $parent->parent_id          = $id_baru_disimpan;
                    $parent->instrumen_id       = $request->instrumen_id;
                    $parent->isi_pertanyaan     = $request->isi_pertanyaan_sub[$i];
                    $parent->status_unggah      = $request->status_unggah_sub[$i];
                    $parent->status_ambil_lagi  = $request->status_ambil_lagi_sub[$i];
                    $parent->save();

                    $capaian = new Capaian;
                        $pilihan =  [[
                                        'nilai'             => 1,
                                        'isi_pilihan'       => $request->pilihan1[$i],
                                        ],
                                        [
                                        'nilai'             => 2,
                                        'isi_pilihan'       => $request->pilihan2[$i],
                                        ],
                                        [
                                        'nilai'             => 3,
                                        'isi_pilihan'       => $request->pilihan3[$i],
                                        ],
                                        [
                                        'nilai'             => 4,
                                        'isi_pilihan'       => $request->pilihan4[$i],
                                        ],
                                    ];
                    $capaian->pertanyaan_id     = $parent->id;
                    $capaian->pilihan           = $pilihan;
                    $capaian->save();

                    $nourut++;
                }
            }

            $data =  $data->save() ? 1 : 0;
            if ($data) {
                $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil disimpan']);
            } else {
                $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal disimpan']);
            }
        }
        return response()->json($respon);
    }

    public function simpan_jawaban(Request $request)
    {
        $id = (int) $request->id_jawaban;

        $periode       = Periode::orderBy('id','desc')->where('status', 1)->first();

        if(empty($periode)){
            $respon = array('status'=>false, 'pesan' => ['msg' => 'Maaf, Data Periode Tidak Ada Yang Aktif, Silahkan hubungi LPM UIR']);
            return response()->json($respon);

        }else{

            $update = DataDiri::updateOrCreate(['pertanyaan_id' => $id,'user_id' => Auth::user()->id, 'periode_id' => $periode->id], [
                'jawaban'           => $request->jawaban,
                'pertanyaan_id'     => $id,
                'periode_id'        => $periode->id
            ]);

        }

        return response()->json($update);
    }

    public function simpan_capaian(Request $request)
    {


        $periode       = Periode::orderBy('id','desc')->where('status', 1)->first();

        if(empty($periode)){
            $respon = array('status'=>false, 'pesan' => ['msg' => 'Maaf, Data Periode Tidak Ada Yang Aktif, Silahkan hubungi LPM UIR']);
            return response()->json($respon);

        }

        $pertanyaan = Pertanyaan::whereIn('id', $request->pertanyaan_id)->get();
        foreach($pertanyaan as $p){
            if(!is_null($request->jawaban_nilai_.$p->id)){
                $isi    = 'jawaban_isi_'.$p->id;
                $nilai  = 'jawaban_nilai_'.$p->id;
                $url    = 'url_file_datadiri_'.$p->id;

                    $update = DataDiri::updateOrCreate(['pertanyaan_id' => $p->id,'user_id' => Auth::user()->id, 'periode_id' => $periode->id], [
                        'jawaban'           => $request->$isi,
                        'nilai_id'          => $request->$nilai,
                        'pertanyaan_id'     => $p->id,
                        'periode_id'        => $periode->id,
                        'url_file_datadiri' => $request->$url
                    ]);
            }
        }

        // Simpan dokumens
        #tidak dipakai lagi

        // $dokumen = Pertanyaan::whereIn('id', $request->dokumen_id)->get();
        // foreach($dokumen as $p){
        //     if(!is_null($request->dokumen_.$p->id)){
        //         $doc    = 'dokumen_'.$p->id;
        //         if ($file = $request->file($doc)) {

        //             $validator = Validator::make($request->all(), [
        //                 'dokumen_'.$p->id 		        => 'file|mimes:pdf|max:2048',

        //             ]);

        //             if ($validator->fails()) {
        //                 $respon = array('status'=>false, 'pesan' => $validator->messages());
        //             } else {

        //                 $fullName=$file->getClientOriginalName();
        //                 $name=explode('.',$fullName)[0];

        //                 $originalImage = str_replace(str_split(',&\\/:;*?"<>|()." "'), '-', $name);
        //                 $replaceImage=strtolower($originalImage);
        //                 $gambarName = time().'_'.$replaceImage.'.'.$file->getClientOriginalExtension();
        //                 $destinationPath = storage_path().'/backend/files/instrumen';

        //                 $file->move($destinationPath, $gambarName);

        //                 DataDiri::updateOrCreate(
        //                     ['pertanyaan_id'        => $p->id, 'user_id'        => \Auth::user()->id], [
        //                     'file_data_diri'        => $gambarName,
        //                 ]);
        //             }
        //         }
        //     }
        // }


        // #end Dokumen

        // $periode       = Periode::orderBy('id','desc')->where('status', 1)->first();

        // if(empty($periode)){
        //     $respon = array('status'=>false, 'pesan' => ['msg' => 'Maaf, Data Periode Tidak Ada Yang Aktif, Silahkan hubungi LPM UIR']);
        //     return response()->json($respon);

        // }
         $update2 = Narasi::updateOrCreate(['instrumen_id' => $request->instrumen_id,'user_id' => Auth::user()->id, 'periode_id' => $periode->id], [
                        'instrumen_id'      => $request->instrumen_id,
                        'narasi_1'          => $request->narasi_1,
                        'narasi_2'          => $request->narasi_2,
                        'narasi_3'          => $request->narasi_3,
                        'narasi_4'          => $request->narasi_4,
                        'narasi_5'          => $request->narasi_5,
                        'periode_id'        => $periode->id
                    ]);
        // dd($coba);
        return response()->json($update);
    }

    public function simpan_dokumen(Request $request)
    {
        // $id = $request->dokumen_id;
        // dd($request->file('dokumen'));

        if ($file = $request->file('dokumen')) {

            $validator = Validator::make($request->all(), [
                'dokumen' 		        => 'file|mimes:pdf|max:2048',

            ]);

            if ($validator->fails()) {
                $respon = array('status'=>false, 'pesan' => $validator->messages());
            } else {

                $fullName=$file->getClientOriginalName();
                $name=explode('.',$fullName)[0];

                $originalImage = str_replace(str_split(',&\\/:;*?"<>|()." "'), '-', $name);
                $replaceImage=strtolower($originalImage);
                $gambarName = time().$replaceImage.'.'.$file->getClientOriginalExtension();
                $destinationPath = storage_path().'/backend/files/instrumen';

                $file->move($destinationPath, $gambarName);

                DataDiri::updateOrCreate(
                    ['pertanyaan_id'        => $id, 'user_id'        => \Auth::user()->id], [
                    'file_data_diri'        => $gambarName,
                ]);
            }
        }

        return Redirect::back();
    }

    public function publish(Request $request)
    {
        // dd($request->instrumen_id);
        for($i=0;$i<count($request->id_pertanyaan);$i++){
            $update = DataDiri::updateOrCreate(['pertanyaan_id' => $request->id_pertanyaan[$i],'user_id' => Auth::user()->id], [
                'publish'           => 1,
            ]);
        }

        $update = Narasi::updateOrCreate(['instrumen_id' => $request->instrumen_id,'user_id' => Auth::user()->id], [
            'publish'           => 1,
        ]);

        if ($update) {
            $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil disimpan']);
        } else {
            $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal disimpan']);
        }

        return response()->json($respon);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data           = Pertanyaan::find($id);
        $instrumen      = Instrumen::pluck('nama_instrumen','id');
        $unggah         = array(0 => 'Tidak Aktif', 1 => 'Aktif');
        $ambil_lagi     = array(NULL => 'Tidak', 1 => 'Ya');
        $capaian        = Capaian::where('pertanyaan_id', $id)->first() ?? null;

        // $list = $capaian->pilihan;
        // for($i=0;$i<count($list);$i++){
        //         $list[$i]['isi_pilihan'];
        // }
        // dd($list);
        return view('backend.pertanyaan.ubah', compact('data', 'instrumen', 'unggah', 'ambil_lagi', 'capaian'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'instrumen_id'               => 'required',
            'isi_pertanyaan'             => 'required',
       ]);
        if ($validator->fails()) {
            $respon = array('status'=>false, 'pesan' => $validator->messages());
        } else {
            $data = Pertanyaan::find($id);

                $update = $data->update($request->all());

                $capaian = Capaian::find($request->capaian_id);
                        $pilihan =  [[
                                        'nilai'             => 1,
                                        'isi_pilihan'       => $request->pilihan1,
                                        ],
                                        [
                                        'nilai'             => 2,
                                        'isi_pilihan'       => $request->pilihan2,
                                        ],
                                        [
                                        'nilai'             => 3,
                                        'isi_pilihan'       => $request->pilihan3,
                                        ],
                                        [
                                        'nilai'             => 4,
                                        'isi_pilihan'       => $request->pilihan4,
                                        ],
                                    ];
                    $capaian->pilihan           = $pilihan;
                    $capaian->update();


            if ($capaian) {
                $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil diubah']);
            } else {
                $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal diubah']);
            }
        }
        return response()->json($respon);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function hapus($id)
    {
        $data = Pertanyaan::find($id);
        return view('backend.pertanyaan.hapus', ['data' => $data]);
    }

    public function destroy($id)
    {
        $data = Pertanyaan::find($id);
        if ($data->delete()) {
            $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil dihapus']);
        } else {
            $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal dihapus']);
        }
        return response()->json($respon);
    }
}
