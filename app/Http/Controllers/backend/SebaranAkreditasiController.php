<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Permission;
use App\User;
use App\SebaranAkreditasi;
use App\Unit;
use Auth;
use Illuminate\Config;
use Illuminate\Http\Request;
use View;
use Yajra\DataTables\DataTables;
use Validator;
use DB;
use Illuminate\Support\Arr;

class SebaranAkreditasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.sebaran_akreditasi.index');
    }

    public function data(Request $request)
    {
        $data = SebaranAkreditasi::orderBy('sebaran_akreditasis.id', 'DESC')->with('unit');
        if(\Auth::user()->permissions_id == 3 || \Auth::user()->permissions_id == 4 || \Auth::user()->permissions_id == 5){
            $data->join('units','units.id','sebaran_akreditasis.unit_id')
            ->where('parent_id', \Auth::user()->unit_id);
        }
        return DataTables::of($data->get())

            ->addColumn('actions',function($data) {
                $actions = '<a data-id="'.$data->id.'"  class="btn btn-tbl-edit btn-xs ubah"><i class="fa fa-pencil"></i></a>';
                $actions .= '<a  data-id="'.$data->id.'" class="btn btn-tbl-delete btn-xs hapus"><i class="fa fa-trash-o"></i></a>';
                return $actions;
            })
            ->addColumn('dokumen',function($data) {
                if($data->file_akreditasi){
                    $file = '<a href='.url('sebaran_akreditasi/ambil_file/'.$data->file_akreditasi).' target="_blank" class="btn btn-info btn-tbl-edit btn-xs"><i class="fa fa-download"></i> Unduh</a>';
                }else{
                    $file = "<span class='btn btn-danger btn-xs'>no file</span>";

                }

                return $file;
            })
            ->addColumn('status_kadaluarsa',function($data) {

                $sisa = sisa_hari($data->tanggal_kadaluarsa);
                $data = '<span class="btn btn-success btn-xs">Masih Berlaku</span>';

                if($sisa < 366){
                    $data  = '<span class="btn btn-warning btn-xs">Dalam Pemantauan BANPT</span> <br><b style="font-size:12px">'.$sisa.' hari lagi Kadaluarsa </b>';
                }

                if($sisa < 0){
                    $data  = '<span class="btn btn-danger btn-xs">Lewat Kadaluarsa</span>';
                }
                return $data;
            })
            ->addIndexColumn()
            ->rawColumns(['actions','status_kadaluarsa','dokumen'])
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data           = SebaranAkreditasi::all();
        $unit = Unit::whereNotNull('parent_id')->select("id", DB::raw("CONCAT(units.nama_unit,' ',units.jenjang_pendidikan) as full_name"))
        ->pluck('full_name', 'id');

        if(\Auth::user()->permissions_id == 3 || \Auth::user()->permissions_id == 4 || \Auth::user()->permissions_id == 5){
            $unit           = Unit::where('parent_id', \Auth::user()->unit_id)->pluck('nama_unit','id');
        }

        return view('backend.sebaran_akreditasi.tambah', compact('data','unit'));
    }


    public function ambilFile ($file)
    {
        $data = SebaranAkreditasi::where('file_akreditasi',$file)->first();
        return response()->file(storage_path('/backend/files/akreditasi/'.$data->file_akreditasi));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [

        ]);
        if ($validator->fails()) {
            $respon = array('status'=>false, 'pesan' => $validator->messages());
        } else {
            $data = new SebaranAkreditasi;

        // $data   = SebaranAkreditasi::create($request->except('file_akreditasi'));
        if ($file = $request->file('file_akreditasi')) {
            $fullName=$file->getClientOriginalName();
            $name=explode('.',$fullName)[0];

            $originalImage = str_replace(str_split(',&\\/:;*?"<>|()." "'), '-', $name);
            $replaceImage=strtolower($originalImage);
            $gambarName = time().$replaceImage.'.'.$file->getClientOriginalExtension();
            $destinationPath = storage_path().'/backend/files/akreditasi';

            $file->move($destinationPath, $gambarName);
            $data->file_akreditasi = $gambarName;

        }

        $data->unit_id                  = $request->unit_id;
        $data->sk_akreditasi            = $request->sk_akreditasi;
        $data->akreditasi               = $request->akreditasi;
        $data->tanggal_penetapan        = $request->tanggal_penetapan;
        $data->tanggal_kadaluarsa       = $request->tanggal_kadaluarsa;
        $data->status_kadaluarsa        = $request->status_kadaluarsa;

        $data =  $data->save() ? 1 : 0;
        if ($data) {
            $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil disimpan']);
        } else {
            $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal disimpan']);
        }
    }
    return response()->json($respon);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data           = SebaranAkreditasi::find($id);
        $unit = Unit::whereNotNull('parent_id')->select("id", DB::raw("CONCAT(units.nama_unit,' ',units.jenjang_pendidikan) as full_name"))
        ->pluck('full_name', 'id');
        if(\Auth::user()->permissions_id == 3 || \Auth::user()->permissions_id == 4 || \Auth::user()->permissions_id == 5){
            $unit           = Unit::where('parent_id', \Auth::user()->unit_id)->pluck('nama_unit','id');
        }
        $akreditasi     = Arr::prepend(collect(config('master.akreditasi'))->toArray(), 'Pilih Akreditasi', '');

        return view('backend.sebaran_akreditasi.ubah', compact('data','unit','akreditasi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [

       ]);
        if ($validator->fails()) {
            $respon = array('status'=>false, 'pesan' => $validator->messages());
        } else {
            $data   = SebaranAkreditasi::find($id);
            // $update = $data->update($request->except('file_akreditasi'));

            if ($file = $request->file('file_akreditasi')) {
                $fullName=$file->getClientOriginalName();
                $name=explode('.',$fullName)[0];

                $originalImage = str_replace(str_split(',&\\/:;*?"<>|()." "'), '-', $name);
                $replaceImage=strtolower($originalImage);
                $gambarName = time().$replaceImage.'.'.$file->getClientOriginalExtension();
                $destinationPath = storage_path().'/backend/files/akreditasi';

                $file->move($destinationPath, $gambarName);
                $data->file_akreditasi = $gambarName;

            }

        $data->unit_id                  = $request->unit_id;
        $data->sk_akreditasi            = $request->sk_akreditasi;
        $data->akreditasi               = $request->akreditasi;
        $data->tanggal_penetapan        = $request->tanggal_penetapan;
        $data->tanggal_kadaluarsa       = $request->tanggal_kadaluarsa;
        $data->status_kadaluarsa        = $request->status_kadaluarsa;

        $update =  $data->update() ? 1 : 0;

            if ($update) {
                $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil diubah']);
            } else {
                $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal diubah']);
            }
        }
        return response()->json($respon);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function hapus($id)
    {
        $data = SebaranAkreditasi::find($id);
        return view('backend.sebaran_akreditasi.hapus', ['data' => $data]);
    }

    public function destroy($id)
    {
        $data = SebaranAkreditasi::find($id);

        if ($data->delete()) {
            $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil dihapus']);
        } else {
            $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal dihapus']);
        }
        return response()->json($respon);
    }
}
