<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Validator;
use App\Instrumen;
use App\Pertanyaan;
use App\DataDiri;
use App\Indikator;
use App\IndikatorKinerja;
use App\DataIndikatorKinerja;
use App\Unit;
use App\User;
use App\Narasi;
use App\Pimpinan;
use Auth;
use PDF;

class CetakController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function cetak_indikator_kinerja(Request $request)
    {
        $units         = Unit::whereNull('parent_id')->get();
        if($request->unit == "null"){
            $unit_id = Auth::user()->unit_id;
        }else{
            $unit_id = $request->unit;
        }
        $datas  = with( new IndikatorKinerja)->data($unit_id,$request->jenis,$request->periode_id)->groupBy('indikator_kinerja_id')->get();
        $dataku = with( new IndikatorKinerja)->data($unit_id,$request->jenis,$request->periode_id)->groupBy('indikator_id')->get();

        $fakultas   =   Unit::where('id', $unit_id)->first();
        $user  = with(new User)->where('unit_id', $unit_id)->where( 'permissions_id',4)->first();
        $request->jenis == 1 ? $status = "Utama" : $status = "Tambahan";

        $pimpinan = Pimpinan::where('unit_id',  $unit_id)->where('jabatan','Dekan')->first();

        $pdf = PDF::loadView('backend.view_indikator_kinerja.report', compact('dataku', 'datas', 'units','fakultas', 'status','user','pimpinan'));
        return $pdf->stream('laporan_indikator_kinerja.pdf', array('Attachment'=>0));
    }

    public function cetak_instrumen(Request $request)
    {
        $units         = Unit::whereNull('parent_id')->get();
        if($request->unit == "null"){
            $unit_id = Auth::user()->unit_id;
        }else{
            $unit_id = $request->unit;
        }
        $instrumens     = \DB::table('pertanyaans')
                        ->select(
                                'instrumens.id as id',
                                'nama_instrumen',
                                'keterangan' )
                        ->join('instrumens','instrumens.id','pertanyaans.instrumen_id')
                        ->groupBy('instrumens.id')
                        ->get();

        $d = \DB::table('pertanyaans')
                ->select(
                        'data_diris.id as id',
                        'pertanyaans.id as id_pertanyaan',
                        'instrumens.id as instrumen_id',
                        'file_data_diri',
                        'isi_pertanyaan',
                        'jawaban',
                        'nilai_id',
                        'nilai_asessor_id',
                        'user_id',
                        'as_parent',
                        'catatan')
                ->join('data_diris','data_diris.pertanyaan_id','pertanyaans.id')
                ->leftjoin('instrumens','instrumens.id','pertanyaans.instrumen_id')
                ->where('unit_id', $unit_id);
                if($request->periode){
                    $d->where('periode_id', $request->periode);
                }
                $d->orderBy('pertanyaans.id', 'ASC');
                $datas = $d->get();

        $narasis    = Narasi::get();
        $fakultas   = Unit::where('id', $unit_id)->first();
        $periode_id = $request->periode;

        $pimpinan = Pimpinan::where('unit_id',  $unit_id)->where('jabatan','Dekan')->first();
        $pdf = PDF::loadView('backend.view_instrumen.report', compact('instrumens', 'datas', 'units','fakultas','narasis','periode_id','pimpinan'));
        return $pdf->setPaper('F4', 'landscape')->stream('laporan_instrumen.pdf', array('Attachment'=>0));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
