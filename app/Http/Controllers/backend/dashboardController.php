<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\DataDiri;
use App\DataIndikatorKinerja;
use App\IndikatorKinerja;
use App\Periode;
use App\Unit;
use App\Pertanyaan;

class dashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_proses              = new DataDiri;
        $pertanyaan               = Pertanyaan::whereNull('as_parent')->count();
        $pertanyaan_unggah        = Pertanyaan::whereNull('as_parent')->where('status_unggah', 1)->count();
        $jumlah_indikator_all     = IndikatorKinerja::whereNull('as_parent')->whereNull('deleted_at')->where('status_utama',1)->where('status_aktif',1);
            if(\Auth::user()->unit_id == 10){
                $jumlah_indikator_all->whereIn('status_unit', [0,10]);
            }else{
                $jumlah_indikator_all->whereIn('status_unit', [0,11]);
            }
            $jumlah_indikator_all->get();

        // dd($jumlah_indikator_all->count());

        $chart_kriteria_all       = $data_proses->chart_kriteria_all();
        $chart_kriteria_1         = $data_proses->chart_kriteria(1);
        $chart_kriteria_2         = $data_proses->chart_kriteria(2);
        $chart_kriteria_3         = $data_proses->chart_kriteria(3);
        $chart_kriteria_4         = $data_proses->chart_kriteria(4);
        $chart_kriteria_5         = $data_proses->chart_kriteria(5);
        $chart_kriteria_6         = $data_proses->chart_kriteria(6);
        $chart_kriteria_7         = $data_proses->chart_kriteria(7);
        $chart_kriteria_8         = $data_proses->chart_kriteria(8);
        $chart_kriteria_9         = $data_proses->chart_kriteria(9);

        $presentase_evaluasi      = $data_proses->cek_progres_evaluasi()->count() / $pertanyaan * 100;
        $presentase_unggah        = $data_proses->cek_progres_unggah()->count() / $pertanyaan_unggah * 100;

        $presentase_evaluasi_lpm  = $data_proses->cek_progres_evaluasi();

        $presentase_unggah_lpm    = $data_proses->cek_progres_unggah();

        $unit_id                  = \Auth::user()->unit_id;
        //target
        $p_indikator_t            = with(new DataIndikatorKinerja)
                                    ->cek_progres_indikator()->whereNotNull('target')
                                    ->where('unit_id', $unit_id)
                                    ->where('indikator_kinerjas.status_utama', 1)
                                    ->where('indikator_kinerjas.status_aktif',1)
                                    ->whereNull('indikator_kinerjas.deleted_at');
                                    if(\Auth::user()->unit_id == 10){
                                        $p_indikator_t->whereIn('status_unit', [0,10]);
                                    }else{
                                        $p_indikator_t->whereIn('status_unit', [0,11]);
                                    }
                                    // dd(count($p_indikator_t->get()));
        $presentase_indikator_t   = number_format(count($p_indikator_t->get()) / $jumlah_indikator_all->count() * 100, 2);
        //capaian mid
        $p_indikator_m            = with(new DataIndikatorKinerja)
                                    ->cek_progres_indikator()
                                    ->whereNotNull('capaianmid')
                                    ->where('unit_id', $unit_id)
                                    ->where('indikator_kinerjas.status_utama', 1)
                                    ->where('indikator_kinerjas.status_aktif',1)
                                    ->whereNull('indikator_kinerjas.deleted_at');
                                    if(\Auth::user()->unit_id == 10){
                                        $p_indikator_m->whereIn('status_unit', [0,10]);
                                    }else{
                                        $p_indikator_m->whereIn('status_unit', [0,11]);
                                    }
        $presentase_indikator_m   = number_format(count($p_indikator_m->get()) / $jumlah_indikator_all->count() * 100, 2);
        //capaian
        $p_indikator_c            = with(new DataIndikatorKinerja)
                                    ->cek_progres_indikator()
                                    ->whereNotNull('capaian')
                                    ->where('unit_id', $unit_id)
                                    ->where('indikator_kinerjas.status_utama', 1)
                                    ->where('indikator_kinerjas.status_aktif',1)
                                    ->whereNull('indikator_kinerjas.deleted_at');
                                    if(\Auth::user()->unit_id == 10){
                                        $p_indikator_c->whereIn('status_unit', [0,10]);
                                    }else{
                                        $p_indikator_c->whereIn('status_unit', [0,11]);
                                    }
        $presentase_indikator_c   = number_format($p_indikator_c->count() / $jumlah_indikator_all->count() * 100, 2);

        // $presentase_indikator     = ($presentase_indikator_t + $presentase_indikator_m + $presentase_indikator_c) / 3;

        $running_text             = with(new Periode)->cek_periode();

        if($running_text['alias'] ?? null == 'c_target'){
            $presentase_indikator     = $presentase_indikator_t;
        }else
        if($running_text['alias'] ?? null == 'c_mid'){
            $presentase_indikator     = $presentase_indikator_m;
        }else
        if($running_text['alias'] ?? null == 'c_akhir'){
            $presentase_indikator     = $presentase_indikator_c;
        }else{
            $presentase_indikator     = null;
        }

        $units                    = Unit::whereNull('parent_id')->get();


        foreach($units as $unit){

            $jumlah_indikator         = IndikatorKinerja::whereNull('as_parent')->whereNull('deleted_at')->where('status_utama',1)->where('status_aktif',1);
            if($unit->id == 10){
                $jumlah_indikator->whereIn('status_unit', [0,10]);
            }else{
                $jumlah_indikator->whereIn('status_unit', [0,11]);
            }
            $jumlah_indikator->get();

            // dd($jumlah_indikator->count());
            // Persentase Evaluasi
            $progres_eva                    = $data_proses->cek_progres_evaluasi()->where('unit_id', $unit->id)->count();
            $persen_eva[$unit->id]          = number_format($progres_eva / $pertanyaan * 100, 2);

            // Persentase Proses Unggah
            $progres_unggah                 = $data_proses->cek_progres_unggah()->where('unit_id', $unit->id)->count();
            $persen_unggah[$unit->id]       = number_format($progres_unggah / $pertanyaan_unggah * 100, 2);

            // Persentase pengisian target
            $p_target[$unit->id]            = with(new DataIndikatorKinerja)
                                                ->cek_progres_indikator()
                                                ->whereNotNull('target')
                                                ->where('unit_id', $unit->id)
                                                ->where('indikator_kinerjas.status_utama', 1)
                                                ->where('indikator_kinerjas.status_aktif',1);
                                                if($unit->id == 10){
                                                    $p_target[$unit->id]->whereIn('status_unit', [0,10]);
                                                }else{
                                                    $p_target[$unit->id]->whereIn('status_unit', [0,11]);
                                                }
            $persen_target[$unit->id]       = number_format(count($p_target[$unit->id]->get()) /  $jumlah_indikator->count() * 100, 2);

            // Persentase pengisian target
            $p_mid[$unit->id]               = with(new DataIndikatorKinerja)
                                                ->cek_progres_indikator()
                                                ->whereNotNull('capaianmid')
                                                ->where('unit_id', $unit->id)
                                                ->where('indikator_kinerjas.status_utama', 1)
                                                ->where('indikator_kinerjas.status_aktif',1);
                                                if($unit->id == 10){
                                                    $p_mid[$unit->id]->whereIn('status_unit', [0,10]);
                                                }else{
                                                    $p_mid[$unit->id]->whereIn('status_unit', [0,11]);
                                                }
            $persen_mid[$unit->id]          = number_format(count($p_mid[$unit->id]->get()) / $jumlah_indikator->count() * 100, 2);

            // Persentase pengisian target
            $p_akhir[$unit->id]             = with(new DataIndikatorKinerja)
                                                ->cek_progres_indikator()
                                                ->whereNotNull('capaian')
                                                ->where('unit_id', $unit->id)
                                                ->where('indikator_kinerjas.status_utama', 1)
                                                ->where('indikator_kinerjas.status_aktif',1);
                                                if($unit->id == 10){
                                                    $p_akhir[$unit->id]->whereIn('status_unit', [0,10]);
                                                }else{
                                                    $p_akhir[$unit->id]->whereIn('status_unit', [0,11]);
                                                }
            $persen_akhir[$unit->id]        = number_format(count($p_akhir[$unit->id]->get()) / $jumlah_indikator->count() * 100, 2);

        }
        // dd($p_target[3]);


        return view('backend.dashboard'
        ,[
                'chart_kriteria_all'    => $chart_kriteria_all,
                'chart_kriteria_1'      => $chart_kriteria_1,
                'chart_kriteria_2'      => $chart_kriteria_2,
                'chart_kriteria_3'      => $chart_kriteria_3,
                'chart_kriteria_4'      => $chart_kriteria_4,
                'chart_kriteria_5'      => $chart_kriteria_5,
                'chart_kriteria_6'      => $chart_kriteria_6,
                'chart_kriteria_7'      => $chart_kriteria_7,
                'chart_kriteria_8'      => $chart_kriteria_8,
                'chart_kriteria_9'      => $chart_kriteria_9,
                'presentase_evaluasi'   => $presentase_evaluasi,
                'presentase_unggah'     => $presentase_unggah,
                'presentase_indikator'  => $presentase_indikator,
                'running_text'          => $running_text,
                'units'                 => $units,
                'persen_eva'            => $persen_eva,
                'persen_unggah'         => $persen_unggah,
                'persen_target'         => $persen_target,
                'persen_mid'            => $persen_mid,
                'persen_akhir'          => $persen_akhir,
            ]
        );

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
