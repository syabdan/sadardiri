<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Permission;
use App\Indikator;
use App\IndikatorKinerja;
use App\Menu;
use App\Unit;
use App\Dosen;
use App\Periode;
use Auth;
use Illuminate\Config;
use Illuminate\Http\Request;
use Redirect;
use View;
use Yajra\DataTables\DataTables;
use Validator;
use Illuminate\Support\Arr;

class Indikator_kinerjaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.indikator_kinerja.index');
    }
    public function utama()
    {
        $title = 'Indikator Kinerja Utama';
        return view('backend.indikator_kinerja.utama', compact('title'));
    }
    public function tambahan()
    {
        $title = 'Indikator Kinerja Tambahan';
        return view('backend.indikator_kinerja.tambahan', compact('title'));
    }

    public function data(Request $request)
    {
        \Auth::user()->permissions_id == 1 ? $data = IndikatorKinerja::with(['user','indikator'])->orderBy('id', 'DESC')->get() : $data = IndikatorKinerja::with('indikator')->where('user_id',\Auth::user()->id)->orderBy('id', 'DESC')->get();
        return DataTables::of($data)

            ->addColumn('actions',function($data) {
                $actions = '<a data-id="'.$data->id.'"  class="btn btn-tbl-edit btn-xs ubah"><i class="fa fa-pencil"></i></a>';
                $actions .= '<a  data-id="'.$data->id.'" class="btn btn-tbl-delete btn-xs hapus"><i class="fa fa-trash-o"></i></a>';
                return $actions;
            })
            ->addColumn('status_unggah',function($data) {
                $data->status_unggah == 1 ? $return = '<span class="label label-sm label-success">Aktif</span>' : $return = '<span class="label label-sm label-danger">Tidak Aktif</span>';
                return $return;
            })
            ->addColumn('aktif',function($data) {
                $data->deleted_at === NULL ? $return = '<span class="label label-sm label-success">Aktif</span>' : $return = '<span class="label label-sm label-danger">Tidak Aktif</span>';
                return $return;
            })
            ->addColumn('isi_indikator_kinerja',function($data) {
                return "<div style='border:1px solid #ccc; padding:10px; max-height:100px;overflow:scroll;'>$data->isi_indikator_kinerja</div>";
            })
            ->addColumn('group_unit',function($data) {
                if($data->status_unit == 0){
                    return '<span class="label label-sm label-success">Semua Unit</span>';
                }else
                if($data->status_unit == 10){
                    return '<span class="label label-sm label-primary">Pascasarjana</span>';
                }else
                if($data->status_unit == 11){
                    return '<span class="label label-sm label-primary">Fakultas</span>';
                }else{
                    return '-';
                }
            })
            ->addIndexColumn()
            ->rawColumns(['actions','status_unggah','isi_indikator_kinerja','aktif','group_unit'])
            ->make(true);
    }

    public function data_tambahan_utama(Request $request)
    {
        $request->jenis == 1 ? $data = IndikatorKinerja::with(['user','indikator'])->where('status_utama', 1)->orderBy('id', 'DESC')->get() : $data = IndikatorKinerja::with(['user','indikator'])->where('user_id',\Auth::user()->id)->orderBy('id', 'DESC')->get();

        if(\Auth::user()->permissions_id == 1){
            $request->jenis == 1 ? $data = IndikatorKinerja::with(['user','indikator'])->where('status_utama', 1)->orderBy('id', 'DESC')->get() : $data = IndikatorKinerja::with(['user','indikator'])->where('status_utama',0)->orderBy('id', 'DESC')->get();
        }
        return DataTables::of($data)

            ->addColumn('actions',function($data) {
                $actions = '<a data-id="'.$data->id.'"  class="btn btn-tbl-edit btn-xs ubah"><i class="fa fa-pencil"></i></a>';
                $actions .= '<a  data-id="'.$data->id.'" class="btn btn-tbl-delete btn-xs hapus"><i class="fa fa-trash-o"></i></a>';
                return $actions;
            })
            ->addColumn('status_unggah',function($data) {
                $data->status_unggah == 1 ? $return = '<span class="label label-sm label-success">Aktif</span>' : $return = '<span class="label label-sm label-danger">Tidak Aktif</span>';
                return $return;
            })
            ->addColumn('aktif',function($data) {
                $data->deleted_at === NULL ? $return = '<span class="label label-sm label-success">Aktif</span>' : $return = '<span class="label label-sm label-danger">Tidak Aktif</span>';
                return $return;
            })
            ->addColumn('isi_indikator_kinerja',function($data) {
                return "<div style='border:1px solid #ccc; padding:10px; max-height:100px;overflow:scroll;'>$data->isi_indikator_kinerja</div>";
            })
            ->addColumn('group_unit',function($data) {
                if($data->status_unit == 0){
                    return '<span class="label label-sm label-default">Semua Unit</span>';
                }else
                if($data->status_unit == 10){
                    return '<span class="label label-sm label-primary">Pascasarjana</span>';
                }else
                if($data->status_unit == 11){
                    return '<span class="label label-sm label-info">Fakultas</span>';
                }else{
                    return '-';
                }
            })

            ->addIndexColumn()
            ->rawColumns(['actions','status_unggah','isi_indikator_kinerja', 'aktif','group_unit'])
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data           = IndikatorKinerja::all();
        $indikator      = Indikator::pluck('keterangan','id');
        $unggah         = array(0 => 'Tidak Aktif', 1 => 'Aktif');
        $status_units   = Arr::prepend(collect(config('master.status_unit'))->toArray(), 'Pilih Kelompok Unit', '');

        return view('backend.indikator_kinerja.tambah', compact('data','indikator','unggah', 'status_units'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'indikator_id'                      => 'required',
            'isi_indikator_kinerja'             => 'required',
        ]);
        if ($validator->fails()) {
            $respon = array('status'=>false, 'pesan' => $validator->messages());
        } else {
            $data = IndikatorKinerja::create($request->all());
            if($request->has('isi_indikator_sub')){
                $id_baru_disimpan = $data->id;
                IndikatorKinerja::find($id_baru_disimpan)->update(['as_parent' => 1]); // update status as_parent

                $nourut = 1;
                for($i=0;$i<count($request->isi_indikator_sub);$i++)
                {
                    $parent = new IndikatorKinerja;
                    $parent->parent_id              = $id_baru_disimpan;
                    $parent->indikator_id           = $request->indikator_id;
                    $parent->isi_indikator_kinerja  = $request->isi_indikator_sub[$i];
                    $parent->status_unggah          = $request->status_unggah_sub[$i];
                    $parent->save();
                    $nourut++;
                }
            }

            $data =  $data->save() ? 1 : 0;
            if ($data) {
                $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil disimpan']);
            } else {
                $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal disimpan']);
            }
        }
        return response()->json($respon);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data           = IndikatorKinerja::find($id);
        $indikator      = Indikator::pluck('keterangan','id');
        $unggah         = array(0 => 'Tidak Aktif', 1 => 'Aktif');
        $aktif          = array("0000-00-00 00:00:00" => 'Tidak Aktif', NULL => 'Aktif');
        $status_units   = Arr::prepend(collect(config('master.status_unit'))->toArray(), 'Pilih Kelompok Unit', '');

        return view('backend.indikator_kinerja.ubah', compact('data', 'indikator', 'unggah', 'aktif', 'status_units'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'indikator_id'                      => 'required',
            'isi_indikator_kinerja'             => 'required',
       ]);
        if ($validator->fails()) {
            $respon = array('status'=>false, 'pesan' => $validator->messages());
        } else {
            $data = IndikatorKinerja::find($id);

                $update = $data->update($request->all());

            if ($update) {
                $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil diubah']);
            } else {
                $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal diubah']);
            }
        }
        return response()->json($respon);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function hapus($id)
    {
        $data = IndikatorKinerja::find($id);
        return view('backend.indikator_kinerja.hapus', ['data' => $data]);
    }

    public function destroy($id)
    {
        $data = IndikatorKinerja::find($id);
        if ($data->delete()) {
            $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil dihapus']);
        } else {
            $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal dihapus']);
        }
        return response()->json($respon);
    }
}
