<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Permission;
use App\User;
use App\Pimpinan;
use App\Unit;
use Auth;
use Illuminate\Config;
use Illuminate\Http\Request;
use View;
use Yajra\DataTables\DataTables;
use Validator;
use DB;
use Illuminate\Support\Arr;

class PimpinanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas      = Pimpinan::all();
        if(\Auth::user()->permissions_id == 3 || \Auth::user()->permissions_id == 4){
            $datas      = Pimpinan::where('unit_id', \Auth::user()->unit_id)->get();
        }
        return view('backend.pimpinan.index', compact('datas'));
    }

    public function data(Request $request)
    {
        $data = Pimpinan::orderBy('pimpinans.id', 'DESC')->with('unit');
        if(\Auth::user()->permissions_id == 3 || \Auth::user()->permissions_id == 4){
            $data->join('units','units.id','pimpinans.unit_id')
            ->where('parent_id', \Auth::user()->unit_id);
        }
        return DataTables::of($data->get())

            ->addColumn('actions',function($data) {
                $actions = '<a data-id="'.$data->id.'"  class="btn btn-tbl-edit btn-xs ubah"><i class="fa fa-pencil"></i></a>';
                $actions .= '<a  data-id="'.$data->id.'" class="btn btn-tbl-delete btn-xs hapus"><i class="fa fa-trash-o"></i></a>';
                return $actions;
            })

            ->addIndexColumn()
            ->rawColumns(['actions'])
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data                   = Pimpinan::all();
        $jabatan                = Arr::prepend(collect(config('master.jabatan'))->toArray(), 'Pilih Jabatan', '');
        $unit                   = Unit::whereNull('parent_id')->pluck('nama_unit', 'id');


        return view('backend.pimpinan.tambah', compact('data', 'unit', 'jabatan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [

        ]);
        if ($validator->fails()) {
            $respon = array('status'=>false, 'pesan' => $validator->messages());
        } else {
            $data   = Pimpinan::create($request->all());


        if ($data) {
            $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil disimpan']);
        } else {
            $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal disimpan']);
        }
    }
    return response()->json($respon);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data           = Pimpinan::find($id);
        $jabatan        = Arr::prepend(collect(config('master.jabatan'))->toArray(), 'Pilih Jabatan', '');
        $unit           = Unit::whereNull('parent_id')->pluck('nama_unit', 'id');

        return view('backend.pimpinan.ubah', compact('data','unit', 'jabatan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [

       ]);
        if ($validator->fails()) {
            $respon = array('status'=>false, 'pesan' => $validator->messages());
        } else {
            $data   = Pimpinan::find($id);
            $update = $data->update($request->all());

            if ($update) {
                $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil diubah']);
            } else {
                $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal diubah']);
            }
        }
        return response()->json($respon);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function hapus($id)
    {
        $data = Pimpinan::find($id);
        return view('backend.pimpinan.hapus', ['data' => $data]);
    }

    public function destroy($id)
    {
        $data = Pimpinan::find($id);

        if ($data->delete()) {
            $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil dihapus']);
        } else {
            $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal dihapus']);
        }
        return response()->json($respon);
    }
}
