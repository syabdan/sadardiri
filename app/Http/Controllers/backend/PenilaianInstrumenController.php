<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Permission;
use App\Instrumen;
use App\Pertanyaan;
use App\DataDiri;
use App\Menu;
use App\Unit;
use App\Dosen;
use App\Periode;
use App\Nilai;
use App\Narasi;
use App\User;
use Auth;
use Illuminate\Config;
use Illuminate\Http\Request;
use Redirect;
use View;
use Yajra\DataTables\DataTables;
use Validator;

class PenilaianInstrumenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $instrumens     = \DB::table('pertanyaans')
                        ->select(
                                'instrumens.id as id',
                                'nama_instrumen' )
                        ->join('instrumens','instrumens.id','pertanyaans.instrumen_id')
                        ->groupBy('instrumens.id')
                        ->get();

        $datas = \DB::table('pertanyaans')
                ->select(
                        'data_diris.id as id',
                        'pertanyaans.id as id_pertanyaan',
                        'instrumens.id as instrumen_id',
                        'url_file_datadiri',
                        'isi_pertanyaan',
                        'jawaban',
                        'nilai_id',
                        'nilai_asessor_id',
                        'user_id',
                        'as_parent')
                ->join('data_diris','data_diris.pertanyaan_id','pertanyaans.id')
                ->leftjoin('instrumens','instrumens.id','pertanyaans.instrumen_id')
                ->where('unit_id', \Auth::user()->unit_id)
                // ->where('publish', 1)
                // if(\Auth::user()->permissions_id == 4){
                //     $datas->where('user_id', \Auth::user()->id);
                // }
                // if(\Auth::user()->permissions_id == 3){
                //     $datas->where('assessor_id', \Auth::user()->id);
                // }
                ->orderBy('pertanyaans.id', 'ASC')
                ->get();
        // dd($datas);
        return view('backend.penilaian_instrumen.index', compact('instrumens','datas'));
    }

    public function view ($id_unit=NULL,$periode_id=NULL)
    {
        $units         = Unit::whereNull('parent_id')->get();

        // if(\Auth::user()->permissions_id == 5){
        //     $id_unit = \Auth::user()->unit_id;
        // }
        // $datas = with( new Pertanyaan)->data($id_unit)->get();

        // $dataku = with( new Pertanyaan)->groupBy('id')->where([['as_parent',1]])->get();
        // $user  = with(new User)->where('unit_id', $id_unit)->where( 'permissions_id',4)->orderBy('id', 'desc')->first();
        // dd($dataku);

        if($id_unit){
            $id_unit = $id_unit;
        }else{
            $id_unit = Auth::user()->unit_id;
        }
        $instrumens     = \DB::table('pertanyaans')
                        ->select(
                                'instrumens.id as id',
                                'nama_instrumen' )
                        ->join('instrumens','instrumens.id','pertanyaans.instrumen_id')
                        ->groupBy('instrumens.id')
                        ->get();

        $d = \DB::table('pertanyaans')
                ->select(
                        'data_diris.id as id',
                        'pertanyaans.id as id_pertanyaan',
                        'instrumens.id as instrumen_id',
                        'url_file_datadiri',
                        'isi_pertanyaan',
                        'jawaban',
                        'nilai_id',
                        'nilai_asessor_id',
                        'user_id',
                        'as_parent')
                ->join('data_diris','data_diris.pertanyaan_id','pertanyaans.id')
                ->leftjoin('instrumens','instrumens.id','pertanyaans.instrumen_id');
            if(\Auth::user()->permissions_id == 5){
                $d->where('unit_id', \Auth::user()->unit_id);
            }else{
                $d->where('unit_id', $id_unit);
            }
            if($periode_id){
                $d->where('periode_id', $periode_id);
            }
                // $d->where('publish', 1);
                $d->orderBy('pertanyaans.id', 'ASC');

                $datas = $d->get();

        $fakultas       =   Unit::where('id', $id_unit)->first();
        $periodes       = Periode::orderBy('id','desc')->get();

        return view('backend.view_instrumen.index', compact('instrumens', 'datas', 'units','fakultas', 'id_unit','periodes','periode_id'));
    }

    public function data_pertanyaan($id)
    {
        $data = Narasi::
                where([['instrumen_id', $id],['unit_id', \Auth::user()->unit_id]])->get();

        return DataTables::of($data)
            ->addIndexColumn()
            ->make(true);
    }

    public function create()
    {
        $instrumens     = Instrumen::all()->pluck('nama_instrumen','id');
        $nilai          = Nilai::all()->pluck(['awal','akhir'],'id');

        $datas = \DB::table('pertanyaans')
                ->select(
                        'data_diris.id as id_diri',
                        'pertanyaans.id as id',
                        'instrumens.id as instrumen_id',
                        'file_data_diri',
                        'isi_pertanyaan',
                        'jawaban',
                        'nilai_id',
                        'as_parent')
                ->leftjoin('data_diris','data_diris.pertanyaan_id','pertanyaans.id')
                ->leftjoin('instrumens','instrumens.id','pertanyaans.instrumen_id')
                ->orderBy('pertanyaans.id', 'ASC')
                ->get();

        return view('backend.penilaian_instrumen.tambah', compact('instrumens','datas','nilai'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // $data = Instrumen::create($request->all());
        $data       =   new DataDiri();

        $data =  $data->save() ? 1 : 0;
        if ($data) {
            $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil disimpan']);
        } else {
            $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal disimpan']);
        }

    return response()->json($respon);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // dd($id);
        $instrumens      = Instrumen::all()->pluck('nama_instrumen','id');
        $nilais          = Nilai::all();

        $datas          = \DB::table('pertanyaans')
                            ->select(
                                    'data_diris.id as id_diri',
                                    'pertanyaans.id as id',
                                    'instrumens.id as instrumen_id',
                                    'file_data_diri',
                                    'isi_pertanyaan',
                                    'jawaban',
                                    'nilai_id',
                                    'nilai_asessor_id',
                                    'catatan',
                                    'as_parent')
                            ->leftjoin('data_diris','data_diris.pertanyaan_id','pertanyaans.id')
                            ->leftjoin('instrumens','instrumens.id','pertanyaans.instrumen_id')
                            ->where('data_diris.id', $id)
                            ->orderBy('pertanyaans.id', 'ASC')
                            ->first();

        $nilai         = array('' => 'Pilih Nilai', 1 => '1 - Belum Memenuhi', 2 => '2 - Sebagian Memenuhi', 3 => '3 - Memenuhi Standar', 4 => '4 - Melampaui Standar');

                            // dd($datas);
        return view('backend.penilaian_instrumen.ubah', compact('instrumens','datas','nilais','nilai'));
    }

    public function reupload($id){

        $datas = Datadiri::where('id', $id)->first();
        return view('backend.penilaian_instrumen.reupload', compact('datas'));

    }

    public function submit_reupload(Request $request, $id){

        $data =  Datadiri::where('id', $id)->first();

            // if ($file = $request->file('file')) {
            //     $fullName=$file->getClientOriginalName();
            //     $name=explode('.',$fullName)[0];

            //     $originalImage = str_replace(str_split(',&\\/:;*?"<>|()." "'), '-', $name);
            //     $replaceImage=strtolower($originalImage);
            //     $gambarName = time().$replaceImage.'.'.$file->getClientOriginalExtension();
            //     $destinationPath = storage_path().'/backend/files/instrumen';

            //     $file->move($destinationPath, $gambarName);
            //     $data->file_data_diri = $gambarName;
            // }
            $data->url_file_datadiri = $request->url_file_datadiri;
            $update = $data->update();

            if ($update) {
                $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil diubah']);
            } else {
                $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal diubah']);
            }

            return response()->json($respon);

    }

    public function catatan($id)
    {
        $datas          = \DB::table('pertanyaans')
                            ->select(
                                    'data_diris.id as id_diri',
                                    'pertanyaans.id as id',
                                    'instrumens.id as instrumen_id',
                                    'catatan')
                            ->leftjoin('data_diris','data_diris.pertanyaan_id','pertanyaans.id')
                            ->leftjoin('instrumens','instrumens.id','pertanyaans.instrumen_id')
                            ->where('data_diris.id', $id)
                            ->orderBy('pertanyaans.id', 'ASC')
                            ->first();

        return view('backend.penilaian_instrumen.lihat_catatan_auditor', compact('datas', 'id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            // dd($request->nilai_id);
            $update = DataDiri::updateOrCreate(['id' => $request->id_diri], [
                'nilai_asessor_id'      => $request->nilai_asessor_id,
                'catatan'               => $request->catatan,
            ]);

            if ($update) {
                $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil diubah']);
            } else {
                $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal diubah']);
            }

        return response()->json($respon);
    }

    public function detail($id)
    {
        $id = $id;
        $narasi = Narasi::where([['instrumen_id',$id],['unit_id', \Auth::user()->unit_id]])->first();
        return view('backend.penilaian_instrumen.lihat_detail', compact('id', 'narasi'));
    }

    public function ambilFile ($file)
    {
        $data = DataDiri::where('file_data_diri',$file)->first();
        return response()->file(storage_path('/backend/files/instrumen/'.$data->file_data_diri));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function hapus($id)
    {

    }

    public function destroy($id)
    {

    }
}
