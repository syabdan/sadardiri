<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Permission;
use App\Indikator;
use App\IndikatorKinerja;
use App\DataIndikatorKinerja;
use App\Menu;
use App\Unit;
use App\User;
use App\Nilai;
use App\nilaiProdi;
use App\Periode;
use Auth;
use Illuminate\Config;
use Illuminate\Http\Request;
use Redirect;
use View;
use Yajra\DataTables\DataTables;
use Validator;

class PenilaianIndikatorKinerjaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $indikators     = IndikatorKinerja::select(
                                    'indikators.id as indikator_id',
                                    'nama_indikator', 'keterangan' )
                            ->join('indikators','indikators.id','indikator_kinerjas.indikator_id')
                            ->groupBy('indikators.id')
                            ->get();

        $datas          =  IndikatorKinerja::select(
                            'indikator_kinerjas.id as id',
                            'indikator_id',
                            'indikator_kinerjas.parent_id as parent_id',
                            'isi_indikator_kinerja', 'keterangan',
                            'status_utama',
                            'status_aktif',
                            'status_unit')
                        ->join('indikators','indikators.id','indikator_kinerjas.indikator_id')
                        ->where('status_utama', 1)
                        ->whereNull('indikator_kinerjas.deleted_at')
                        // ->orderBy('indikator_kinerjas.id', 'ASC')
                        ->get();
        $groupUnit      = with(new User)->groupUnit();
        // dd($datas);
        return view('backend.penilaian_indikator_kinerja.index', compact('indikators','datas','groupUnit'));
    }

    public function view ($id_unit=NULL,$jenis=null,$periode_id=NULL)
    {

        $units         = Unit::all();
        $jenis == 1 ? $status = "Utama" : $status = "Tambahan";

        if($id_unit == "null"){
            $id_unit = Auth::user()->unit_id;
        }

        $datas      = with( new IndikatorKinerja)->data($id_unit,$jenis,$periode_id)->groupBy('indikator_kinerja_id')->get();
        $dataku     = with( new IndikatorKinerja)->data($id_unit,$jenis,$periode_id)->groupBy('indikator_id')->get();
        $user       = with(new User)->where('unit_id', $id_unit)->where( 'permissions_id',4)->first();
        $fakultas   = Unit::where('id', $id_unit)->first();
        $periodes   = Periode::orderBy('id','desc')->get();
        $groupUnit      = with(new User)->groupUnit();
        // dd($dataku);
        return view('backend.view_indikator_kinerja.index', compact('units', 'datas', 'user', 'status', 'dataku','id_unit','jenis', 'fakultas','periodes','periode_id', 'groupUnit'));
    }

    public function jenis($jenis)
    {
        // dd($jenis);
        $jenis == 'utama' ? $id_utama = 1 : $id_utama = 0;

        $indikators     = IndikatorKinerja::select(
                                'indikators.id as indikator_id',
                                'nama_indikator', 'keterangan' )
                        ->join('indikators','indikators.id','indikator_kinerjas.indikator_id')
                        ->groupBy('indikators.id')
                        ->whereNull('indikator_kinerjas.deleted_at')
                        ->get();


            $data = IndikatorKinerja::select(
                    'indikator_kinerjas.id as id',
                    'indikator_id',
                    'indikator_kinerjas.parent_id as parent_id',
                    'isi_indikator_kinerja')
            ->join('indikators','indikators.id','indikator_kinerjas.indikator_id');
            if($id_utama == 0){
                $data->where('status_utama', $id_utama)->where('user_id', \Auth::user()->id);
            }else{
                $data->where('status_utama', $id_utama);
            }

            if(\Auth::user()->unit_id == 10){
                $data->whereIn('status_unit', [10,0]);

            }else {
                $data->whereIn('status_unit', [11,0]);

            }
            $data->orderBy('indikator_kinerjas.id', 'ASC')
            ->whereNull('indikator_kinerjas.deleted_at');


            $datas = $data->get();

            $groupUnit      = with(new User)->groupUnit();
        return view('backend.penilaian_indikator_kinerja.index', compact('indikators','datas','groupUnit'));
    }

    public function data_prodi($id){
        $data = nilaiProdi::join('units', 'units.id','nilai_prodis.unit_id')
                ->where([['indikator_kinerja_id', $id],['units.parent_id', \Auth::user()->unit_id]])
                ->groupBy('unit_id')->get();

        return DataTables::of($data)
            ->addIndexColumn()
            ->make(true);
    }

    public function create()
    {
        $indikators     = Indikator::all()->pluck('nama_indikator','id');

        $datas = \DB::table('indikator_kinerjas')
                ->select('indikator_kinerjas.id as id',
                'data_indikator_kinerjas.file_indikator_kinerja as file',
                'data_indikator_kinerjas.target as target',
                'data_indikator_kinerjas.capaianmid as capaianmid',
                'data_indikator_kinerjas.capaian as capaian',
                'data_indikator_kinerjas.nilai_id as nilai',
                'data_indikator_kinerjas.persentase as persentase',
                'data_indikator_kinerjas.publish as publish')
                ->leftjoin('data_indikator_kinerjas','data_indikator_kinerjas.indikator_kinerja_id','indikator_kinerjas.id')
                ->leftjoin('indikators','indikators.id','indikator_kinerjas.indikator_id')
                ->orderBy('data_indikator_kinerjas.id', 'DESC')
                ->get();

        return view('backend.penilaian_indikator_kinerja.tambah', compact('indikators','datas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $data = IndikatorKinerja::create($request->all());
        $data                               =   new DataIndikatorKinerja();
        $data->indikator_kinerja_id         =   $request->indikator_kinerja_id;
        $data->target                       =   $request->target;
        $data->capaianmid                   =   $request->capaianmid;
        $data->capaian                      =   $request->capaian;

        $periode       = Periode::orderBy('id','desc')->where('status', 1)->first();
        if(empty($periode)){
            $respon = array('status'=>false, 'pesan' => ['msg' => 'Maaf, Data Periode Tidak Ada Yang Aktif, Silahkan hubungi LPM UIR']);
            return response()->json($respon);

        }else{
            $data->periode_id                   =   $periode->id;
        }

        $data =  $data->save() ? 1 : 0;
        if ($data) {
            $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil disimpan']);
        } else {
            $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal disimpan']);
        }

        return response()->json($respon);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        $indikators         = Indikator::all()->pluck('nama_indikator','id');
        $datas              = IndikatorKinerja::find($id);
        $indikators         = DataIndikatorKinerja::where([['indikator_kinerja_id', $datas->id], ['created_by', \Auth::user()->id]])->first();
        $indikators_unit    = nilaiProdi::select('nilai_prodis.id', 'unit_id', 'indikator_kinerja_id','target','capaianmid', 'capaian', 'dokumen', 'tahun', 'nama_unit', 'jenjang_pendidikan', 'parent_id', 'nilai_prodis.created_at', 'nilai_prodis.updated_at')
                            ->orderBy('nilai_prodis.updated_at','DESC')
                            ->rightjoin('units','units.id', 'nilai_prodis.unit_id')
                            ->where([['indikator_kinerja_id', $datas->id], ['parent_id', \Auth::user()->unit_id]])
                            ->get()
                            ->unique('unit_id');
        $periodes           = with(new Periode)->cek_periode();

        if(sizeof($indikators_unit) == 0){
            $indikators_unit              = Unit::select('id as unit_id','nama_unit','jenjang_pendidikan')->where('parent_id',\Auth::user()->unit_id)->get();
        }

        if(empty($periodes)){
            $respon = 'Maaf, Periode Pengisian Belum Dibuka atau Sudah Berakhir !';
            return response()->json($respon);
        }

        return view('backend.penilaian_indikator_kinerja.ubah', compact('indikators','datas','periodes', 'indikators', 'indikators_unit'));
    }

    public function catatan($id)
    {
        $datas                      = IndikatorKinerja::find($id);
        $data_indikator_kinerja     = DataIndikatorKinerja::where([['indikator_kinerja_id', $datas->id], ['created_by', \Auth::user()->id]])->first();

        return view('backend.penilaian_indikator_kinerja.lihat_catatan_auditor', compact('data_indikator_kinerja', 'id'));
    }

    public function nilai($id)
    {
        $indikators    = Indikator::all()->pluck('nama_indikator','id');
        $data          = IndikatorKinerja::find($id);
        $datas         = DataIndikatorKinerja::where([['indikator_kinerja_id', $data->id], ['unit_id', \Auth::user()->unit_id]])->first();
        $nilais        = Nilai::all()->pluck('nilai','id');

        return view('backend.penilaian_indikator_kinerja.atur_nilai', compact('indikators','datas','nilais', 'id'));
    }

    public function detail($id)
    {
        $id = $id;
        return view('backend.penilaian_indikator_kinerja.lihat_detail', compact('id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cek_image($image){
        if ($file = $request->file($image)) {

            $validator = Validator::make($request->all(), [
                'file_indikator_kinerja'     => 'mimes:pdf',

           ]);

            if ($validator->fails()) {
                $respon = array('status'=>false, 'pesan' => ['msg' => 'Ekstensi / Ukuran File Anda Tidak Sesuai !']);
                return response()->json($respon);

            } else {

                $fullName=$file->getClientOriginalName();
                $name=explode('.',$fullName)[0];

                $originalImage = str_replace(str_split(',&\\/:;*?"<>|()." "'), '-', $name);
                $replaceImage=strtolower($originalImage);
                $gambarName = time().$replaceImage.'.'.$file->getClientOriginalExtension();
                $destinationPath = storage_path().'/backend/files/indikator_kinerja/';

                $file->move($destinationPath, $gambarName);

                $data       = DataIndikatorKinerja::updateOrCreate(
                                                ['indikator_kinerja_id'       => $id , 'created_by'  => \Auth::user()->id], [
                                                'file_indikator_kinerja'      => $gambarName,
                                                ]);
            }
        }

    }

    public function reupload($id){

        $datas = DataIndikatorKinerja::where('id', $id)->first();
        return view('backend.penilaian_indikator_kinerja.reupload', compact('datas'));

    }

    public function submit_reupload(Request $request, $id){

        $data =  DataIndikatorKinerja::where('id', $id)->first();

            // if ($file = $request->file('file')) {
            //     $fullName=$file->getClientOriginalName();
            //     $name=explode('.',$fullName)[0];

            //     $originalImage = str_replace(str_split(',&\\/:;*?"<>|()." "'), '-', $name);
            //     $replaceImage=strtolower($originalImage);
            //     $gambarName = time().$replaceImage.'.'.$file->getClientOriginalExtension();
            //     $destinationPath = storage_path().'/backend/files/indikator_kinerja';

            //     $file->move($destinationPath, $gambarName);
            //     $data->file_indikator_kinerja = $gambarName;
            // }

            $data->url_file_indikator = $request->url_file_indikator;
            $update = $data->update();

            if ($update) {
                $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil diubah']);
            } else {
                $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal diubah']);
            }

            return response()->json($respon);

    }

    public function update(Request $request, $id)
    {

            $data =  DataIndikatorKinerja::where([['indikator_kinerja_id', $id], ['created_by', \Auth::user()->id]])->first();
            $data->url_file_indikator = $request->url_file_indikator;
            $update = $data->update();
            if ($update) {
                $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil diubah']);
            } else {
                $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal diubah']);
            }

        return response()->json($respon);
    }

    public function hitung(Request $request, $id, $jenis)
    {

        if ($file = $request->file('file_indikator_kinerja')) {
            $validator = Validator::make($request->all(), [
                'file_indikator_kinerja'     => 'mimetypes:application/pdf|size:10240',

           ]);
            if ($validator->fails()) {
                $respon = array('status'=>false, 'pesan' => $validator->messages());
            } else {
                $fullName=$file->getClientOriginalName();
                $name=explode('.',$fullName)[0];

                $originalImage = str_replace(str_split(',&\\/:;*?"<>|()." "'), '-', $name);
                $replaceImage=strtolower($originalImage);
                $gambarName = time().$replaceImage.'.'.$file->getClientOriginalExtension();
                $destinationPath = storage_path().'/backend/files/indikator_kinerja';

                $file->move($destinationPath, $gambarName);

                $data       = DataIndikatorKinerja::updateOrCreate(
                                                ['indikator_kinerja_id'       => $id , 'created_by'  => \Auth::user()->id], [
                                                'file_indikator_kinerja'      => $gambarName,
                                                ]);
            }
        }

        $id_unit                        = $request->id_unit;
        $target_unit                    = $request->target_unit;
        $capaianmid_unit                = $request->capaianmid_unit;
        $capaian_unit                   = $request->capaian_unit;
        $banyak_unit                    = count($id_unit);

        $avg_target         = '0';
        $avg_capaianmid     = '0';
        $avg_capaian        = '0';

        $pembagi_target     = 0;
        $pembagi_capaianmid = 0;
        $pembagi_capaian    = 0;

        $rata_target        = 0;
        $rata_capaianmid    = 0;
        $rata_capaian       = 0;



        for($i=0;$i<$banyak_unit;$i++){
            $avg_target     = $avg_target + $target_unit[$i];
            $avg_capaianmid = $avg_capaianmid + $capaianmid_unit[$i];
            $avg_capaian    = $avg_capaian + $capaian_unit[$i];

            // Count Pembagi
            if($target_unit[$i] != 0) {
                $pembagi_target++;
            }

            if($capaianmid_unit[$i] != 0){
                $pembagi_capaianmid++;
            }

            if($capaian_unit[$i] != 0){
                $pembagi_capaian++;
            }

            // #Count Pembagi

            $update_nilai_unit = nilaiProdi::updateOrCreate(['indikator_kinerja_id' => $id, 'unit_id' => $id_unit[$i]],[
                'unit_id'               => $id_unit[$i],
                'target'                => $target_unit[$i],
                'capaianmid'            => $capaianmid_unit[$i],
                'capaian'               => $capaian_unit[$i],
                'tahun'                 => date('Y')
            ]);
        }
        if($jenis == 'avg'){
            if($pembagi_target != 0){
                $rata_target           = $avg_target / $pembagi_target;
            }

            if($pembagi_capaianmid != 0){
                $rata_capaianmid       = $avg_capaianmid / $pembagi_capaianmid;
            }

            if($pembagi_capaian != 0){
                $rata_capaian          = $avg_capaian / $pembagi_capaian;
            }

        }

        if($jenis == 'sum'){
            $rata_target           = $avg_target;
            $rata_capaianmid       = $avg_capaianmid;
            $rata_capaian          = $avg_capaian;
        }

        if($request->alias == 'c_target'){
            $rata_capaianmid=NULL;
            $rata_capaian=NULL;
        }
        if($request->alias == 'c_mid'){
            $rata_capaian=NULL;
        }


        if(!is_null($rata_capaian)){
            if($rata_target == 0){
                $hitung = 0;
            }else{
                $hitung =  ( (double)$rata_capaian / (double)$rata_target) * 100; // hitung nilai target capaian
            }

            if($rata_capaian===0){
                $rata_capaian='0.00';
            }
            if($rata_capaianmid===0){
                $rata_capaianmid='0.00';
            }
            if($rata_target===0){
                $rata_target='0.00';
            }
        }else if(!is_null($rata_capaianmid)){  // Jika periode input capaian tengah
            if($rata_target == 0){
                $hitung = 0;
            }else{

                $hitung =  ( (double)$rata_capaianmid / (double)$rata_target) * 100; // hitung nilai target capaian
            }



            if($rata_capaianmid===0){
                $rata_capaianmid='0.00';
            }
            if($rata_target===0){
                $rata_target='0.00';
            }

        }else if(!is_null($rata_target)){ // Jika periode input target
            $hitung = 0;
            if($rata_capaianmid===NULL){
                $rata_capaianmid=NULL;
            }
            if($rata_capaian===NULL){
                $rata_capaian=NULL;
            }

            if($rata_target===0){
                $rata_target='0.00';
            }
        }
        else{
            $hitung = 0;
        }


        // $hitung =  (((int) $rata_capaianmid + (int) $rata_capaian) / (int) $rata_target) * 100; // hitung nilai target capaian

        $nilai = Nilai::select('nilai','id')->where([['awal','<=', $hitung],['akhir','>=', $hitung]])->first();
        // dd($hitung.' - '.$nilai['id']);

        $periode       = Periode::orderBy('id','desc')->where('status', 1)->first();

        if(empty($periode)){
            $respon = array('status'=>false, 'pesan' => ['msg' => 'Maaf, Data Periode Tidak Ada Yang Aktif, Silahkan hubungi LPM UIR']);
            return response()->json($respon);
        }


        // dd($rata_capaianmid);

        $update = DataIndikatorKinerja::updateOrCreate(
            ['indikator_kinerja_id' => $id, 'created_by'  => \Auth::user()->id],
            [
                // 'indikator_kinerja_id'      => $request->id,
                'target'                    => $rata_target,
                'capaianmid'                => $rata_capaianmid,
                'capaian'                   => $rata_capaian,
                'nilai_id'                  => $nilai['id'] ?? NULL,
                'persentase'                => $hitung ?? NULL,
                'periode_id'                => $periode->id,
            ]);



            if ($update) {
                $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil diubah']);
            } else {
                $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal diubah']);
            }

        return response()->json($respon);
    }

    public function update_nilai (Request $request, $id)
    {
        // dd($request->created_by);
        $update = DataIndikatorKinerja::find($id);
        $update->nilai_asessor_id       = $request->nilai_asessor_id;
        $update->catatan                = $request->catatan;
        $update->update();
            if ($update) {
                $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil diubah']);
            } else {
                $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal diubah']);
            }

        return response()->json($respon);
    }

    public function ambilFile ($file)
    {
        $data = DataIndikatorKinerja::where('file_indikator_kinerja',$file)->first();
        return response()->file(storage_path('/backend/files/indikator_kinerja/'.$data->file_indikator_kinerja));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function hapus($id)
    {

    }

    public function destroy($id)
    {

    }
}
