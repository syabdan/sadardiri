<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Dokumen;
use App\Permission;
use App\User;
use App\Unit;
use Auth;
use Illuminate\Config;
use Illuminate\Http\Request;
use View;
use Yajra\DataTables\DataTables;
use Validator;
use DB;
use Illuminate\Support\Arr;

class DokumenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas      = Dokumen::where('unit_id', \Auth::user()->unit_id)->get();
        return view('backend.unggah.index', compact('datas'));
    }

    public function data(Request $request)
    {
        $data = Dokumen::with('periode')->orderBy('dokumens.id', 'DESC')->where('unit_id', \Auth::user()->unit_id)->get();
        return DataTables::of($data)
            ->addColumn('jenis',function($data) {
                $data->jenis == 1 ? $jenis = 'Instrumen Evaluasi Diri' : $jenis = 'Indikator Kinerja';
                return $jenis;
            })
            ->addColumn('file',function($data) {
                $dok = url("/unggah/ambil_file/".$data->file);
                $file = '<a href="'.$dok.'" target="_blank" class="btn btn-tbl-upload btn-xs ">'.$data->file.'<i class="fa fa-upload"></i></a>';

                return $file;
            })
            ->addColumn('actions',function($data) {
                $actions = '<a data-id="'.$data->id.'"  class="btn btn-tbl-edit btn-xs ubah"><i class="fa fa-pencil"></i></a>';
                $actions .= '<a  data-id="'.$data->id.'" class="btn btn-tbl-delete btn-xs hapus"><i class="fa fa-trash-o"></i></a>';
                return $actions;
            })

            ->addIndexColumn()
            ->rawColumns(['actions','jenis','file'])
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data                   = Dokumen::all();
        $jenis                  = Arr::prepend(collect(config('master.jenis_dokumen'))->toArray(), 'Pilih Jenis Dokumen', '');

        return view('backend.unggah.tambah', compact('data', 'jenis'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [

        ]);
        if ($validator->fails()) {
            $respon = array('status'=>false, 'pesan' => $validator->messages());
        } else {
            $data = new Dokumen;

            if ($file = $request->file('file')) {
                $fullName=$file->getClientOriginalName();
                $name=explode('.',$fullName)[0];

                $originalImage = str_replace(str_split(',&\\/:;*?"<>|()." "'), '-', $name);
                $replaceImage=strtolower($originalImage);
                $gambarName = time().$replaceImage.'.'.$file->getClientOriginalExtension();
                $destinationPath = storage_path().'/backend/files/dokumen';

                $file->move($destinationPath, $gambarName);
                $data->file = $gambarName;
            }

            $data->jenis            = $request->jenis;
            $data->periode_id       = getPeriodeId();


            $data =  $data->save();

            if ($data) {
                $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil disimpan']);
            } else {
                $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal disimpan']);
            }
        }
    return response()->json($respon);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data           = Dokumen::find($id);
        $jenis          = Arr::prepend(collect(config('master.jenis_dokumen'))->toArray(), 'Pilih Jenis Dokumen', '');

        return view('backend.unggah.ubah', compact('data','jenis'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [

       ]);
        if ($validator->fails()) {
            $respon = array('status'=>false, 'pesan' => $validator->messages());
        } else {
            $data   = Dokumen::find($id);
            $update = $data->update($request->all());

            if ($update) {
                $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil diubah']);
            } else {
                $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal diubah']);
            }
        }
        return response()->json($respon);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function ambilFile ($file)
    {
        $data = Dokumen::where('file',$file)->first();
        return response()->file(storage_path('/backend/files/dokumen/'.$data->file));
    }

    public function hapus($id)
    {
        $data = Dokumen::find($id);
        return view('backend.unggah.hapus', ['data' => $data]);
    }

    public function destroy($id)
    {
        $data = Dokumen::find($id);

        if ($data->delete()) {
            $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil dihapus']);
        } else {
            $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal dihapus']);
        }
        return response()->json($respon);
    }
}
