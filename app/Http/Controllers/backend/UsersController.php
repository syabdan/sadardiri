<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Permission;
use App\User;
use App\Menu;
use App\Unit;
use App\Dosen;
use App\Periode;
use Auth;
use Illuminate\Config;
use Illuminate\Http\Request;
use Redirect;
use View;
use Yajra\DataTables\DataTables;
use Validator;
use DB;
use Hash;
use Intervention\Image\Facades\Image;


class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.users.index');
    }

    public function data(Request $request)
    {
        $data = User::select(['id','nama_user', 'email','permissions_id'])
                ->orderBy('id', 'DESC')->get();
        return DataTables::of($data)
            ->editColumn('level',function(User $data) {
                return $data->permissionsGroup->permission_name;
            })

            ->addColumn('actions',function($data) {
                $actions = '<a user-id="'.$data->id.'"  class="btn btn-tbl-edit btn-xs ubah"><i class="fa fa-pencil"></i></a>';
                if($data->id != Auth::user()->id){
                    $actions .= '<a  user-id="'.$data->id.'" class="btn btn-tbl-delete btn-xs hapus"><i class="fa fa-trash-o"></i></a>';

                }

                return $actions;
            })
            ->addIndexColumn()
            ->rawColumns(['actions','level'])
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data           = User::all();
        $dosen          = Dosen::select("id", DB::raw("CONCAT(dosens.gelar_depan,' ',dosens.nama_dosen,', ',dosens.gelar_belakang) as nama_dosen"))->pluck('nama_dosen','id');
        $unit           = Unit::whereNull('parent_id')->pluck('nama_unit', 'id');
        $permission     = Permission::all()->pluck('permission_name', 'id');
        $periode        = Periode::all()->pluck('nama_periode','id');
        return view('backend.users.tambah', compact('data', 'dosen', 'unit','periode','permission'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'nama_user'         => 'required|string|max:255',
            'email'             => 'required|string|email|max:255|unique:users',
            'password'          => 'min:6',
			'pic' 		        => 'file|image|mimes:jpeg,png,jpg|max:2048',

        ]);
        if ($validator->fails()) {
            $respon = array('status'=>false, 'pesan' => $validator->messages());
        } else {
            $data = new User;

            if ($file = $request->file('pic')) {
                $fullName=$file->getClientOriginalName();
                $name=explode('.',$fullName)[0];

                $originalImage = str_replace(str_split(',&\\/:;*?"<>|()." "'), '-', $name);
                $replaceImage=strtolower($originalImage);
                $gambarName = time().$replaceImage.'.'.$file->getClientOriginalExtension();
                $destinationPath = public_path().'/backend/assets/img/user/';

                $file->move($destinationPath, $gambarName);
                $data->pic = $gambarName;
            }
            $data->nama_user            = $request->nama_user;
            $data->email                = $request->email;
            $data->hp                   = $request->hp;
            $data->permissions_id       = $request->permissions_id;
            $data->unit_id              = $request->unit_id;
            $data->tanggal_awal         = $request->tanggal_awal;
            $data->tanggal_akhir        = $request->tanggal_akhir;
            $data->dosen_id             = $request->dosen_id;
            $data->password             = bcrypt($request->password);

            $user =  $data->save() ? 1 : 0;
            if ($user) {
                $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil disimpan']);
            } else {
                $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal disimpan']);
            }
        }
        return response()->json($respon);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data           = User::find($id);
        $dosen          = Dosen::select("id", DB::raw("CONCAT(dosens.gelar_depan,' ',dosens.nama_dosen,', ',dosens.gelar_belakang) as nama_dosen"))->pluck('nama_dosen','id');
        $unit           = Unit::whereNull('parent_id')->pluck('nama_unit', 'id');
        $permission     = Permission::all()->pluck('permission_name', 'id');
        return view('backend.users.ubah', compact('data', 'dosen', 'unit','permission'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'nama_user'         => 'required|string|max:255',
            // 'email'             => 'required|string|email|max:255|unique:users,email,'.$id
        ]);
        if ($validator->fails()) {
            $respon = array('status'=>false, 'pesan' => $validator->messages());
        } else {
            $user = User::find($id);
            if ($request->filled('password')) {
                $update = $user->update($request->except('password'));

                $user->password = bcrypt($request->password);
                $user->update();

            } else {
                $update = $user->update($request->except('password','pic'));
                if ($file = $request->file('pic')) {
                    $fullName=$file->getClientOriginalName();
                    $name=explode('.',$fullName)[0];

                    $originalImage = str_replace(str_split(',&\\/:;*?"<>|()." "'), '-', $name);
                    $replaceImage=strtolower($originalImage);
                    $gambarName = time().'_'.$replaceImage.'.'.$file->getClientOriginalExtension();
                    $destinationPath = public_path().'/backend/assets/img/user/';

                    $file->move($destinationPath, $gambarName);
                    $user->pic = $gambarName;
                    $user->update();
                }
            }
            if ($update) {
                $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil diubah']);
            } else {
                $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal diubah']);
            }
        }
        return response()->json($respon);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function hapus($id)
    {
        $user = User::find($id);
        return view('backend.users.hapus', ['user' => $user]);
    }

    public function destroy($id)
    {
        $user = User::find($id);
        if ($user->id == \Auth::id()) {
            return response()->json(array('status'=>false, 'pesan' => ['msg' => 'Maaf, tidak bisa menghapus akun sendiri. Silahkan Hubungi Administrator.']));
        }
        if ($user->delete()) {
            $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil dihapus']);
        } else {
            $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal dihapus']);
        }
        return response()->json($respon);
    }

}
