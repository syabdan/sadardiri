<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Permission;
use App\User;
use App\SebaranPegawai;
use App\Unit;
use Auth;
use Illuminate\Config;
use Illuminate\Http\Request;
use View;
use Yajra\DataTables\DataTables;
use Validator;
use DB;
use Illuminate\Support\Arr;

class SebaranPegawaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas      = SebaranPegawai::paginate(10);
        if(\Auth::user()->permissions_id == 3 || \Auth::user()->permissions_id == 4 || \Auth::user()->permissions_id == 5){
            $datas      = SebaranPegawai::where('unit_id', \Auth::user()->unit_id)->paginate(10);
        }
        return view('backend.sebaran_pegawai.index', compact('datas'));
    }

    public function data(Request $request)
    {
        $data = SebaranPegawai::orderBy('sebaran_pegawais.id', 'DESC')->with('unit');
        if(\Auth::user()->permissions_id == 3 || \Auth::user()->permissions_id == 4 || \Auth::user()->permissions_id == 5){
            $data->join('units','units.id','sebaran_pegawais.unit_id')
            ->where('parent_id', \Auth::user()->unit_id);
        }
        return DataTables::of($data->get())

            ->addColumn('actions',function($data) {
                $actions = '<a data-id="'.$data->id.'"  class="btn btn-tbl-edit btn-xs ubah"><i class="fa fa-pencil"></i></a>';
                $actions .= '<a  data-id="'.$data->id.'" class="btn btn-tbl-delete btn-xs hapus"><i class="fa fa-trash-o"></i></a>';
                return $actions;
            })

            ->addIndexColumn()
            ->rawColumns(['actions'])
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data                   = SebaranPegawai::all();
        $tingkat_pendidikan     = Arr::prepend(collect(config('master.tingkat_pendidikan'))->toArray(), 'Pilih Tingkat Pendidikan', '');
        $unit = Unit::whereNull('parent_id')->pluck('nama_unit', 'id');

        if(\Auth::user()->permissions_id == 3 || \Auth::user()->permissions_id == 4 || \Auth::user()->permissions_id == 5){
            $unit = Unit::whereNull('parent_id')->where('id', \Auth::user()->unit_id)->pluck('nama_unit', 'id');

        }

        return view('backend.sebaran_pegawai.tambah', compact('data', 'unit', 'tingkat_pendidikan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [

        ]);
        if ($validator->fails()) {
            $respon = array('status'=>false, 'pesan' => $validator->messages());
        } else {
        $data = new SebaranPegawai;

        $data->unit_id                  = $request->unit_id;
        $data->tingkat_pendidikan       = $request->tingkat_pendidikan;
        $data->tetap                    = $request->tetap;
        $data->hampir_tetap             = $request->hampir_tetap;
        $data->kontrak                  = $request->kontrak;

        $data->jumlah                   = $request->tetap + $request->hampir_tetap + $request->kontrak;

        $data =  $data->save() ? 1 : 0;

        if ($data) {
            $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil disimpan']);
        } else {
            $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal disimpan']);
        }
    }
    return response()->json($respon);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data           = SebaranPegawai::find($id);
        $tingkat_pendidikan     = Arr::prepend(collect(config('master.tingkat_pendidikan'))->toArray(), 'Pilih Tingkat Pendidikan', '');
        $unit = Unit::whereNull('parent_id')->pluck('nama_unit', 'id');

        if(\Auth::user()->permissions_id == 3 || \Auth::user()->permissions_id == 4 || \Auth::user()->permissions_id == 5){
            $unit = Unit::whereNull('parent_id')->where('id', \Auth::user()->unit_id)->pluck('nama_unit', 'id');

        }

        return view('backend.sebaran_pegawai.ubah', compact('data','unit', 'tingkat_pendidikan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [

       ]);
        if ($validator->fails()) {
            $respon = array('status'=>false, 'pesan' => $validator->messages());
        } else {
            $data   = SebaranPegawai::find($id);

            $data->unit_id                  = $request->unit_id;
            $data->tingkat_pendidikan       = $request->tingkat_pendidikan;
            $data->tetap                    = $request->tetap;
            $data->hampir_tetap             = $request->hampir_tetap;
            $data->kontrak                  = $request->kontrak;
            $data->jumlah                   = $request->tetap + $request->hampir_tetap + $request->kontrak;

        $data =  $data->update() ? 1 : 0;

            if ($data) {
                $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil diubah']);
            } else {
                $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal diubah']);
            }
        }
        return response()->json($respon);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function hapus($id)
    {
        $data = SebaranPegawai::find($id);
        return view('backend.sebaran_pegawai.hapus', ['data' => $data]);
    }

    public function destroy($id)
    {
        $data = SebaranPegawai::find($id);

        if ($data->delete()) {
            $respon = array('status'=>true, 'pesan' => ['msg' => 'Data berhasil dihapus']);
        } else {
            $respon = array('status'=>false, 'pesan' => ['msg' => 'Data gagal dihapus']);
        }
        return response()->json($respon);
    }
}
