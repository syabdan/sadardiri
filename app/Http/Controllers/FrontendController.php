<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\DataDiri;
use App\DataIndikatorKinerja;
use App\Unit;
use App\SebaranAkreditasi;
use App\SebaranDosen;

class FrontendController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with('unit')->where('permissions_id', 4)->orderBy('unit_id')->distinct()->get();
        return view('frontend.dashboard', compact('users'));
    }

    public function lihat($id_unit)
    {
        $users                      = User::with('unit')->where('permissions_id', 4)->orderBy('unit_id')->distinct()->get();
        $nama_unit                  = Unit::find($id_unit);

        // For Radar Chart
        $chart_kriteria_all         = with(new DataDiri)->frontend_chart_kriteria_all($id_unit);

        // For Bar Chart
        $indikator                  = with(new DataIndikatorKinerja)->frontend_bar_chart($id_unit);

        // For Pie Chart
        $sebaran                    = with(new SebaranAkreditasi)->frontend_pie_chart($id_unit);
        $sebaran_dosen              = with(new SebaranDosen)->frontend_pie_chart($id_unit);

        return view('frontend.unit.index'
        ,[
            'chart_kriteria_all'    => $chart_kriteria_all,
            'indikator'             => $indikator,
            'sebaran'               => $sebaran,
            'sebaran_dosen'         => $sebaran_dosen,
            'unit'                  => $nama_unit
        ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
