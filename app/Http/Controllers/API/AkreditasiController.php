<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\SebaranAkreditasi;
use App\Http\Resources\Akreditasi as AkreditasiResource;
use App\Http\Controllers\API\BaseController as BaseController;
use Validator;

class AkreditasiController extends BaseController
{
    public function ambil(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'name' => 'required',
            'key' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }else{
            $name = $request->name;
            $key = $request->key;

            if($name == 'sipresma' && $key == 'Sipresma2020!@#'){
                $akreditasi     = SebaranAkreditasi::select('unit_id','sk_akreditasi','akreditasi')->get();

                // return $akreditasi;
                return $this->sendResponse($akreditasi, 'Akreditasi retrieved  successfully.');
            }else{
                return $this->sendResponse([], 'Anda Tidak Memiliki Hak Akses !');
            }
        }


    }
}
