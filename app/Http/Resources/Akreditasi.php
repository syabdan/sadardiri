<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Akreditasi extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'unit_id'       => $this->unit_id,
            'sk_akreditasi' => $this->sk_akreditasi,
            'akreditasi'    => $this->akreditasi,
        ];
    }

    public function withResponse($request, $response)
    {
        $response->header('X-Value', 'True');
    }
}
