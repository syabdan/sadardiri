<?php

namespace App\Http\Middleware;

use Closure;
use Route;
use App\Menu;
use App\Permission;

class aksesMenuMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user			= \Auth::user();
        $current	=	explode(".", Route::currentRouteName());

        $submenu	=	Menu::where('link', $request->segment(1))->orderBy('id','desc')->first();
        // dd($request->segment(1));

        if($submenu !== NULL){
                // if($submenu->tampil){
                    $data_sections_arr = explode(",", \Auth::user()->permissionsGroup->data_menus);

                    if(in_array($submenu->id,$data_sections_arr)){
                        return $next($request);
                    }else{
                        return redirect('/dashboard');
                    }
                // }else{
                //     return $next($request);
                // }

        }else{
            return redirect('/');
            // return response()->json(array('status' => false, 'pesan' => ['msg' => 'Tidak Ada akses']));
        }
    }
}
