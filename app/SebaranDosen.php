<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class SebaranDosen extends Model
{
    use Notifiable;
    use SoftDeletes;

    protected $dates    =   ['deleted_at'];
    protected $fillable = [
        'unit_id', 'non_fungsional_s2', 'asisten_ahli_s2', 'lektor_s2', 'lektor_kepala_s2', 'non_fungsional_s3', 'asisten_ahli_s3', 'lektor_s3', 'lektor_kepala_s3', 'guru_besar_s3', 'jumlah'
    ];

    public function unit()
    {
        return $this->belongsTo('App\Unit', 'unit_id');
    }

    // Code for pie chart akreditasi
    public function rasio_sebaran($unit_id)
    {
        $query = \DB::table('units AS p_unit')
        ->leftJoin('units AS c_unit', 'p_unit.id', '=', 'c_unit.parent_id')
        ->join('sebaran_dosens','sebaran_dosens.unit_id','c_unit.id')
        ->where('p_unit.id', $unit_id)
        ->whereNull('sebaran_dosens.deleted_at')
        ->get();

        if($query){

            $total_non_fungsional_s2 = 0;
            $total_asisten_ahli_s2 = 0;
            $total_lektor_s2 = 0;
            $total_lektor_kepala_s2 = 0;
            $total_non_fungsional_s3 = 0;
            $total_asisten_ahli_s3 = 0;
            $total_lektor_s3 = 0;
            $total_lektor_kepala_s3 = 0;
            $total_guru_besar_s3 = 0;
            $total_jumlah = 0;
            foreach($query as $data){
                $total_non_fungsional_s2 = $total_non_fungsional_s2 + $data->non_fungsional_s2;
                $total_asisten_ahli_s2 = $total_asisten_ahli_s2 + $data->asisten_ahli_s2;
                $total_lektor_s2 = $total_lektor_s2 + $data->lektor_s2;
                $total_lektor_kepala_s2 = $total_lektor_kepala_s2 + $data->lektor_kepala_s2;
                $total_non_fungsional_s3 = $total_non_fungsional_s3 + $data->non_fungsional_s3;
                $total_asisten_ahli_s3 = $total_asisten_ahli_s3 + $data->asisten_ahli_s3;
                $total_lektor_s3 = $total_lektor_s3 + $data->lektor_s3;
                $total_lektor_kepala_s3 = $total_lektor_kepala_s3 + $data->lektor_kepala_s3;
                $total_guru_besar_s3 = $total_guru_besar_s3 + $data->guru_besar_s3;
                $total_jumlah = $total_jumlah + $data->jumlah;

            }

            $jumlah_non_fungsional = number_format(($total_non_fungsional_s2 + $total_non_fungsional_s3) ?? 0 / $total_jumlah, 2);
            $jumlah_asisten_ahli   = number_format(($total_asisten_ahli_s2 + $total_asisten_ahli_s3) ?? 0 / $total_jumlah, 2);
            $jumlah_lektor         = number_format(($total_lektor_s2 + $total_lektor_s3) ?? 0 / $total_jumlah, 2);
            $jumlah_lektor_kepala  = number_format(($total_lektor_kepala_s2 + $total_lektor_kepala_s3) ?? 0 / $total_jumlah, 2);
            $jumlah_guru_besar     = number_format($total_guru_besar_s3 ?? 0 / $total_jumlah, 2);

            $data2 = array();
            $data2['Non Fungsional'] = $jumlah_non_fungsional;
            $data2['Asisten Ahli']   = $jumlah_asisten_ahli;
            $data2['Lektor']         = $jumlah_lektor;
            $data2['Lektor Kepala']  = $jumlah_lektor_kepala;
            $data2['Guru Besar']     = $jumlah_guru_besar;
        }

        return $data2;
    }

    public function frontend_pie_chart($unit_id)
    {
        $data = array();
        foreach ($this->rasio_sebaran($unit_id) as $key => $value) {
            $data[] = [$key, (int) $value, false];
        }
        // dd(json_encode($data));

            return ['data' =>  $data];

    }

    // #Code for pie chart akreditasi

}
