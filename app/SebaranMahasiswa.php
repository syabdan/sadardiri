<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SebaranMahasiswa extends Model
{
    protected $dates    =   ['deleted_at'];
    protected $fillable = [
        'unit_id', 'jumlah_mhs_aktif', 'jumlah_mhs_nonaktif', 'jumlah_mhsasing_aktif', 'jumlah_mhsasing_nonaktif'
    ];

    public function unit()
    {
        return $this->belongsTo('App\Unit', 'unit_id');
    }
}
